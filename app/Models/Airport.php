<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class Airport extends Model {
    public $timestamps = false;

    public function region(){
        return $this->hasOne('Region', 'code', 'iso_region');
    }
}