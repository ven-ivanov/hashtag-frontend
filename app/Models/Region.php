<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class Region extends Model {
    public $timestamps = false;
}