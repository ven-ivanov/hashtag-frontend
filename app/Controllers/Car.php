<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Car
{
	public function index(Request $request, Application $app)
	{
		return $app['twig']->render('car/index.twig', array(
			'menu' => 'drive',
		));
	}

	public function search(Request $request, Application $app)
	{
		$app['session']->set('pickUpCity', explode(',', $request->get('pickUpCity'))[0]);
		$app['session']->set('differentplace', explode(',', $request->get('differentplace'))[0]);
		$app['session']->set('pickUpDate', explode(',', $request->get('pickUpDate'))[0]);
		$app['session']->set('pickUpTime', explode(',', $request->get('pickUpTime'))[0]);
		$app['session']->set('returnDate', explode(',', $request->get('returnDate'))[0]);
		$app['session']->set('dropOffTime', explode(',', $request->get('dropOffTime'))[0]);
		$app['session']->set('size', explode(',', $request->get('size'))[0]);
		
		return $app['twig']->render('car/search.twig', array(
			'menu' => 'drive',
		));
	}

	public function options(Request $request, Application $app)
	{
		return $app['twig']->render('car/options.twig', array(
			'menu' => 'drive',
		));
	}

	public function checkout(Request $request, Application $app)
	{
		return $app['twig']->render('car/checkout.twig', array(
			'menu' => 'drive',
		));
	}


}