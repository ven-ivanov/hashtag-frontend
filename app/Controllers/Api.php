<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Models;
use TwitterOAuth;
use OAuthConsumer;

class Api
{
	public function lastTweets(Request $request, Application $app)
	{
		//todo: cache

		$connection = new TwitterOAuth($app['config']['twitter.api_key'], $app['config']['twitter.api_secret'], $app['config']['twitter.token'], $app['config']['twitter.token_secret']);
		$tweets = $connection->get('statuses/user_timeline', array('screen_name' => 'hashtagtravels', 'exclude_replies' => 'true', 'include_rts' => 'false', 'count' => 5));

		$tweets_arr = array();

		if(!empty($tweets)) {
			foreach($tweets as $tweet) {

				# Access as an object
				$tweetText = $tweet->text;

				# Make links active
				// $tweetText = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r\\2", $tweetText);

				# Linkify user mentions
				// $tweetText = preg_replace("/@(w+)/", '<a href="http://www.twitter.com/$1" target="_blank">@$1</a>', $tweetText);

				# Linkify tags
				// $tweetText = preg_replace("/#(w+)/", '<a href="http://search.twitter.com/search?q=$1" target="_blank">#$1</a>', $tweetText);

				# Output
				array_push($tweets_arr, $tweetText);

			}
		}

		return $app->json($tweets_arr);
	}

}