<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Corporate
{
	public function about_us(Request $request, Application $app)
	{
		return $app['twig']->render('corporate/about.twig', array(
			'menu' => 'about'
		));
	}

	public function hashtag_rewards(Request $request, Application $app)
	{
		return $app['twig']->render('corporate/hashtag-rewards.twig', array(
			'menu' => 'hashtag-rewards'
		));
	}

	public function terms(Request $request, Application $app)
	{
		return $app['twig']->render('corporate/terms.twig', array(
			'menu' => 'terms'
		));
	}

	public function privacy_policy(Request $request, Application $app)
	{
		return $app['twig']->render('corporate/privacy.twig', array(
			'menu' => 'privacy-policy'
		));
	}

}
