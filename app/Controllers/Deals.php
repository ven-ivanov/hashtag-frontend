<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Deals
{
	public function index(Request $request, Application $app)
	{
		return $app['twig']->render('deals/index.twig', array(
			'menu' => 'deals',
		));
	}

	public function search(Request $request, Application $app)
	{
		return $app['twig']->render('deals/search.twig', array(
			'menu' => 'deals',
		));
	}

	public function hotel(Request $request, Application $app)
	{
		return $app['twig']->render('hotel/detail.twig', array(
			'menu' => 'deals',
		));
	}

	public function flight(Request $request, Application $app)
	{
		return $app['twig']->render('flight/search.twig', array(
			'menu' => 'deals',
		));
	}

	public function flight_options(Request $request, Application $app)
	{
		return $app['twig']->render('flight/options.twig', array(
			'menu' => 'deals',
		));
	}

	public function checkout(Request $request, Application $app)
	{
		return $app['twig']->render('deals/checkout.twig', array(
			'menu' => 'deals',
		));
	}


}
