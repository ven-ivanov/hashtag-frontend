<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Payment
{
	public static $key = '32651d0696c7226981a0e3cec7fc476212caa5f56232bd2b7c4fb4def65447228c401d68585ad73e549edd87e00d469fad525075acbe8576c5ffe6d8d7dc7bf6';
	public static $merchantId = '1100003990';

	public function index(Request $request, Application $app)
	{

	}

	public function getSign(Request $request, Application $app)
	{
		$payload = json_decode($request->getContent());

		$amount = $payload->amount;
		$currency = $payload->currency;
		$ref = $payload->ref;

		return Payment::sign(Payment::$key, Payment::$merchantId, $amount, $currency, $ref);
	}

	public function callback(Request $request, Application $app)
	{
		echo "<pre>";
		return print_r($_POST);
	}


	private static function hexstr($hex){
		// translate byte array to hex string
		$string="";
		for ($i=0;$i<strlen($hex)-1;$i+=2)
			$string.=chr(hexdec($hex[$i].$hex[$i+1]));
		return $string;
	}


	private static function hmac ($key, $data){
		// RFC 2104 HMAC implementation for php.
		// Creates an md5 HMAC.
		// Eliminates the need to install mhash to compute a HMAC

		$b = 64; // byte length for md5
		if (strlen($key) > $b) {
			 $key = pack("H*",md5($key));
		}
		$key  = str_pad($key, $b, chr(0x00));
		$ipad = str_pad('', $b, chr(0x36));
		$opad = str_pad('', $b, chr(0x5c));
		$k_ipad = $key ^ $ipad ;
		$k_opad = $key ^ $opad;

		return md5($k_opad  . pack("H*",md5($k_ipad . $data)));
	}


	public static function sign($key, $merchId, $amount, $ccy, $idno){
		$str=$merchId.$amount.$ccy.$idno;
		$key2=Payment::hexstr($key);
		return Payment::hmac($key2, $str);
	}

}