<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Hotel
{
	public function index(Request $request, Application $app)
	{
		return $app['twig']->render('hotel/index.twig', array(
			'menu' => 'sleep',
		));
	}

	public function search(Request $request, Application $app)
	{
		$app['session']->set('locality', explode(',', $request->get('locality'))[0]);
		$app['session']->set('formatted', explode(',', $request->get('formatted'))[0]);
		$app['session']->set('arrivalDate', explode(',', $request->get('arrivalDate'))[0]);
		$app['session']->set('departureDate', explode(',', $request->get('departureDate'))[0]);
		$app['session']->set('numberOfAdults', $request->get('numberOfAdults'));
		$app['session']->set('childAges', $request->get('childAges'));
		$app['session']->set('nationality', $request->get('nationality'));

		return $app['twig']->render('hotel/search.twig', array(
			'menu' => 'sleep'
		));
	}

	public function detail(Request $request, Application $app)
	{
		return $app['twig']->render('hotel/detail.twig', array(
			'menu' => 'sleep',
		));
	}

	public function checkout(Request $request, Application $app)
	{
		return $app['twig']->render('hotel/checkout.twig', array(
			'menu' => 'sleep',
			'datatrans' => false
		));
	}

	public function checkout_payment(Request $request, Application $app)
	{
		$data = $request->request->all();

		// print_r($data);
		// exit();

		$sign2 = Payment::sign(Payment::$key, Payment::$merchantId, $data['amount'], $data['currency'], $data['uppTransactionId']);

		if($data['status'] == 'success' && $sign2 !== $data['sign2']){
			return new Response('Fraud', 500);
		}

		return $app['twig']->render('hotel/checkout.twig', array(
			'menu' => 'sleep',
			'datatrans' => $data
		));
	}
}