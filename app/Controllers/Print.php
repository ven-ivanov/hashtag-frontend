<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PrintPage
{

	public function flight(Request $request, Application $app)
	{
		return $app['twig']->render('print/flight.twig');
	}

	public function flightConfirmation(Request $request, Application $app)
	{
		return $app['twig']->render('print/flight.twig', array(
			'confirmation' => true
		));
	}



	public function hotel(Request $request, Application $app)
	{
		return $app['twig']->render('print/hotel.twig');
	}

	public function hotelConfirmation(Request $request, Application $app)
	{
		return $app['twig']->render('print/hotel.twig', array(
			'confirmation' => true
		));
	}



	public function car(Request $request, Application $app)
	{
		return $app['twig']->render('print/car.twig');
	}

	public function carConfirmation(Request $request, Application $app)
	{
		return $app['twig']->render('print/car.twig', array(
			'confirmation' => true
		));
	}

	public function deals(Request $request, Application $app)
	{
		return $app['twig']->render('print/deals.twig');
	}

	public function dealsConfirmation(Request $request, Application $app)
	{
		return $app['twig']->render('print/deals.twig', array(
			'confirmation' => true
		));
	}

}
