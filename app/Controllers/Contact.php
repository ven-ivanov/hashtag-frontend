<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Contact
{
	public function contact(Request $request, Application $app)
	{
		return $app['twig']->render('contact/index.twig', array(
			'menu' => 'contact'
		));
	}

	public function send (Request $request, Application $app) {

		// Configurable values
		$sender = 'noreply@hashtagtravels.com';
		$to = 'info@hashtagtravels.com';

		// TODO: server-side validation of form data - bad e-mail throws exception!
		$fields = array('name', 'phone', 'email', 'subject', 'message');
		foreach ($fields as $f) {
			$form_data[$f] = $request->get($f);
		}

		$from = array($form_data['email'] => $form_data['name']);
		$subject = "Contact form: " . $form_data['subject'];
		$body = $app['twig']->render('email/contact-email.twig', array('form_data' => $form_data));
		$message = \Swift_Message::newInstance();
		$message->setSubject($subject);
		$message->setSender($sender);
		$message->setFrom($from);
		$message->setTo($to);
		$message->setBody($body);
		$app['mailer']->send($message);

		// TODO: The form should be AJAX-submitted eventually
		return $app->redirect($app['url_generator']->generate('home'));
	}
}
