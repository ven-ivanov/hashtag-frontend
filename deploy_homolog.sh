#!/bin/bash

ssh ec2-user@hashtagtravels.de 'bash -s' <<'ENDSSH'
  cd /var/www/mamp/htdocs
  git pull origin homolog
  sudo service httpd restart
ENDSSH
