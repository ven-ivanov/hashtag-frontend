#!/bin/bash

ssh ubuntu@54.187.60.174 'bash -s' <<'ENDSSH'
  cd /var/www/html/hashtag-travels
  git pull origin master
  sudo service php5-fpm restart
  sudo service nginx restart
ENDSSH
