var gulp = require('gulp');
var stylus = require('gulp-stylus');
var watch = require('gulp-watch');
var nib = require('nib');
var livereload = require('gulp-livereload');
var replace = require('gulp-replace');
var sourcemaps = require('gulp-sourcemaps');

var uglify = require('gulp-uglify');
var usemin = require('gulp-usemin');
var minifyCss = require('gulp-minify-css');
// var rev = require('gulp-rev');

var ftp = require('gulp-ftp');

var gulpFilter = require('gulp-filter');

gulp.task('dev', function(){
	watch({glob: 'web/public/styl/**/*.styl'}, function(files){
		gulp.src('web/public/styl/main.styl')
			.pipe(stylus({errors: true, use: [nib()]}))
			.pipe(gulp.dest('web/public/css/'))
			.pipe(livereload())
	})
})


gulp.task('usemin', function() {
	var jsFilter = gulpFilter('**/*.js');
	var cssFilter = gulpFilter('**/*.css');

	gulp.src('./app/Views/layout.twig')
		.pipe(replace('{{app.config.staticpath}}', '../../web/public'))
		.pipe(usemin({
			css: [minifyCss(), 'concat'],
			js: [uglify()],
		}))
		.pipe(jsFilter)
		.pipe(gulp.dest('web/public/js/'))
		.pipe(jsFilter.restore())
		.pipe(cssFilter)
		.pipe(gulp.dest('web/public/css/'))
});

gulp.task('ftp', function(){
	return gulp.src([
			'**/*',
			'!web/public/**/*',
		]).pipe(ftp({
		host: 'ftp.altaherholding.com',
		user: 'jesse@altaherholding.com',
		pass: 'lausanne2014',
		remotePath: '/frontend/'
	}))
})


var awspublish = require('gulp-awspublish');

gulp.task('amazon', function() {

	// create a new publisher
	var publisher = awspublish.create({
		key: 'AKIAJTDVTCWKXCM2NZMA',
		secret: '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',
		bucket: 'hashtag-frontend-static',
		endpoint: 's3-us-west-2.amazonaws.com'
	});

	return gulp.src([
			'web/public/**/*',
			'!web/public/img/airlines/big/**/*',
			'!web/public/img/airlines/small/**/*'
		])

		 // gzip, Set Content-Encoding headers and add .gz extension
		.pipe(awspublish.gzip())

		// publisher will add Content-Length, Content-Type and  headers specified above
		// If not specified it will set x-amz-acl to public-read by default
		.pipe(publisher.publish({
			'Cache-Control': 'max-age=315360000, no-transform, public'
		}))

		// create a cache file to speed up consecutive uploads
		.pipe(publisher.cache())

		 // print upload updates to console
		.pipe(awspublish.reporter());
});


gulp.task('prod', ['usemin', 'amazon'])

