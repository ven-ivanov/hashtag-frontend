//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('DealsSearchApp', ['DealsSearchApp.Controllers', 'DealsSearchApp.Services', 'DealsSearchApp.Directives', 'HashtagControllers', 'HashtagServices', 'HashtagFilters'])
	.config(function($interpolateProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})
	.run(function($rootScope){
		$rootScope.modal = false;

		///////////// WARNING
		// App.Modules['deals-map'].run();
		///////////// WARNING

		$rootScope.orderBy = {
			key: 'price',
			reverse: false
		}

	})


//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('DealsSearchApp.Controllers', [])

	.controller('filters', function($scope, $rootScope, DealsService) {


		DealsService.getResults().then(function(response){

			$scope.stars = 0;
			var airlines = [];

			$scope.deals = response.deals;

			_.each(response.deals, function(deal){
				_.each(deal.segments, function(segments) {
					_.each(segments, function(segment) {

						if(!_.where(airlines, segment.carrier).length) {
							airlines.push(segment.carrier);
						}

					});
				});
			});


			$scope.airlines = airlines;
			$scope.airline = 'Any';

			// trigger change filters
			var keysToTriggerChangeFilters = ['stars', 'airline'];

			_.each(keysToTriggerChangeFilters, function(item){
				$scope.$watch(item, _.throttle(function(current, newval){
					$rootScope.$emit('changeFilters', {
						stars: +$scope.stars,
						airline: $scope.airline
					})
				}, 500), true)
			});

		});




	})

	.controller('results', function($scope, $rootScope, $location, DealsService, Router) {

		DealsService.getResults().then(function(response) {

			$scope.currency = response.currency;
			$scope.destination = response.destination;
			$scope.departure = response.departure;
			$scope.arrival = response.arrival;

			$scope.deals = response.deals;

			App.Modules['deals-map'].run(response);

			$rootScope.$on('changeFilters', function(e, filters){
				DealsService.filter(filters).then(function(filteredDeals){
					$scope.deals = filteredDeals;
				});
			});

			$rootScope.$on('resetLoading', function(a,c) {
				var deals = response.deals;
				_.each(deals, function(deal) {
					deal.loading = false;
				});
				$scope.deals = deals;
			});

			$rootScope.filtersSelected = [];

			$rootScope.filters = _.flatten(_.map(response.deals, function(deal) { return deal.filters; }), true);
			$rootScope.filters = _.uniq($rootScope.filters);

			$rootScope.filters = _.map($rootScope.filters, function(filter) {
				return { name: filter, active: false };
			})

			$rootScope.mapFilter = function(e, filter) {
				e.preventDefault();

				var index = $rootScope.filtersSelected.indexOf(filter.name);

				filter.active = !filter.active;

				if(index < 0) {
					$rootScope.filtersSelected.push(filter.name);
				} else {
					$rootScope.filtersSelected.splice(index, 1);
				}

				App.Modules['deals-map'].filter($rootScope.filtersSelected, $rootScope.filters);
			}

			function setHotelDetail(id) {
				var hotel = _.where(response.deals, { id: +id });
				if(hotel.length) {
					$rootScope.hotelDetail(false, hotel[0]);
				}
			}

			function closeDetail() {
				$rootScope.detail = false;
			}

			$rootScope.$on('$locationChangeSuccess', function() {
				Router.run($location.path(), {
					'/': closeDetail,
					'/hotel/(:id)': setHotelDetail
				})
			})

			$rootScope.$emit('$locationChangeSuccess');

		});

	})


	.controller('detail', function($rootScope, $scope, $q, $location, HotelDetailService) {

		$scope.openShareModal = function(e){
			e.preventDefault();
			$rootScope.modal = 'hotel-deals';
			$rootScope.shareUrl = $location.absUrl();
		}

		$rootScope.hotelDetail = function(e, hotel) {
			if(e) e.preventDefault();

			var id = hotel.id;

			hotel.loading = true;

			console.log('OPAA', hotel);

			$q.all([HotelDetailService.getDetail(id, location.search), HotelDetailService.getRoomPictures(id)]).then(function(responses){

				var hotel = responses[0];
				var pictures = responses[1];

			 	$rootScope.detail = hotel;

			 	$rootScope.$emit('resetLoading');

				_.each(hotel.rooms, function(room) {
					var picture = _.where(pictures, { roomId: room.id });
					if(picture.length) room.picture = picture[0].url;
				});

				$scope.hotel = hotel;

			});

		}


	})



//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('DealsSearchApp.Services', [])

	.service('DealsService', function($http, $q) {

		var getResultsPromise;
		this.getResults = function(){

			if(getResultsPromise) return getResultsPromise;

			var defer = $q.defer();

			$http.get(App.uploadsDir + '/deals.json').then(function(response){
				defer.resolve(response.data);
			});

			return getResultsPromise = defer.promise;
		}


		this.filter = function(filters) {

			var defer = $q.defer();

			getResultsPromise.then(function(data) {

				defer.resolve(_.filter(data.deals, function(deal) {

					var valid = true;


					if(filters.stars && deal.stars != filters.stars) {
						valid = false;
					}


					if(filters.airline != 'Any' && filters.airline != null && valid) {

						var carrierCode = false;

						_.each(deal.segments, function(segments) {
							_.each(segments, function(segment) {
								if(segment.carrier.code == filters.airline) {
									carrierCode = true;
								}
							});
						});


						if(!carrierCode) {
							valid = false;
						}

					}

					return valid;

				}));

			});

			return defer.promise;

		}

	})


	.service('HotelDetailService', function($http, $q) {

		this.getDetail = function(id, search){

			var defer = $q.defer();

			// $http.get(App.baseAPIUrl + '/hotel/' + id + search).then(function(response){
			$http.get(App.uploadsDir + '/hotel-detail.json').then(function(response){
				defer.resolve(response.data);
			});

			return defer.promise;
		}


		this.getRoomPictures = function(id) {

			var defer = $q.defer();

			// $http.get(App.baseAPIUrl + '/hotel/images?hotelId=' + id).then(function(response){
			$http.get(App.uploadsDir + '/hotel-pictures.json').then(function(response){
				defer.resolve(response.data);
			});

			return defer.promise;
		}

	})




//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('DealsSearchApp.Directives', [])

	.directive('onLastRepeat', function() {
		return function(scope, element, attrs) {
			if (scope.$last) setTimeout(function(){
				App.Modules['carrousel'].run();
			}, 1);
		};
	})

	.directive('copyClipboard', function() {
		return function(scope, element, attrs) {
			var client = new ZeroClipboard( element );

			client.on( "ready", function( readyEvent ) {
				client.on( "aftercopy", function( event ) {
					alert("Copied text to clipboard: " + event.data["text/plain"] );
				});
			});
		};
	})

