//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('HashtagControllers', [])

	.controller('share', function($rootScope, $scope, $location, $timeout) {

		$scope.closeModal = function(e){
			e.preventDefault();
			// $location.path('/')
			$rootScope.modal = false;
		}

		$scope.shareFB = function(e) {

			e.preventDefault();
			e.stopImmediatePropagation();

			// var obj = {};
			// var detail = $rootScope.modal == 'car' ? $rootScope.selectedCar : $rootScope.detail;


			// if($rootScope.modal == 'car') {
			// 	detail.description = _.map(detail.features, function(feature){
			// 		return feature.name;
			// 	}).join(', ');
			// } else if($rootScope.modal == 'flight') {
			// 	console.log($rootScope.flightDetail.segments);
			// 	detail = {
			// 		title: $rootScope.flightDetail.segments[0][0].origin + ' to ' + $rootScope.flightDetail.segments[0][$rootScope.flightDetail.segments[0].length - 1].destination + ' - ' + App.selectedCurrency + $rootScope.flightDetail.totalPrice,
			// 		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam veniam iste quisquam quae incidunt, est doloremque mollitia in nulla optio.',
			// 		picture: 'http://hashtagtravels.de/hashtag-travels/web/public/img/logo.png'
			// 	}
			// }


			obj = {
				method: 'feed',
				// name: detail.title,
				link: $rootScope.shareUrl,
				// picture: detail.picture,
				// caption: 'hashtagtravels.com',
				// description: detail.description
			};

			FB.ui(obj, function(response) {
				if (response && response.post_id) {
					console.log('Post was published.');
				} else {
					console.log('Post was not published.');
				}
			});

		}


		$scope.submitEmail = function(e) {
			$timeout(function(){

				var parsleyValidation = $(e.target).parsley('validate');
				if(parsleyValidation.validationResult) {
					console.log($scope.form);
				}

			});

			return false;
		}

	})