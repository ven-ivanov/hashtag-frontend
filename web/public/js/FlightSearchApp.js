//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('FlightSearchApp', ['FlightSearch.Controllers', 'FlightSearch.Directives', 'FlightService', 'HashtagControllers', 'HashtagServices', 'HashtagFilters', 'ngCookies', 'LocalStorageModule'])
	.config(function($interpolateProvider, $httpProvider, $locationProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
		$locationProvider.html5Mode(true);
	})
	.run(function($rootScope, $location, FlightService){
		$rootScope.modal = false;

		$rootScope.closeModal = function(e){
			e.preventDefault();
			$rootScope.modal = false;
		}


		var errorHandlerTimeout;
		$rootScope.errorHandler = function(response){

			clearTimeout(errorHandlerTimeout);
			errorHandlerTimeout = setTimeout(function(){
				// alert(response.error);
			}, 300)
		}

	})


//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('FlightSearch.Controllers', [])

	.controller('filters', function($scope, FlightService, $q, $rootScope, $location, CABIN_CLASSES_CODE, FLIGHT_STOPS){

		$scope.isFilterVisible = false;

		$scope.classes = CABIN_CLASSES_CODE;

		//Stops
		$scope.stops = FLIGHT_STOPS


		var priceMin = 99999999999999;
		var priceMax = 0;

		var departureMin = 99999999999999999;
		var departureMax = 0;

		var durationMin = 99999999999999999;
		var durationMax = 0;

		FlightService.getResults(location.search).then(function(response){

			var classes = [];
			var stops = [];
			var airlines = [];

			_.each(response.fareGroup, function(fareGroup){

				priceMin = Math.min(priceMin, fareGroup.totalPrice)
				priceMax = Math.max(priceMax, fareGroup.totalPrice)

				dateFormatFix = (fareGroup.flightGroup[0][0].segments[0].departure).replace(' ', 'T')

				departureMin = Math.min(departureMin, +new Date(dateFormatFix))
				departureMax = Math.max(departureMax, +new Date(dateFormatFix))

				var duration = fareGroup.flightGroup[0][0].segments[0].duration;
				durationMin = Math.min(durationMin, parseInt(duration.replace(':', ''), 10))
				durationMax = Math.max(durationMax, parseInt(duration.replace(':', ''), 10))


				_.each(fareGroup.flightGroup, function(flightDestination){
					_.each(flightDestination, function(flight){

						//Classes
						if(_.indexOf(classes, flight.segments[0].cabinClass.code) == -1){
							classes.push(flight.segments[0].cabinClass.code)
						}

						//Stops
						var flightStops = (flight.segments.length > 3) ? 3 : flight.segments.length;

						if(_.indexOf(stops, flightStops) == -1){
							stops.push(flight.segments.length)
						}

					})

				})

			})

			// Airlines
			FlightService.getResultsMatrix().then(function(resultsMatrix){
				_.each(resultsMatrix.matrix, function(matrixItemStops){

					var price;
					var carrier;

					_.each(matrixItemStops, function(matrixItem){
						if(!price){
							price = matrixItem.totalPrice;
							carrier = matrixItem.carrier;
						} else {
							price = Math.min(price, matrixItem.totalPrice)
						}
					})

					airlines.push({
						carrier: carrier,
						price: price,
						checked: true
					})

				})

				$scope.airlines = airlines;

				$rootScope.$on('filterAirlines', function(e, airline){

					_.each($scope.airlines, function(airlineArrItem){
						if(airline.code != airlineArrItem.carrier.code) {
							airlineArrItem.checked = false;
						} else {
							airlineArrItem.checked = true;
						}
					})

					$scope.isFilterVisible = true;
				})

				$scope.selectAllAirlines = function(){
					_.each($scope.airlines, function(airlineArrItem){
						airlineArrItem.checked = true;;
					})
				}
			})


			//Classes
			$scope.classes = _.map(classes, function(code){ return _.extend({}, CABIN_CLASSES_CODE[code], {checked: true})});


			//Stops
			$scope.stops = _.map(stops, function(qtd){
				return _.extend({}, FLIGHT_STOPS[qtd], {checked: true})
			});

			//Currency
			$scope.currency = App.selectedCurrency;

			// Price
			$scope.priceBounds = {
				from: Math.floor(priceMin),
				to: Math.ceil(priceMax)
			}

			$scope.price = {
				from: Math.floor(priceMin),
				to: Math.ceil(priceMax)
			}

			// Departure
			$scope.departureBounds = {
				from: departureMin,
				to: departureMax
			}

			$scope.departure = {
				from: departureMin,
				to: departureMax
			}

			// Duration
			$scope.durationBounds = {
				from: durationMin,
				to: durationMax
			}

			$scope.duration = {
				from: durationMin,
				to: durationMax
			}

			var scopeKeysToTriggerChangeFilters = ['price', 'departure', 'duration', 'classes', 'stops', 'airlines'];

			_.each(scopeKeysToTriggerChangeFilters, function(item){
				$scope.$watch(item, _.throttle(function(current, newval){
					$rootScope.$emit('changeFilters', {
						price: $scope.price,
						departure: $scope.departure,
						duration: $scope.duration,
						classes: _.chain($scope.classes).where({checked: true}).pluck('value').value(),
						stops: _.chain($scope.stops).where({checked: true}).pluck('text').value(),
						airlines: _.chain($scope.airlines).where({checked: true}).pluck('carrier').value(),
					})
				}, 500), true)
			})


		})

	})


	.controller('results', function($scope, FlightService, $q, $rootScope, $location, $interval, Router, localStorageService){

		$scope.selectedFlights = {}
		$scope.selectedFlightsArr = []
		$scope.selectedFareGroupIndex;

		$scope.btnLoadMore = function(e) {
			e.preventDefault();
			$scope.limit += 10;
		}

		$scope.setSelectedFlightsArr = function(){

			var fareGroupIndex = this.fareGroupIndex
			var flightGroupIndex = this.flightGroupIndex;

			if($scope.selectedFareGroupIndex != fareGroupIndex){
				$scope.selectedFareGroupIndex = fareGroupIndex;

				_.each($scope.selectedFlights, function(flight, index){
					if(flightGroupIndex != index){
						delete $scope.selectedFlights[index];
					}

				})
			}

			$scope.selectedFlightsArr = [];
			_.each($scope.selectedFlights, function(flight, index){
				$scope.selectedFlightsArr.push(flight);
			})
		}

		$scope.getSelectedFlightsUrl = function(){
			var url = '#/flight/';

			_.each($scope.selectedFlights, function(flight, index){
				var flightNumbers = _.pluck(flight.segments, 'flightNumber');
				url += flightNumbers.join(',')
				url += '|'
			})

			return url.slice(0, -1);
		}

		$scope.loading = true;
		$scope.loading_percent = 0;

		$scope.possibleOrders = [
			{
				key: 'totalPrice',
				reverse: false,
				title: 'Lowest Price'
			},
			{
				key: 'totalPrice',
				reverse: true,
				title: 'Highest Price'
			},
		]

		$scope.orderBy = $scope.possibleOrders[0];

		$scope.limit = 10;

		var interval = $interval(function(){
			var add = Math.floor((Math.random() * 4));
			$scope.loading_percent = Math.min(83, $scope.loading_percent + add);
		}, 350)

		FlightService.getResults(location.search).then(angular.noop, angular.noop, function(percent){
			$interval.cancel(interval);
			$scope.loading_percent = Math.max($scope.loading_percent, Math.round(percent * 100));
		})

		var previousFlightGroupIndex;
		var previousFareGroupIndex;
		$scope.setFlightMap = function(flight, fareGroupIndex, flightGroupIndex){

			if(previousFareGroupIndex == fareGroupIndex && previousFlightGroupIndex == flightGroupIndex) return;

			var routes = [];
			_.each(flight.segments, function(segment, index){
				if(_.indexOf(routes, segment.origin) == -1) routes.push(segment.origin)
				if(_.indexOf(routes, segment.destination) == -1) routes.push(segment.destination)
			})

			FlightService.getAirportsCode().then(function(airports){
				App.Modules['results-map'].update(routes, airports)
			})

			previousFareGroupIndex = fareGroupIndex;
			previousFlightGroupIndex = flightGroupIndex;

		}


		$scope.selectFlight = function(e){
			e.preventDefault();

			var selectedFare = this.fare;
			var selectedFlights = this.selectedFlights;

			localStorageService.set('selectedFareID', selectedFare.id);
			localStorageService.set('selectedFlights', selectedFlights);
			localStorageService.set('flightCheckoutCurrentStateIndex', null)
			localStorageService.set('pricing', null)

			setTimeout(function(){
				window.location = 'checkout';
			})
		}


		// $scope.getPlatingCarrierCode = function(flight){
		// 	return (flight.platingCarrier.code) ? flight.platingCarrier.code : flight.segments[0][0].carrier.code;
		// }


		$scope.filterByAirline = function(airline){
			$rootScope.$emit('filterAirlines', airline);
		}

		$q.all([FlightService.getResults(location.search), FlightService.getResultsMatrix(), FlightService.getAirportsCode()]).then(function(responses){
			// console.log(responses[0])
			// console.log(responses[1])

			$scope.fareGroup = responses[0].fareGroup;
			$scope.fareGroupLengthOriginal = responses[0].fareGroup.length;
			$scope.resultsMatrix = responses[1];
			$scope.airports = responses[2];
			$scope.loading = false;

			if($scope.fareGroup.length == 0) {
				$rootScope.no_results = true;
				return false;
			}

			$scope.setFlightMap($scope.fareGroup[0].flightGroup[0][0], 0);

			localStorageService.set('passengers', responses[0].passengers);
			localStorageService.set('search', $location.search());

			$rootScope.$on('changeFilters', function(e, filters){
				FlightService.filter(filters).then(function(filteredFareGroups){
					$scope.limit = 10;
					$scope.fareGroup = filteredFareGroups;
					$scope.selectedFlights = {}
					$scope.selectedFlightsArr = [];
				})
			})

			function setFlightDetail(selectedFlights) {

				var flights = selectedFlights.split('|');
				var found = {};

				var fareGroup = _.find(responses[0].fareGroup, function(fare){
					found[fare.id] = [];

					return _.find(fare.flightGroup, function(flightGroup, flightGroupIndex){
						return _.find(flightGroup, function(flight, flightIndex){
							if(flights[flightGroupIndex] == _.pluck(flight.segments, 'flightNumber').join(',')){
								found[fare.id].push(flightGroupIndex + ':' + flightIndex);
							}

							return found[fare.id].length == flights.length;
						})
					})
				})

				if(fareGroup) {
					FlightService.setSelectedFareGroup(fareGroup, found[fareGroup.id].join('-'));
					$rootScope.flightDetail = flights[0];
					$rootScope.modal = 'modal-details';
				}
			}

			function closeDetail() {
				$rootScope.modal = false;
				$rootScope.detail = false;
			}

			$rootScope.$on('$locationChangeSuccess', function() {
				Router.run($location.hash(), {
					'/': closeDetail,
					'/flight/(:selectedFlights)': setFlightDetail
				})
			})

			$rootScope.$emit('$locationChangeSuccess');
		}, function(){
			$scope.loading = false;
			$rootScope.no_results = true;
			$interval.cancel(interval);
		})

	})

	.controller('modal-details', function($scope, FlightService, $q, $rootScope, $location, localStorageService){

		// $scope.selectFlight = function(e, next){
		// 	e.preventDefault();
		// 	localStorage.setItem('flight', JSON.stringify(this.flight))
		// 	window.location = next;
		// }

		$scope.openShareModal = function(e){
			e.preventDefault();
			$rootScope.modal = 'flight';
			$rootScope.shareUrl = $location.absUrl();
			$location.hash('');
		}

		// this.getSelectedFareGroup = function(){
		// 	return selectedFareGroup;
		// }
		// this.getSelectedFlights = function(){
		// 	return selectedFlights;
		// }

		$scope.selectFlight = function(e){
			e.preventDefault();

			var selectedFare = FlightService.getSelectedFareGroup();
			var selectedFlights = [];

			_.each(FlightService.getSelectedFlights().split('-'), function(flightGrp){
				var key_val = flightGrp.split(':');
				selectedFlights.push($scope.fareGroup.flightGroup[key_val[0]][key_val[1]]);
			})

			localStorageService.set('selectedFareID', selectedFare.id);
			localStorageService.set('selectedFlights', selectedFlights);
			localStorageService.set('flightCheckoutCurrentStateIndex', null)

			setTimeout(function(){
				window.location = 'checkout';
			})
		}

		FlightService.getAirportsCode().then(function(airports){
			$scope.fareGroup = FlightService.getSelectedFareGroup();

			// console.log(FlightService.getSelectedFareGroup());
			// console.log(FlightService.getSelectedFlights());

			var selectedFlights = [];

			_.each(FlightService.getSelectedFlights().split('-'), function(flightGrp){
				var key_val = flightGrp.split(':');
				selectedFlights.push($scope.fareGroup.flightGroup[key_val[0]][key_val[1]]);
			})

			$scope.selectedFlights = selectedFlights
			$scope.airports = airports;

			localStorageService.set('airports', airports);
			localStorageService.set('selectedFlights', selectedFlights);
		})

	})

	.controller('print-details', function($scope, FlightService, $q, $rootScope, $location, localStorageService){
		$scope.completeDetailedPassengers = localStorageService.get('completeDetailedPassengers')
		$scope.pricing = localStorageService.get('pricing')
		$scope.pnr = localStorageService.get('pnr')
		$scope.selectedFlights = localStorageService.get('selectedFlights')
		$scope.airports = localStorageService.get('airports');
		$scope.flightDetails = localStorageService.get('flightDetails');
	})

//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('FlightSearch.Directives', [])
	.directive('layover', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				var from = scope.$eval(attrs['from']);
				var to = scope.$eval(attrs['to']);
				var layover = moment(to).subtract(moment(from))
				angular.element(elm).text(layover.format('HH[h]mm[min]'))
			}

		}
	})

	.directive('internalTabs', function(){
		return {
			restrict: 'C',
			link: function(scope, elm, attrs){
				App.Modules['internal-tabs'].run();
			}

		}
	})

	.directive('overnight', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				var departure = scope.$eval(attrs['departure']);
				var arrival = scope.$eval(attrs['arrival']);

				if(moment(departure).format('DD') != moment(arrival).format('DD')) {
					angular.element(elm).addClass('modal__flight-details--overnight-active');
				} else {
					angular.element(elm).removeClass('modal__flight-details--overnight-active');
				}
			}

		}
	})

	//https://github.com/vasyabigi/angular-nouislider
	.directive('slider', function () {
		return {
			restrict: 'A',
			scope: {
				start: '@',
				step: '@',
				end: '@',
				callback: '@',
				margin: '@',
				ngModel: '=',
				ngFrom: '=',
				ngTo: '='
			},
			link: function (scope, element, attrs) {
				var callback, fromParsed, parsedValue, slider, toParsed;
				slider = $(element);
				callback = scope.callback ? scope.callback : 'slide';
				if (scope.ngFrom != null && scope.ngTo != null) {
					fromParsed = null;
					toParsed = null;
					slider.noUiSlider({
						start: [
							scope.ngFrom || scope.start,
							scope.ngTo || scope.end
						],
						step: parseFloat(scope.step || 1),
						connect: true,
						margin: parseFloat(scope.margin || 0),
						range: {
							min: [parseFloat(scope.start)],
							max: [parseFloat(scope.end)]
						}
					});
					slider.on(callback, function () {
						var from, to, _ref;
						_ref = slider.val(), from = _ref[0], to = _ref[1];
						fromParsed = parseFloat(from);
						toParsed = parseFloat(to);
						return scope.$apply(function () {
							scope.ngFrom = fromParsed;
							return scope.ngTo = toParsed;
						});
					});

					scope.$watch('ngFrom', function (newVal, oldVal) {
						if (newVal !== fromParsed) {
							return slider.val([
								newVal,
								null
							]);
						}
					});

					return scope.$watch('ngTo', function (newVal, oldVal) {
						if (newVal !== toParsed) {
							return slider.val([
								null,
								newVal
							]);
						}
					});
				} else {
					parsedValue = null;
					slider.noUiSlider({
						start: [scope.ngModel || scope.start],
						step: parseFloat(scope.step || 1),
						range: {
							min: [parseFloat(scope.start)],
							max: [parseFloat(scope.end)]
						}
					});
					slider.on(callback, function () {
						parsedValue = parseFloat(slider.val());
						return scope.$apply(function () {
							return scope.ngModel = parsedValue;
						});
					});
					return scope.$watch('ngModel', function (newVal, oldVal) {
						if (newVal !== parsedValue) {
							return slider.val(newVal);
						}
					});
				}
			}
		};
	})

	.directive('resultsMap', function($rootScope, FlightService){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				App.Modules['results-map'].angularLink(scope, elm, attrs)
				App.Modules['tooltip'].run()
			}
		}
	})

	.directive('assistOverflow', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs) {

				// var lastScrollTop = 0;
				// var scrollTopDifference = 0;
				// var isHidden = false;
				// var $header = $('.flight-search__fixed-header');
				// var inTheEnd = false;

				// $(elm).on('scroll', function(e){

				// 	var scrollTop = $(this).scrollTop();
				// 	var height = $(this).outerHeight()
				// 	var scrollHeight = $(this).get(0).scrollHeight;

				// 	if(scrollTop > lastScrollTop) {

				// 		scrollTopDifference += lastScrollTop - scrollTop;

				// 		if(scrollTopDifference < -10 && !isHidden) {
				// 			$header.addClass('is-hidden');
				// 			isHidden = true;
				// 		}

				// 	} else if (scrollHeight > scrollTop + height) {

				// 		scrollTopDifference = 0;

				// 		if(isHidden) {
				// 			$header.removeClass('is-hidden');
				// 			isHidden = false;
				// 		}
				// 	}

				// 	lastScrollTop = scrollTop;

				// 	if(scrollTop + 100 > Math.abs(height - scrollHeight)){
				// 		inTheEnd = true;
				// 		scope.$apply(function(){
				// 			if(scope.fareGroup.length > scope.limit){
				// 				scope.limit += 5;
				// 			}
				// 		})
				// 	} else {
				// 		inTheEnd = false;
				// 	}

				// })
			}
		}
	})

	.directive('resultsLoader', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs) {
				var activeTextIndex = 0;
				var resultsLoaderTexts = $('.results-list__loader-li');

				setInterval(function(){
					$(resultsLoaderTexts[activeTextIndex]).removeClass('active');

					if(activeTextIndex + 1 === resultsLoaderTexts.length){
						activeTextIndex = 0;
					} else {
						activeTextIndex++;
					}

					setTimeout(function(){
						$(resultsLoaderTexts[activeTextIndex]).addClass('active');
					}, 500)
				}, 3000)

				$(resultsLoaderTexts[activeTextIndex]).addClass('active');
			}
		}
	})


