//  _____ _ _ _
// |  ___(_) | |_ ___ _ __ ___
// | |_  | | | __/ _ \ '__/ __|
// |  _| | | | ||  __/ |  \__ \
// |_|   |_|_|\__\___|_|  |___/

angular.module('HashtagFilters', ['HashtagConstants'])

	// search
	.filter('formatDate', function(){
		return function(data, format){
			return moment(data).format(format);
		}
	})

	.filter('formatDuration', function(){
		return function(data){
			// return data;
			data = data + '';
			if(data.length < 4) data = '0' + data;

			var hour = parseInt(data.slice(0, 2), 10);
			var minutes = parseInt(data.slice(-2), 10);

			while(minutes - 60 >= 0){
				hour++;
				minutes -= 60;
			}

			if((hour + '').length == 1) hour = '0' + hour;
			if((minutes + '').length == 1) minutes = '0' + minutes;

			return hour + ':' + minutes;
		}
	})

	.filter('passengerType', function(){
		return function(data, format){
			var types = {
				ADT: 'Adult',
				CHD: 'Child',
				INF: 'Infant'
			}
			return types[data];
		}
	})

	.filter('stops', function(){
		return function(data){
			if(data == 0) return 'Direct';
			return (data > 1) ? data + ' stops' : data + ' stop';
		}
	})

	.filter('cabinClass', function(CABIN_CLASSES_CODE){
		return function(data){
			return CABIN_CLASSES_CODE[data].text;
		}
	})


	//checkout
	.filter('pricingKind', function(PRICING_KINDS){
		return function(data, format){
			return PRICING_KINDS[data].text;
		}
	})
	.filter('insurance', function(){
		return function(data, format){
			var types = {
				1: 0,
				2: 15,
				3: 25
			}
			return types[data];
		}
	})

	.filter('bagWeight', function(){
		return function(data, format){
			var result = data.match(/[0-9]+KG/);
			return (result) ? result[0] : "";
		}
	})

	.filter('encode', function(){
		return function(data, format){
			return encodeURIComponent(data);
		}
	})

	.filter('airlinesFromSegments', function(){
		return function(segments){
			var carriersCode = [];
			var carriers = []

			_.each(segments, function(segment){
				var code = segment.carrier.code;

				if(_.indexOf(carriersCode, code) === -1){
					carriersCode.push(code)
					carriers.push(segment.carrier)
				}
			})


			return carriers;
		}
	})

	.filter('firstWord', function(){
		return function(name){
			return name.split(' ')[0];
		}
	})

	.filter('characteristics', function(){
		return function(data, available){
			var characteristics = ""
			var status_map = {
				"A"  : "Available",
				"T"  : "Taken",
				"U"  : "Unknown",
				"N"  : "No seat at this place"
			}

			var characteristic_map = {
				"B"  : "Bassinet",
				"CL" : "Closet",
				"H"  : "Handicapped",
				"I"  : "Adult with infant",
				"L"  : "Leg Space",
				"LA" : "Lavatory",
				"ST" : "Stairs"
			}

			_.each(data, function(characteristic) {
				if(typeof(characteristic) == 'string'){
					if(characteristic_map[characteristic]){
						if(characteristic == 'A' && !available)
							return
						
						characteristics += characteristic_map[characteristic] + ', '
					};
				} else {
					characteristics += status_map[characteristic.status] + ', '
				}
			})

			if(characteristics.length > 0)
				characteristics = characteristics.substring(0, characteristics.length - 2);

			return characteristics;
		}
	})

	.filter('orderObjectBy', function() {
		return function(items, field, reverse) {
			var filtered = [];
			angular.forEach(items, function(item) {
				filtered.push(item);
			});
			filtered.sort(function (a, b) {
				return (a[field] > b[field] ? 1 : -1);
			});
			if(reverse) filtered.reverse();
			return filtered;
		};
	});
