//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('CarSearchApp', ['CarSearchApp.Controllers', 'CarSearchApp.Directives', 'CarService', 'HashtagControllers', 'HashtagServices', 'HashtagFilters', 'ngCookies', 'LocalStorageModule'])
	.config(function($interpolateProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})
	.run(function($rootScope, CarService){
		$rootScope.modal = false;
	});

//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('CarSearchApp.Controllers', [])

	.controller('results', function($rootScope, $scope, $q, $location, CarService, Router, localStorageService){

		$scope.limit = 4;

		$scope.btnLoadMore = function(e){
			$scope.limit += 3;
		}

		CarService.getResults(location.search).then(function (data) {
			$scope.results = data;

			$scope.orderBy = {
				key: 'price.total',
				reverse: false
			}


			$scope.$watch('orderBy.value', function(val){
				var key = '';

				if(val == 'price') {
					key = 'price.total';
				} else if(val == 'comsumption') {
					key = 'features[3].name';
				} else {
					key = 'company.name';
				}

				$scope.orderBy.reverse = true;
				$scope.orderBy.key = key;
				$scope.changeOrder(null, key);
			});


			$scope.changeOrder = function(e, key){
				if(e) e.preventDefault();

				if($scope.orderBy.key == key){
					$scope.orderBy.reverse = !($scope.orderBy.reverse)
				} else {
					$scope.orderBy = {
						key: key,
						reverse: false
					}
				}

				$scope.limit = 3;
			}

			$scope.openShareModal = function(e, car){
				e.preventDefault();
				$rootScope.modal = 'car';
				$rootScope.selectedCar = car;
				$rootScope.shareUrl = $location.absUrl() + '#/car/' + car.id;
			}

			function unsetCars() {
				var cars = data;
				_.each(cars, function(car) {
					car.active = false;
				});
			}

			function setCar(id) {
				var cars = _.map(data, function(car) {
					if(car.id == +id) {
						car.active = true;
					}
				});
			}


			$rootScope.$on('$locationChangeSuccess', function() {
				Router.run($location.path(), {
					'/': unsetCars,
					'/car/(:id)': setCar
				})
			})

			$rootScope.$emit('$locationChangeSuccess');

		});

		$scope.selectCar = function(e){
			e.preventDefault();

			var selectedCar = this.car;
			localStorageService.set('selectedCar', selectedCar);

			setTimeout(function(){
				window.location = 'checkout';
			})
		}
	})




//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('CarSearchApp.Directives', [])

	.directive('assistWidth', function() {
		return {
			restrict: 'A',
			link: function($scope, elm, attrs) {
				$(window).resize(function() {
					$scope.$apply(function(){
						$scope.width = $(window).width();
					});
				});
				setTimeout(function() {
					$(window).trigger('resize');
				}, 1);
			}
		}
	})

	.directive('copyClipboard', function() {
		return function(scope, element, attrs) {
			var client = new ZeroClipboard( element );

			client.on( "ready", function( readyEvent ) {
				client.on( "aftercopy", function( event ) {
					alert("Copied text to clipboard: " + event.data["text/plain"] );
				});
			});
		};
	})







