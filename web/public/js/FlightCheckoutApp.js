//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('FlightCheckoutApp', ['FlightCheckout.Controllers', 'FlightCheckout.Services', 'FlightCheckout.Directives',
        'FlightService', 'HashtagControllers', 'HashtagFilters', 'ngCookies', 'LocalStorageModule', 'HashtagDirectives'])
	.config(function($interpolateProvider, $httpProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})
	.run(function($rootScope, State, FlightService, $locale, ERROR_MESSAGES, $location, localStorageService){

		$locale.NUMBER_FORMATS.PATTERNS[1].negPre = '-';
		$locale.NUMBER_FORMATS.PATTERNS[1].negSuf = '';

		$rootScope.state = State.getCurrentState();
		$rootScope.previousState = State.getPreviousState();

		$rootScope.backToPreviousState = function(e){
			e.preventDefault();

			if(State.getPreviousState()){
				State.backToPreviousState()
			} else {
				var search = '?';
				_.each(localStorageService.get('search'), function(value, key){
					search += key + '=' + value + '&'
				})
				search.slice(0, -1);
				window.location = $(e.target).attr('href') + search;
			}
		}

		$rootScope.modal = false;

		$rootScope.closeModal = function(e){
			e.preventDefault();
			$rootScope.modal = false;
		}

		$rootScope.hasCoupon = false;
		$rootScope.showCoupon = function(e){
			e.preventDefault();
			$rootScope.modal = 'coupon';
		}

		State.onStateChange = function(){
			$rootScope.state = State.getCurrentState();
			$rootScope.previousState = State.getPreviousState();
		}

		// window.onbeforeunload = function(){
		// 	return "Are you sure you want to leave the checkout? Your input data will not be saved.";
		// }


		var errorHandlerTimeout;
		$rootScope.errorHandler = function(response){

			clearTimeout(errorHandlerTimeout);
			// window.onbeforeunload = null;

			$rootScope.loading = false;

			errorHandlerTimeout = setTimeout(function(){

				switch(response.error){
					case 'invalid.flight.id': {
						alert(ERROR_MESSAGES[response.error]);
						history.go(-1);
						break;
					}

					case 'invalid.previous.search': {
						alert(ERROR_MESSAGES[response.error]);
						history.go(-1);
						break;
					}

					default: {
						alert(response.error);
					}
				}
			}, 300)

		}

	})

	.constant('LOADING_MESSAGES', {
		FLIGHT_PRICING: {
			title: 'Loading your trip',
			message: 'This process can take a few seconds.'
		},

		PERSONAL_DETAILS: {
			title: 'Validating your personal details',
			message: 'Please wait...'
		},

		FLIGHT_OPTIONS: {
			title: 'Loading options for your flight',
			message: 'This process can take a few seconds.'
		},

		BOOKING: {
			title: 'Booking your flight',
			message: 'This process can take a few seconds.'
		},

		TICKETING: {
			title: 'Ticketing your flight',
			message: 'This process can take a few seconds.'
		},

		SEATS: {
			title: 'Booking your seats',
			message: 'This process can take a few seconds.'
		},

		INSURANCE: {
			title: 'Processing your request',
			message: 'This process can take a few seconds.'
		}

	})

	.constant('ERROR_MESSAGES', {
		'invalid.flight.id': 'Invalid flight id. You will be redirected back to your flight search.',
		'invalid.previous.search': 'Invalid previous search. Redirecting...',
	})

	.constant('PRICING_KINDS', {
		"FEE": {value: "FEE", text: "Booking Fee"},
		"BAS": {value: "BAS", text: "Booking"},
		"TAX": {value: "TAX", text: "Taxes"},
		"CPN": {value: "CPN", text: "Coupon"},
		"TWT": {value: "TWT", text: "Twitter Discount"},
	})


	.constant('FLIGHT_STOPS', {
		1: {text: 'Direct'},
		2: {text: '1'},
		3: {text: '2+'},
	})


//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('FlightCheckout.Controllers', [])

	.controller('summary', function($scope, FlightService, $q, $rootScope, localStorageService){

		FlightService.getAirportsCode().then(function(airports){
			$scope.airports = airports;
		})

		$scope.summaryDetailsOpened = 0;
		$scope.summaryDetailsAllVisible = false;
		$scope.flights = localStorageService.get('selectedFlights');
		$scope.openSummaryDetailItem = function(flightIndex){
			$scope.summaryDetailsOpened = ($scope.summaryDetailsOpened !== flightIndex) ? flightIndex : '';
		}

		$scope.summarySeatDetailsOpened = 0;
		$scope.summarySeatDetailsAllVisible = false;
		$scope.openSummarySeatDetailItem = function(detailIndex){
			$scope.summarySeatDetailsOpened = ($scope.summarySeatDetailsOpened !== detailIndex) ? detailIndex : '';
		}

		FlightService.getCheckout().then(function(response){
			if(response.error){
				alert(response.error);
				return;
			}

			if(response.currency != App.selectedCurrency)
				window.location = "/flight/search" + localStorageService.get('flightSearchQuery')

			$scope.pricing = response.pricing;
			$scope.currency = response.currency;
			$scope.totalPricing = response.totalprice;

			FlightService.getSeatMap()
		})

		FlightService.getPassengers().then(function(passengers){
			$scope.passengers = passengers;
		})

		$rootScope.$on('updateSeats', function(e, data){
			$scope.flightDetails = [];

			_.each(data, function(flight){
				var flightDetail = {}
				var flightInfos = flight.flightKey.split('|')

				var flightFrom = flightInfos[0]
				var flightTo = flightInfos[2]
				var flightNumber = flightInfos[flightInfos.length - 1]

				flightDetail.name = flightNumber
				flightDetail.from = flightFrom
				flightDetail.to = flightTo

				flightDetail.passengers = []

				_.each(flight.passengers, function(data){
					var passenger = {}
					var completeDetailedPassenger = _.find(localStorageService.get('completeDetailedPassengers'), { 'id': data.id });

					if(completeDetailedPassenger){
						var firstName = completeDetailedPassenger.personalDetails.firstName
						var lastName = completeDetailedPassenger.personalDetails.lastName

						passenger.name = firstName.charAt(0) + '. ' + lastName
						passenger.seat = data.seat
						flightDetail.passengers.push(passenger)
					}
				})
				$scope.flightDetails.push(flightDetail);
			})

			localStorageService.set('flightDetails', $scope.flightDetails);
		})

		$rootScope.$on('updatePricing', function(e, data){

			_.each(data, function(pricing){
				if(pricing.type == 'CPN' || pricing.type == 'TWT'){
					pricing.editable = true;
				}
			})

			$scope.pricing = data;
			$scope.totalPricing = totalPricing(data);

			localStorageService.set("pricing", $scope.pricing)
			localStorageService.set("totalPricing", $scope.totalPricing)
		})

		// $scope.removeItem = function(e){
		// 	e.preventDefault();

		// 	if(this.p.type == 'CPN'){
		// 		FlightService.removeCoupon();
		// 		$rootScope.hasCoupon = false;
		// 		$scope.pricing.splice(this.i, 1);
		// 	}

		// 	if(this.p.type == 'TWT'){
		// 		FlightService.removeTwitterDiscount();
		// 		$rootScope.hasTwitterDiscount = false;
		// 		$scope.pricing.splice(this.i, 1);
		// 	}

		// }

	})


	.controller('personal-details', function($scope, FlightService, $q, $rootScope, State, LOADING_MESSAGES, $timeout, localStorageService, PHONE_COUNTRY_CODES){
		$scope.carriers = []
		flights = localStorageService.get('selectedFlights');

		_.each(flights, function(flight){
			_.each(flight.segments, function(segments){
				if(!_.find($scope.carriers, segments.carrier))
					$scope.carriers.push(segments.carrier)
			})
		})

		$scope.phone_country_codes = [{phoneCode: '', title: 'Select'}].concat(PHONE_COUNTRY_CODES);

		FlightService.getPassengers().then(function(passengers){
			$scope.passengers = passengers;
		})

		$scope.$watch('user', function(user){
			if(!user) return;

			FlightService.getPassengers().then(function(passengers){
				passengers[0].personalDetails = {
					firstName: user.first_name,
					lastName: user.last_name,
					email: user.email,
					title: user.title,
					phoneCode: (user.phone_code) ? user.phone_code : '',
					phoneNumber: (user.phone_number) ? '+' + user.phone_code + user.phone_number : ''
				}
			})
		})

		$scope.user = App.user || {};

		$scope.submitPersonalDetails = function(e){
			e.preventDefault();

			$timeout(function(){
				var parsleyValidation = $(e.target).parsley('validate');
				if(parsleyValidation.validationResult) {

					$rootScope.loading = LOADING_MESSAGES['PERSONAL_DETAILS'];

					_.each($scope.passengers, function(passenger){
						var phone_code = '+' + passenger.personalDetails.phoneCode
						passenger.personalDetails.phoneNumber = passenger.personalDetails.phoneNumber.replace(phone_code, '')
					})

					FlightService.setPersonalDetails($scope.passengers, $scope.user).then(function(response){
						localStorageService.set('completeDetailedPassengers', $scope.passengers)

						//Success
						$rootScope.loading = LOADING_MESSAGES['FLIGHT_OPTIONS'];

						FlightService.setPNR(response.data.PNR);

						FlightService.getSeatMap()['finally'](function(){
							$rootScope.loading = false;
							State.nextState();
						})

					}, function(response){
						//Error
						$rootScope.loading = false;
						$scope.error_message = response.data.error;
					})
				}
			})

		}
	})

	.controller('booking-options', function($scope, FlightService, $q, $rootScope, State){

		FlightService.getSeatMap().then(function(response){
			var there_is_seatmap = false;
			_.each(response.seatMapInfo.flightData, function(flightData){
				there_is_seatmap = there_is_seatmap || !!(flightData.seatDetails && flightData.seatDetails.row && flightData.seatDetails.row.length);
			})

			$scope.seatMap = there_is_seatmap;
		})

		$scope.submitBookingOptions = function(e){
			e.preventDefault();
			State.nextState();
		}

		$scope.showManageLuggage = function(e){
			e.preventDefault();
			$rootScope.modal = 'manage-luggage';
		}

		$scope.showInsurance = function(e){
			e.preventDefault();
			$rootScope.modal = 'insurance';
		}

		$scope.showSelectSeat = function(e){
			e.preventDefault();
			$rootScope.modal = 'select-seat';
		}

		$scope.showMoreSugestions = false;
	})

	.controller('payment-details', function($scope, FlightService, $q, $rootScope, State, $timeout, LOADING_MESSAGES, $http, localStorageService){

		if(window.DATATRANS_RESPONSE){

			if(window.DATATRANS_RESPONSE.status == 'success'){
				State.nextState();
				console.log(DATATRANS_RESPONSE)
				$rootScope.loading = LOADING_MESSAGES['TICKETING'];

				FlightService.ticket(DATATRANS_RESPONSE).then(function(){
					$rootScope.loading = false;
				}, function(){
					$rootScope.loading = false;
				})

				return;

			} else {
				$scope.error_message = window.DATATRANS_RESPONSE.errorDetail;
			}
		}

		FlightService.getPaymentDetails().then(function(paymentDetails){
			$scope.payment = paymentDetails;

			$scope.payment.method = 'ECA';
			$scope.payment.creditCardNumber = '5200000000000007'
			$scope.payment.expirationMonth = 12
			$scope.payment.expirationYear = 15
			$scope.payment.securityCode = 123

		})

		// $scope.$watch('payment', function(data){
		// 	if(!data) return;

		// 	$http.post('/payment/sign', {amount: data.amount, currency: data.currency, ref: data.ref}).then(function(response){
		// 		$scope.payment.sign = response.data;
		// 		console.error('WARNING, GETTING SIGN FROM PHP. INSECURE!!')
		// 		// console.log(response.data)
		// 	});
		// }, true)

		$scope.submitPaymentDetails = function(e){
			$rootScope.loading = LOADING_MESSAGES['BOOKING'];

			if(!$scope.payment.ref) {
				e.preventDefault();
				console.log($scope.payment)

				FlightService.book().then(function(data){

					$scope.payment.amount = data.totalBookingAmount;
					$scope.payment.currency = App.selectedCurrency;
					$scope.payment.ref = data.pnr;
					$scope.payment.sign = data.datasign;

					console.log(data)

					if(data.isPnrCreated) {
						localStorageService.set('pnr', data);

						setTimeout(function(){
							// $rootScope.loading = false;
							$(e.target).submit();
						}, 500);

					} else {
						console.error('Pnr Not created')
						$rootScope.loading = false;
						State.nextState();
					}

				}, function(){
					$rootScope.loading = false;
				})
			}


			// $timeout(function(){
				// var parsleyValidation = $(e.target).parsley('validate');
				// if(parsleyValidation.validationResult) {

					// $rootScope.loading = LOADING_MESSAGES['BOOKING'];

					// FlightService.book().then(function(){
						// $rootScope.loading = false;

						// State.nextState();
					// })


				// }
			// })
		}

	})

	.controller('coupon', function($scope, $q, $rootScope, State, FlightService){
		$scope.states = {
			waiting: 0,
			validating: 1,
			invalid: 2,
			valid: 3
		}

		$scope.state = $scope.states.waiting;

		$scope.useCoupon = function(e){
			e.preventDefault();
			$rootScope.modal = false;
			$rootScope.hasCoupon = true;

			$rootScope.$emit('updatePricing', $scope.pricing);
		}

	})

	.controller('manage-luggage', function($scope, FlightService, $q, $rootScope, State){
		// $q.all([FlightService.getDetail()]).then(function(responses){
		// 	$scope.passengers = responses[0].passengers;
		// 	$scope.selectedPassenger = responses[0].passengers[0];

		// 	$scope.getLuggageInfoByType = function(type){
		// 		return _.filter(responses[0].luggage, function(luggage){
		// 			return luggage.passengerType == type;
		// 		})[0].luggage;
		// 	}
		// })


	})

	.controller('select-seat', function($scope, FlightService, $q, $rootScope, State, localStorageService, LOADING_MESSAGES){

		$scope.currentFlightIndex = 0;
		$scope.currentPassengerIndex = 0;

		function generateZones(seat_lines){
			var seatHeight = 40;
			var currentCategory;
			var zones = [];
			var linesCategory = 1;
			var top = 0;
			_.each(seat_lines, function(line, row){
				var category = line[0].category;

				if(currentCategory !== category){
					zones.push({
						category: (currentCategory === undefined) ? category : currentCategory,
						height: linesCategory * seatHeight,
						top: top,
						price: line[0].price
					})

					top += linesCategory * seatHeight;
					linesCategory = 1;
				} else {
					linesCategory++;
				}
				currentCategory = category;
			});

			// return zones;
			return [
				{
					category: 'foo',
					height: 4 * seatHeight,
					top: 0,
					price: 200
				},
				{
					category: 'bar',
					height: 2 * seatHeight,
					top: 160,
					price: 300
				}
			];
		};

		function getCorridors(seatDisplay){
			var corridors = [];
			_.each(seatDisplay, function(seatDisplay){
				if(_.isObject(seatDisplay) && seatDisplay.columns.length){
					for(var i = 1; i < seatDisplay.columns.length; i++){
						if(seatDisplay.columns[i].position == 'A' && seatDisplay.columns[i-1].position == seatDisplay.columns[i].position){
							corridors.push(seatDisplay.columns[i-1].value)
						}
					}
				}
			})

			return corridors;
		}

		function updateAirplane(){
			// $scope.zones = generateZones($scope.flights[$scope.currentFlightIndex].seats);
			$scope.seatDetails = $scope.flights[$scope.currentFlightIndex].seatDetails;
			$scope.seatDisplay = $scope.flights[$scope.currentFlightIndex].seatDisplay;
			$scope.corridors = getCorridors($scope.seatDisplay);
			$scope.error_message = false;

			if($scope.seatDetails){
				_.each($scope.seatDetails.row, function(row){
					$scope.setRowExtraFieldsClasses(row);

					_.each(row.seat, function(seat){
						$scope.setSeatExtraFieldsClasses(row, seat);
					})
				})
			}
		}

		$scope.setCurrentFlight = function(e){
			e.preventDefault();
			$scope.currentFlightIndex = this.i;
			updateAirplane();
		}

		$scope.setCurrentPassenger = function(e){
			e.preventDefault();
			$scope.currentPassengerIndex = this.j;
		}

		$scope.setPassengerSeat = function(e){
			e.preventDefault();

			var seat = this.row.number + this.seat.column;
			$scope.passengers[$scope.currentPassengerIndex]['selectedSeat'][$scope.currentFlightIndex] = seat;

			//Current flight index
			_.find($scope.flights, function(flightData, index){
				if(flightData.seatDetails && flightData.seatDetails.row && flightData.seatDetails.row.length){
					return _.find($scope.passengers, function(passenger, passengerIndex){
						if(!passenger['selectedSeat'][index]){
							$scope.currentFlightIndex = index;
							$scope.currentPassengerIndex = passengerIndex;
							return true;
						}
					})

				}
			})
			updateAirplane();
		}

		$scope.setRowExtraFieldsClasses = function(row){
			className = this.getSeatOrRowExtraFields('row', row, {
				"EL" : "exit-left",
				"E" : "exit-both",
				"ER" : "exit-right",
				"K" : "overwing"
			});

			row.extra_fields = {
				className: className
			};
		}

		$scope.setSeatExtraFieldsClasses = function(row, seat){
			var seatCode = row.number + seat.column;
			var status = seat.status;
			var name = '';
			var className = '';
			var available = true;
			var corridor = false;

			// A = Available
			// T = Taken
			// U = Unknown
			// N = No seat at this place

			if(status === 'A')
			{
				available = true;
				className = 'available'

			} else if(status === 'T')
			{
				available = false;
				className = 'reserved'

			} else if(status === 'U')
			{
				available = false;
				className = 'unavailable'

			} else if(status === 'N')
			{
				available = false;
				className = 'no-seat'
			}

			//Friend
			_.each($scope.passengers, function(passenger) {
				if(passenger.selectedSeat[$scope.currentFlightIndex] == seatCode){
					className = 'selected selected-friend';
					name = (passenger.personalDetails) ? passenger.personalDetails.firstName : '';
					available = false;
				}
			})

			//Mine
			if($scope.passengers[$scope.currentPassengerIndex].selectedSeat[$scope.currentFlightIndex] == seatCode) {
				className = 'selected';
				name = ($scope.passengers[$scope.currentPassengerIndex].personalDetails) ? $scope.passengers[$scope.currentPassengerIndex].personalDetails.firstName : '';
				available = true;
			}

			//Corridor
			if(_.indexOf($scope.corridors, seat.column) !== -1){
				corridor = true;
				className += ' corridor'
			}

			seat.characteristic.unshift({"status":status});
			className += this.getSeatOrRowExtraFields('seat', seat, {
				"B" : "bassinet",
				"CL" : "closet",
				"H" : "handicapped",
				"I" : "with-child",
				"L" : "leg-space",
				"LA" : "lavatory",
				"ST" : "stairs"
			});

			seat.extra_fields = {
				originalStatus: status,
				className: className,
				available: available,
				name: name,
				corridor: corridor
			};
		}

		$scope.getSeatOrRowExtraFields = function(type, seatOrRow, map){
			var characteristics = ""
			var characteristicName = ( type == 'row' ) ? 'rowCharacteristic' : 'characteristic'

			_.each(seatOrRow[characteristicName], function(characteristic) {
				if(characteristic && map[characteristic]){
					characteristics += ' ' + map[characteristic]
				};
			})

			return characteristics;
		}

		$q.all([FlightService.getSeatMap(), FlightService.getAirportsCode(), FlightService.getPassengers()]).then(function(responses){

			$scope.airports = responses[1];
			$scope.flights = responses[0].seatMapInfo.flightData;
			$scope.passengers = responses[2];

			$rootScope.$emit('updatePassengers', $scope.passengers)

			_.each($scope.passengers, function(passenger){
				passenger.selectedSeat = passenger.selectedSeat || {};
			})

			//Current flight index
			_.find(responses[0].seatMapInfo.flightData, function(flightData, index){
				if(flightData.seatDetails && flightData.seatDetails.row && flightData.seatDetails.row.length){
					$scope.currentFlightIndex = index;
					return true;
				}
			})

			updateAirplane();

			$scope.bookSeats = function(e){

				var nextUnselectedSeat = _.find($scope.flights, function(flightData, index){
					if(flightData.seatDetails && flightData.seatDetails.row && flightData.seatDetails.row.length){
						return _.find($scope.passengers, function(passenger, passengerIndex){
							if(!passenger['selectedSeat'][index]){
								return true;
							}
						})
					}
				})

				if(nextUnselectedSeat) return alert('You need to select for all passengers');

				$rootScope.modal = false;
				$rootScope.loading = LOADING_MESSAGES['SEATS'];

				FlightService.bookSeats($scope.flights, $scope.passengers).then(function(response){
					$rootScope.loading = false;
				}, function(response){
					$rootScope.loading = false;
					$rootScope.modal = 'select-seat';
					$scope.error_message = response.data.error;
				})
			}

			$scope.closeSelectSeat = function(e){
				e.preventDefault();

				if(confirm('Do you want to book the selected seats?')){
					$scope.bookSeats(e);
				} else {
					$rootScope.modal = false;
				}
			}
		})


	})


	.controller('confirmation', function($scope, FlightService, $q, $rootScope, State, localStorageService){
		$scope.pnr = localStorageService.get('pnr');
	})


	.controller('insurance', function($scope, $rootScope, $timeout, FlightService, State, LOADING_MESSAGES){

		$scope.showCancellation = function(e){
			e.preventDefault();
			$rootScope.modal = 'insurance-details';
		}

		$scope.showFullCover = function(e){
			e.preventDefault();
			$rootScope.modal = 'insurance-details';
		}

		$scope.submitInsurance = function(e){
			e.preventDefault();

			$rootScope.modal = false;
			$rootScope.loading = LOADING_MESSAGES['INSURANCE'];

			FlightService.bookInsurance($scope.passengers).then(function(){
				$rootScope.loading = false;
			})

		}

		$scope.setActivePassenger = function(){
			$scope.passengerActive = this.p;
		}

		FlightService.getPassengers().then(function(passengers){
			$scope.passengers = passengers;
			$scope.passengerActive = passengers[0];

			_.each($scope.passengers, function(passenger){
				passenger.selectedInsurance = passenger.selectedInsurance || 'decline'
			})
		})


	})

	.controller('insurance-details', function($scope, $rootScope){

		$scope.showInsurance = function(e) {
			e.preventDefault();
			$rootScope.modal = 'insurance';
		}

	})


//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('FlightCheckout.Services', ['LocalStorageModule'])
	.service('State', function(localStorageService){
		var currentStateIndex = parseInt(localStorageService.get('flightCheckoutCurrentStateIndex'), 10) || 0;
		var states = [
			{name: 'personal-details', string: 'Personal Details'},
			{name: 'booking-options', string: 'Booking Options'},
			{name: 'payment-details', string: 'Payment Details'},
			{name: 'confirmation', string: 'Confirmation'},
		]

		this.setState = function(index){
			localStorageService.set('flightCheckoutCurrentStateIndex', index)
			currentStateIndex = index;
			this.onStateChange(index);
		}

		this.nextState = function(){
			this.setState(currentStateIndex+1);
		}

		this.backToPreviousState = function(){
			this.setState(currentStateIndex-1);
		}

		this.getPreviousState = function(){
			return states[currentStateIndex-1];
		}

		this.getCurrentState = function(){
			return states[currentStateIndex];
		}

		this.onStateChange = function(){};

	})


//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('FlightCheckout.Directives', [])
	.directive('layover', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				var from = scope.$eval(attrs['from']);
				var to = scope.$eval(attrs['to']);
				var layover = moment(to).subtract(moment(from))
				angular.element(elm).text(layover.format('HH[h]mm[min]'))
			}

		}
	})

	.directive('internalTabs', function(){
		return {
			restrict: 'C',
			link: function(scope, elm, attrs){
				App.Modules['internal-tabs'].run();
			}

		}
	})

	.directive('share', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				App.Modules['modal'].run();
				App.Modules['share'].run();
			}

		}
	})

	.directive('tweetForDiscount', function(FlightService, $rootScope){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){

				setTimeout(function(){
					twttr.ready(function () {
						twttr.events.bind('tweet', function (e) {
							scope.$apply(function(){

								FlightService.setTwitterDiscount().then(function(response){
									$rootScope.$emit('updatePricing', response.pricing);
									$rootScope.hasTwitterDiscount = true;
								})
							})
						});
					});
				}, 500)
			}

		}
	})

	.directive('parsleyValidate', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				$(elm).parsley();
			}

		}
	})

	// .directive('checkoutCreditCardInput', function(){
	// 	return {
	// 		restrict: 'C',
	// 		require: 'ngModel',
	// 		link: function(scope, elm, attrs, ctrl){
	// 			var justNumbersReg = /^\d+$/;

	// 			$(elm).validateCreditCard(function(result) {

	// 				var creditCardVal = elm.val(),
	// 					lng = creditCardVal.length;

	// 				// is it not a number?
	// 				if( !justNumbersReg.test(creditCardVal[lng - 1]) ) {
	// 					elm.val( creditCardVal.substr(0, lng - 1) );
	// 					return;
	// 				}

	// 				creditCardVal = creditCardVal.split(' ').join('');

	// 				// apply spaces mask
	// 				if (creditCardVal.length > 0) {
	// 					creditCardVal = creditCardVal.match( new RegExp('.{1,4}', 'g') ).join(' ');
	// 				}

	// 				elm.val( creditCardVal );

	// 				// get the type
	// 				if(result.card_type !== null) {

	// 					elm.attr('data-type-card', result.card_type.name);
	// 					// elm.removeClass('parsley-error');
	// 					// $('#parsley-id-' + elm.attr('data-parsley-id')).empty();

	// 				} else {
	// 					elm.attr('data-type-card', '');
	// 					// elm.addClass('parsley-error');
	// 					// $('#parsley-id-' + elm.attr('data-parsley-id')).html('<li>' + elm.attr('data-parsley-error-message') + '</li>');
	// 				}

	// 				scope.$apply();

	// 			});

	// 		}

	// 	}
	// })

	.directive('couponDiscount', function(FlightService){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				var timeout;

				$(elm).on('keyup', function(){

					clearTimeout(timeout);

					timeout = setTimeout(function(){
						var val = $(elm).val();

						scope.$apply(function(){
							scope.state = scope.states.validating;
						})

						FlightService.setCoupon(val).then(function(response){
							scope.state = scope.states.valid;
							scope.couponValue = response.couponValue;
							scope.currency = response.currency;
							scope.pricing = response.pricing;

						}, function(){
							scope.states.invalid;
						})

					}, 300)
				});
			}

		}
	})


	.directive('hoverZones', function($timeout){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				var zones = [];

				scope.$watch('zones', function(data){
					if(!data) return;

					$timeout(function(){
						offsets = $(elm).offset()
						zones = [];

						$(elm).find('.modal--select-your-seat--zone').each(function(){
							var elm = $(this);
							var offsets = elm.offset()

							zones.push({
								el: elm,
								top: offsets.top,
								left: offsets.left,
								width: elm.outerWidth(),
								height: elm.outerHeight()
							})
						})
					}, 10)

				}, true)


				$(elm).on('mousemove', function(e){

					window.requestAnimationFrame(function(){
						var left = e.clientX;
						var top = e.clientY + elm[0].scrollTop;

						_.each(zones, function(zone){
							if(
								left >= zone.left
								&& top >= zone.top
								&& left <= (zone.left + zone.width)
								&& top <= (zone.top + zone.height)
							){
								zone.el.addClass('is-hovered');
							} else {
								zone.el.removeClass('is-hovered');
							}
						})
					})

				})
			}

		}
	})

	.directive('airplane', function($timeout){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				var start_top = 100
				var row_height = 40

				scope.$watch('currentFlightIndex', function(data){
					scope.overwingRows = []
				}, true)

				scope.$watch('seatDetails', function(data){
					if(!data) return;

					scope.rows = data.row;

					$timeout(function(){
						scope.overwingRows = []

						$(elm).find('.modal--select-your-seat--airplane__middle .overwing').each(function(){
							elem = $(this)
							scope.overwingRows.push(elem.data('row-number'))
						})
					}, 10)
				}, true)

				scope.$watch('overwingRows', function(data){
					if(!data || !data.length)
						var no_rows = true

					$timeout(function(){
						var total_rows = (!no_rows) ? data.length : 0
						var first_row = data[0]
						var last_row = data[total_rows - 1]

						if(total_rows){
							for (var i = 0; i < scope.rows.length; i++) {
								if (scope.rows[i]['number'] == first_row) {
									var pos = i + 1;
								}
							}
						}

						var wing_height = row_height * total_rows;
						var wing_top = start_top * pos;

						$(elm).find('.left_wing, .right_wing').css({
							height: wing_height,
							top: wing_top
						})
					}, 10)
				}, true)
			}

		}
	})

	.directive('phonenumber', function($timeout){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				$(elm).intlTelInput({
					defaultCountry: "ch",
					preferredCountries: [ "ch", "us", "gb" ],
				})

				$(elm).change(function(){
					scope.p.personalDetails.phoneNumber = $(this).val()
				})
			}

		}
	})

	.directive('countrycode', function($timeout){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				$(elm).change(function(){
					scope.p.personalDetails.phoneCode = parseInt($(this).val())
				})
			}

		}
	})
