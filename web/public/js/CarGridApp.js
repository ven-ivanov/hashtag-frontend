//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('CarGridApp', ['CarGrid.Controllers', 'CarGrid.Services', 'CarGrid.Directives'])
	.config(function($interpolateProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})
	.run(function($rootScope, CarService){
		$rootScope.modal = false;

		$rootScope.closeModal = function(e){
			e.preventDefault();
			$rootScope.modal = false;
		}
	});

//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('CarGrid.Controllers', [])

	.controller('results', function($scope, CarService, $q, $rootScope){
		CarService.getResults().then(function (data) {
			$scope.results = data;
			$scope.reverse = false;
		});
	})

//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('CarGrid.Services', [])

	.service('CarService', function($http, $q){

		var getResultsPromise;
		this.getResults = function(search){
			if(getResultsPromise) return getResultsPromise;

			var defer = $q.defer();

			$http.get(App.uploadsDir + '/car-grid.json').then(function(response){
				var grouped = _.chain(response.data).groupBy(function(item){
					return item.category.parent;
				}).map(function(group){
					return group;
				}).value()

				defer.resolve(grouped);
			})

			return getResultsPromise = defer.promise;
		}
	});

//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('CarGrid.Directives', [])
	.directive('carsScroll', function() {
		return function(scope, element, attrs) {

			var initialized = false;
			scope.$watch('results', function(data, previous){
				if(!data || initialized) return;
				initialized = true;
				setTimeout(function(){
					App.Modules['mosaic'].run();
				})

			})

		};
	})

	.directive('moreDetails', function($templateCache, $compile, $interpolate) {
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				$(elm).click(function(){
					var container = $(this).parentsUntil( ".main" )

					var template = $templateCache.get("car-detail.html")
					template = angular.element($interpolate(template)(scope))
					template = template.addClass('is-active')
					
					container.append(template)

					template = $compile(template)(scope)
					scope.$apply();

					App.Modules['carrousel'].run()
				})
			}
		}
	})

	.directive('slide', function($templateCache, $compile, $interpolate) {
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				scope.image.baseurl = attrs.url
				template = "<div class='carrousel-item-image' style='background-image: url(" + scope.image.baseurl + "/" + scope.image.name + ");'></div>"
				elm.append(template)
			}
		}
	})
