//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('CarrouselApp', ['Carrousel.Directives'])
	.config(function($interpolateProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})


//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('Carrousel.Directives', [])
	.directive('carrousel', function($http){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				var initialized = false;

				$http.get(App.uploadsDir + '/carrousel_'+attrs['carrousel']+'.json').then(function(response){
					if(!response.data || initialized) return;
					scope.results = response.data;
					initialized = true;
					setTimeout(function(){
						_.each(scope.results, function(result, index){
							$.simpleWeather({
								location: result.city,
								unit: 'c',
								success: function(weather) {
									html = weather.temp+'&deg;'+weather.units.temp;
									$("#weather" + index).html(html);
								},
								error: function(error) {
									$("#weather" + index).html('<p>'+error+'</p>');
								}
							});
						})

						App.Modules['carrousel'].run();
						$('.carrousel').addClass('is-active');
					})
				})

			}
		}
	})