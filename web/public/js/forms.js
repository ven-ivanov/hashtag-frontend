App.forms = (function(){

	"use strict";

	function run(){

		$(':input').inputmask();
		$('.datepicker-container').calendar();

		/**
		 * customCheckbox
		 */
		var $customCheckbox = $('.customCheckbox');

		if( $customCheckbox.length ){

			$customCheckbox.on('change', 'input', function(e) {
				var $el = $(this);
				var $container = $(e.delegateTarget);
				var toggle = eval('(' + $container.data('toggle') + ')');

				if( $el.is(':checked') ){
					$container.addClass('active');
				} else {
					$container.removeClass('active');
				}

				for(var k in toggle) {
					$(toggle[k]).toggleClass(k);
				}

			});
		}

		/**
		* customRadio
		*/
		// var $customRadio = $('.customRadio');

		// if( $customRadio.length ){
		// 	$customRadio.on('change', 'input', function(e) {
		// 		var $el = $(this);
		// 		var $container = $(e.delegateTarget);
		// 		var toggle = eval('(' + $container.data('toggle') + ')');

		// 		if( $el.is(':checked') ){
		// 			$container.addClass('active');
		// 		} else {
		// 			$container.removeClass('active');
		// 		}

		// 		for(var k in toggle) {
		// 			$(toggle[k]).toggleClass(k);
		// 		}

		// 	});
		// }

		/**
		 * customSelect
		 */
		// $('.customSelect--multiple select').multipleSelect({
		// 	placeholder: $('.customSelect--multiple select').attr('data-placeholder'),
		// 	onClick: function($el, view){

		// 		$('.customSelect--multiple select option[value="'+ view.value +'"]').attr('selected', view.checked)
		// 		$('.customSelect--multiple select').trigger('change');

		// 		if(view.checked) {
		// 			$el.addClass('active');
		// 		} else {
		// 			$el.removeClass('active');
		// 		}
		// 	},
		// 	onCheckAll: function($el) {
		// 		$el.addClass('active');
		// 	},
		// 	onUncheckAll: function($el) {
		// 		$el.removeClass('active');
		// 	}
		// });


		setAmountTrigger()


		$('select[value]').val(function(){
			return $(this).attr('value');
		})

	}

	function setAmountTrigger(){
		$('.amountpicker').off('click')
		$('.amountpicker').on('click', 'a', function(e){
			e.preventDefault();
			e.stopImmediatePropagation();

			var input = $(e.delegateTarget).find('.amountpicker-qtd'),
				min = input.attr('min') || 0,
				max = input.attr('max') || 9999999,
				val = parseInt(input.val() || 0, 10),
				amount = ($(this).text() == "+") ? +1 : -1,
				newVal = val + amount;

			newVal = Math.min(Math.max(newVal, min), max);

			if( $(e.delegateTarget).hasClass('amountpicker--time') && newVal < 10){
				newVal = '0' + newVal;
			}

			input.val(newVal);

			return false;
		})
	}

	return {
		run: run,
		setAmountTrigger: setAmountTrigger,
	}

}())
