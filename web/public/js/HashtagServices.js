//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('HashtagServices', [])

	.service('Router', function(){

		var optionalParam = /\((.*?)\)/g;
		var namedParam = /(\(\?)?:\w+/g;
		var splatParam = /\*\w+/g;
		var escapeRegExp = /[\-{}\[\]+?.,\\\^$|#\s]/g;


		var _routeToRegExp = function(route) {

			route = route.replace(escapeRegExp, '\\$&')
					.replace(optionalParam, '(?:$1)?')
					.replace(namedParam, function(match, optional) {
						return optional ? match : '([^/?]+)';
					})
					.replace(splatParam, '([^?]*?)');

			return new RegExp('^' + route + '(?:\\?([\\s\\S]*))?$');
		}


		var _extractParameters = function(route, fragment) {
			var params = route.exec(fragment);

			if(params)
				params = params.slice(1);
			else
				params = null;

			return _.map(params, function(param, i) {
				if (i === params.length - 1) return param || null;
				return param ? decodeURIComponent(param) : null;
			});
		}


		this.run = function (path, routes){

			if(!path || !routes) return;

			_.each(routes, function(callback, key){
				var route = _routeToRegExp(key);

				if(route.test(path) && typeof callback == 'function') {
					var args = _extractParameters(route, path);
					callback.apply(this, args);
				}
			})


		}

	})