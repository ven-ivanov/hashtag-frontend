//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('CarService', [])

	.service('CarService', function($http, $q, TEST, localStorageService){

		var getResultsPromise;
		this.getResults = function(search){
			if(getResultsPromise) return getResultsPromise;

			var defer = $q.defer();

			search = search.replace(/(&?\w+=((?=$)|(?=&)))/g,'');
			search += '&language=' + App.selectedLanguage;
			search += '&currency=' + App.selectedCurrency;
			search += '&bookerCountry=' + localStorage['userCountryCode'];

			if(search.indexOf('differentplace=on') < 0)
				search += '&returnCity=' + this.getParameterByName('pickUpCity');

			localStorageService.set('carSearchQuery', search)

			$http.get((TEST) ? App.uploadsDir + '/car.json' : App.baseAPIUrl + '/car/cars_availability' + search, {withCredentials: true})
				.then(function(response){
					defer.resolve(response.data.result)
				})

			return getResultsPromise = defer.promise;
		}

		this.getParameterByName = function(name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
				results = regex.exec(location.search);
			return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		}

		var getDetailPromise;
		this.getDetail = function(){
			if(getDetailPromise) return getDetailPromise;

			var defer = $q.defer();
			var passengers = localStorageService.get('selectedCar');

			defer.resolve(passengers);

			return getDetailPromise = defer.promise;
		}

		var createOrderPromise;
		this.createOrder = function(){
			if(createOrderPromise) return createOrderPromise;

			var defer = $q.defer();

			var selectedCar = localStorageService.get('selectedCar');

			var data = {
				"Language":App.selectedLanguage,
				"Currency":selectedCar.currency,
				"RequestId":selectedCar.id,
				"RateId":selectedCar.rateId,
				"PickUpDateTime":selectedCar.pickUpDateTime,
				"PickUpOfficeId":selectedCar.pickUpOfficeId,
				"ReturnDateTime":selectedCar.returnDateTime,
				"ReturnOfficeId":selectedCar.returnOfficeId
			}

			$http.post(App.baseAPIUrl + '/car/order', data)
				.then(function(response){
					defer.resolve(response.data);
				})

			return createOrderPromise = defer.promise;
		}

		var getOptionalsPromise;
		this.getOptionals = function(id){
			if(getOptionalsPromise) return getOptionalsPromise;

			var defer = $q.defer();

			console.log('id', id)

			$http.get(App.baseAPIUrl + '/car/order/' + id + '/available_options', {withCredentials: true})
				.then(function(response){
					defer.resolve(response)
				})

			return getOptionalsPromise = defer.promise;
		}
	});