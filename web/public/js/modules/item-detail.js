App.Modules['item-detail'] = (function(){

	'use strict';

	function run() {

		$('.item-detail__info').on('click', '.item-detail__more-detail, .item-detail__less-detail', function(e){
			e.preventDefault();
			$(e.delegateTarget).toggleClass('item-detail__info--detail')
		});

		$('.item-detail__header').on('click', '.item-detail__btn-photos', function(e) {
			e.preventDefault();
			$(this).toggleClass('item-detail__btn-photos--active');
			$(e.delegateTarget).find('.item-detail__thumbs').slideToggle();
		});

		$('.item-detail__thumbs').on('click', 'a', function(e) {
			e.preventDefault();

			var $el = $(this);

			$el.addClass('item-detail__thumb--loading');
			thumbLightbox('render', $el.data('src'), function(){
				$el.removeClass('item-detail__thumb--loading');
			});

		});


		var $thumbLightbox = $('.item-detail__thumb-lightbox');

		$thumbLightbox.on('click', function(){
			thumbLightbox();
		});

		function thumbLightbox(action, src, callback) {

			if(action == 'render') {

				var obj = new Image();

				obj.onload = function(){
					var w = $(window),
						wWidth = w.width(),
						wHeight = w.height(),
						newHeight = parseInt((wWidth * obj.height) / obj.width);

					$thumbLightbox.append('<img src="'+ src +'" width="100%" />');
					$thumbLightbox.fadeToggle(400);

					if(typeof callback === 'function') {
						callback();
					}
				}
				obj.src = src;

			} else {
				$thumbLightbox.fadeToggle(400).find('img').remove();
			}
		}

	}

	return {
		run : run
	}

}());