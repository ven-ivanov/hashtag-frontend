App.Modules['search-tool'] = (function(){

	'use strict';

	function run(){
		$('.search-tool__tabs-links').on('click', 'a', function(e) {
			e.preventDefault();

			var	tab = $(this).data('tab'),
				activeTab = $('.search-tool__tab.active'),
				activeLink = $('.search-tool__tabs-links a.active');

			activeTab.removeClass('active');
			activeLink.removeClass('active');

			$('.search-tool__tab.tab-' + tab).addClass('active');
			$('.search-tool__tabs-links a[data-tab='+tab+']').addClass('active');


			$('.datepicker-container').calendar('close');

		})


		$('.label-landing').on('keydown', 'input', function() {
			owl.destroy();
			$('.carrousel').remove();
			$('.mainContent-map').removeClass('hidden')
			$('.main-footer__menu-item a').eq(0).addClass('active')
		})



		$('.search-tool__form-flight-hotel').on('click', '.btn--search', function(e) {
			var $form = $(e.delegateTarget),
				isFlightHotel = $(e.target).hasClass('btn-search-flight-hotel');

			if(isFlightHotel) {
				$form.attr('action', $form.attr('action-flight-hotel'));
			} else {
				$form.attr('action', $form.attr('action-flight'));
			}
		});



		$('.search-tool-fieldset-multicity').on('click', '.add-flight', function(e){
			e.preventDefault();
			var newDestination = $('.multicity-fieldset').last().clone();
			newDestination.insertAfter($('.multicity-fieldset').last());

			var newInputs = newDestination.find('.input-autocomplete--flight');
				newInputs.eq(0).val(newInputs.eq(1).val());
				newInputs.eq(1).val('');

			App.Modules['autocomplete_flight'].run();
			newInputs.eq(1).focus();

			$('.multicity-fieldset').last().find('.datepicker-container').calendar();
			fixRemoveFlight()
		});

		$('.search-tool-fieldset-multicity').on('click', '.remove-flight', function(e){
			e.preventDefault();
			$('.multicity-fieldset').last().remove();
			App.multicityDates.pop();
			fixRemoveFlight()
		});

		$('.advanced_options a').on('click', function(e){
			e.preventDefault();
			var element = $(e.currentTarget).parent()
			var container = element.find('.checkboxes')
			var span = $(e.currentTarget).find('span')

			if(container.css('display') == 'none'){
				span.text('-')
				container.show()
			}
			else{
				span.text('+')
				container.hide()
			}
		});

		var numberOfRooms = 1;

		// $('#search_hotels button').click(function(e){
		// 	e.preventDefault();      

		// 	var values = {};
		// 	var form = $('#search_hotels');
		// 	var url = form.attr('action');	
		// 	var inputs = form.find('input');

		// 	inputs.each(function() {
		// 		values[this.name] = this.value;
		// 	});

		// 	// do something
		// 	console.log(values)
		// 	console.log('submited')
		// });

		$('#search_hotels').find('input[name=formatted]').change(function(){
			var form = $('#search_hotels').parsley('validate');

			if($(this).val().length > 0 && $('#search_hotels').find('#nationality').val().length > 0)
				form.validationResult = true
		})

		$('#search_hotels').find('#nationality').change(function(){
			var form = $('#search_hotels').parsley('validate');

			if($(this).val().length > 0 && $('#search_hotels').find('input[name=formatted]').val().length > 0)
				form.validationResult = true
		})

		$('#search_hotels').submit(function (e) {
			e.preventDefault()

			var isValid = $(this).parsley('validate')
			isValid = isValid.validationResult

			var values = {};
			var numberOfAdults = ''
			var childAges = ''

			var form = $('#search_hotels');
			var locality = form.find('input[name=locality]').val()
			var formatted = form.find('input[name=formatted]').val()
			var arrivalDate = form.find('input[name=arrivalDate]').val()
			var departureDate = form.find('input[name=departureDate]').val()
			var nationality = form.find('#nationality').val()
			var url = form.attr('action');	
			var rooms = form.find('.room-fieldset');

			rooms.each(function(index, room) {
				var room = $(room)
				var input_rooms = room.find('input[name=rooms]')
				var input_adults = room.find('input[name=adults]')
				var input_childs = room.find('input[name=childs]')

				numberOfAdults += input_adults.val() + ','

				for(var i = 0; i < input_childs.val(); i++){
					childAges += input_rooms.val() + '_' + 7 + ','
				}
			});

			values.numberOfRooms = rooms.length
			values.locality = locality
			values.formatted = formatted
			values.arrivalDate = arrivalDate
			values.departureDate = departureDate
			values.nationality = nationality
			
			if(numberOfAdults.length > 0)
				values.numberOfAdults = numberOfAdults.slice(0, -1)
			
			if(childAges.length > 0)
				values.childAges = childAges.slice(0, -1)

			var result = decodeURIComponent($.param(values));

			if(isValid)
				window.location = url + '?' + result
		});

		$('.search-tool-fieldset--sleep').on('click', '.add-room', function(e){
			e.preventDefault();

			if(numberOfRooms >= 10) return false;

			numberOfRooms = $('#numberOfRooms').val(parseInt($('#numberOfRooms').val()) + 1).val();
			
			var newRoom = $('.room-fieldset').last().clone();
			newRoom.attr('id', 'room-fieldset' + numberOfRooms)
			newRoom.find('input[name=rooms]').val(numberOfRooms)
			newRoom.find('input[name=adults]').val(1)
			newRoom.find('input[name=childs]').val(0)
			newRoom.insertAfter($('.room-fieldset').last());

			var newInputs = newRoom.find('.input-autocomplete--flight');
				newInputs.eq(0).val(newInputs.eq(1).val());
				newInputs.eq(1).val('');

			newInputs.eq(1).focus();
			App.forms.setAmountTrigger()
			fixRemoveRoom()
		});

		$('.search-tool-fieldset--sleep').on('click', '.remove-room', function(e){
			e.preventDefault();
			$('.room-fieldset').last().remove();
			numberOfRooms = $('#numberOfRooms').val(parseInt($('#numberOfRooms').val()) - 1);
			fixRemoveRoom()
		});

		function fixRemoveFlight(){
			if($('.multicity-fieldset').length > 2) {
				$('.remove-flight').removeClass('hidden')
			} else {
				$('.remove-flight').addClass('hidden')
			}
		}

		function fixRemoveRoom(){
			if($('.room-fieldset').length > 1) {
				$('.remove-room').removeClass('hidden')
			} else {
				$('.remove-room').addClass('hidden')
			}

			if($('.room-fieldset').length >= 10) {
				$('.add-room').addClass('hidden')
			} else {
				$('.add-room').removeClass('hidden')				
			}
		}
	}

	return {
		run: run
	}

})();