App.Modules['table-history'] = (function(){

	'use strict';

	function run(){

		$('.table-history__item').on('click', '.table-history__btn', function(e) {
			if(!device.phone){
				e.preventDefault();
				e.stopPropagation();
				$(this).toggleClass('table-history__btn--active');
				$(e.delegateTarget).toggleClass('table-history__item--details')
			}

		});

		$('.table-history__item').on('click', function(e){
			e.preventDefault();

			if(device.phone){
				window.location = $(this).find('.table-history__btn').attr('href')
			}
		})

	}

	return {
		run : run
	}

}());