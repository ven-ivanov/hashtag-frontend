App.Modules['internal-tabs'] = (function(){

	'use strict';

	function run(){

		$('.internal-tabs').on('click', '.internal-tabs__tab-link', function(e) {
			e.stopImmediatePropagation();
			e.preventDefault();

			$('.internal-tabs__tab-link--active').removeClass('internal-tabs__tab-link--active')
			$('.internal-tabs__tab--active').removeClass('internal-tabs__tab--active')

			$(this).addClass('internal-tabs__tab-link--active')
			$('.internal-tabs__tab').eq($(this).index()).addClass('internal-tabs__tab--active')

		});

	}

	return {
		run : run
	}

}());