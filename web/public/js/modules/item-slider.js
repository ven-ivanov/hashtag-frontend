App.Modules['item-slider'] = (function(){

	"use strict";

	function run() {

		var owl = $('.item-slider__wrapper').owlCarousel({
			itemsMobile: false,
			singleItem: true,
			pagination: true,
			navigation: true,
			autoPlay: true,
			stopOnHover: true,
			addClassActive: true,
			rewindNav: false,
			slideSpeed: 500,
			paginationSpeed: 500,

			afterMove: function(){
				if(owl.currentItem === owl.itemsAmount -1){
					setTimeout(function(){
						owl.jumpTo(1)
					}, owl.options.slideSpeed)
				}

				if(owl.currentItem === 0) {
					setTimeout(function(){
						owl.jumpTo(owl.itemsAmount-2);
					}, owl.options.slideSpeed)
				}
			}
		}).data('owlCarousel');

		if(owl){
			owl.addItem(owl.$owlItems.last().html(), 0);
			owl.addItem(owl.$owlItems.eq(1).html(), owl.$owlItems.length);
			owl.paginationWrapper.find(".owl-page").first().remove();
			owl.paginationWrapper.find(".owl-page").last().remove();
			owl.jumpTo(1);
		}

	}

	return {
		run : run
	}

}());