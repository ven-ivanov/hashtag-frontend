;(function(){

	'use strict';

	var $mobileMenu = $('#mobile-menu');

	function widthIsPhone() {
		return $(window).width() < 600 ? true : false
	}

	function goTop() {
		$('html, body').css('scrollTop', 0);
	}


	if (widthIsPhone()) {
		$('#main-home').addClass('main-home__open');
	}

	// $(document.body).on('click', function(e){
	// 	$('#corporateMenu').removeClass('open');
	// })

	// $('#mobile-menu').on('click', function (event) {
	// 	if ($(this).hasClass('open'))
	// 		$('#mobile-menu').toggleClass('open');
	// });

	$('#corporateMenu').on('click', function (event) {
		if ($(event.target).hasClass('open'))
				$('.corporateMenu-close').click();
	});

	$('.corporateMenu-close').on('click', function(e){
		$('#corporateMenu').removeClass('open');
		return false;
	});


	$('.main-header__menu').on('click', '.js-open-corporate-menu, .main-header__my-account', function(e) {

		var $el = $(e.currentTarget),
			$box = $(e.delegateTarget);

		if( $el.hasClass('js-open-corporate-menu') ) {

			if (widthIsPhone()) {
				$mobileMenu.toggleClass('mobile-menu--open-left').removeClass('mobile-menu--open-right');
			} else {
				$('#corporateMenu').toggleClass('open');
			}

		} else if (widthIsPhone()) {
			if ($(this).hasClass('main-header__my-account--signin'))
				return
			else
				$mobileMenu.toggleClass('mobile-menu--open-right').removeClass('mobile-menu--open-left');
		}


		$box.find('.main-header__menu-item--active').not( $el ).removeClass('main-header__menu-item--active');
		$el.toggleClass('main-header__menu-item--active');

		goTop();

	});

	$('.mobile-menu__left-item-fly, .mobile-navigation__menu-item-fly').on('click', function (e) {
		e.preventDefault();

		var corp_menu = $('.js-open-corporate-menu');

		if($('#main-home').length < 1){
			window.location = './?fly';
			return;
		}

		$('.mobile-menu__left-item-fly').addClass('mobile-menu__left-item--active');
		if (corp_menu.hasClass('main-header__menu-item--active'))
			corp_menu.click();

		$('#main-home').removeClass('main-home__open');

		return false;
	});


	$('.mobile-menu__right').on('click', '.btnCloseRightMenu', function(e) {
		$mobileMenu.removeClass('mobile-menu--open-right');
		$('.main-header__my-account').removeClass('main-header__menu-item--active');
	});

	$('.open-contact, .close-contact').on('click', function (e) {
		e.preventDefault();
		$('.contact-page').toggleClass('is-active');
	})

	$('.href_select').on('change', function(){
		window.location.href = $(this).val();
	})

})();
