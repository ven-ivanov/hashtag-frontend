App.Modules['autocomplete_airlines'] = (function(){

	"use strict";

	var KEY_CODES = {
		'ENTER': 13,
		'UP': 38,
		'DOWN': 40,
		'ESC': 27,
		'BACKSPACE': 8,
		'DELETE': 46
	}


	function run(){
		var $container = $('<ul />').addClass('input-autocomplete--results');
		var templateItem = _.template('<li class="<%=(active) ? "input-autocomplete--results--active" : ""%>"><%=name%> <strong>(<%=airline_code%>)</strong></li>');
		var $inputs = $('.input-autocomplete--airline');
		var $input;
		var $tool;
		var data = {};


		$inputs.on('focus', function(){
			$input = $(this);
			$tool = $input.closest('.search-tool');
			$container.appendTo($input.parent());
		})

		$inputs.on('blur', function(){
			var next;
			var className;
			var multicityIndex = $input.closest('.search-tool-fieldset-multicity').find('.multicity-fieldset').index($input.closest('.multicity-fieldset'));

			if(data.response){
				if(data.activeIndex == undefined)
					data.activeIndex = 0;

				$input.val(data.response[data.activeIndex].name)
				$input.prev().val(data.response[data.activeIndex].airline_code)

				next = $input.closest('.search-tool-fieldset').find('.input-autocomplete--airline').filter(function(index, elm){
					return $(elm).val().length <= 0
				})

				if($input.closest('.input-icon').hasClass('input-icon--takeoff')) {
					className = '.input-icon--takeoff';
				} else {
					className = '.input-icon--landing';
				}

			}

			data = {}
			$container = $container.empty().detach();
			$tool.removeClass('search-tool--searching');
			$input.removeClass('input-autocomplete--thinking')
			if(request) request.abort();
			if(next) next.eq(0).focus();

		})


		$inputs.on('keydown', function(e){

			switch (e.keyCode) {
				case KEY_CODES.UP : {

					if(data.activeIndex === undefined){
						data.activeIndex = 0;
					} else {
						data.activeIndex = Math.max(0, data.activeIndex - 1);
					}
					break;
				}

				case KEY_CODES.DOWN : {
					if(data.activeIndex === undefined){
						data.activeIndex = 0;
					} else {
						data.activeIndex = Math.min(data.response.length-1, data.activeIndex + 1);
					}

					break;
				}

				case KEY_CODES.ESC : {
					data.activeIndex = undefined;
					$(e.target).trigger('blur');

					break;
				}

				case KEY_CODES.ENTER : {
					e.preventDefault();
					$(e.target).trigger('blur');

					break;
				}

				case KEY_CODES.BACKSPACE || KEY_CODES.DELETE : {
					fetch();

					break;
				}
			}

			render();
		})

		$inputs.on('keypress', function(e){
			fetch();
		})


		$container.on('mouseenter', 'li', function(e){
			var previousIndex = data.activeIndex;
			data.activeIndex = $(this).index();

			if(previousIndex !== data.activeIndex) render();
		})

		$container.on('mouseleave', 'li', function(e){
			data.activeIndex = undefined;
			render();
		})


		var timeout;
		var request;

		function fetch(){

			clearTimeout(timeout);

			timeout = setTimeout(function(){
				var val = $input.val().trim();
				val = normalize(val);

				if(val.length > 2) {
					// $input.addClass('input-autocomplete--thinking')

					// request = $.getJSON(App.baseUrl + '/api/autocomplete?loc=' + encodeURIComponent($input.val()), function(response){
					// 	if(response.length) {
					// 		data.response = response;
					// 		render();
					// 	}

					// 	$input.removeClass('input-autocomplete--thinking')
					// })

					//jQueryUI Autocomplete
					var term = val.toUpperCase();
					var results = $.grep(airlines, function(value) {
						return (value.airline_code == term) || (value.name.toUpperCase().indexOf(term) >= 0);
					});
					if (results.length > 1) {
						var distances = [];
						var top25 = [];
						for (var i = 0; i < results.length; i++) {
							var dValue = levenshtein(term, results[i].airline_code);
							var dLabel = levenshtein(term, results[i].name);
							if($.inArray( dValue + '-' + dLabel, distances ) == -1){
								distances.push(dValue + '-' + dLabel);
								top25[dValue + '-' + dLabel] = results[i];
							}
						}
						results = [];
						distances = distances.sort();
						for (var i = 0; i < (distances.length < 25 - 1 ? distances.length : 25); i++) {
							results.push(top25[distances[i]]);
						}
					}
					/////

					data.response = results;

					render();

				}

			}, 100)

		}

		//jQueryUI Autocomplete
		function levenshtein (a, b) {
			var i;
			var j;
			var cost;
			var d = new Array();
			if (a.length == 0) {
				return b.length;
			}
			if (b.length == 0) {
				return a.length;
			}
			for (i = 0; i <= a.length; i++) {
				d[i] = new Array();
				d[i][0] = i;
			}
			for (j = 0; j <= b.length; j++) {
				d[0][j] = j;
			}
			for (i = 1; i <= a.length; i++) {
				for (j = 1; j <= b.length; j++) {
					if (a.charAt(i - 1) == b.charAt(j - 1)) {
						cost = 0;
					} else {
						cost = 1;
					}
					d[i][j] = Math.min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost);
					if (i > 1 && j > 1 && a.charAt(i - 1) == b.charAt(j - 2) && a.charAt(i - 2) == b.charAt(j - 1)) {
						d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);
					}
				}
			}
			return d[a.length][b.length];
		}
		///////

		function render(){
			if(!data.response) return;

			if(data.response.length) {
				$tool.addClass('search-tool--searching');
			} else {
				$tool.removeClass('search-tool--searching');
			}

			var html = ''
			
			_.each(data.response, function(result, index){
				result.name = result.name.replace(/ \(([A-Z]{3})\)/, "");
				html += templateItem(_.extend({}, {active: (index === data.activeIndex)}, result));
			})
			$container.html(html)
		}

	}


	var airlines = [
	  {
		"airline_code": "1U",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Polyot Sirena",
		"l": "0"
	  },
	  {
		"airline_code": "2D",
		"airline_3lc": "AOG",
		"iata_nr": "561",
		"name": "Southeast Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "2E",
		"airline_3lc": "EIX",
		"iata_nr": "",
		"name": "Smokey Bay Air",
		"l": "0"
	  },
	  {
		"airline_code": "2F",
		"airline_3lc": "FTA",
		"iata_nr": "517",
		"name": "Frontier Flying Service",
		"l": "0"
	  },
	  {
		"airline_code": "2Q",
		"airline_3lc": "",
		"iata_nr": "227",
		"name": "Letaba Airways",
		"l": "0"
	  },
	  {
		"airline_code": "2S",
		"airline_3lc": "",
		"iata_nr": "579",
		"name": "Island Express",
		"l": "0"
	  },
	  {
		"airline_code": "2T",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Canada 3000",
		"l": "0"
	  },
	  {
		"airline_code": "2W",
		"airline_3lc": "WLC",
		"iata_nr": "227",
		"name": "Welcome Air",
		"l": "0"
	  },
	  {
		"airline_code": "2Z",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aerolitoral",
		"l": "0"
	  },
	  {
		"airline_code": "3C",
		"airline_3lc": "",
		"iata_nr": "310",
		"name": "Camai Air",
		"l": "0"
	  },
	  {
		"airline_code": "3D",
		"airline_3lc": "DNM",
		"iata_nr": "390",
		"name": "Denim Air",
		"l": "0"
	  },
	  {
		"airline_code": "3E",
		"airline_3lc": "OMQ",
		"iata_nr": "",
		"name": "AIR CHOICE ONE",
		"l": "0"
	  },
	  {
		"airline_code": "3F",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "African West Air",
		"l": "0"
	  },
	  {
		"airline_code": "3H",
		"airline_3lc": "AIE",
		"iata_nr": "466",
		"name": "Air Inuit",
		"l": "0"
	  },
	  {
		"airline_code": "3J",
		"airline_3lc": "WZP",
		"iata_nr": "",
		"name": "Air Alliance",
		"l": "0"
	  },
	  {
		"airline_code": "3K",
		"airline_3lc": "JSA",
		"iata_nr": "375",
		"name": "Jetstar Asia Airways",
		"l": "0"
	  },
	  {
		"airline_code": "3L",
		"airline_3lc": "",
		"iata_nr": "576",
		"name": "InterSky",
		"l": "0"
	  },
	  {
		"airline_code": "3M",
		"airline_3lc": "",
		"iata_nr": "449",
		"name": "SILVER AIRWAYS CORP",
		"l": "0"
	  },
	  {
		"airline_code": "3N",
		"airline_3lc": "URG",
		"iata_nr": "746",
		"name": "AirVantage Inc",
		"l": "0"
	  },
	  {
		"airline_code": "3P",
		"airline_3lc": "",
		"iata_nr": "253",
		"name": "TIARA AIR ARUBA",
		"l": "0"
	  },
	  {
		"airline_code": "3Q",
		"airline_3lc": "CYH",
		"iata_nr": "592",
		"name": "Yunnan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "3R",
		"airline_3lc": "GAI",
		"iata_nr": "520",
		"name": "Gromov Airline",
		"l": "0"
	  },
	  {
		"airline_code": "3S",
		"airline_3lc": "",
		"iata_nr": "875",
		"name": "AIR ANTILLES EXPRESS",
		"l": "0"
	  },
	  {
		"airline_code": "3T",
		"airline_3lc": "URN",
		"iata_nr": "359",
		"name": "Contact Air",
		"l": "0"
	  },
	  {
		"airline_code": "3U",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sichuan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "3V",
		"airline_3lc": "TAY",
		"iata_nr": "756",
		"name": "Waglisla Air",
		"l": "0"
	  },
	  {
		"airline_code": "3Z",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "TRAVEL SERVICE POLSKA",
		"l": "0"
	  },
	  {
		"airline_code": "4A",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aviana Airways",
		"l": "0"
	  },
	  {
		"airline_code": "4B",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Olson Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "4C",
		"airline_3lc": "ARE",
		"iata_nr": "035",
		"name": "Aires",
		"l": "0"
	  },
	  {
		"airline_code": "4D",
		"airline_3lc": "ASD",
		"iata_nr": "903",
		"name": "Air Sinai",
		"l": "0"
	  },
	  {
		"airline_code": "4E",
		"airline_3lc": "TNR",
		"iata_nr": "",
		"name": "Tanana Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "4G",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Shenzhen Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "4H",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Hannas Air Saltspring",
		"l": "0"
	  },
	  {
		"airline_code": "4J",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Transaero Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "4K",
		"airline_3lc": "KBA",
		"iata_nr": "",
		"name": "Kenn Borek Air Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "4L",
		"airline_3lc": "MJX",
		"iata_nr": "764",
		"name": "Euroline",
		"l": "0"
	  },
	  {
		"airline_code": "4M",
		"airline_3lc": "",
		"iata_nr": "469",
		"name": "LAN ARGENTINA",
		"l": "0"
	  },
	  {
		"airline_code": "4N",
		"airline_3lc": "",
		"iata_nr": "287",
		"name": "Air North",
		"l": "0"
	  },
	  {
		"airline_code": "4Q",
		"airline_3lc": "SFW",
		"iata_nr": "741",
		"name": "SAFI AIRWAYS",
		"l": "0"
	  },
	  {
		"airline_code": "4R",
		"airline_3lc": "HHI",
		"iata_nr": "",
		"name": "HI Hamburg International",
		"l": "0"
	  },
	  {
		"airline_code": "4S",
		"airline_3lc": "",
		"iata_nr": "377",
		"name": "East West Airlines (India)",
		"l": "0"
	  },
	  {
		"airline_code": "4T",
		"airline_3lc": "BHP",
		"iata_nr": "",
		"name": "Belair Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "4U",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Germanwings",
		"l": "0"
	  },
	  {
		"airline_code": "4V",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Voyageur Airways",
		"l": "0"
	  },
	  {
		"airline_code": "4W",
		"airline_3lc": "VNA",
		"iata_nr": "",
		"name": "Warbelows Air Ventures",
		"l": "0"
	  },
	  {
		"airline_code": "4Y",
		"airline_3lc": "YAA",
		"iata_nr": "476",
		"name": "Yute Air Alaska Inc.",
		"l": "0"
	  },
	  {
		"airline_code": "4Z",
		"airline_3lc": "",
		"iata_nr": "749",
		"name": "Airmidlands",
		"l": "0"
	  },
	  {
		"airline_code": "5A",
		"airline_3lc": "AIP",
		"iata_nr": "511",
		"name": "Alpine Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "5B",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Bellair Inc",
		"l": "0"
	  },
	  {
		"airline_code": "5C",
		"airline_3lc": "CAC",
		"iata_nr": "",
		"name": "Conquest Airlines Corp",
		"l": "0"
	  },
	  {
		"airline_code": "5D",
		"airline_3lc": "SLI",
		"iata_nr": "642",
		"name": "AEROLITORAL",
		"l": "0"
	  },
	  {
		"airline_code": "5E",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Siam Ga",
		"l": "0"
	  },
	  {
		"airline_code": "5F",
		"airline_3lc": "CIR",
		"iata_nr": "",
		"name": "Arctic Circle Air",
		"l": "0"
	  },
	  {
		"airline_code": "5H",
		"airline_3lc": "FFV",
		"iata_nr": "540",
		"name": "Five Fourty Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "5J",
		"airline_3lc": "CEB",
		"iata_nr": "203",
		"name": "Cebu Air",
		"l": "0"
	  },
	  {
		"airline_code": "5K",
		"airline_3lc": "ODS",
		"iata_nr": "654",
		"name": "Kenmore Air",
		"l": "0"
	  },
	  {
		"airline_code": "5L",
		"airline_3lc": "",
		"iata_nr": "275",
		"name": "AeroSur",
		"l": "0"
	  },
	  {
		"airline_code": "5N",
		"airline_3lc": "AUL",
		"iata_nr": "316",
		"name": "JSC NORDAVIA",
		"l": "0"
	  },
	  {
		"airline_code": "5P",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sky Europe Hungary",
		"l": "0"
	  },
	  {
		"airline_code": "5S",
		"airline_3lc": "PSV",
		"iata_nr": "",
		"name": "Airspeed Aviation Inc.",
		"l": "0"
	  },
	  {
		"airline_code": "5T",
		"airline_3lc": "",
		"iata_nr": "518",
		"name": "Aviacion del Noroeste",
		"l": "0"
	  },
	  {
		"airline_code": "5U",
		"airline_3lc": "LDE",
		"iata_nr": "022",
		"name": "Skagway Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "5W",
		"airline_3lc": "AEU",
		"iata_nr": "",
		"name": "Astraeus",
		"l": "1"
	  },
	  {
		"airline_code": "5Z",
		"airline_3lc": "",
		"iata_nr": "541",
		"name": "Bismilah Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "6A",
		"airline_3lc": "CHP",
		"iata_nr": "095",
		"name": "Aviacsa",
		"l": "0"
	  },
	  {
		"airline_code": "6B",
		"airline_3lc": "BLX",
		"iata_nr": "951",
		"name": "Britannia Airways",
		"l": "0"
	  },
	  {
		"airline_code": "6C",
		"airline_3lc": "CMY",
		"iata_nr": "",
		"name": "Cape Smythe Air",
		"l": "0"
	  },
	  {
		"airline_code": "6D",
		"airline_3lc": "PAS",
		"iata_nr": "019",
		"name": "Alaska Island Air",
		"l": "0"
	  },
	  {
		"airline_code": "6E",
		"airline_3lc": "",
		"iata_nr": "493",
		"name": "Malmo Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "6G",
		"airline_3lc": "AWW",
		"iata_nr": "800",
		"name": "Las Vegas Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "6H",
		"airline_3lc": "ISR",
		"iata_nr": "818",
		"name": "Israir Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "6J",
		"airline_3lc": "",
		"iata_nr": "602",
		"name": "Southeast European",
		"l": "0"
	  },
	  {
		"airline_code": "6L",
		"airline_3lc": "AKK",
		"iata_nr": "709",
		"name": "Aklak Air Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "6M",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Minas Linhas Aereas",
		"l": "0"
	  },
	  {
		"airline_code": "6Q",
		"airline_3lc": "SLL",
		"iata_nr": "921",
		"name": "Slovak Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "6S",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Ketchikan Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "6U",
		"airline_3lc": "UKR",
		"iata_nr": "891",
		"name": "Air Ukraine",
		"l": "0"
	  },
	  {
		"airline_code": "6V",
		"airline_3lc": "MRW",
		"iata_nr": "",
		"name": "Mars RK",
		"l": "0"
	  },
	  {
		"airline_code": "6W",
		"airline_3lc": "",
		"iata_nr": "026",
		"name": "Saratov Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "6Y",
		"airline_3lc": "LTC",
		"iata_nr": "",
		"name": "Nicaraguense de Aviacion (NICA)",
		"l": "0"
	  },
	  {
		"airline_code": "7A",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Haines Airways",
		"l": "0"
	  },
	  {
		"airline_code": "7B",
		"airline_3lc": "KJC",
		"iata_nr": "499",
		"name": "Kras Air",
		"l": "0"
	  },
	  {
		"airline_code": "7C",
		"airline_3lc": "COY",
		"iata_nr": "",
		"name": "Columbia Pacific Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "7D",
		"airline_3lc": "UDC",
		"iata_nr": "897",
		"name": "Donbassaero Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "7E",
		"airline_3lc": "AWU",
		"iata_nr": "",
		"name": "Sylt Air",
		"l": "0"
	  },
	  {
		"airline_code": "7F",
		"airline_3lc": "FAB",
		"iata_nr": "245",
		"name": "First Air",
		"l": "0"
	  },
	  {
		"airline_code": "7H",
		"airline_3lc": "ERH",
		"iata_nr": "808",
		"name": "ERA Aviation Inc",
		"l": "0"
	  },
	  {
		"airline_code": "7J",
		"airline_3lc": "",
		"iata_nr": "502",
		"name": "Tajik Air",
		"l": "0"
	  },
	  {
		"airline_code": "7K",
		"airline_3lc": "KGL",
		"iata_nr": "",
		"name": "Kogalymavia",
		"l": "0"
	  },
	  {
		"airline_code": "7L",
		"airline_3lc": "CRN",
		"iata_nr": "164",
		"name": "Aerocaribbean",
		"l": "0"
	  },
	  {
		"airline_code": "7M",
		"airline_3lc": "TYM",
		"iata_nr": "691",
		"name": "Tyumen Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "7N",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Inland Aviation Services",
		"l": "0"
	  },
	  {
		"airline_code": "7P",
		"airline_3lc": "BTV",
		"iata_nr": "671",
		"name": "Batavia Air",
		"l": "0"
	  },
	  {
		"airline_code": "7Q",
		"airline_3lc": "TLR",
		"iata_nr": "033",
		"name": "Tebisti Air Libya",
		"l": "0"
	  },
	  {
		"airline_code": "7R",
		"airline_3lc": "RLU",
		"iata_nr": "362",
		"name": "Rusline",
		"l": "0"
	  },
	  {
		"airline_code": "7T",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Trans Cote Inc",
		"l": "0"
	  },
	  {
		"airline_code": "7V",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Pelican Air",
		"l": "0"
	  },
	  {
		"airline_code": "7W",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aviation Assistance",
		"l": "0"
	  },
	  {
		"airline_code": "7Y",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Mid Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "7Z",
		"airline_3lc": "LBH",
		"iata_nr": "569",
		"name": "Laker Airways  Bahamas)",
		"l": "0"
	  },
	  {
		"airline_code": "8A",
		"airline_3lc": "",
		"iata_nr": "380",
		"name": "Atlas Blue",
		"l": "0"
	  },
	  {
		"airline_code": "8B",
		"airline_3lc": "BCC",
		"iata_nr": "820",
		"name": "Bussiness Air",
		"l": "0"
	  },
	  {
		"airline_code": "8D",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Servant Air",
		"l": "0"
	  },
	  {
		"airline_code": "8E",
		"airline_3lc": "BRG",
		"iata_nr": "",
		"name": "Bering Air",
		"l": "0"
	  },
	  {
		"airline_code": "8F",
		"airline_3lc": "FFR",
		"iata_nr": "",
		"name": "Fischer Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "8G",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Girjet",
		"l": "0"
	  },
	  {
		"airline_code": "8H",
		"airline_3lc": "BGH",
		"iata_nr": "366",
		"name": "BH Air",
		"l": "0"
	  },
	  {
		"airline_code": "8K",
		"airline_3lc": "KOZ",
		"iata_nr": "650",
		"name": "Angel Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "8L",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Servicio Aereo Leo Lopez",
		"l": "0"
	  },
	  {
		"airline_code": "8N",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Barents Airlink",
		"l": "0"
	  },
	  {
		"airline_code": "8P",
		"airline_3lc": "PCO",
		"iata_nr": "905",
		"name": "Pacific Coastal Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "8R",
		"airline_3lc": "",
		"iata_nr": "300",
		"name": "Sol L\u00edneas A\u00e9reas",
		"l": "0"
	  },
	  {
		"airline_code": "8T",
		"airline_3lc": "",
		"iata_nr": "744",
		"name": "Air Tindi Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "8V",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Wright Air Services",
		"l": "0"
	  },
	  {
		"airline_code": "8Y",
		"airline_3lc": "PBU",
		"iata_nr": "",
		"name": "Air Burundi",
		"l": "0"
	  },
	  {
		"airline_code": "9A",
		"airline_3lc": "ATL",
		"iata_nr": "",
		"name": "Visa Airways",
		"l": "0"
	  },
	  {
		"airline_code": "9B",
		"airline_3lc": "",
		"iata_nr": "450",
		"name": "Accesrail",
		"l": "0"
	  },
	  {
		"airline_code": "9C",
		"airline_3lc": "",
		"iata_nr": "357",
		"name": "Wimbi Dira Airways",
		"l": "0"
	  },
	  {
		"airline_code": "9D",
		"airline_3lc": "",
		"iata_nr": "371",
		"name": "TOUMAI AIR TCHAD",
		"l": "0"
	  },
	  {
		"airline_code": "9E",
		"airline_3lc": "FLG",
		"iata_nr": "430",
		"name": "ENDEAVOR AIR",
		"l": "0"
	  },
	  {
		"airline_code": "9J",
		"airline_3lc": "",
		"iata_nr": "321",
		"name": "Pacific Island Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "9K",
		"airline_3lc": "KAP",
		"iata_nr": "306",
		"name": "Cape Air",
		"l": "0"
	  },
	  {
		"airline_code": "9L",
		"airline_3lc": "",
		"iata_nr": "426",
		"name": "Colgan Air",
		"l": "0"
	  },
	  {
		"airline_code": "9M",
		"airline_3lc": "GLR",
		"iata_nr": "634",
		"name": "Central Mountain Air",
		"l": "0"
	  },
	  {
		"airline_code": "9N",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Satena",
		"l": "0"
	  },
	  {
		"airline_code": "9P",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Panair S.P.A.",
		"l": "0"
	  },
	  {
		"airline_code": "9Q",
		"airline_3lc": "PBA",
		"iata_nr": "543",
		"name": "Pb Air",
		"l": "0"
	  },
	  {
		"airline_code": "9R",
		"airline_3lc": "VAP",
		"iata_nr": "346",
		"name": "Phuket Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "9S",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Swazi Express",
		"l": "0"
	  },
	  {
		"airline_code": "9T",
		"airline_3lc": "ABS",
		"iata_nr": "909",
		"name": "Transwest Air Limited",
		"l": "0"
	  },
	  {
		"airline_code": "9U",
		"airline_3lc": "MLD",
		"iata_nr": "572",
		"name": "Air Moldova",
		"l": "0"
	  },
	  {
		"airline_code": "9V",
		"airline_3lc": "ROI",
		"iata_nr": "742",
		"name": "Avior",
		"l": "0"
	  },
	  {
		"airline_code": "9W",
		"airline_3lc": "JAI",
		"iata_nr": "589",
		"name": "Jet Airways PVT",
		"l": "0"
	  },
	  {
		"airline_code": "9Y",
		"airline_3lc": "AMV",
		"iata_nr": "",
		"name": "AMC Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "AA",
		"airline_3lc": "AAL",
		"iata_nr": "001",
		"name": "American Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "AB",
		"airline_3lc": "BER",
		"iata_nr": "745",
		"name": "Air Berlin",
		"l": "1"
	  },
	  {
		"airline_code": "AC",
		"airline_3lc": "ACA",
		"iata_nr": "014",
		"name": "Air Canada",
		"l": "1"
	  },
	  {
		"airline_code": "AD",
		"airline_3lc": "",
		"iata_nr": "577",
		"name": "AZUL LINHAS AEREAS BRASILEIRAS",
		"l": "0"
	  },
	  {
		"airline_code": "AE",
		"airline_3lc": "MDA",
		"iata_nr": "803",
		"name": "Mandarin Airlines Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "AF",
		"airline_3lc": "AFR",
		"iata_nr": "057",
		"name": "Air France",
		"l": "1"
	  },
	  {
		"airline_code": "AG",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Provincial Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "AH",
		"airline_3lc": "DAH",
		"iata_nr": "124",
		"name": "Air Algerie",
		"l": "0"
	  },
	  {
		"airline_code": "AI",
		"airline_3lc": "AIC",
		"iata_nr": "",
		"name": "Air India",
		"l": "0"
	  },
	  {
		"airline_code": "AJ",
		"airline_3lc": "NIG",
		"iata_nr": "",
		"name": "Aerocontractors",
		"l": "0"
	  },
	  {
		"airline_code": "AK",
		"airline_3lc": "AXM",
		"iata_nr": "807",
		"name": "Airasia Sdn Bhd",
		"l": "0"
	  },
	  {
		"airline_code": "AL",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Midwest Connect",
		"l": "0"
	  },
	  {
		"airline_code": "AM",
		"airline_3lc": "AMX",
		"iata_nr": "139",
		"name": "Aeromexico",
		"l": "0"
	  },
	  {
		"airline_code": "AN",
		"airline_3lc": "RLA",
		"iata_nr": "",
		"name": "HOP AIRLINAIR",
		"l": "0"
	  },
	  {
		"airline_code": "AO",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Australian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "AQ",
		"airline_3lc": "AAH",
		"iata_nr": "327",
		"name": "Aloha Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "AR",
		"airline_3lc": "ARG",
		"iata_nr": "044",
		"name": "Aerolineas Argentinas",
		"l": "1"
	  },
	  {
		"airline_code": "AS",
		"airline_3lc": "ASA",
		"iata_nr": "027",
		"name": "Alaska Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "AT",
		"airline_3lc": "RAM",
		"iata_nr": "147",
		"name": "Royal Air Maroc",
		"l": "0"
	  },
	  {
		"airline_code": "AU",
		"airline_3lc": "AUT",
		"iata_nr": "143",
		"name": "Austral Lineas Aerea",
		"l": "0"
	  },
	  {
		"airline_code": "AV",
		"airline_3lc": "AVA",
		"iata_nr": "134",
		"name": "Avianca",
		"l": "1"
	  },
	  {
		"airline_code": "AW",
		"airline_3lc": "DIR",
		"iata_nr": "",
		"name": "P.T. Dirgantara Air Serv",
		"l": "0"
	  },
	  {
		"airline_code": "AY",
		"airline_3lc": "FIN",
		"iata_nr": "105",
		"name": "Finnair",
		"l": "1"
	  },
	  {
		"airline_code": "AZ",
		"airline_3lc": "AZA",
		"iata_nr": "055",
		"name": "Alitalia",
		"l": "1"
	  },
	  {
		"airline_code": "B2",
		"airline_3lc": "BRU",
		"iata_nr": "628",
		"name": "Belavia",
		"l": "1"
	  },
	  {
		"airline_code": "B4",
		"airline_3lc": "GSM",
		"iata_nr": "",
		"name": "Flyglobespan",
		"l": "0"
	  },
	  {
		"airline_code": "B7",
		"airline_3lc": "UIA",
		"iata_nr": "525",
		"name": "Uni Airways",
		"l": "0"
	  },
	  {
		"airline_code": "B8",
		"airline_3lc": "ERT",
		"iata_nr": "637",
		"name": "Eritrean Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "B9",
		"airline_3lc": "",
		"iata_nr": "491",
		"name": "Iran Air Tours",
		"l": "0"
	  },
	  {
		"airline_code": "BA",
		"airline_3lc": "BAW",
		"iata_nr": "125",
		"name": "British Airways",
		"l": "1"
	  },
	  {
		"airline_code": "BD",
		"airline_3lc": "BMA",
		"iata_nr": "236",
		"name": "British Midland",
		"l": "1"
	  },
	  {
		"airline_code": "BF",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Markair",
		"l": "0"
	  },
	  {
		"airline_code": "BG",
		"airline_3lc": "BBC",
		"iata_nr": "997",
		"name": "Biman Bangladesh",
		"l": "0"
	  },
	  {
		"airline_code": "BH",
		"airline_3lc": "",
		"iata_nr": "993",
		"name": "Hawkair",
		"l": "0"
	  },
	  {
		"airline_code": "BI",
		"airline_3lc": "RBA",
		"iata_nr": "672",
		"name": "Royal Brunei Air",
		"l": "1"
	  },
	  {
		"airline_code": "BJ",
		"airline_3lc": "LBT",
		"iata_nr": "796",
		"name": "Nouvelair",
		"l": "0"
	  },
	  {
		"airline_code": "BK",
		"airline_3lc": "PDI",
		"iata_nr": "522",
		"name": "Potomac Air",
		"l": "0"
	  },
	  {
		"airline_code": "BL",
		"airline_3lc": "PIC",
		"iata_nr": "550",
		"name": "JETSTAR PACIFIC AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "BM",
		"airline_3lc": "BMI",
		"iata_nr": "480",
		"name": "BMI REGIONAL",
		"l": "0"
	  },
	  {
		"airline_code": "BN",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Horizon Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "BO",
		"airline_3lc": "",
		"iata_nr": "666",
		"name": "Bouraq Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "BP",
		"airline_3lc": "BOT",
		"iata_nr": "636",
		"name": "Air Botswana",
		"l": "0"
	  },
	  {
		"airline_code": "BR",
		"airline_3lc": "EVA",
		"iata_nr": "695",
		"name": "EVA Airways Corporation",
		"l": "0"
	  },
	  {
		"airline_code": "BU",
		"airline_3lc": "CNO",
		"iata_nr": "154",
		"name": "SAS Norway",
		"l": "0"
	  },
	  {
		"airline_code": "BV",
		"airline_3lc": "BPA",
		"iata_nr": "004",
		"name": "Blue Panorama Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "BW",
		"airline_3lc": "BWA",
		"iata_nr": "106",
		"name": "Caribbean Airline",
		"l": "0"
	  },
	  {
		"airline_code": "BX",
		"airline_3lc": "CST",
		"iata_nr": "970",
		"name": "Coast Air K\/S",
		"l": "0"
	  },
	  {
		"airline_code": "C2",
		"airline_3lc": "ALU",
		"iata_nr": "",
		"name": "Air Luxor Stp",
		"l": "0"
	  },
	  {
		"airline_code": "C4",
		"airline_3lc": "IMX",
		"iata_nr": "",
		"name": "Zimex Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "C8",
		"airline_3lc": "CGO",
		"iata_nr": "488",
		"name": "Chicago Express Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "C9",
		"airline_3lc": "",
		"iata_nr": "251",
		"name": "Cirrus Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "CA",
		"airline_3lc": "CCA",
		"iata_nr": "999",
		"name": "Air China",
		"l": "1"
	  },
	  {
		"airline_code": "CB",
		"airline_3lc": "SAY",
		"iata_nr": "969",
		"name": "Scotairways",
		"l": "0"
	  },
	  {
		"airline_code": "CE",
		"airline_3lc": "NTW",
		"iata_nr": "567",
		"name": "Nationwide Air",
		"l": "0"
	  },
	  {
		"airline_code": "CF",
		"airline_3lc": "SDR",
		"iata_nr": "419",
		"name": "City Airline",
		"l": "0"
	  },
	  {
		"airline_code": "CG",
		"airline_3lc": "",
		"iata_nr": "626",
		"name": "Airlines Png",
		"l": "0"
	  },
	  {
		"airline_code": "CH",
		"airline_3lc": "BMJ",
		"iata_nr": "872",
		"name": "Bemidji Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "CI",
		"airline_3lc": "CAL",
		"iata_nr": "297",
		"name": "China Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "CJ",
		"airline_3lc": "CFE",
		"iata_nr": "",
		"name": "BA Cityflyer",
		"l": "0"
	  },
	  {
		"airline_code": "CK",
		"airline_3lc": "NDR",
		"iata_nr": "",
		"name": "China Cargo Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "CL",
		"airline_3lc": "CLH",
		"iata_nr": "683",
		"name": "Lufthansa CityLine",
		"l": "0"
	  },
	  {
		"airline_code": "CM",
		"airline_3lc": "CMP",
		"iata_nr": "230",
		"name": "Copa Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "CN",
		"airline_3lc": "",
		"iata_nr": "895",
		"name": "Grand China Air",
		"l": "0"
	  },
	  {
		"airline_code": "CO",
		"airline_3lc": "COA",
		"iata_nr": "005",
		"name": "Continental Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "CP",
		"airline_3lc": "",
		"iata_nr": "003",
		"name": "COMPASS AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "CQ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sunshine Express",
		"l": "0"
	  },
	  {
		"airline_code": "CS",
		"airline_3lc": "CMI",
		"iata_nr": "596",
		"name": "Continental Micronesia",
		"l": "0"
	  },
	  {
		"airline_code": "CU",
		"airline_3lc": "CUB",
		"iata_nr": "136",
		"name": "Cubana Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "CV",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Chathams",
		"l": "0"
	  },
	  {
		"airline_code": "CW",
		"airline_3lc": "MRS",
		"iata_nr": "778",
		"name": "Air Marshall Islands",
		"l": "0"
	  },
	  {
		"airline_code": "CX",
		"airline_3lc": "CPA",
		"iata_nr": "160",
		"name": "Cathay Pacific",
		"l": "1"
	  },
	  {
		"airline_code": "CY",
		"airline_3lc": "CYP",
		"iata_nr": "048",
		"name": "Cyprus Airways",
		"l": "1"
	  },
	  {
		"airline_code": "CZ",
		"airline_3lc": "CSN",
		"iata_nr": "784",
		"name": "China Southern",
		"l": "0"
	  },
	  {
		"airline_code": "D2",
		"airline_3lc": "",
		"iata_nr": "103",
		"name": "Severstal Aircompany",
		"l": "0"
	  },
	  {
		"airline_code": "D3",
		"airline_3lc": "",
		"iata_nr": "991",
		"name": "Daallo Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "D5",
		"airline_3lc": "DAE",
		"iata_nr": "",
		"name": "Dauair",
		"l": "0"
	  },
	  {
		"airline_code": "D6",
		"airline_3lc": "INL",
		"iata_nr": "625",
		"name": "Inter Air",
		"l": "0"
	  },
	  {
		"airline_code": "D8",
		"airline_3lc": "DJB",
		"iata_nr": "",
		"name": "Djibouti Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "DB",
		"airline_3lc": "BZH",
		"iata_nr": "750",
		"name": "Hop Brit Air",
		"l": "0"
	  },
	  {
		"airline_code": "DC",
		"airline_3lc": "GAO",
		"iata_nr": "",
		"name": "Golden Air Commuter",
		"l": "0"
	  },
	  {
		"airline_code": "DD",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Nok Air",
		"l": "0"
	  },
	  {
		"airline_code": "DE",
		"airline_3lc": "CFG",
		"iata_nr": "881",
		"name": "Condor",
		"l": "0"
	  },
	  {
		"airline_code": "DF",
		"airline_3lc": "ABH",
		"iata_nr": "059",
		"name": "Aerolineas De Baleares",
		"l": "0"
	  },
	  {
		"airline_code": "DH",
		"airline_3lc": "BLR",
		"iata_nr": "480",
		"name": "Independence Air",
		"l": "0"
	  },
	  {
		"airline_code": "DI",
		"airline_3lc": "BAG",
		"iata_nr": "944",
		"name": "dba",
		"l": "1"
	  },
	  {
		"airline_code": "DJ",
		"airline_3lc": "VOZ",
		"iata_nr": "856",
		"name": "VIRGIN AUSTRALIA AIRLINE",
		"l": "0"
	  },
	  {
		"airline_code": "DK",
		"airline_3lc": "VKG",
		"iata_nr": "630",
		"name": "THOMAS COOK AIRLINES A\/S",
		"l": "0"
	  },
	  {
		"airline_code": "DL",
		"airline_3lc": "DAL",
		"iata_nr": "006",
		"name": "Delta Air Lines",
		"l": "1"
	  },
	  {
		"airline_code": "DM",
		"airline_3lc": "DAN",
		"iata_nr": "349",
		"name": "Maersk Air",
		"l": "0"
	  },
	  {
		"airline_code": "DO",
		"airline_3lc": "RVL",
		"iata_nr": "965",
		"name": "Air Vallee",
		"l": "0"
	  },
	  {
		"airline_code": "DP",
		"airline_3lc": "AMM",
		"iata_nr": "091",
		"name": "First Choice Airways",
		"l": "0"
	  },
	  {
		"airline_code": "DQ",
		"airline_3lc": "CXT",
		"iata_nr": "457",
		"name": "Coastal Air Transport",
		"l": "0"
	  },
	  {
		"airline_code": "DS",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Easyjet Switzerland",
		"l": "0"
	  },
	  {
		"airline_code": "DT",
		"airline_3lc": "DTA",
		"iata_nr": "118",
		"name": "TAAG Angola Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "DU",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Norwegian Long Haul As",
		"l": "0"
	  },
	  {
		"airline_code": "DV",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Pll Scat Aircompany",
		"l": "0"
	  },
	  {
		"airline_code": "DW",
		"airline_3lc": "UCR",
		"iata_nr": "187",
		"name": "Aero-charter",
		"l": "0"
	  },
	  {
		"airline_code": "DX",
		"airline_3lc": "DTR",
		"iata_nr": "",
		"name": "Danish Air Transport",
		"l": "0"
	  },
	  {
		"airline_code": "DY",
		"airline_3lc": "NAX",
		"iata_nr": "328",
		"name": "Norwegian Air Shuttle",
		"l": "0"
	  },
	  {
		"airline_code": "DZ",
		"airline_3lc": "NOE",
		"iata_nr": "",
		"name": "Transcaraibes Air Intl",
		"l": "0"
	  },
	  {
		"airline_code": "E2",
		"airline_3lc": "GRN",
		"iata_nr": "",
		"name": "Rio Grande Air",
		"l": "0"
	  },
	  {
		"airline_code": "E4",
		"airline_3lc": "",
		"iata_nr": "532",
		"name": "Aero Asia International",
		"l": "0"
	  },
	  {
		"airline_code": "E5",
		"airline_3lc": "RBG",
		"iata_nr": "906",
		"name": "AIR ARABIA EGYPT",
		"l": "0"
	  },
	  {
		"airline_code": "E7",
		"airline_3lc": "EAF",
		"iata_nr": "158",
		"name": "Fly European",
		"l": "0"
	  },
	  {
		"airline_code": "E8",
		"airline_3lc": "ELG",
		"iata_nr": "789",
		"name": "Alpi Eagles",
		"l": "0"
	  },
	  {
		"airline_code": "ED",
		"airline_3lc": "AXE",
		"iata_nr": "",
		"name": "AIR EXPLORE",
		"l": "0"
	  },
	  {
		"airline_code": "EE",
		"airline_3lc": "",
		"iata_nr": "350",
		"name": "Aero Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "EF",
		"airline_3lc": "FEA",
		"iata_nr": "265",
		"name": "Far Eastern Air Transport",
		"l": "0"
	  },
	  {
		"airline_code": "EG",
		"airline_3lc": "JAA",
		"iata_nr": "688",
		"name": "Japan Asia Airways",
		"l": "0"
	  },
	  {
		"airline_code": "EH",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Nippon Network",
		"l": "0"
	  },
	  {
		"airline_code": "EI",
		"airline_3lc": "EIN",
		"iata_nr": "053",
		"name": "Aer Lingus",
		"l": "1"
	  },
	  {
		"airline_code": "EJ",
		"airline_3lc": "NEA",
		"iata_nr": "",
		"name": "New England Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "EK",
		"airline_3lc": "UAE",
		"iata_nr": "176",
		"name": "Emirates Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "EL",
		"airline_3lc": "ANK",
		"iata_nr": "768",
		"name": "Air Nippon Co.",
		"l": "0"
	  },
	  {
		"airline_code": "EM",
		"airline_3lc": "",
		"iata_nr": "282",
		"name": "Aero Benin",
		"l": "0"
	  },
	  {
		"airline_code": "EN",
		"airline_3lc": "DLA",
		"iata_nr": "101",
		"name": "Air Dolomiti S P A",
		"l": "0"
	  },
	  {
		"airline_code": "EQ",
		"airline_3lc": "TAE",
		"iata_nr": "269",
		"name": "TAME C A",
		"l": "0"
	  },
	  {
		"airline_code": "ET",
		"airline_3lc": "ETH",
		"iata_nr": "071",
		"name": "Ethiopian Air Lines",
		"l": "1"
	  },
	  {
		"airline_code": "EV",
		"airline_3lc": "ASQ",
		"iata_nr": "862",
		"name": "Express Jet",
		"l": "0"
	  },
	  {
		"airline_code": "EW",
		"airline_3lc": "EWG",
		"iata_nr": "104",
		"name": "Eurowings",
		"l": "1"
	  },
	  {
		"airline_code": "EX",
		"airline_3lc": "SDO",
		"iata_nr": "309",
		"name": "Air Santo Domingo",
		"l": "0"
	  },
	  {
		"airline_code": "EY",
		"airline_3lc": "",
		"iata_nr": "607",
		"name": "Etihad Airways",
		"l": "0"
	  },
	  {
		"airline_code": "EZ",
		"airline_3lc": "SUS",
		"iata_nr": "",
		"name": "Sun Air of Scandinavia",
		"l": "0"
	  },
	  {
		"airline_code": "F3",
		"airline_3lc": "FSW",
		"iata_nr": "",
		"name": "Faso Airways",
		"l": "0"
	  },
	  {
		"airline_code": "F4",
		"airline_3lc": "NBK",
		"iata_nr": "",
		"name": "Albarka Air Services Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "F5",
		"airline_3lc": "",
		"iata_nr": "447",
		"name": "Cosmic Air",
		"l": "0"
	  },
	  {
		"airline_code": "F9",
		"airline_3lc": "",
		"iata_nr": "422",
		"name": "Frontier Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "FA",
		"airline_3lc": "SFR",
		"iata_nr": "",
		"name": "SAFAIR",
		"l": "0"
	  },
	  {
		"airline_code": "FB",
		"airline_3lc": "LZB",
		"iata_nr": "623",
		"name": "Bulgaria Air",
		"l": "0"
	  },
	  {
		"airline_code": "FC",
		"airline_3lc": "",
		"iata_nr": "216",
		"name": "Finncomm Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "FE",
		"airline_3lc": "",
		"iata_nr": "519",
		"name": "Primaris Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "FF",
		"airline_3lc": "TOW",
		"iata_nr": "305",
		"name": "Tower Air",
		"l": "0"
	  },
	  {
		"airline_code": "FG",
		"airline_3lc": "AFG",
		"iata_nr": "255",
		"name": "Ariana Afghan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "FI",
		"airline_3lc": "ICE",
		"iata_nr": "108",
		"name": "Icelandair",
		"l": "1"
	  },
	  {
		"airline_code": "FJ",
		"airline_3lc": "FJI",
		"iata_nr": "260",
		"name": "Air Pacific Limited",
		"l": "0"
	  },
	  {
		"airline_code": "FK",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Keewatin Air",
		"l": "0"
	  },
	  {
		"airline_code": "FN",
		"airline_3lc": "RGL",
		"iata_nr": "259",
		"name": "Regional Air Lines",
		"l": "0"
	  },
	  {
		"airline_code": "FQ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Brindabella Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "FR",
		"airline_3lc": "RYR",
		"iata_nr": "",
		"name": "RyanAir LTD Dublin",
		"l": "0"
	  },
	  {
		"airline_code": "FS",
		"airline_3lc": "ACL",
		"iata_nr": "393",
		"name": "SYPHAX AIRLINES SA",
		"l": "0"
	  },
	  {
		"airline_code": "FU",
		"airline_3lc": "LIT",
		"iata_nr": "659",
		"name": "Air Littoral",
		"l": "0"
	  },
	  {
		"airline_code": "FV",
		"airline_3lc": "SDM",
		"iata_nr": "195",
		"name": "Rossiya-Russian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "FW",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Fair Incorporated",
		"l": "0"
	  },
	  {
		"airline_code": "FX",
		"airline_3lc": "FXA",
		"iata_nr": "",
		"name": "Fedex",
		"l": "0"
	  },
	  {
		"airline_code": "FZ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Master Airways",
		"l": "0"
	  },
	  {
		"airline_code": "G5",
		"airline_3lc": "",
		"iata_nr": "816",
		"name": "Jsc Airline Enkor",
		"l": "0"
	  },
	  {
		"airline_code": "G7",
		"airline_3lc": "GJS",
		"iata_nr": "573",
		"name": "GOJET AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "G8",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Go Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "G9",
		"airline_3lc": "ABY",
		"iata_nr": "",
		"name": "Air Arabia",
		"l": "0"
	  },
	  {
		"airline_code": "GA",
		"airline_3lc": "GIA",
		"iata_nr": "126",
		"name": "Garuda Indonesia",
		"l": "1"
	  },
	  {
		"airline_code": "GB",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Great Barrier Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "GC",
		"airline_3lc": "GNR",
		"iata_nr": "034",
		"name": "Gambia Intl Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "GD",
		"airline_3lc": "AHA",
		"iata_nr": "",
		"name": "Air Alpha Greenland A\/s",
		"l": "0"
	  },
	  {
		"airline_code": "GE",
		"airline_3lc": "TNA",
		"iata_nr": "170",
		"name": "Trans Asia Airways",
		"l": "0"
	  },
	  {
		"airline_code": "GF",
		"airline_3lc": "GFA",
		"iata_nr": "072",
		"name": "Gulf Air Company",
		"l": "1"
	  },
	  {
		"airline_code": "GG",
		"airline_3lc": "AHD",
		"iata_nr": "",
		"name": "Air Guyane",
		"l": "0"
	  },
	  {
		"airline_code": "GH",
		"airline_3lc": "GHA",
		"iata_nr": "237",
		"name": "Ghana Airways",
		"l": "0"
	  },
	  {
		"airline_code": "GI",
		"airline_3lc": "IKA",
		"iata_nr": "420",
		"name": "Itek Air",
		"l": "0"
	  },
	  {
		"airline_code": "GJ",
		"airline_3lc": "",
		"iata_nr": "736",
		"name": "Eurofly Spa",
		"l": "0"
	  },
	  {
		"airline_code": "GL",
		"airline_3lc": "GRL",
		"iata_nr": "631",
		"name": "Greenlandair Inc.",
		"l": "0"
	  },
	  {
		"airline_code": "GN",
		"airline_3lc": "AGN",
		"iata_nr": "185",
		"name": "Air Gabon",
		"l": "0"
	  },
	  {
		"airline_code": "GO",
		"airline_3lc": "GOE",
		"iata_nr": "",
		"name": "Go",
		"l": "0"
	  },
	  {
		"airline_code": "GP",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Palau Trans Pacific Air",
		"l": "0"
	  },
	  {
		"airline_code": "GQ",
		"airline_3lc": "BSY",
		"iata_nr": "387",
		"name": "Big Sky Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "GR",
		"airline_3lc": "AUR",
		"iata_nr": "924",
		"name": "Aurigny Air Services",
		"l": "0"
	  },
	  {
		"airline_code": "GT",
		"airline_3lc": "GBL",
		"iata_nr": "171",
		"name": "GB Airways",
		"l": "0"
	  },
	  {
		"airline_code": "GU",
		"airline_3lc": "GUG",
		"iata_nr": "240",
		"name": "Aviateca",
		"l": "0"
	  },
	  {
		"airline_code": "GV",
		"airline_3lc": "",
		"iata_nr": "461",
		"name": "Aero Flight",
		"l": "0"
	  },
	  {
		"airline_code": "GW",
		"airline_3lc": "KIL",
		"iata_nr": "113",
		"name": "KUBAN AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "GX",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Genavia S R L",
		"l": "0"
	  },
	  {
		"airline_code": "GY",
		"airline_3lc": "GYA",
		"iata_nr": "",
		"name": "Tri Mg Intra Asia",
		"l": "0"
	  },
	  {
		"airline_code": "GZ",
		"airline_3lc": "",
		"iata_nr": "755",
		"name": "Air Rarotonga",
		"l": "0"
	  },
	  {
		"airline_code": "H2",
		"airline_3lc": "SKU",
		"iata_nr": "605",
		"name": "Sky Airline",
		"l": "0"
	  },
	  {
		"airline_code": "H3",
		"airline_3lc": "",
		"iata_nr": "458",
		"name": "Harbour Air",
		"l": "0"
	  },
	  {
		"airline_code": "H4",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Heli Securite",
		"l": "0"
	  },
	  {
		"airline_code": "H9",
		"airline_3lc": "",
		"iata_nr": "624",
		"name": "Pegasus Hava Tasimaciligi",
		"l": "0"
	  },
	  {
		"airline_code": "HA",
		"airline_3lc": "HAL",
		"iata_nr": "173",
		"name": "Hawaiian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "HC",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aero-tropics",
		"l": "0"
	  },
	  {
		"airline_code": "HD",
		"airline_3lc": "ADO",
		"iata_nr": "",
		"name": "Hokkaido International",
		"l": "0"
	  },
	  {
		"airline_code": "HE",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Luftfahrtgesellschaft Walter GmbH LGW",
		"l": "0"
	  },
	  {
		"airline_code": "HF",
		"airline_3lc": "HLF",
		"iata_nr": "617",
		"name": "Hapag Lloyd Flug",
		"l": "1"
	  },
	  {
		"airline_code": "HG",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Niki",
		"l": "0"
	  },
	  {
		"airline_code": "HI",
		"airline_3lc": "",
		"iata_nr": "563",
		"name": "Papillon Airways Inc",
		"l": "0"
	  },
	  {
		"airline_code": "HJ",
		"airline_3lc": "HST",
		"iata_nr": "039",
		"name": "Hellenic Star Airways",
		"l": "0"
	  },
	  {
		"airline_code": "HM",
		"airline_3lc": "SEY",
		"iata_nr": "061",
		"name": "Air Seychelles",
		"l": "0"
	  },
	  {
		"airline_code": "HO",
		"airline_3lc": "DKH",
		"iata_nr": "018",
		"name": "JUNEYAO AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "HP",
		"airline_3lc": "AWE",
		"iata_nr": "401",
		"name": "America West Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "HQ",
		"airline_3lc": "HMY",
		"iata_nr": "668",
		"name": "Harmony Airways",
		"l": "0"
	  },
	  {
		"airline_code": "HT",
		"airline_3lc": "IMP",
		"iata_nr": "730",
		"name": "HELLENIC IMPERIAL AIRWAY",
		"l": "0"
	  },
	  {
		"airline_code": "HV",
		"airline_3lc": "TRA",
		"iata_nr": "979",
		"name": "Transavia Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "HW",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "North-Wright Air Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "HX",
		"airline_3lc": "",
		"iata_nr": "851",
		"name": "Hong Kong Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "HY",
		"airline_3lc": "UZB",
		"iata_nr": "250",
		"name": "Uzbekistan Airways",
		"l": "0"
	  },
	  {
		"airline_code": "HZ",
		"airline_3lc": "SHU",
		"iata_nr": "598",
		"name": "Sakhalinskie Aviatrassy",
		"l": "0"
	  },
	  {
		"airline_code": "IA",
		"airline_3lc": "IAW",
		"iata_nr": "073",
		"name": "Iraqi Airways",
		"l": "0"
	  },
	  {
		"airline_code": "IB",
		"airline_3lc": "IBE",
		"iata_nr": "075",
		"name": "Iberia",
		"l": "1"
	  },
	  {
		"airline_code": "IC",
		"airline_3lc": "IAC",
		"iata_nr": "058",
		"name": "Nacil DBA Indian airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IE",
		"airline_3lc": "SOL",
		"iata_nr": "193",
		"name": "Solomon Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IF",
		"airline_3lc": "",
		"iata_nr": "769",
		"name": "Islas Airways",
		"l": "0"
	  },
	  {
		"airline_code": "IG",
		"airline_3lc": "ISS",
		"iata_nr": "191",
		"name": "Meridiana",
		"l": "1"
	  },
	  {
		"airline_code": "II",
		"airline_3lc": "CSQ",
		"iata_nr": "175",
		"name": "Business Air Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "IJ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "TAT European Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IK",
		"airline_3lc": "ITX",
		"iata_nr": "166",
		"name": "Loken Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "IL",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Istanbul Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IM",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Australia Asia Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IP",
		"airline_3lc": "",
		"iata_nr": "312",
		"name": "Atyrau Aue Joly",
		"l": "0"
	  },
	  {
		"airline_code": "IQ",
		"airline_3lc": "AUB",
		"iata_nr": "614",
		"name": "Augsburg Airways",
		"l": "0"
	  },
	  {
		"airline_code": "IR",
		"airline_3lc": "IRA",
		"iata_nr": "096",
		"name": "Iran Air",
		"l": "0"
	  },
	  {
		"airline_code": "IS",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Eagle Air",
		"l": "0"
	  },
	  {
		"airline_code": "IT",
		"airline_3lc": "",
		"iata_nr": "090",
		"name": "Kingfisher Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IU",
		"airline_3lc": "ASN",
		"iata_nr": "",
		"name": "Air Straubing",
		"l": "0"
	  },
	  {
		"airline_code": "IW",
		"airline_3lc": "AOM",
		"iata_nr": "646",
		"name": "AOM French Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IX",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Flandre Air Airline",
		"l": "0"
	  },
	  {
		"airline_code": "IY",
		"airline_3lc": "IYE",
		"iata_nr": "635",
		"name": "Yemenia Yemen Airways",
		"l": "0"
	  },
	  {
		"airline_code": "IZ",
		"airline_3lc": "AIZ",
		"iata_nr": "238",
		"name": "Arkia Israeli Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "J2",
		"airline_3lc": "AHY",
		"iata_nr": "771",
		"name": "Azerbaijan Air",
		"l": "0"
	  },
	  {
		"airline_code": "J7",
		"airline_3lc": "CVC",
		"iata_nr": "601",
		"name": "Valujet Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "J8",
		"airline_3lc": "BVT",
		"iata_nr": "801",
		"name": "Berjaya Air",
		"l": "0"
	  },
	  {
		"airline_code": "J9",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Jazeera Airways",
		"l": "0"
	  },
	  {
		"airline_code": "JA",
		"airline_3lc": "",
		"iata_nr": "995",
		"name": "B&H AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "JB",
		"airline_3lc": "JBA",
		"iata_nr": "613",
		"name": "Helijet Airways",
		"l": "0"
	  },
	  {
		"airline_code": "JC",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "JAL EXPRESS",
		"l": "0"
	  },
	  {
		"airline_code": "JD",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Deer Jet",
		"l": "0"
	  },
	  {
		"airline_code": "JE",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Tulca",
		"l": "0"
	  },
	  {
		"airline_code": "JF",
		"airline_3lc": "LAB",
		"iata_nr": "510",
		"name": "L A B Flying Service",
		"l": "0"
	  },
	  {
		"airline_code": "JG",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Teranga (Gambia)",
		"l": "0"
	  },
	  {
		"airline_code": "JH",
		"airline_3lc": "NES",
		"iata_nr": "264",
		"name": "Nordeste-Linhas Aereas Regionals",
		"l": "0"
	  },
	  {
		"airline_code": "JI",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Midway Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "JJ",
		"airline_3lc": "TAM",
		"iata_nr": "957",
		"name": "Tam Linhas Aereas",
		"l": "0"
	  },
	  {
		"airline_code": "JK",
		"airline_3lc": "JKK",
		"iata_nr": "680",
		"name": "Spanair",
		"l": "1"
	  },
	  {
		"airline_code": "JL",
		"airline_3lc": "JAL",
		"iata_nr": "131",
		"name": "Japan Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "JM",
		"airline_3lc": "AJM",
		"iata_nr": "201",
		"name": "Air Jamaica",
		"l": "0"
	  },
	  {
		"airline_code": "JN",
		"airline_3lc": "XLA",
		"iata_nr": "",
		"name": "Japan Air Commuter",
		"l": "0"
	  },
	  {
		"airline_code": "JO",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "JET TIME",
		"l": "0"
	  },
	  {
		"airline_code": "JP",
		"airline_3lc": "ADR",
		"iata_nr": "165",
		"name": "Adria Airways",
		"l": "0"
	  },
	  {
		"airline_code": "JQ",
		"airline_3lc": "JST",
		"iata_nr": "41",
		"name": "JetStar",
		"l": "0"
	  },
	  {
		"airline_code": "JR",
		"airline_3lc": "SER",
		"iata_nr": "078",
		"name": "Aero California",
		"l": "0"
	  },
	  {
		"airline_code": "JS",
		"airline_3lc": "KOR",
		"iata_nr": "120",
		"name": "Air Koryo",
		"l": "0"
	  },
	  {
		"airline_code": "JT",
		"airline_3lc": "",
		"iata_nr": "990",
		"name": "Regal Bahamas International",
		"l": "0"
	  },
	  {
		"airline_code": "JU",
		"airline_3lc": "JAT",
		"iata_nr": "115",
		"name": "Yugoslav Airlines - JAT",
		"l": "0"
	  },
	  {
		"airline_code": "JV",
		"airline_3lc": "BLS",
		"iata_nr": "632",
		"name": "Bearskin Lake Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "JY",
		"airline_3lc": "IWY",
		"iata_nr": "653",
		"name": "Air Turks and Caicos",
		"l": "0"
	  },
	  {
		"airline_code": "JZ",
		"airline_3lc": "SKX",
		"iata_nr": "752",
		"name": "Skyways AB",
		"l": "0"
	  },
	  {
		"airline_code": "K7",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Company Yakutaviatrans",
		"l": "0"
	  },
	  {
		"airline_code": "K8",
		"airline_3lc": "",
		"iata_nr": "241",
		"name": "Zambia Skyways",
		"l": "0"
	  },
	  {
		"airline_code": "K9",
		"airline_3lc": "ESD",
		"iata_nr": "40",
		"name": "Esen Air",
		"l": "0"
	  },
	  {
		"airline_code": "KA",
		"airline_3lc": "HDA",
		"iata_nr": "043",
		"name": "Dragonair Hong Kong Dragon Air",
		"l": "0"
	  },
	  {
		"airline_code": "KB",
		"airline_3lc": "DRK",
		"iata_nr": "787",
		"name": "Druk Air",
		"l": "0"
	  },
	  {
		"airline_code": "KD",
		"airline_3lc": "KNI",
		"iata_nr": "93",
		"name": "KD Avia",
		"l": "0"
	  },
	  {
		"airline_code": "KE",
		"airline_3lc": "KAL",
		"iata_nr": "180",
		"name": "Korean Air",
		"l": "1"
	  },
	  {
		"airline_code": "KF",
		"airline_3lc": "KFB",
		"iata_nr": "142",
		"name": "Blue One",
		"l": "0"
	  },
	  {
		"airline_code": "KG",
		"airline_3lc": "",
		"iata_nr": "102",
		"name": "King Island Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "KH",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Kyrnair",
		"l": "0"
	  },
	  {
		"airline_code": "KI",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Atlantique",
		"l": "0"
	  },
	  {
		"airline_code": "KK",
		"airline_3lc": "OGE",
		"iata_nr": "610",
		"name": "Atlasjet International Airways",
		"l": "0"
	  },
	  {
		"airline_code": "KL",
		"airline_3lc": "KLM",
		"iata_nr": "074",
		"name": "KLM Royal Dutch Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "KM",
		"airline_3lc": "AMC",
		"iata_nr": "643",
		"name": "Air Malta Company",
		"l": "1"
	  },
	  {
		"airline_code": "KN",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Morris Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "KO",
		"airline_3lc": "KHO",
		"iata_nr": "",
		"name": "AIRCOMPANY KHORS",
		"l": "0"
	  },
	  {
		"airline_code": "KP",
		"airline_3lc": "",
		"iata_nr": "032",
		"name": "Asky",
		"l": "0"
	  },
	  {
		"airline_code": "KQ",
		"airline_3lc": "KQA",
		"iata_nr": "706",
		"name": "Kenya Airways",
		"l": "0"
	  },
	  {
		"airline_code": "KR",
		"airline_3lc": "EAA",
		"iata_nr": "265",
		"name": "Air Bischkek",
		"l": "0"
	  },
	  {
		"airline_code": "KS",
		"airline_3lc": "PEN",
		"iata_nr": "339",
		"name": "PenAir",
		"l": "0"
	  },
	  {
		"airline_code": "KT",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Turtle Airways",
		"l": "0"
	  },
	  {
		"airline_code": "KU",
		"airline_3lc": "KAC",
		"iata_nr": "229",
		"name": "Kuwait Airways",
		"l": "0"
	  },
	  {
		"airline_code": "KV",
		"airline_3lc": "MVD",
		"iata_nr": "348",
		"name": "Kavminvodyavia Airline",
		"l": "1"
	  },
	  {
		"airline_code": "KW",
		"airline_3lc": "KSD",
		"iata_nr": "",
		"name": "Carnival Air Lines",
		"l": "0"
	  },
	  {
		"airline_code": "KX",
		"airline_3lc": "CAY",
		"iata_nr": "378",
		"name": "Cayman Airways",
		"l": "0"
	  },
	  {
		"airline_code": "KY",
		"airline_3lc": "EQL",
		"iata_nr": "980",
		"name": "Waterwings Airways",
		"l": "0"
	  },
	  {
		"airline_code": "L2",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Love Air",
		"l": "0"
	  },
	  {
		"airline_code": "L5",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Luftransport",
		"l": "0"
	  },
	  {
		"airline_code": "L8",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Leisure Air",
		"l": "0"
	  },
	  {
		"airline_code": "L9",
		"airline_3lc": "TLW",
		"iata_nr": "",
		"name": "Air Mali S.A.",
		"l": "0"
	  },
	  {
		"airline_code": "LA",
		"airline_3lc": "LAN",
		"iata_nr": "045",
		"name": "LAN Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "LB",
		"airline_3lc": "LLB",
		"iata_nr": "051",
		"name": "Lloyd Aereo Boliviano",
		"l": "0"
	  },
	  {
		"airline_code": "LC",
		"airline_3lc": "",
		"iata_nr": "753",
		"name": "EQUATORIAL CONGO AIRLINE",
		"l": "0"
	  },
	  {
		"airline_code": "LD",
		"airline_3lc": "",
		"iata_nr": "402",
		"name": "Lineas Aereas del Estado",
		"l": "0"
	  },
	  {
		"airline_code": "LF",
		"airline_3lc": "",
		"iata_nr": "971",
		"name": "LAO CENTRAL AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "LG",
		"airline_3lc": "LGL",
		"iata_nr": "149",
		"name": "Luxair",
		"l": "0"
	  },
	  {
		"airline_code": "LH",
		"airline_3lc": "",
		"iata_nr": "220",
		"name": "Lufthansa",
		"l": "1"
	  },
	  {
		"airline_code": "LI",
		"airline_3lc": "LIA",
		"iata_nr": "140",
		"name": "Liat Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "LJ",
		"airline_3lc": "SLA",
		"iata_nr": "690",
		"name": "Sierra National",
		"l": "0"
	  },
	  {
		"airline_code": "LK",
		"airline_3lc": "LXR",
		"iata_nr": "040",
		"name": "Goldfields Air Services",
		"l": "0"
	  },
	  {
		"airline_code": "LL",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Bell-Air",
		"l": "0"
	  },
	  {
		"airline_code": "LM",
		"airline_3lc": "LVG",
		"iata_nr": "857",
		"name": "ALM\/Antillean Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "LN",
		"airline_3lc": "LAA",
		"iata_nr": "148",
		"name": "Libyan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "LO",
		"airline_3lc": "LOT",
		"iata_nr": "080",
		"name": "LOT Polish Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "LP",
		"airline_3lc": "LPE",
		"iata_nr": "544",
		"name": "Lanperu",
		"l": "0"
	  },
	  {
		"airline_code": "LR",
		"airline_3lc": "LRC",
		"iata_nr": "133",
		"name": "TACSA, Lineas Aereas Costaricenses S.A",
		"l": "0"
	  },
	  {
		"airline_code": "LS",
		"airline_3lc": "EXS",
		"iata_nr": "949",
		"name": "Jet2.com",
		"l": "0"
	  },
	  {
		"airline_code": "LT",
		"airline_3lc": "LTU",
		"iata_nr": "431",
		"name": "AIR LITUANICA",
		"l": "1"
	  },
	  {
		"airline_code": "LU",
		"airline_3lc": "",
		"iata_nr": "972",
		"name": "Lan Express",
		"l": "0"
	  },
	  {
		"airline_code": "LV",
		"airline_3lc": "LBC",
		"iata_nr": "639",
		"name": "Aeropostal",
		"l": "0"
	  },
	  {
		"airline_code": "LW",
		"airline_3lc": "ANV",
		"iata_nr": "568",
		"name": "Pacific Wings",
		"l": "0"
	  },
	  {
		"airline_code": "LX",
		"airline_3lc": "SWR",
		"iata_nr": "724",
		"name": "Swiss Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "LY",
		"airline_3lc": "ELY",
		"iata_nr": "114",
		"name": "El Al Israel Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "LZ",
		"airline_3lc": "LBY",
		"iata_nr": "",
		"name": "Belle Air Company",
		"l": "0"
	  },
	  {
		"airline_code": "M6",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "CASM",
		"l": "0"
	  },
	  {
		"airline_code": "M8",
		"airline_3lc": "MKN",
		"iata_nr": "616",
		"name": "Moscow Airways",
		"l": "0"
	  },
	  {
		"airline_code": "M9",
		"airline_3lc": "",
		"iata_nr": "011",
		"name": "Motor Sich JSC",
		"l": "1"
	  },
	  {
		"airline_code": "MA",
		"airline_3lc": "MAH",
		"iata_nr": "182",
		"name": "Malev Hungarian Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "MB",
		"airline_3lc": "MNB",
		"iata_nr": "716",
		"name": "Menege Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "MD",
		"airline_3lc": "MDG",
		"iata_nr": "258",
		"name": "Air Madagascar",
		"l": "0"
	  },
	  {
		"airline_code": "ME",
		"airline_3lc": "MEA",
		"iata_nr": "076",
		"name": "Middle East Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "MF",
		"airline_3lc": "CXA",
		"iata_nr": "731",
		"name": "Xiamen Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "MG",
		"airline_3lc": "CCP",
		"iata_nr": "",
		"name": "Champion Air",
		"l": "0"
	  },
	  {
		"airline_code": "MH",
		"airline_3lc": "MAS",
		"iata_nr": "232",
		"name": "Malaysia Airline",
		"l": "1"
	  },
	  {
		"airline_code": "MI",
		"airline_3lc": "SLK",
		"iata_nr": "629",
		"name": "SilkAir",
		"l": "0"
	  },
	  {
		"airline_code": "MJ",
		"airline_3lc": "MLR",
		"iata_nr": "817",
		"name": "Mihin Lanka",
		"l": "0"
	  },
	  {
		"airline_code": "MK",
		"airline_3lc": "MAU",
		"iata_nr": "239",
		"name": "Air Mauritius",
		"l": "1"
	  },
	  {
		"airline_code": "MM",
		"airline_3lc": "SAM",
		"iata_nr": "334",
		"name": "Sociedad Aeronautica Medellin",
		"l": "0"
	  },
	  {
		"airline_code": "MN",
		"airline_3lc": "CAW",
		"iata_nr": "161",
		"name": "COMAIR LTD",
		"l": "0"
	  },
	  {
		"airline_code": "MO",
		"airline_3lc": "CAV",
		"iata_nr": "622",
		"name": "Calm Air International",
		"l": "0"
	  },
	  {
		"airline_code": "MP",
		"airline_3lc": "MPH",
		"iata_nr": "129",
		"name": "Martinair Holland",
		"l": "0"
	  },
	  {
		"airline_code": "MQ",
		"airline_3lc": "EGF",
		"iata_nr": "093",
		"name": "AMERICAN EAGLE",
		"l": "0"
	  },
	  {
		"airline_code": "MR",
		"airline_3lc": "MML",
		"iata_nr": "861",
		"name": "Hunnu Air",
		"l": "0"
	  },
	  {
		"airline_code": "MS",
		"airline_3lc": "MSR",
		"iata_nr": "077",
		"name": "Egyptair",
		"l": "0"
	  },
	  {
		"airline_code": "MU",
		"airline_3lc": "CES",
		"iata_nr": "781",
		"name": "China Eastern",
		"l": "0"
	  },
	  {
		"airline_code": "MV",
		"airline_3lc": "RML",
		"iata_nr": "904",
		"name": "Great American Airways Inc",
		"l": "0"
	  },
	  {
		"airline_code": "MW",
		"airline_3lc": "",
		"iata_nr": "415",
		"name": "Mokulele Flight Service",
		"l": "0"
	  },
	  {
		"airline_code": "MX",
		"airline_3lc": "MXA",
		"iata_nr": "132",
		"name": "Mexicana De Aviacion",
		"l": "0"
	  },
	  {
		"airline_code": "MZ",
		"airline_3lc": "MNA",
		"iata_nr": "621",
		"name": "Merpati Nusantara Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "N4",
		"airline_3lc": "MTC",
		"iata_nr": "300",
		"name": "National Airlines Chile",
		"l": "0"
	  },
	  {
		"airline_code": "N6",
		"airline_3lc": "ACQ",
		"iata_nr": "929",
		"name": "Aero Continente",
		"l": "0"
	  },
	  {
		"airline_code": "N9",
		"airline_3lc": "NCN",
		"iata_nr": "",
		"name": "North Coast Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "NA",
		"airline_3lc": "NAO",
		"iata_nr": "455",
		"name": "Executive Air Charter",
		"l": "0"
	  },
	  {
		"airline_code": "ND",
		"airline_3lc": "",
		"iata_nr": "159",
		"name": "Inter-Canadien",
		"l": "0"
	  },
	  {
		"airline_code": "NE",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "SkyEurope Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "NF",
		"airline_3lc": "AVN",
		"iata_nr": "218",
		"name": "Air Vanuatu Limited",
		"l": "0"
	  },
	  {
		"airline_code": "NG",
		"airline_3lc": "LDA",
		"iata_nr": "231",
		"name": "Lauda Air",
		"l": "0"
	  },
	  {
		"airline_code": "NH",
		"airline_3lc": "ANA",
		"iata_nr": "205",
		"name": "All Nippon Airways",
		"l": "1"
	  },
	  {
		"airline_code": "NI",
		"airline_3lc": "PGA",
		"iata_nr": "685",
		"name": "Portugalia",
		"l": "0"
	  },
	  {
		"airline_code": "NJ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Northeast Express Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "NK",
		"airline_3lc": "",
		"iata_nr": "487",
		"name": "Spirit Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "NL",
		"airline_3lc": "SAI",
		"iata_nr": "740",
		"name": "Shaheen Air International",
		"l": "0"
	  },
	  {
		"airline_code": "NM",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "manx2",
		"l": "0"
	  },
	  {
		"airline_code": "NN",
		"airline_3lc": "MOV",
		"iata_nr": "823",
		"name": "VIM AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "NO",
		"airline_3lc": "",
		"iata_nr": "703",
		"name": "NEOS SPA",
		"l": "0"
	  },
	  {
		"airline_code": "NP",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Piccolo Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "NQ",
		"airline_3lc": "AJX",
		"iata_nr": "",
		"name": "Orbi Georgian Airways",
		"l": "0"
	  },
	  {
		"airline_code": "NR",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Pamir Air",
		"l": "0"
	  },
	  {
		"airline_code": "NS",
		"airline_3lc": "SRJ",
		"iata_nr": "",
		"name": "Caucasus Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "NU",
		"airline_3lc": "JTA",
		"iata_nr": "353",
		"name": "Japan Transocean Air",
		"l": "0"
	  },
	  {
		"airline_code": "NV",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Northwest Territorial Airways",
		"l": "0"
	  },
	  {
		"airline_code": "NW",
		"airline_3lc": "NWA",
		"iata_nr": "012",
		"name": "Northwest Airlines Inc",
		"l": "1"
	  },
	  {
		"airline_code": "NZ",
		"airline_3lc": "ANZ",
		"iata_nr": "086",
		"name": "Air New Zealand",
		"l": "1"
	  },
	  {
		"airline_code": "OA",
		"airline_3lc": "OAL",
		"iata_nr": "050",
		"name": "Olympic Air",
		"l": "0"
	  },
	  {
		"airline_code": "OC",
		"airline_3lc": "OAV",
		"iata_nr": "",
		"name": "Sunshine Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "OD",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Natalco Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "OF",
		"airline_3lc": "",
		"iata_nr": "442",
		"name": "Sunstate Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "OG",
		"airline_3lc": "",
		"iata_nr": "130",
		"name": "AIR ONIX",
		"l": "0"
	  },
	  {
		"airline_code": "OH",
		"airline_3lc": "COM",
		"iata_nr": "886",
		"name": "Comair",
		"l": "0"
	  },
	  {
		"airline_code": "OJ",
		"airline_3lc": "OLA",
		"iata_nr": "",
		"name": "Air St. Barthelemy",
		"l": "0"
	  },
	  {
		"airline_code": "OK",
		"airline_3lc": "CSA",
		"iata_nr": "064",
		"name": "Czech Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "OL",
		"airline_3lc": "OLT",
		"iata_nr": "704",
		"name": "OLT Express",
		"l": "0"
	  },
	  {
		"airline_code": "OM",
		"airline_3lc": "MGL",
		"iata_nr": "289",
		"name": "MIAT - Mongolian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "ON",
		"airline_3lc": "RON",
		"iata_nr": "123",
		"name": "Air Nauru",
		"l": "0"
	  },
	  {
		"airline_code": "OO",
		"airline_3lc": "SKW",
		"iata_nr": "302",
		"name": "SkyWest Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "OQ",
		"airline_3lc": "",
		"iata_nr": "878",
		"name": "Chongqing Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "OR",
		"airline_3lc": "TFL",
		"iata_nr": "343",
		"name": "ArkeFly",
		"l": "0"
	  },
	  {
		"airline_code": "OS",
		"airline_3lc": "AUA",
		"iata_nr": "257",
		"name": "Austrian Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "OT",
		"airline_3lc": "PEL",
		"iata_nr": "",
		"name": "AEROPELICAN AIR SERVICES",
		"l": "0"
	  },
	  {
		"airline_code": "OU",
		"airline_3lc": "CTN",
		"iata_nr": "831",
		"name": "Croatia Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "OV",
		"airline_3lc": "ELL",
		"iata_nr": "960",
		"name": "Estonian Air Lines",
		"l": "0"
	  },
	  {
		"airline_code": "OW",
		"airline_3lc": "EGF",
		"iata_nr": "478",
		"name": "Metavia Airlines Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "OY",
		"airline_3lc": "",
		"iata_nr": "650",
		"name": "Andes Lineas Aereas",
		"l": "0"
	  },
	  {
		"airline_code": "OZ",
		"airline_3lc": "AAR",
		"iata_nr": "988",
		"name": "Asiana Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "P3",
		"airline_3lc": "PHG",
		"iata_nr": "388",
		"name": "Promech Inc",
		"l": "0"
	  },
	  {
		"airline_code": "P5",
		"airline_3lc": "RPB",
		"iata_nr": "845",
		"name": "AeroRepublica",
		"l": "0"
	  },
	  {
		"airline_code": "P8",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "P.L.A.S. S\/A",
		"l": "0"
	  },
	  {
		"airline_code": "PB",
		"airline_3lc": "",
		"iata_nr": "967",
		"name": "Provincial Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "PC",
		"airline_3lc": "PGT",
		"iata_nr": "624",
		"name": "Pegasus Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "3Y",
		"airline_3lc": "KAE",
		"iata_nr": "",
		"name": "Kartika Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "PG",
		"airline_3lc": "BKP",
		"iata_nr": "829",
		"name": "Bangkok Airways",
		"l": "0"
	  },
	  {
		"airline_code": "PH",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Transavia Denmark",
		"l": "0"
	  },
	  {
		"airline_code": "PI",
		"airline_3lc": "SUF",
		"iata_nr": "252",
		"name": "Sunflower Airlines Limited",
		"l": "0"
	  },
	  {
		"airline_code": "PK",
		"airline_3lc": "PIA",
		"iata_nr": "214",
		"name": "Pakistan International Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "PL",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aeroperu",
		"l": "0"
	  },
	  {
		"airline_code": "PM",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Tropic Air",
		"l": "0"
	  },
	  {
		"airline_code": "PN",
		"airline_3lc": "",
		"iata_nr": "711",
		"name": "S.N.A.M.",
		"l": "0"
	  },
	  {
		"airline_code": "PO",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aeropelican Air Services",
		"l": "0"
	  },
	  {
		"airline_code": "PR",
		"airline_3lc": "PAL",
		"iata_nr": "079",
		"name": "Philippine Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "PS",
		"airline_3lc": "",
		"iata_nr": "566",
		"name": "Air Ukraine International",
		"l": "0"
	  },
	  {
		"airline_code": "PT",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "West Air Sweden",
		"l": "0"
	  },
	  {
		"airline_code": "PU",
		"airline_3lc": "PUA",
		"iata_nr": "286",
		"name": "Pluna",
		"l": "0"
	  },
	  {
		"airline_code": "PV",
		"airline_3lc": "PNR",
		"iata_nr": "",
		"name": "Latvian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "PX",
		"airline_3lc": "ANG",
		"iata_nr": "656",
		"name": "Air Niugini",
		"l": "0"
	  },
	  {
		"airline_code": "PY",
		"airline_3lc": "SLM",
		"iata_nr": "192",
		"name": "Surinam Airways",
		"l": "0"
	  },
	  {
		"airline_code": "Q2",
		"airline_3lc": "",
		"iata_nr": "986",
		"name": "Island Aviation Services",
		"l": "0"
	  },
	  {
		"airline_code": "Q3",
		"airline_3lc": "MAZ",
		"iata_nr": "391",
		"name": "Zambian Airways",
		"l": "0"
	  },
	  {
		"airline_code": "Q4",
		"airline_3lc": "",
		"iata_nr": "213",
		"name": "Swazi Express Airways",
		"l": "0"
	  },
	  {
		"airline_code": "Q7",
		"airline_3lc": "SBM",
		"iata_nr": "",
		"name": "SkyBahamas Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "QA",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Cimber",
		"l": "0"
	  },
	  {
		"airline_code": "QC",
		"airline_3lc": "CRD",
		"iata_nr": "040",
		"name": "Camair-Co",
		"l": "0"
	  },
	  {
		"airline_code": "QD",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Grand Airways",
		"l": "0"
	  },
	  {
		"airline_code": "QE",
		"airline_3lc": "",
		"iata_nr": "894",
		"name": "Air Moorea",
		"l": "0"
	  },
	  {
		"airline_code": "QF",
		"airline_3lc": "QFA",
		"iata_nr": "081",
		"name": "Qantas Airways",
		"l": "1"
	  },
	  {
		"airline_code": "QG",
		"airline_3lc": "FLR",
		"iata_nr": "574",
		"name": "Dynamic Air",
		"l": "0"
	  },
	  {
		"airline_code": "QH",
		"airline_3lc": "LYN",
		"iata_nr": "",
		"name": "Qwestair",
		"l": "0"
	  },
	  {
		"airline_code": "QI",
		"airline_3lc": "CIM",
		"iata_nr": "647",
		"name": "Cimber Air",
		"l": "0"
	  },
	  {
		"airline_code": "QK",
		"airline_3lc": "JZA",
		"iata_nr": "983",
		"name": "Jazz Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "QL",
		"airline_3lc": "",
		"iata_nr": "383",
		"name": "Lesotho Airways",
		"l": "0"
	  },
	  {
		"airline_code": "QM",
		"airline_3lc": "AML",
		"iata_nr": "167",
		"name": "Air Malawi",
		"l": "0"
	  },
	  {
		"airline_code": "QO",
		"airline_3lc": "OGN",
		"iata_nr": "",
		"name": "Air Maroochy Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "QP",
		"airline_3lc": "",
		"iata_nr": "853",
		"name": "Airkenya Aviation Limited",
		"l": "0"
	  },
	  {
		"airline_code": "QQ",
		"airline_3lc": "FWQ",
		"iata_nr": "323",
		"name": "Reno Air",
		"l": "0"
	  },
	  {
		"airline_code": "QR",
		"airline_3lc": "QTR",
		"iata_nr": "157",
		"name": "Qatar Airways",
		"l": "0"
	  },
	  {
		"airline_code": "QS",
		"airline_3lc": "TVS",
		"iata_nr": "797",
		"name": "SmartWings",
		"l": "0"
	  },
	  {
		"airline_code": "QT",
		"airline_3lc": "",
		"iata_nr": "361",
		"name": "S A R Avions Taxis",
		"l": "0"
	  },
	  {
		"airline_code": "QU",
		"airline_3lc": "UGX",
		"iata_nr": "581",
		"name": "Uganda Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "QV",
		"airline_3lc": "LAO",
		"iata_nr": "627",
		"name": "Lao Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "QW",
		"airline_3lc": "BWG",
		"iata_nr": "",
		"name": "Blue Wings",
		"l": "0"
	  },
	  {
		"airline_code": "QX",
		"airline_3lc": "QXE",
		"iata_nr": "481",
		"name": "Horizon Air",
		"l": "0"
	  },
	  {
		"airline_code": "QZ",
		"airline_3lc": "AWQ",
		"iata_nr": "975",
		"name": "PT Indonesia AirAsia",
		"l": "0"
	  },
	  {
		"airline_code": "R3",
		"airline_3lc": "JSC",
		"iata_nr": "849",
		"name": "AIRCOMPANY YAKUTIA",
		"l": "0"
	  },
	  {
		"airline_code": "R4",
		"airline_3lc": "SDM",
		"iata_nr": "948",
		"name": "Russia",
		"l": "0"
	  },
	  {
		"airline_code": "R6",
		"airline_3lc": "SBK",
		"iata_nr": "207",
		"name": "Arizona Airways Inc",
		"l": "0"
	  },
	  {
		"airline_code": "R7",
		"airline_3lc": "OCA",
		"iata_nr": "717",
		"name": "Aserca Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "RA",
		"airline_3lc": "RNA",
		"iata_nr": "285",
		"name": "Nepal Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "RB",
		"airline_3lc": "SYR",
		"iata_nr": "070",
		"name": "Syrian Arab Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "RC",
		"airline_3lc": "FLI",
		"iata_nr": "767",
		"name": "Atlantic Airways Faroe Islands",
		"l": "0"
	  },
	  {
		"airline_code": "RE",
		"airline_3lc": "REA",
		"iata_nr": "809",
		"name": "Aer Arann",
		"l": "0"
	  },
	  {
		"airline_code": "RG",
		"airline_3lc": "VRG",
		"iata_nr": "042",
		"name": "Linhas Aereas",
		"l": "1"
	  },
	  {
		"airline_code": "RH",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Robin Hood Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "RI",
		"airline_3lc": "MDL",
		"iata_nr": "",
		"name": "P.T. Mandala Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "RJ",
		"airline_3lc": "RJA",
		"iata_nr": "512",
		"name": "Royal Jordanian",
		"l": "0"
	  },
	  {
		"airline_code": "RK",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Royal Khmer Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "RL",
		"airline_3lc": "SMS",
		"iata_nr": "",
		"name": "UltrAir",
		"l": "0"
	  },
	  {
		"airline_code": "RM",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Wings West Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "RN",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sum Air Services",
		"l": "0"
	  },
	  {
		"airline_code": "RO",
		"airline_3lc": "ROT",
		"iata_nr": "281",
		"name": "Tarom",
		"l": "0"
	  },
	  {
		"airline_code": "RP",
		"airline_3lc": "CHQ",
		"iata_nr": "363",
		"name": "CHAUTAUQUA AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "RQ",
		"airline_3lc": "KMR",
		"iata_nr": "384",
		"name": "Air Engiadina",
		"l": "0"
	  },
	  {
		"airline_code": "RR",
		"airline_3lc": "RFR",
		"iata_nr": "",
		"name": "Royal Air Force-38 Transport Group",
		"l": "0"
	  },
	  {
		"airline_code": "RS",
		"airline_3lc": "",
		"iata_nr": "360",
		"name": "Intercontinental De Aviacion",
		"l": "0"
	  },
	  {
		"airline_code": "RT",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Lincoln Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "RU",
		"airline_3lc": "SKI",
		"iata_nr": "025",
		"name": "Northern Commuter Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "RV",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Reeve Aleutian Airw",
		"l": "0"
	  },
	  {
		"airline_code": "RW",
		"airline_3lc": "RLD",
		"iata_nr": "586",
		"name": "Rheinland Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "RX",
		"airline_3lc": "",
		"iata_nr": "652",
		"name": "Regent Airways",
		"l": "0"
	  },
	  {
		"airline_code": "RY",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Rwanda",
		"l": "0"
	  },
	  {
		"airline_code": "S3",
		"airline_3lc": "",
		"iata_nr": "249",
		"name": "Santa Barbara Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "S5",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "SHUTTLE AMERICA",
		"l": "0"
	  },
	  {
		"airline_code": "S6",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Saint Martin",
		"l": "0"
	  },
	  {
		"airline_code": "S7",
		"airline_3lc": "SBI",
		"iata_nr": "421",
		"name": "S7 Airline",
		"l": "0"
	  },
	  {
		"airline_code": "SA",
		"airline_3lc": "SAA",
		"iata_nr": "083",
		"name": "South African Airways",
		"l": "1"
	  },
	  {
		"airline_code": "SB",
		"airline_3lc": "ACI",
		"iata_nr": "063",
		"name": "Aircalin",
		"l": "0"
	  },
	  {
		"airline_code": "SC",
		"airline_3lc": "CDG",
		"iata_nr": "324",
		"name": "Aeroposta",
		"l": "0"
	  },
	  {
		"airline_code": "SD",
		"airline_3lc": "SUD",
		"iata_nr": "200",
		"name": "Sudan Airways",
		"l": "0"
	  },
	  {
		"airline_code": "SE",
		"airline_3lc": "SEU",
		"iata_nr": "473",
		"name": "XL Airways France",
		"l": "0"
	  },
	  {
		"airline_code": "SF",
		"airline_3lc": "",
		"iata_nr": "515",
		"name": "Shanghai Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "SG",
		"airline_3lc": "JGO",
		"iata_nr": "",
		"name": "Sempati Air",
		"l": "0"
	  },
	  {
		"airline_code": "SH",
		"airline_3lc": "AIS",
		"iata_nr": "887",
		"name": "Shar Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "SI",
		"airline_3lc": "BCI",
		"iata_nr": "821",
		"name": "Blue Islands",
		"l": "0"
	  },
	  {
		"airline_code": "SK",
		"airline_3lc": "SAS",
		"iata_nr": "117",
		"name": "SAS Scandinavian Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "SL",
		"airline_3lc": "RSL",
		"iata_nr": "293",
		"name": "Rio Sul",
		"l": "0"
	  },
	  {
		"airline_code": "SM",
		"airline_3lc": "SWE",
		"iata_nr": "496",
		"name": "Taino Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "SN",
		"airline_3lc": "BEL",
		"iata_nr": "082",
		"name": "Brussels Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "SP",
		"airline_3lc": "SAT",
		"iata_nr": "737",
		"name": "SATA Air Acores",
		"l": "0"
	  },
	  {
		"airline_code": "SQ",
		"airline_3lc": "SIA",
		"iata_nr": "618",
		"name": "Singapore Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "SR",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Swissair",
		"l": "1"
	  },
	  {
		"airline_code": "ST",
		"airline_3lc": "GMI",
		"iata_nr": "",
		"name": "Germania",
		"l": "0"
	  },
	  {
		"airline_code": "SU",
		"airline_3lc": "AFL",
		"iata_nr": "555",
		"name": "Aeroflot",
		"l": "1"
	  },
	  {
		"airline_code": "SV",
		"airline_3lc": "SVA",
		"iata_nr": "065",
		"name": "Saudi Arabian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "SW",
		"airline_3lc": "NMB",
		"iata_nr": "186",
		"name": "Air Namibia",
		"l": "0"
	  },
	  {
		"airline_code": "SX",
		"airline_3lc": "SRK",
		"iata_nr": "772",
		"name": "SkyWork Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "SY",
		"airline_3lc": "SCX",
		"iata_nr": "337",
		"name": "Sun Country Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "SZ",
		"airline_3lc": "SMR",
		"iata_nr": "413",
		"name": "AIRCOMPANY SOMON AIR",
		"l": "0"
	  },
	  {
		"airline_code": "T2",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Bacia Amazonica Transportes TABA",
		"l": "0"
	  },
	  {
		"airline_code": "T3",
		"airline_3lc": "",
		"iata_nr": "467",
		"name": "Eastern Airways",
		"l": "0"
	  },
	  {
		"airline_code": "T4",
		"airline_3lc": "",
		"iata_nr": "661",
		"name": "TRIP LINHAS AEREAS",
		"l": "0"
	  },
	  {
		"airline_code": "T6",
		"airline_3lc": "TVR",
		"iata_nr": "204",
		"name": "Trinity Air Bahamas",
		"l": "0"
	  },
	  {
		"airline_code": "T8",
		"airline_3lc": "",
		"iata_nr": "839",
		"name": "Transportes Aeros Neuquen",
		"l": "0"
	  },
	  {
		"airline_code": "TA",
		"airline_3lc": "TAI",
		"iata_nr": "202",
		"name": "Taca International Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "TC",
		"airline_3lc": "ATC",
		"iata_nr": "197",
		"name": "Air Tanzania",
		"l": "0"
	  },
	  {
		"airline_code": "TE",
		"airline_3lc": "LIL",
		"iata_nr": "874",
		"name": "Lithuanian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "TF",
		"airline_3lc": "SCW",
		"iata_nr": "276",
		"name": "Malmo Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "TG",
		"airline_3lc": "THA",
		"iata_nr": "217",
		"name": "Thai Airways Intl",
		"l": "1"
	  },
	  {
		"airline_code": "TI",
		"airline_3lc": "BIA",
		"iata_nr": "",
		"name": "Baltic International Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "TJ",
		"airline_3lc": "",
		"iata_nr": "735",
		"name": "TAS Airways S P A",
		"l": "0"
	  },
	  {
		"airline_code": "TK",
		"airline_3lc": "THY",
		"iata_nr": "235",
		"name": "Turkish Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "TL",
		"airline_3lc": "ANO",
		"iata_nr": "935",
		"name": "AIRNORTH",
		"l": "0"
	  },
	  {
		"airline_code": "TM",
		"airline_3lc": "LAM",
		"iata_nr": "068",
		"name": "LAM Mozambique",
		"l": "0"
	  },
	  {
		"airline_code": "O7",
		"airline_3lc": "",
		"iata_nr": "441",
		"name": "Ozjet",
		"l": "0"
	  },
	  {
		"airline_code": "TP",
		"airline_3lc": "TAP",
		"iata_nr": "047",
		"name": "TAP Portugal",
		"l": "1"
	  },
	  {
		"airline_code": "TQ",
		"airline_3lc": "TDM",
		"iata_nr": "038",
		"name": "Tandem Aero",
		"l": "0"
	  },
	  {
		"airline_code": "TR",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "TransBrasil",
		"l": "0"
	  },
	  {
		"airline_code": "TS",
		"airline_3lc": "TSC",
		"iata_nr": "649",
		"name": "Air Transat",
		"l": "0"
	  },
	  {
		"airline_code": "TT",
		"airline_3lc": "KLA",
		"iata_nr": "843",
		"name": "Air Lithuania",
		"l": "0"
	  },
	  {
		"airline_code": "TU",
		"airline_3lc": "TAR",
		"iata_nr": "199",
		"name": "Tunis Air",
		"l": "0"
	  },
	  {
		"airline_code": "TW",
		"airline_3lc": "TWA",
		"iata_nr": "015",
		"name": "TWA Trans World Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "TY",
		"airline_3lc": "TPC",
		"iata_nr": "190",
		"name": "Air Caledonie",
		"l": "0"
	  },
	  {
		"airline_code": "TZ",
		"airline_3lc": "AMT",
		"iata_nr": "366",
		"name": "American Trans Air",
		"l": "0"
	  },
	  {
		"airline_code": "U2",
		"airline_3lc": "EZY",
		"iata_nr": "",
		"name": "Easyjet",
		"l": "0"
	  },
	  {
		"airline_code": "U6",
		"airline_3lc": "SVR",
		"iata_nr": "262",
		"name": "Ural Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "UA",
		"airline_3lc": "UAL",
		"iata_nr": "016",
		"name": "United Airlines",
		"l": "1"
	  },
	  {
		"airline_code": "UB",
		"airline_3lc": "UBA",
		"iata_nr": "209",
		"name": "Myanma Airways International",
		"l": "0"
	  },
	  {
		"airline_code": "UC",
		"airline_3lc": "LCO",
		"iata_nr": "145",
		"name": "Lan Chile Cargo",
		"l": "0"
	  },
	  {
		"airline_code": "UD",
		"airline_3lc": "HER",
		"iata_nr": "848",
		"name": "Hex Air",
		"l": "0"
	  },
	  {
		"airline_code": "UE",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air LA",
		"l": "0"
	  },
	  {
		"airline_code": "UF",
		"airline_3lc": "UKM",
		"iata_nr": "828",
		"name": "SARO Servicios Aereos Rutas Oriente",
		"l": "0"
	  },
	  {
		"airline_code": "UG",
		"airline_3lc": "TUI",
		"iata_nr": "150",
		"name": "Sevenair",
		"l": "0"
	  },
	  {
		"airline_code": "UH",
		"airline_3lc": "EUS",
		"iata_nr": "116",
		"name": "US Helicopter Corp",
		"l": "0"
	  },
	  {
		"airline_code": "UI",
		"airline_3lc": "ECA",
		"iata_nr": "",
		"name": "NorlandAir",
		"l": "0"
	  },
	  {
		"airline_code": "UJ",
		"airline_3lc": "LMU",
		"iata_nr": "110",
		"name": "ALMASRIA UNIVERSAL AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "UK",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air UK",
		"l": "0"
	  },
	  {
		"airline_code": "UL",
		"airline_3lc": "ALK",
		"iata_nr": "603",
		"name": "SriLankan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "UM",
		"airline_3lc": "AZW",
		"iata_nr": "168",
		"name": "Air Zimbabwe",
		"l": "0"
	  },
	  {
		"airline_code": "UN",
		"airline_3lc": "TSO",
		"iata_nr": "670",
		"name": "Transaero Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "UO",
		"airline_3lc": "HKE",
		"iata_nr": "128",
		"name": "Hongkong Express Airways",
		"l": "0"
	  },
	  {
		"airline_code": "UP",
		"airline_3lc": "BHS",
		"iata_nr": "111",
		"name": "Bahamasair",
		"l": "0"
	  },
	  {
		"airline_code": "UR",
		"airline_3lc": "",
		"iata_nr": "454",
		"name": "British International Helicopters",
		"l": "0"
	  },
	  {
		"airline_code": "US",
		"airline_3lc": "USA",
		"iata_nr": "037",
		"name": "US Airways",
		"l": "1"
	  },
	  {
		"airline_code": "UU",
		"airline_3lc": "REU",
		"iata_nr": "760",
		"name": "Air Austral",
		"l": "0"
	  },
	  {
		"airline_code": "UV",
		"airline_3lc": "",
		"iata_nr": "662",
		"name": "Air Kangaroo Island",
		"l": "0"
	  },
	  {
		"airline_code": "UW",
		"airline_3lc": "UVG",
		"iata_nr": "751",
		"name": "Perimeter Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "UX",
		"airline_3lc": "",
		"iata_nr": "996",
		"name": "Air Europa",
		"l": "0"
	  },
	  {
		"airline_code": "UY",
		"airline_3lc": "UYC",
		"iata_nr": "604",
		"name": "Cameroon Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "V2",
		"airline_3lc": "AKT",
		"iata_nr": "501",
		"name": "Valdresfly A.S.",
		"l": "0"
	  },
	  {
		"airline_code": "V4",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "VLM Vlaamse Luchttransportmaatschap",
		"l": "0"
	  },
	  {
		"airline_code": "VA",
		"airline_3lc": "VAU",
		"iata_nr": "795",
		"name": "V Australia",
		"l": "0"
	  },
	  {
		"airline_code": "VB",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "DUO Airways",
		"l": "0"
	  },
	  {
		"airline_code": "VC",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Servivensa",
		"l": "0"
	  },
	  {
		"airline_code": "VD",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Liberte",
		"l": "0"
	  },
	  {
		"airline_code": "VE",
		"airline_3lc": "VLE",
		"iata_nr": "",
		"name": "C.A.I. Second S.P.A",
		"l": "0"
	  },
	  {
		"airline_code": "VG",
		"airline_3lc": "VLM",
		"iata_nr": "978",
		"name": "VLM Nederland B.V.",
		"l": "0"
	  },
	  {
		"airline_code": "VH",
		"airline_3lc": "",
		"iata_nr": "152",
		"name": "Aeropostal",
		"l": "0"
	  },
	  {
		"airline_code": "VI",
		"airline_3lc": "VES",
		"iata_nr": "381",
		"name": "Vieques Air Link",
		"l": "0"
	  },
	  {
		"airline_code": "VJ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Kampuchea Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "VK",
		"airline_3lc": "VGN",
		"iata_nr": "786",
		"name": "AIR NIGERIA",
		"l": "0"
	  },
	  {
		"airline_code": "VM",
		"airline_3lc": "",
		"iata_nr": "424",
		"name": "Regional Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "VN",
		"airline_3lc": "HVN",
		"iata_nr": "738",
		"name": "Vietnam Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "VO",
		"airline_3lc": "TYR",
		"iata_nr": "734",
		"name": "Tyrolean Airways",
		"l": "1"
	  },
	  {
		"airline_code": "VP",
		"airline_3lc": "VQI",
		"iata_nr": "",
		"name": "Villa Air",
		"l": "0"
	  },
	  {
		"airline_code": "VQ",
		"airline_3lc": "VKH",
		"iata_nr": "904",
		"name": "VIKING HELLAS AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "VR",
		"airline_3lc": "TCV",
		"iata_nr": "696",
		"name": "TACV Cabo Verde Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "VS",
		"airline_3lc": "VIR",
		"iata_nr": "932",
		"name": "Virgin Atlantic",
		"l": "1"
	  },
	  {
		"airline_code": "VT",
		"airline_3lc": "VTA",
		"iata_nr": "135",
		"name": "Air Tahiti",
		"l": "0"
	  },
	  {
		"airline_code": "VU",
		"airline_3lc": "",
		"iata_nr": "943",
		"name": "Air Ivoire",
		"l": "0"
	  },
	  {
		"airline_code": "VW",
		"airline_3lc": "TAO",
		"iata_nr": "942",
		"name": "Transportes Aeromar",
		"l": "0"
	  },
	  {
		"airline_code": "VX",
		"airline_3lc": "VRD",
		"iata_nr": "984",
		"name": "Virgin America",
		"l": "0"
	  },
	  {
		"airline_code": "VY",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Vueling",
		"l": "0"
	  },
	  {
		"airline_code": "W2",
		"airline_3lc": "FXT",
		"iata_nr": "365",
		"name": "Flexflight",
		"l": "0"
	  },
	  {
		"airline_code": "W3",
		"airline_3lc": "ARA",
		"iata_nr": "725",
		"name": "Arik Air International",
		"l": "0"
	  },
	  {
		"airline_code": "WA",
		"airline_3lc": "KLC",
		"iata_nr": "",
		"name": "KLM Cityhopper",
		"l": "0"
	  },
	  {
		"airline_code": "WB",
		"airline_3lc": "RWD",
		"iata_nr": "459",
		"name": "Rwandair Express",
		"l": "0"
	  },
	  {
		"airline_code": "WC",
		"airline_3lc": "ISV",
		"iata_nr": "",
		"name": "Islena Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "WE",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Rheintalflug",
		"l": "0"
	  },
	  {
		"airline_code": "WF",
		"airline_3lc": "WIF",
		"iata_nr": "701",
		"name": "Wideroe",
		"l": "0"
	  },
	  {
		"airline_code": "WG",
		"airline_3lc": "WSG",
		"iata_nr": "",
		"name": "Taiwan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "WH",
		"airline_3lc": "CNW",
		"iata_nr": "783",
		"name": "China Northwest",
		"l": "0"
	  },
	  {
		"airline_code": "WI",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Rottnest Airbus",
		"l": "0"
	  },
	  {
		"airline_code": "WJ",
		"airline_3lc": "LAL",
		"iata_nr": "927",
		"name": "Laborador Airways",
		"l": "0"
	  },
	  {
		"airline_code": "WK",
		"airline_3lc": "EDW",
		"iata_nr": "",
		"name": "Edelweiss Air",
		"l": "0"
	  },
	  {
		"airline_code": "WL",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aeroperlas",
		"l": "0"
	  },
	  {
		"airline_code": "WM",
		"airline_3lc": "WIA",
		"iata_nr": "295",
		"name": "Windward Island Airways",
		"l": "0"
	  },
	  {
		"airline_code": "WN",
		"airline_3lc": "SWA",
		"iata_nr": "526",
		"name": "Southwest Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "WP",
		"airline_3lc": "",
		"iata_nr": "347",
		"name": "Aloha Islandair Inc",
		"l": "0"
	  },
	  {
		"airline_code": "WQ",
		"airline_3lc": "RMV",
		"iata_nr": "534",
		"name": "Service Aerien Francias",
		"l": "0"
	  },
	  {
		"airline_code": "WR",
		"airline_3lc": "HRH",
		"iata_nr": "971",
		"name": "Royal Tongan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "WT",
		"airline_3lc": "NGA",
		"iata_nr": "087",
		"name": "Nigeria Airways",
		"l": "0"
	  },
	  {
		"airline_code": "WU",
		"airline_3lc": "",
		"iata_nr": "795",
		"name": "Wuhan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "WW",
		"airline_3lc": "BMI",
		"iata_nr": "",
		"name": "BMI Baby",
		"l": "0"
	  },
	  {
		"airline_code": "WX",
		"airline_3lc": "BCY",
		"iata_nr": "689",
		"name": "Cityjet",
		"l": "0"
	  },
	  {
		"airline_code": "WY",
		"airline_3lc": "OAS",
		"iata_nr": "910",
		"name": "Oman Air",
		"l": "0"
	  },
	  {
		"airline_code": "X2",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "China Xinhua Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "XF",
		"airline_3lc": "VLK",
		"iata_nr": "277",
		"name": "Australia Air International",
		"l": "0"
	  },
	  {
		"airline_code": "XJ",
		"airline_3lc": "MES",
		"iata_nr": "582",
		"name": "Mesaba Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "XK",
		"airline_3lc": "CCM",
		"iata_nr": "146",
		"name": "Air Corsica",
		"l": "0"
	  },
	  {
		"airline_code": "XL",
		"airline_3lc": "LNE",
		"iata_nr": "462",
		"name": "Lanecuador Aerolane SA",
		"l": "0"
	  },
	  {
		"airline_code": "XO",
		"airline_3lc": "CXJ",
		"iata_nr": "651",
		"name": "Xinjiang Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "XP",
		"airline_3lc": "CXP",
		"iata_nr": "",
		"name": "Casino Express Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "XQ",
		"airline_3lc": "",
		"iata_nr": "564",
		"name": "Sun Express",
		"l": "0"
	  },
	  {
		"airline_code": "XT",
		"airline_3lc": "SKT",
		"iata_nr": "893",
		"name": "Skystar Airways",
		"l": "0"
	  },
	  {
		"airline_code": "XU",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Link Airways",
		"l": "0"
	  },
	  {
		"airline_code": "XV",
		"airline_3lc": "",
		"iata_nr": "351",
		"name": "Air Express I Norrkoping",
		"l": "0"
	  },
	  {
		"airline_code": "XW",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sky Express",
		"l": "0"
	  },
	  {
		"airline_code": "XY",
		"airline_3lc": "KNE",
		"iata_nr": "593",
		"name": "Ryan Air",
		"l": "0"
	  },
	  {
		"airline_code": "XZ",
		"airline_3lc": "EXY",
		"iata_nr": "",
		"name": "SOUTH AFRICAN EXPRESS",
		"l": "0"
	  },
	  {
		"airline_code": "YC",
		"airline_3lc": "LLM",
		"iata_nr": "",
		"name": "YAMAL AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "YD",
		"airline_3lc": "MTW",
		"iata_nr": "425",
		"name": "Mauritania Airways",
		"l": "0"
	  },
	  {
		"airline_code": "YE",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Grand Canyon Airlines Inc.",
		"l": "0"
	  },
	  {
		"airline_code": "YH",
		"airline_3lc": "WCW",
		"iata_nr": "600",
		"name": "Air Baffin",
		"l": "0"
	  },
	  {
		"airline_code": "YI",
		"airline_3lc": "RSI",
		"iata_nr": "806",
		"name": "Air Sunshine",
		"l": "0"
	  },
	  {
		"airline_code": "YJ",
		"airline_3lc": "AMV",
		"iata_nr": "",
		"name": "AMC Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "YK",
		"airline_3lc": "KYV",
		"iata_nr": "056",
		"name": "Cyprus Turkish Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "YL",
		"airline_3lc": "BSA",
		"iata_nr": "722",
		"name": "Air Bissau International",
		"l": "0"
	  },
	  {
		"airline_code": "YM",
		"airline_3lc": "MGX",
		"iata_nr": "409",
		"name": "Montenegro Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "YN",
		"airline_3lc": "CRQ",
		"iata_nr": "219",
		"name": "Air Creebec",
		"l": "0"
	  },
	  {
		"airline_code": "YO",
		"airline_3lc": "MCM",
		"iata_nr": "747",
		"name": "Heli Air Monaco",
		"l": "0"
	  },
	  {
		"airline_code": "YP",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aero Lloyd",
		"l": "1"
	  },
	  {
		"airline_code": "YQ",
		"airline_3lc": "",
		"iata_nr": "342",
		"name": "Air Company Polet",
		"l": "0"
	  },
	  {
		"airline_code": "YR",
		"airline_3lc": "EGJ",
		"iata_nr": "398",
		"name": "Scenic Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "YS",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Hop Regional",
		"l": "0"
	  },
	  {
		"airline_code": "YT",
		"airline_3lc": "",
		"iata_nr": "030",
		"name": "Skywest Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "YU",
		"airline_3lc": "",
		"iata_nr": "725",
		"name": "Aerolineas Dominicanas",
		"l": "0"
	  },
	  {
		"airline_code": "YV",
		"airline_3lc": "ASH",
		"iata_nr": "533",
		"name": "Mesa Airlines Inc",
		"l": "0"
	  },
	  {
		"airline_code": "YX",
		"airline_3lc": "MEP",
		"iata_nr": "453",
		"name": "Republic Airline",
		"l": "0"
	  },
	  {
		"airline_code": "YZ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Transportes da Guine",
		"l": "0"
	  },
	  {
		"airline_code": "Z2",
		"airline_3lc": "STY",
		"iata_nr": "536",
		"name": "Styrian Spirit",
		"l": "0"
	  },
	  {
		"airline_code": "ZA",
		"airline_3lc": "",
		"iata_nr": "433",
		"name": "Interavia Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "ZB",
		"airline_3lc": "MON",
		"iata_nr": "974",
		"name": "Monarch Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "ZC",
		"airline_3lc": "RSN",
		"iata_nr": "141",
		"name": "Royal Swazi National",
		"l": "0"
	  },
	  {
		"airline_code": "ZD",
		"airline_3lc": "",
		"iata_nr": "338",
		"name": "Ross Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "ZE",
		"airline_3lc": "ESR",
		"iata_nr": "839",
		"name": "Eaststar Jet",
		"l": "0"
	  },
	  {
		"airline_code": "ZF",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Airborne of Sweden",
		"l": "0"
	  },
	  {
		"airline_code": "ZG",
		"airline_3lc": "AEJ",
		"iata_nr": "379",
		"name": "Sabair Airlines Pty. Ltd.",
		"l": "0"
	  },
	  {
		"airline_code": "ZI",
		"airline_3lc": "AAF",
		"iata_nr": "439",
		"name": "Aigle Azur",
		"l": "0"
	  },
	  {
		"airline_code": "ZJ",
		"airline_3lc": "TED",
		"iata_nr": "",
		"name": "Teddy Air A\/S",
		"l": "0"
	  },
	  {
		"airline_code": "ZK",
		"airline_3lc": "GLA",
		"iata_nr": "846",
		"name": "Great Lakes Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "ZL",
		"airline_3lc": "HZL",
		"iata_nr": "899",
		"name": "REGIONAL EXPRESS",
		"l": "0"
	  },
	  {
		"airline_code": "ZM",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Scibe Airlift",
		"l": "0"
	  },
	  {
		"airline_code": "ZO",
		"airline_3lc": "GRP",
		"iata_nr": "507",
		"name": "Southflight Aviation Limited",
		"l": "0"
	  },
	  {
		"airline_code": "ZP",
		"airline_3lc": "VIG",
		"iata_nr": "315",
		"name": "Virgin Air",
		"l": "0"
	  },
	  {
		"airline_code": "ZQ",
		"airline_3lc": "",
		"iata_nr": "521",
		"name": "Ansett New Zealand",
		"l": "0"
	  },
	  {
		"airline_code": "ZR",
		"airline_3lc": "KKH",
		"iata_nr": "451",
		"name": "Alexandria Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "ZS",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sama Airways",
		"l": "0"
	  },
	  {
		"airline_code": "ZT",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Titan Airways",
		"l": "0"
	  },
	  {
		"airline_code": "ZU",
		"airline_3lc": "HCY",
		"iata_nr": "032",
		"name": "Helios Airways",
		"l": "0"
	  },
	  {
		"airline_code": "ZV",
		"airline_3lc": "AMW",
		"iata_nr": "471",
		"name": "Air Midwest",
		"l": "0"
	  },
	  {
		"airline_code": "ZW",
		"airline_3lc": "AWI",
		"iata_nr": "303",
		"name": "Air Wisconsin",
		"l": "0"
	  },
	  {
		"airline_code": "ZX",
		"airline_3lc": "GGN",
		"iata_nr": "",
		"name": "Air Alliance",
		"l": "0"
	  },
	  {
		"airline_code": "ZY",
		"airline_3lc": "SHY",
		"iata_nr": "438",
		"name": "German Sky Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "YY",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "YY",
		"l": "0"
	  },
	  {
		"airline_code": "AP",
		"airline_3lc": "",
		"iata_nr": "867",
		"name": "Air One",
		"l": "0"
	  },
	  {
		"airline_code": "HR",
		"airline_3lc": "HHN",
		"iata_nr": "169",
		"name": "Hahn Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "A3",
		"airline_3lc": "AEE",
		"iata_nr": "390",
		"name": "Aegean Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "C6",
		"airline_3lc": "CJA",
		"iata_nr": "",
		"name": "Canjet Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "BT",
		"airline_3lc": "",
		"iata_nr": "657",
		"name": "Air Baltic",
		"l": "0"
	  },
	  {
		"airline_code": "N2",
		"airline_3lc": "",
		"iata_nr": "394",
		"name": "Daghestan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "Z8",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Pulkovo Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "PE",
		"airline_3lc": "PEV",
		"iata_nr": "335",
		"name": "PEOPLES VIENNALINE",
		"l": "0"
	  },
	  {
		"airline_code": "A6",
		"airline_3lc": "LPV",
		"iata_nr": "527",
		"name": "Air Alps Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "*A",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Star Alliance",
		"l": "0"
	  },
	  {
		"airline_code": "X3",
		"airline_3lc": "BKL",
		"iata_nr": "",
		"name": "Tuifly",
		"l": "0"
	  },
	  {
		"airline_code": "B6",
		"airline_3lc": "JBU",
		"iata_nr": "279",
		"name": "JetBlue Airways",
		"l": "0"
	  },
	  {
		"airline_code": "NB",
		"airline_3lc": "",
		"iata_nr": "373",
		"name": "Sterling",
		"l": "0"
	  },
	  {
		"airline_code": "BE",
		"airline_3lc": "BEE",
		"iata_nr": "267",
		"name": "Flybe",
		"l": "0"
	  },
	  {
		"airline_code": "SS",
		"airline_3lc": "",
		"iata_nr": "923",
		"name": "Corsair International",
		"l": "0"
	  },
	  {
		"airline_code": "VZ",
		"airline_3lc": "AIH",
		"iata_nr": "727",
		"name": "my Travel Lite",
		"l": "0"
	  },
	  {
		"airline_code": "AX",
		"airline_3lc": "LOF",
		"iata_nr": "414",
		"name": "TRANS STATES AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "A2",
		"airline_3lc": "EXD",
		"iata_nr": "529",
		"name": "Cielos Del Peru",
		"l": "0"
	  },
	  {
		"airline_code": "A4",
		"airline_3lc": "",
		"iata_nr": "702",
		"name": "AEROCOMERCIAL ORIENTE NORTE",
		"l": "0"
	  },
	  {
		"airline_code": "A5",
		"airline_3lc": "HOP",
		"iata_nr": "163",
		"name": "HOP",
		"l": "0"
	  },
	  {
		"airline_code": "A7",
		"airline_3lc": "MPD",
		"iata_nr": "352",
		"name": "Air Comet",
		"l": "0"
	  },
	  {
		"airline_code": "A8",
		"airline_3lc": "",
		"iata_nr": "719",
		"name": "Benin Golf Air",
		"l": "0"
	  },
	  {
		"airline_code": "A9",
		"airline_3lc": "",
		"iata_nr": "606",
		"name": "Airzena Georgian Airline",
		"l": "0"
	  },
	  {
		"airline_code": "BB",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Seaborne Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "BC",
		"airline_3lc": "SKY",
		"iata_nr": "",
		"name": "Skymark Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "BQ",
		"airline_3lc": "ROM",
		"iata_nr": "926",
		"name": "Aeromar",
		"l": "0"
	  },
	  {
		"airline_code": "BS",
		"airline_3lc": "BIH",
		"iata_nr": "",
		"name": "British International",
		"l": "0"
	  },
	  {
		"airline_code": "I1",
		"airline_3lc": "",
		"iata_nr": "489",
		"name": "Cts Viaggi",
		"l": "0"
	  },
	  {
		"airline_code": "BZ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Keystone Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "B3",
		"airline_3lc": "BLV",
		"iata_nr": "",
		"name": "Bellview Airline\/Nigeria",
		"l": "0"
	  },
	  {
		"airline_code": "B5",
		"airline_3lc": "",
		"iata_nr": "595",
		"name": "Amadeus Flugdienst",
		"l": "0"
	  },
	  {
		"airline_code": "CC",
		"airline_3lc": "",
		"iata_nr": "374",
		"name": "Macair Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "CD",
		"airline_3lc": "LLR",
		"iata_nr": "296",
		"name": "Alliance Air",
		"l": "0"
	  },
	  {
		"airline_code": "CT",
		"airline_3lc": "CYL",
		"iata_nr": "",
		"name": "Alitalia City Liner Spa",
		"l": "0"
	  },
	  {
		"airline_code": "C3",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Trade Air",
		"l": "0"
	  },
	  {
		"airline_code": "C5",
		"airline_3lc": "UCA",
		"iata_nr": "841",
		"name": "Champlain Enterprises",
		"l": "0"
	  },
	  {
		"airline_code": "C7",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Rla Rico Linhas Areas",
		"l": "0"
	  },
	  {
		"airline_code": "DA",
		"airline_3lc": "GEO",
		"iata_nr": "500",
		"name": "Air Georgia",
		"l": "0"
	  },
	  {
		"airline_code": "DG",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "South East Asian Airline",
		"l": "0"
	  },
	  {
		"airline_code": "DN",
		"airline_3lc": "",
		"iata_nr": "440",
		"name": "Senegal Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "DR",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Link",
		"l": "0"
	  },
	  {
		"airline_code": "D4",
		"airline_3lc": "LID",
		"iata_nr": "",
		"name": "Alidaunia S.r.l.",
		"l": "0"
	  },
	  {
		"airline_code": "D7",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Airasiax SDN BHD",
		"l": "0"
	  },
	  {
		"airline_code": "D9",
		"airline_3lc": "DNV",
		"iata_nr": "733",
		"name": "DONAVIA",
		"l": "0"
	  },
	  {
		"airline_code": "EA",
		"airline_3lc": "EHN",
		"iata_nr": "",
		"name": "EAST HORIZON AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "EB",
		"airline_3lc": "",
		"iata_nr": "460",
		"name": "Pullmantur Air",
		"l": "0"
	  },
	  {
		"airline_code": "EC",
		"airline_3lc": "",
		"iata_nr": "284",
		"name": "Openskies",
		"l": "0"
	  },
	  {
		"airline_code": "EO",
		"airline_3lc": "EZR",
		"iata_nr": "",
		"name": "Hewa Bora Airways",
		"l": "0"
	  },
	  {
		"airline_code": "EP",
		"airline_3lc": "IRC",
		"iata_nr": "815",
		"name": "Iran Asseman Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "ER",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air East",
		"l": "0"
	  },
	  {
		"airline_code": "EU",
		"airline_3lc": "EEA",
		"iata_nr": "341",
		"name": "Ecuatoriana",
		"l": "0"
	  },
	  {
		"airline_code": "E3",
		"airline_3lc": "DMO",
		"iata_nr": "497",
		"name": "Domodedovo Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "E6",
		"airline_3lc": "",
		"iata_nr": "241",
		"name": "Avia Express Cruise",
		"l": "0"
	  },
	  {
		"airline_code": "E9",
		"airline_3lc": "",
		"iata_nr": "385",
		"name": "Boston Maine",
		"l": "0"
	  },
	  {
		"airline_code": "FD",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Thai Airasia",
		"l": "0"
	  },
	  {
		"airline_code": "FH",
		"airline_3lc": "FUA",
		"iata_nr": "",
		"name": "Futura International",
		"l": "0"
	  },
	  {
		"airline_code": "FL",
		"airline_3lc": "",
		"iata_nr": "332",
		"name": "Airtran Airways",
		"l": "0"
	  },
	  {
		"airline_code": "FM",
		"airline_3lc": "",
		"iata_nr": "774",
		"name": "Shanghai Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "FO",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Airlines Of Tasmania",
		"l": "0"
	  },
	  {
		"airline_code": "FP",
		"airline_3lc": "FRE",
		"iata_nr": "",
		"name": "Freedom Air",
		"l": "0"
	  },
	  {
		"airline_code": "FT",
		"airline_3lc": "SRH",
		"iata_nr": "084",
		"name": "Siem Reap Airways Intl",
		"l": "0"
	  },
	  {
		"airline_code": "FY",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Flyfirefly",
		"l": "0"
	  },
	  {
		"airline_code": "F2",
		"airline_3lc": "FLM",
		"iata_nr": "",
		"name": "Fly Hava Yollari",
		"l": "0"
	  },
	  {
		"airline_code": "F6",
		"airline_3lc": "FCC",
		"iata_nr": "425",
		"name": "First Cambodia Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "F7",
		"airline_3lc": "",
		"iata_nr": "033",
		"name": "ETIHAD REGIONAL",
		"l": "0"
	  },
	  {
		"airline_code": "F8",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Freedom Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "GK",
		"airline_3lc": "",
		"iata_nr": "798",
		"name": "Jetstar Japan",
		"l": "0"
	  },
	  {
		"airline_code": "GM",
		"airline_3lc": "SVK",
		"iata_nr": "",
		"name": "Air Slovakia",
		"l": "0"
	  },
	  {
		"airline_code": "GS",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Grant Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "G2",
		"airline_3lc": "VXG",
		"iata_nr": "",
		"name": "Avirex Gabon",
		"l": "0"
	  },
	  {
		"airline_code": "G3",
		"airline_3lc": "GLO",
		"iata_nr": "127",
		"name": "Gol Transportes Aereos",
		"l": "0"
	  },
	  {
		"airline_code": "G4",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Allegiant Air",
		"l": "0"
	  },
	  {
		"airline_code": "G6",
		"airline_3lc": "AKW",
		"iata_nr": "397",
		"name": "Angkor Airways",
		"l": "0"
	  },
	  {
		"airline_code": "HH",
		"airline_3lc": "ICB",
		"iata_nr": "",
		"name": "Islandsflug",
		"l": "0"
	  },
	  {
		"airline_code": "HK",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Hamburg Airways",
		"l": "0"
	  },
	  {
		"airline_code": "HN",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Proteus Helicopteres",
		"l": "0"
	  },
	  {
		"airline_code": "HS",
		"airline_3lc": "HIL",
		"iata_nr": "",
		"name": "Svenska Direktflyg",
		"l": "0"
	  },
	  {
		"airline_code": "HU",
		"airline_3lc": "CHH",
		"iata_nr": "880",
		"name": "Hainan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "H5",
		"airline_3lc": "MVL",
		"iata_nr": "428",
		"name": "Magadan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "H6",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Hageland Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "H7",
		"airline_3lc": "EGU",
		"iata_nr": "",
		"name": "Eagle Air",
		"l": "0"
	  },
	  {
		"airline_code": "H8",
		"airline_3lc": "KHB",
		"iata_nr": "560",
		"name": "Dalavia",
		"l": "0"
	  },
	  {
		"airline_code": "ID",
		"airline_3lc": "",
		"iata_nr": "431",
		"name": "Interlink Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IH",
		"airline_3lc": "FCN",
		"iata_nr": "759",
		"name": "Falcon Air",
		"l": "0"
	  },
	  {
		"airline_code": "IN",
		"airline_3lc": "MAK",
		"iata_nr": "367",
		"name": "Macedonian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IO",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Pt Indonesian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "IV",
		"airline_3lc": "JET",
		"iata_nr": "",
		"name": "Wind Jet",
		"l": "0"
	  },
	  {
		"airline_code": "JW",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Skippers Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "JX",
		"airline_3lc": "",
		"iata_nr": "179",
		"name": "Nice Helicopteres",
		"l": "0"
	  },
	  {
		"airline_code": "J3",
		"airline_3lc": "PLR",
		"iata_nr": "325",
		"name": "Northwestern Air Lease",
		"l": "0"
	  },
	  {
		"airline_code": "J4",
		"airline_3lc": "",
		"iata_nr": "488",
		"name": "Jet For You",
		"l": "0"
	  },
	  {
		"airline_code": "J5",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Alaska Seaplane Service",
		"l": "0"
	  },
	  {
		"airline_code": "J6",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Larrys Flying Service",
		"l": "0"
	  },
	  {
		"airline_code": "KC",
		"airline_3lc": "KZR",
		"iata_nr": "465",
		"name": "Air Astana",
		"l": "0"
	  },
	  {
		"airline_code": "KJ",
		"airline_3lc": "AII",
		"iata_nr": "994",
		"name": "Air Incheon",
		"l": "0"
	  },
	  {
		"airline_code": "K2",
		"airline_3lc": "ELO",
		"iata_nr": "",
		"name": "Eurolot",
		"l": "0"
	  },
	  {
		"airline_code": "K3",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Taquan Air Services",
		"l": "0"
	  },
	  {
		"airline_code": "K4",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Kronflyg",
		"l": "0"
	  },
	  {
		"airline_code": "K5",
		"airline_3lc": "",
		"iata_nr": "609",
		"name": "SEAPORT\/WINGS OF ALASKA",
		"l": "0"
	  },
	  {
		"airline_code": "LE",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Laparkan Airways Inc",
		"l": "0"
	  },
	  {
		"airline_code": "LQ",
		"airline_3lc": "",
		"iata_nr": "228",
		"name": "Air Guinea Cargo",
		"l": "0"
	  },
	  {
		"airline_code": "L3",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Ltu Billa Lufttransport",
		"l": "0"
	  },
	  {
		"airline_code": "L4",
		"airline_3lc": "LDI",
		"iata_nr": "372",
		"name": "Lauda Air Spa",
		"l": "0"
	  },
	  {
		"airline_code": "L6",
		"airline_3lc": "",
		"iata_nr": "495",
		"name": "MAURITANIAN AIRLINES INT",
		"l": "0"
	  },
	  {
		"airline_code": "L7",
		"airline_3lc": "LPN",
		"iata_nr": "",
		"name": "Laoag Intl Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "MC",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Mobility Command",
		"l": "0"
	  },
	  {
		"airline_code": "MT",
		"airline_3lc": "TCX",
		"iata_nr": "727",
		"name": "Thomas Cook Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "MY",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "MAYA ISLAND AIR",
		"l": "0"
	  },
	  {
		"airline_code": "M2",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Mahfooz Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "M3",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "North Flying A.s.",
		"l": "0"
	  },
	  {
		"airline_code": "M4",
		"airline_3lc": "AXX",
		"iata_nr": "743",
		"name": "Avioimpex",
		"l": "0"
	  },
	  {
		"airline_code": "M5",
		"airline_3lc": "KEN",
		"iata_nr": "763",
		"name": "Kenmore Air",
		"l": "0"
	  },
	  {
		"airline_code": "M7",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Tropical Airways D Haiti",
		"l": "0"
	  },
	  {
		"airline_code": "NC",
		"airline_3lc": "NJS",
		"iata_nr": "",
		"name": "National Jet Systems",
		"l": "0"
	  },
	  {
		"airline_code": "NT",
		"airline_3lc": "IBB",
		"iata_nr": "474",
		"name": "Binter Canarias",
		"l": "0"
	  },
	  {
		"airline_code": "NX",
		"airline_3lc": "AMU",
		"iata_nr": "675",
		"name": "Air Macau",
		"l": "0"
	  },
	  {
		"airline_code": "NY",
		"airline_3lc": "FNA",
		"iata_nr": "882",
		"name": "Air Iceland",
		"l": "0"
	  },
	  {
		"airline_code": "N3",
		"airline_3lc": "OMS",
		"iata_nr": "753",
		"name": "Omskavia",
		"l": "0"
	  },
	  {
		"airline_code": "N5",
		"airline_3lc": "KRS",
		"iata_nr": "",
		"name": "Skagway Air Service Inc",
		"l": "0"
	  },
	  {
		"airline_code": "N7",
		"airline_3lc": "",
		"iata_nr": "504",
		"name": "Lagun Air",
		"l": "0"
	  },
	  {
		"airline_code": "N8",
		"airline_3lc": "",
		"iata_nr": "851",
		"name": "Cr Airways",
		"l": "0"
	  },
	  {
		"airline_code": "OB",
		"airline_3lc": "",
		"iata_nr": "930",
		"name": "BOLIVIANA DE AVIACION",
		"l": "0"
	  },
	  {
		"airline_code": "OI",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aspiring Air Services",
		"l": "0"
	  },
	  {
		"airline_code": "OP",
		"airline_3lc": "CHK",
		"iata_nr": "370",
		"name": "Chalk Ocean Airways",
		"l": "0"
	  },
	  {
		"airline_code": "OX",
		"airline_3lc": "",
		"iata_nr": "578",
		"name": "Orient Thai Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "PA",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Florida Coastal Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "PF",
		"airline_3lc": "PNW",
		"iata_nr": "400",
		"name": "Palestinian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "PJ",
		"airline_3lc": "",
		"iata_nr": "638",
		"name": "Air St Pierre",
		"l": "0"
	  },
	  {
		"airline_code": "PP",
		"airline_3lc": "PJS",
		"iata_nr": "",
		"name": "Jet Aviation Business",
		"l": "0"
	  },
	  {
		"airline_code": "PQ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Panafrican Airways",
		"l": "0"
	  },
	  {
		"airline_code": "PW",
		"airline_3lc": "",
		"iata_nr": "031",
		"name": "Precision Air",
		"l": "0"
	  },
	  {
		"airline_code": "PZ",
		"airline_3lc": "LAP",
		"iata_nr": "692",
		"name": "TAM Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "P2",
		"airline_3lc": "TMN",
		"iata_nr": "298",
		"name": "Utair Aviation Jsc",
		"l": "0"
	  },
	  {
		"airline_code": "P4",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aero Lineas Sosa",
		"l": "0"
	  },
	  {
		"airline_code": "P6",
		"airline_3lc": "",
		"iata_nr": "356",
		"name": "Trans Air",
		"l": "0"
	  },
	  {
		"airline_code": "P7",
		"airline_3lc": "ESL",
		"iata_nr": "215",
		"name": "East Line Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "P9",
		"airline_3lc": "",
		"iata_nr": "602",
		"name": "PERUVIAN AIR LINES",
		"l": "0"
	  },
	  {
		"airline_code": "QB",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Bisec",
		"l": "0"
	  },
	  {
		"airline_code": "QJ",
		"airline_3lc": "LTP",
		"iata_nr": "937",
		"name": "Latpass Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "QN",
		"airline_3lc": "",
		"iata_nr": "907",
		"name": "Air Armenia",
		"l": "0"
	  },
	  {
		"airline_code": "QY",
		"airline_3lc": "",
		"iata_nr": "615",
		"name": "European Air Transport",
		"l": "0"
	  },
	  {
		"airline_code": "Q5",
		"airline_3lc": "MLA",
		"iata_nr": "519",
		"name": "Forty Mile Air",
		"l": "0"
	  },
	  {
		"airline_code": "Q6",
		"airline_3lc": "",
		"iata_nr": "537",
		"name": "Aero-condor",
		"l": "0"
	  },
	  {
		"airline_code": "Q8",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Trans Air Congo",
		"l": "0"
	  },
	  {
		"airline_code": "Q9",
		"airline_3lc": "",
		"iata_nr": "836",
		"name": "Afrinat International",
		"l": "0"
	  },
	  {
		"airline_code": "RF",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Ord Air Charter Pty Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "RZ",
		"airline_3lc": "LRS",
		"iata_nr": "503",
		"name": "Sansa",
		"l": "0"
	  },
	  {
		"airline_code": "R2",
		"airline_3lc": "ORB",
		"iata_nr": "291",
		"name": "Orenair",
		"l": "0"
	  },
	  {
		"airline_code": "R5",
		"airline_3lc": "MAC",
		"iata_nr": "",
		"name": "Malta Air Charter",
		"l": "0"
	  },
	  {
		"airline_code": "R8",
		"airline_3lc": "KGA",
		"iata_nr": "758",
		"name": "Kyrgyzstan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "R9",
		"airline_3lc": "",
		"iata_nr": "451",
		"name": "Camai Air",
		"l": "0"
	  },
	  {
		"airline_code": "SJ",
		"airline_3lc": "SJY",
		"iata_nr": "",
		"name": "Pt Sriwijaya Air",
		"l": "0"
	  },
	  {
		"airline_code": "S2",
		"airline_3lc": "",
		"iata_nr": "705",
		"name": "Jet Lite I) Ltd.",
		"l": "0"
	  },
	  {
		"airline_code": "S4",
		"airline_3lc": "RZO",
		"iata_nr": "331",
		"name": "Sata International",
		"l": "0"
	  },
	  {
		"airline_code": "S8",
		"airline_3lc": "CAH",
		"iata_nr": "",
		"name": "Charlan Air",
		"l": "0"
	  },
	  {
		"airline_code": "S9",
		"airline_3lc": "HAS",
		"iata_nr": "895",
		"name": "East African Safari Air",
		"l": "0"
	  },
	  {
		"airline_code": "TB",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Jetairfly",
		"l": "0"
	  },
	  {
		"airline_code": "TD",
		"airline_3lc": "",
		"iata_nr": "432",
		"name": "Atlantis European Airway",
		"l": "0"
	  },
	  {
		"airline_code": "TH",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Ba Citiexpress",
		"l": "0"
	  },
	  {
		"airline_code": "TN",
		"airline_3lc": "THT",
		"iata_nr": "244",
		"name": "Air Tahiti Nui",
		"l": "0"
	  },
	  {
		"airline_code": "TX",
		"airline_3lc": "",
		"iata_nr": "427",
		"name": "Air Caraibes",
		"l": "0"
	  },
	  {
		"airline_code": "T5",
		"airline_3lc": "TUA",
		"iata_nr": "542",
		"name": "Turkmenistan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "T7",
		"airline_3lc": "",
		"iata_nr": "294",
		"name": "Twin Jet",
		"l": "0"
	  },
	  {
		"airline_code": "T9",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Transmeridian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "UQ",
		"airline_3lc": "OCM",
		"iata_nr": "",
		"name": "O Connor Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "UT",
		"airline_3lc": "UTA",
		"iata_nr": "",
		"name": "UTAIR AVIATION JSC",
		"l": "0"
	  },
	  {
		"airline_code": "UZ",
		"airline_3lc": "BRQ",
		"iata_nr": "928",
		"name": "Buraq Air",
		"l": "0"
	  },
	  {
		"airline_code": "U4",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "BUDDHA AIR",
		"l": "0"
	  },
	  {
		"airline_code": "U5",
		"airline_3lc": "GWY",
		"iata_nr": "",
		"name": "USA 3000",
		"l": "0"
	  },
	  {
		"airline_code": "U7",
		"airline_3lc": "",
		"iata_nr": "926",
		"name": "Air Uganda",
		"l": "0"
	  },
	  {
		"airline_code": "U8",
		"airline_3lc": "",
		"iata_nr": "669",
		"name": "Armavia",
		"l": "0"
	  },
	  {
		"airline_code": "U9",
		"airline_3lc": "",
		"iata_nr": "966",
		"name": "Tatarstan Air",
		"l": "0"
	  },
	  {
		"airline_code": "VF",
		"airline_3lc": "VLU",
		"iata_nr": "896",
		"name": "Valuair",
		"l": "0"
	  },
	  {
		"airline_code": "VL",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "North Vancouver Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "VV",
		"airline_3lc": "AEW",
		"iata_nr": "870",
		"name": "Aerosvit Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "V5",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Royal Aruban Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "V6",
		"airline_3lc": "VUE",
		"iata_nr": "522",
		"name": "Vuelos Internos Privados",
		"l": "0"
	  },
	  {
		"airline_code": "V7",
		"airline_3lc": "VOE",
		"iata_nr": "712",
		"name": "Volotea",
		"l": "0"
	  },
	  {
		"airline_code": "V8",
		"airline_3lc": "IAR",
		"iata_nr": "",
		"name": "Iliamna Air",
		"l": "0"
	  },
	  {
		"airline_code": "V9",
		"airline_3lc": "BTC",
		"iata_nr": "940",
		"name": "Bashkirian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "V0",
		"airline_3lc": "VCV",
		"iata_nr": "308",
		"name": "CONVIASA",
		"l": "0"
	  },
	  {
		"airline_code": "WD",
		"airline_3lc": "DSR",
		"iata_nr": "761",
		"name": "Das Air Limited",
		"l": "0"
	  },
	  {
		"airline_code": "WO",
		"airline_3lc": "",
		"iata_nr": "468",
		"name": "World Airways",
		"l": "0"
	  },
	  {
		"airline_code": "WS",
		"airline_3lc": "WJA",
		"iata_nr": "838",
		"name": "Westjet",
		"l": "0"
	  },
	  {
		"airline_code": "WV",
		"airline_3lc": "",
		"iata_nr": "612",
		"name": "Swe Fly",
		"l": "0"
	  },
	  {
		"airline_code": "WZ",
		"airline_3lc": "",
		"iata_nr": "682",
		"name": "West African Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "W4",
		"airline_3lc": "",
		"iata_nr": "107",
		"name": "LC BUSRE",
		"l": "0"
	  },
	  {
		"airline_code": "W5",
		"airline_3lc": "IRM",
		"iata_nr": "537",
		"name": "Mahan Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "W7",
		"airline_3lc": "SAH",
		"iata_nr": "271",
		"name": "Sayakhat Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "W8",
		"airline_3lc": "WND",
		"iata_nr": "",
		"name": "Carribean Winds Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "W9",
		"airline_3lc": "",
		"iata_nr": "17",
		"name": "Air Bagan",
		"l": "0"
	  },
	  {
		"airline_code": "XB",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Iata",
		"l": "0"
	  },
	  {
		"airline_code": "XC",
		"airline_3lc": "KDC",
		"iata_nr": "",
		"name": "K D Air",
		"l": "0"
	  },
	  {
		"airline_code": "XD",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Arc",
		"l": "0"
	  },
	  {
		"airline_code": "XE",
		"airline_3lc": "",
		"iata_nr": "477",
		"name": "Expressjet Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "XG",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sun Express Germany",
		"l": "0"
	  },
	  {
		"airline_code": "XM",
		"airline_3lc": "SMX",
		"iata_nr": "",
		"name": "C.A.I. First S.P.A.",
		"l": "0"
	  },
	  {
		"airline_code": "XN",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Xpressair",
		"l": "0"
	  },
	  {
		"airline_code": "XR",
		"airline_3lc": "",
		"iata_nr": "608",
		"name": "VIRGIN AUSTRALIA REGIONAL AIRL",
		"l": "0"
	  },
	  {
		"airline_code": "X4",
		"airline_3lc": "",
		"iata_nr": "584",
		"name": "Vanair Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "X5",
		"airline_3lc": "FBN",
		"iata_nr": "",
		"name": "Afrique Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "X6",
		"airline_3lc": "XYB",
		"iata_nr": "172",
		"name": "AMADEUS SIX",
		"l": "0"
	  },
	  {
		"airline_code": "X7",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "X8",
		"airline_3lc": "DAP",
		"iata_nr": "545",
		"name": "Icardo",
		"l": "0"
	  },
	  {
		"airline_code": "X9",
		"airline_3lc": "NVD",
		"iata_nr": "",
		"name": "Avion Express",
		"l": "0"
	  },
	  {
		"airline_code": "YA",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Nego Airline One",
		"l": "0"
	  },
	  {
		"airline_code": "YG",
		"airline_3lc": "OTL",
		"iata_nr": "",
		"name": "South Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "YW",
		"airline_3lc": "ANS",
		"iata_nr": "694",
		"name": "Air Nostrum",
		"l": "0"
	  },
	  {
		"airline_code": "Y2",
		"airline_3lc": "",
		"iata_nr": "693",
		"name": "Africaone",
		"l": "0"
	  },
	  {
		"airline_code": "Y3",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Worldwide Cargo Services",
		"l": "0"
	  },
	  {
		"airline_code": "Y4",
		"airline_3lc": "VOI",
		"iata_nr": "067",
		"name": "Volaris",
		"l": "0"
	  },
	  {
		"airline_code": "Y5",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Pace Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "Y6",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Cambodia Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "Y7",
		"airline_3lc": "",
		"iata_nr": "362",
		"name": "SilverJet",
		"l": "0"
	  },
	  {
		"airline_code": "Y8",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aircompany Yakutia",
		"l": "0"
	  },
	  {
		"airline_code": "Y9",
		"airline_3lc": "",
		"iata_nr": "780",
		"name": "Kish Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "ZH",
		"airline_3lc": "",
		"iata_nr": "479",
		"name": "Shenzhen Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "ZN",
		"airline_3lc": "NAY",
		"iata_nr": "595",
		"name": "Naysa",
		"l": "0"
	  },
	  {
		"airline_code": "ZZ",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Service",
		"l": "0"
	  },
	  {
		"airline_code": "Z3",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Promech",
		"l": "0"
	  },
	  {
		"airline_code": "Z4",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Zoom Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "Z5",
		"airline_3lc": "",
		"iata_nr": "009",
		"name": "Gmg Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "Z6",
		"airline_3lc": "UDN",
		"iata_nr": "181",
		"name": "Dnieproavia",
		"l": "0"
	  },
	  {
		"airline_code": "Z7",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Laker Airways (Bahamas) LTD",
		"l": "0"
	  },
	  {
		"airline_code": "Z9",
		"airline_3lc": "",
		"iata_nr": "509",
		"name": "Aero Zambia",
		"l": "0"
	  },
	  {
		"airline_code": "1A",
		"airline_3lc": "AGT",
		"iata_nr": "790",
		"name": "Amadeus",
		"l": "0"
	  },
	  {
		"airline_code": "1B",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Abacus",
		"l": "0"
	  },
	  {
		"airline_code": "1C",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Eds Information Business",
		"l": "0"
	  },
	  {
		"airline_code": "1D",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Raddix Solutions Intl",
		"l": "0"
	  },
	  {
		"airline_code": "1E",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Civil Aviat Of China",
		"l": "0"
	  },
	  {
		"airline_code": "1F",
		"airline_3lc": "TTF",
		"iata_nr": "788",
		"name": "Infini Travel Informatio",
		"l": "0"
	  },
	  {
		"airline_code": "1G",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Galileo International",
		"l": "0"
	  },
	  {
		"airline_code": "1H",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Comtech",
		"l": "0"
	  },
	  {
		"airline_code": "1J",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Axess International",
		"l": "0"
	  },
	  {
		"airline_code": "1K",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sutra Inc.",
		"l": "0"
	  },
	  {
		"airline_code": "1L",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Open Skies Inc",
		"l": "0"
	  },
	  {
		"airline_code": "1M",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sirena Jsc",
		"l": "0"
	  },
	  {
		"airline_code": "1N",
		"airline_3lc": "",
		"iata_nr": "892",
		"name": "Navitaire",
		"l": "0"
	  },
	  {
		"airline_code": "1O",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Phoenix Systems Dertours",
		"l": "0"
	  },
	  {
		"airline_code": "1P",
		"airline_3lc": "WSP",
		"iata_nr": "523",
		"name": "Worldspan",
		"l": "0"
	  },
	  {
		"airline_code": "1Q",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Joint-stock Company.ital",
		"l": "0"
	  },
	  {
		"airline_code": "1R",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Hainan Phoenix Info Sys",
		"l": "0"
	  },
	  {
		"airline_code": "1S",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sabre",
		"l": "0"
	  },
	  {
		"airline_code": "1T",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Start",
		"l": "0"
	  },
	  {
		"airline_code": "1V",
		"airline_3lc": "",
		"iata_nr": "588",
		"name": "Galileo International",
		"l": "0"
	  },
	  {
		"airline_code": "1W",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sabre",
		"l": "0"
	  },
	  {
		"airline_code": "1X",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Gabriel",
		"l": "0"
	  },
	  {
		"airline_code": "1Y",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Electronic Data Systems",
		"l": "0"
	  },
	  {
		"airline_code": "1Z",
		"airline_3lc": "APD",
		"iata_nr": "",
		"name": "Fantasia Info Network Lt",
		"l": "0"
	  },
	  {
		"airline_code": "2A",
		"airline_3lc": "DBB",
		"iata_nr": "",
		"name": "Deutsche Bahn AG",
		"l": "0"
	  },
	  {
		"airline_code": "2B",
		"airline_3lc": "",
		"iata_nr": "088",
		"name": "Ak Bars Aero",
		"l": "0"
	  },
	  {
		"airline_code": "2C",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Sncf",
		"l": "0"
	  },
	  {
		"airline_code": "2G",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Northwest Seaplanes",
		"l": "0"
	  },
	  {
		"airline_code": "2H",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Thalys International",
		"l": "0"
	  },
	  {
		"airline_code": "2J",
		"airline_3lc": "VBW",
		"iata_nr": "226",
		"name": "Air Burkina",
		"l": "0"
	  },
	  {
		"airline_code": "2K",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aerolineas Galapagos",
		"l": "0"
	  },
	  {
		"airline_code": "2L",
		"airline_3lc": "OAW",
		"iata_nr": "",
		"name": "Helvetic Airways",
		"l": "0"
	  },
	  {
		"airline_code": "2M",
		"airline_3lc": "MDV",
		"iata_nr": "860",
		"name": "Moldavian Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "2N",
		"airline_3lc": "UMK",
		"iata_nr": "558",
		"name": "Yuzmashavia",
		"l": "0"
	  },
	  {
		"airline_code": "2P",
		"airline_3lc": "GAP",
		"iata_nr": "211",
		"name": "AIRPHIL EXPRESS",
		"l": "0"
	  },
	  {
		"airline_code": "2R",
		"airline_3lc": "VRR",
		"iata_nr": "830",
		"name": "Via Rail Canada Inc",
		"l": "0"
	  },
	  {
		"airline_code": "2U",
		"airline_3lc": "ERO",
		"iata_nr": "383",
		"name": "SUN D\u00b4OR",
		"l": "0"
	  },
	  {
		"airline_code": "2V",
		"airline_3lc": "",
		"iata_nr": "554",
		"name": "Amtrak",
		"l": "0"
	  },
	  {
		"airline_code": "2X",
		"airline_3lc": "XYA",
		"iata_nr": "799",
		"name": "Amadeus Two",
		"l": "0"
	  },
	  {
		"airline_code": "2Y",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Andaman",
		"l": "0"
	  },
	  {
		"airline_code": "3A",
		"airline_3lc": "ALZ",
		"iata_nr": "",
		"name": "Kenosha Aero Inc",
		"l": "0"
	  },
	  {
		"airline_code": "3B",
		"airline_3lc": "BFR",
		"iata_nr": "",
		"name": "Burkina Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "3G",
		"airline_3lc": "AYZ",
		"iata_nr": "411",
		"name": "Atlant-soyuz Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "3W",
		"airline_3lc": "",
		"iata_nr": "482",
		"name": "Euromanx",
		"l": "0"
	  },
	  {
		"airline_code": "3X",
		"airline_3lc": "JAC",
		"iata_nr": "",
		"name": "Japan Air Commuter",
		"l": "0"
	  },
	  {
		"airline_code": "4F",
		"airline_3lc": "ECE",
		"iata_nr": "",
		"name": "Air City",
		"l": "0"
	  },
	  {
		"airline_code": "4O",
		"airline_3lc": "AIJ",
		"iata_nr": "",
		"name": "ABC Aerolineas",
		"l": "0"
	  },
	  {
		"airline_code": "4P",
		"airline_3lc": "APN",
		"iata_nr": "",
		"name": "Air Polonia",
		"l": "0"
	  },
	  {
		"airline_code": "4X",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Mercury",
		"l": "0"
	  },
	  {
		"airline_code": "5G",
		"airline_3lc": "SSV",
		"iata_nr": "884",
		"name": "Skyservice Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "5M",
		"airline_3lc": "",
		"iata_nr": "842",
		"name": "Sibaviatrans",
		"l": "0"
	  },
	  {
		"airline_code": "5Q",
		"airline_3lc": "BEA",
		"iata_nr": "567",
		"name": "BQB LINEAS AEREAS",
		"l": "0"
	  },
	  {
		"airline_code": "5R",
		"airline_3lc": "KAJ",
		"iata_nr": "915",
		"name": "Karthago Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "5V",
		"airline_3lc": "UKW",
		"iata_nr": "062",
		"name": "Lviv Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "5Y",
		"airline_3lc": "IOS",
		"iata_nr": "",
		"name": "Express Rail Link",
		"l": "0"
	  },
	  {
		"airline_code": "6F",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Afrijet Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "6K",
		"airline_3lc": "RIT",
		"iata_nr": "",
		"name": "Asian Spirit",
		"l": "0"
	  },
	  {
		"airline_code": "6N",
		"airline_3lc": "NRD",
		"iata_nr": "",
		"name": "Nordic Airways",
		"l": "0"
	  },
	  {
		"airline_code": "6P",
		"airline_3lc": "",
		"iata_nr": "645",
		"name": "Clubair",
		"l": "0"
	  },
	  {
		"airline_code": "6R",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Aerotransporte De Carga",
		"l": "0"
	  },
	  {
		"airline_code": "6T",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Air Mandalay",
		"l": "0"
	  },
	  {
		"airline_code": "6X",
		"airline_3lc": "XYB",
		"iata_nr": "796",
		"name": "Amadeus Six",
		"l": "0"
	  },
	  {
		"airline_code": "6Z",
		"airline_3lc": "UKS",
		"iata_nr": "516",
		"name": "Ukrainian Cargo Airways",
		"l": "0"
	  },
	  {
		"airline_code": "7G",
		"airline_3lc": "",
		"iata_nr": "513",
		"name": "Bellair Inc",
		"l": "0"
	  },
	  {
		"airline_code": "7I",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "INSEL AIR INTERNATIONAL",
		"l": "0"
	  },
	  {
		"airline_code": "7S",
		"airline_3lc": "RYA",
		"iata_nr": "",
		"name": "Arctic Transportation",
		"l": "0"
	  },
	  {
		"airline_code": "7U",
		"airline_3lc": "ERG",
		"iata_nr": "765",
		"name": "Aviaenergo",
		"l": "0"
	  },
	  {
		"airline_code": "7X",
		"airline_3lc": "XYC",
		"iata_nr": "791",
		"name": "Amadeus Seven",
		"l": "0"
	  },
	  {
		"airline_code": "8J",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "JET4YOU",
		"l": "0"
	  },
	  {
		"airline_code": "8M",
		"airline_3lc": "",
		"iata_nr": "599",
		"name": "Myanmar Airways Intl",
		"l": "0"
	  },
	  {
		"airline_code": "8O",
		"airline_3lc": "",
		"iata_nr": "222",
		"name": "West Coast Air",
		"l": "0"
	  },
	  {
		"airline_code": "8Q",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Baker Aviation",
		"l": "0"
	  },
	  {
		"airline_code": "8U",
		"airline_3lc": "AAW",
		"iata_nr": "546",
		"name": "Afriqiyah Airways",
		"l": "0"
	  },
	  {
		"airline_code": "8W",
		"airline_3lc": "PWF",
		"iata_nr": "",
		"name": "Private Wings",
		"l": "0"
	  },
	  {
		"airline_code": "8X",
		"airline_3lc": "XYD",
		"iata_nr": "798",
		"name": "Amadeus Eight",
		"l": "0"
	  },
	  {
		"airline_code": "8Z",
		"airline_3lc": "LER",
		"iata_nr": "722",
		"name": "Laser",
		"l": "0"
	  },
	  {
		"airline_code": "9F",
		"airline_3lc": "",
		"iata_nr": "814",
		"name": "Eurostar",
		"l": "0"
	  },
	  {
		"airline_code": "9G",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Airport Express Rail Ltd",
		"l": "0"
	  },
	  {
		"airline_code": "9H",
		"airline_3lc": "",
		"iata_nr": "493",
		"name": "Dutch Antilles Express",
		"l": "0"
	  },
	  {
		"airline_code": "9X",
		"airline_3lc": "AXY",
		"iata_nr": "735",
		"name": "New Axis Airways",
		"l": "0"
	  },
	  {
		"airline_code": "B0",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Baboo Airways",
		"l": "0"
	  },
	  {
		"airline_code": "*O",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Oneworld",
		"l": "0"
	  },
	  {
		"airline_code": "*S",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Skyteam",
		"l": "0"
	  },
	  {
		"airline_code": "W6",
		"airline_3lc": "WZZ",
		"iata_nr": "",
		"name": "Wizz Air",
		"l": "0"
	  },
	  {
		"airline_code": "C0",
		"airline_3lc": "CLW",
		"iata_nr": "",
		"name": "Centralwings",
		"l": "0"
	  },
	  {
		"airline_code": "01",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Jet2",
		"l": "0"
	  },
	  {
		"airline_code": "02",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Snowflake",
		"l": "0"
	  },
	  {
		"airline_code": "I4",
		"airline_3lc": "",
		"iata_nr": "434",
		"name": "Intl Airlink",
		"l": "0"
	  },
	  {
		"airline_code": "M1",
		"airline_3lc": "MHS",
		"iata_nr": "",
		"name": "Memphis",
		"l": "0"
	  },
	  {
		"airline_code": "O1",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Onur Air",
		"l": "0"
	  },
	  {
		"airline_code": "03",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Cronus Air",
		"l": "0"
	  },
	  {
		"airline_code": "04",
		"airline_3lc": "BHG",
		"iata_nr": "",
		"name": "Balkan Holidays Air",
		"l": "0"
	  },
	  {
		"airline_code": "05",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Balair",
		"l": "0"
	  },
	  {
		"airline_code": "06",
		"airline_3lc": "BUS",
		"iata_nr": "",
		"name": "Bustransfer",
		"l": "0"
	  },
	  {
		"airline_code": "07",
		"airline_3lc": "FTI",
		"iata_nr": "",
		"name": "FTI Fluggesellschaft",
		"l": "0"
	  },
	  {
		"airline_code": "08",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Schiffstransfer",
		"l": "0"
	  },
	  {
		"airline_code": "09",
		"airline_3lc": "LCN",
		"iata_nr": "",
		"name": "Aeras Canaris",
		"l": "0"
	  },
	  {
		"airline_code": "XX",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "GROUND SERVICE\/GROUND HANDLING SERVICE",
		"l": "0"
	  },
	  {
		"airline_code": "BY",
		"airline_3lc": "TOM",
		"iata_nr": "754",
		"name": "ThomsonFly",
		"l": "0"
	  },
	  {
		"airline_code": "BLE",
		"airline_3lc": "BLE",
		"iata_nr": "",
		"name": "Blue Line",
		"l": "0"
	  },
	  {
		"airline_code": "0D",
		"airline_3lc": "DWT",
		"iata_nr": "",
		"name": "Darwin Airline",
		"l": "0"
	  },
	  {
		"airline_code": "G0",
		"airline_3lc": "GHB",
		"iata_nr": "",
		"name": "Ghana International Airline",
		"l": "0"
	  },
	  {
		"airline_code": "A1",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "G2 Switch Works",
		"l": "0"
	  },
	  {
		"airline_code": "VEL",
		"airline_3lc": "VEL",
		"iata_nr": "",
		"name": "Veraways Kenya",
		"l": "0"
	  },
	  {
		"airline_code": "TOP",
		"airline_3lc": "TOP",
		"iata_nr": "",
		"name": "Air Havacilik Sanayi Ve Ticaret",
		"l": "0"
	  },
	  {
		"airline_code": "UST",
		"airline_3lc": "UST",
		"iata_nr": "",
		"name": "Austro Aereo",
		"l": "0"
	  },
	  {
		"airline_code": "0S",
		"airline_3lc": "MOD",
		"iata_nr": "",
		"name": "Royal Airlines\/India",
		"l": "0"
	  },
	  {
		"airline_code": "K6",
		"airline_3lc": "",
		"iata_nr": "188",
		"name": "CAMBODIA ANGKOR AIR",
		"l": "0"
	  },
	  {
		"airline_code": "O3",
		"airline_3lc": "BVU",
		"iata_nr": "",
		"name": "Bellview Airline\/Sierra Leone",
		"l": "0"
	  },
	  {
		"airline_code": "ECT",
		"airline_3lc": "ECT",
		"iata_nr": "",
		"name": "East Coast Airways\/South Africa",
		"l": "0"
	  },
	  {
		"airline_code": "0A",
		"airline_3lc": "GNT",
		"iata_nr": "982",
		"name": "Amber Air",
		"l": "0"
	  },
	  {
		"airline_code": "8I",
		"airline_3lc": "",
		"iata_nr": "778",
		"name": "Insel Air Aruba",
		"l": "0"
	  },
	  {
		"airline_code": "ASS",
		"airline_3lc": "ASS",
		"iata_nr": "",
		"name": "Air Class Mexico",
		"l": "0"
	  },
	  {
		"airline_code": "SEE",
		"airline_3lc": "SEE",
		"iata_nr": "587",
		"name": "Shaheen Air Cargo",
		"l": "0"
	  },
	  {
		"airline_code": "I7",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Paramount Airways",
		"l": "0"
	  },
	  {
		"airline_code": "9I",
		"airline_3lc": "TKY",
		"iata_nr": "",
		"name": "Thai Sky Airline",
		"l": "0"
	  },
	  {
		"airline_code": "HB",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Homer Air",
		"l": "0"
	  },
	  {
		"airline_code": "I5",
		"airline_3lc": "",
		"iata_nr": "261",
		"name": "Compagnie Aerienne Mali",
		"l": "0"
	  },
	  {
		"airline_code": "J0",
		"airline_3lc": "",
		"iata_nr": "402",
		"name": "Jetlink Express",
		"l": "0"
	  },
	  {
		"airline_code": "O6",
		"airline_3lc": "",
		"iata_nr": "247",
		"name": "Oceanair Linhas Aereas",
		"l": "0"
	  },
	  {
		"airline_code": "0B",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Blue Air",
		"l": "0"
	  },
	  {
		"airline_code": "0C",
		"airline_3lc": "",
		"iata_nr": "495",
		"name": "Catovair",
		"l": "0"
	  },
	  {
		"airline_code": "9O",
		"airline_3lc": "",
		"iata_nr": "357",
		"name": "National Airways Cameroon",
		"l": "0"
	  },
	  {
		"airline_code": "5O",
		"airline_3lc": "FPO",
		"iata_nr": "",
		"name": "Europe Airpost",
		"l": "0"
	  },
	  {
		"airline_code": "DBK",
		"airline_3lc": "DBK",
		"iata_nr": "0",
		"name": "Dubrovnik Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "GJT",
		"airline_3lc": "GJT",
		"iata_nr": "0",
		"name": "GirJet",
		"l": "0"
	  },
	  {
		"airline_code": "ML",
		"airline_3lc": "BIE",
		"iata_nr": "853",
		"name": "Air Mediterranee",
		"l": "0"
	  },
	  {
		"airline_code": "EGN",
		"airline_3lc": "EGN",
		"iata_nr": "",
		"name": "Eagle Aviation France",
		"l": "0"
	  },
	  {
		"airline_code": "I3",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Ivoirienne de Transport",
		"l": "0"
	  },
	  {
		"airline_code": "GXL",
		"airline_3lc": "GXL",
		"iata_nr": "",
		"name": "STAR XL GERMAN AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "FFP",
		"airline_3lc": "FFP",
		"iata_nr": "",
		"name": "Fischer Air Polska",
		"l": "0"
	  },
	  {
		"airline_code": "E0",
		"airline_3lc": "",
		"iata_nr": "646",
		"name": "EOS Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "AHR",
		"airline_3lc": "AHR",
		"iata_nr": "",
		"name": "Air Adriatic",
		"l": "0"
	  },
	  {
		"airline_code": "A0",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "LAvion",
		"l": "0"
	  },
	  {
		"airline_code": "O2",
		"airline_3lc": "GIO",
		"iata_nr": "",
		"name": "Oceanic Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "Y0",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Yellow AirTaxi",
		"l": "0"
	  },
	  {
		"airline_code": "O4",
		"airline_3lc": "",
		"iata_nr": "916",
		"name": "Antrak Air",
		"l": "0"
	  },
	  {
		"airline_code": "4I",
		"airline_3lc": "IZM",
		"iata_nr": "",
		"name": "Izmir Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "O8",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Oasis Hong Kong Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "P1",
		"airline_3lc": "",
		"iata_nr": "351",
		"name": "Publiccharters.com",
		"l": "0"
	  },
	  {
		"airline_code": "I9",
		"airline_3lc": "",
		"iata_nr": "67",
		"name": "Air Italy",
		"l": "0"
	  },
	  {
		"airline_code": "8S",
		"airline_3lc": "",
		"iata_nr": "802",
		"name": "Shun Tak China",
		"l": "0"
	  },
	  {
		"airline_code": "TV",
		"airline_3lc": "",
		"iata_nr": "088",
		"name": "TIBET AIRLINES",
		"l": "0"
	  },
	  {
		"airline_code": "RD",
		"airline_3lc": "RYN",
		"iata_nr": "",
		"name": "Ryan International",
		"l": "0"
	  },
	  {
		"airline_code": "2I",
		"airline_3lc": "SRU",
		"iata_nr": "156",
		"name": "Star Peru",
		"l": "0"
	  },
	  {
		"airline_code": "3I",
		"airline_3lc": "",
		"iata_nr": "393",
		"name": "Aerolineas del sur",
		"l": "0"
	  },
	  {
		"airline_code": "PD",
		"airline_3lc": "",
		"iata_nr": "415",
		"name": "Porter Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "8C",
		"airline_3lc": "DXH",
		"iata_nr": "883",
		"name": "East Star Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "0O",
		"airline_3lc": "",
		"iata_nr": "000",
		"name": "STA Travel",
		"l": "0"
	  },
	  {
		"airline_code": "P0",
		"airline_3lc": "PFZ",
		"iata_nr": "443",
		"name": "PROFLIGHT COMMUTER",
		"l": "0"
	  },
	  {
		"airline_code": "TO",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Transavia France",
		"l": "0"
	  },
	  {
		"airline_code": "T0",
		"airline_3lc": "",
		"iata_nr": "530",
		"name": "Taca Peru\/Trans American Airline S.A.",
		"l": "0"
	  },
	  {
		"airline_code": "0Y",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Yeti Airlines",
		"l": "0"
	  },
	  {
		"airline_code": "FHE",
		"airline_3lc": "FHE",
		"iata_nr": "",
		"name": "Fly Hello",
		"l": "0"
	  },
	  {
		"airline_code": "O5",
		"airline_3lc": "KMZ",
		"iata_nr": "174",
		"name": "Comore Aviation International",
		"l": "0"
	  },
	  {
		"airline_code": "3O",
		"airline_3lc": "",
		"iata_nr": "452",
		"name": "Air Arabia Maroc",
		"l": "0"
	  },
	  {
		"airline_code": "O0",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "STA Traverl ama Test record",
		"l": "0"
	  },
	  {
		"airline_code": "I2",
		"airline_3lc": "",
		"iata_nr": "060",
		"name": "Iberia Express",
		"l": "0"
	  },
	  {
		"airline_code": "5I",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "ALSA GRUPO SLU",
		"l": "0"
	  },
	  {
		"airline_code": "H1",
		"airline_3lc": "",
		"iata_nr": "",
		"name": "Hahn Air Systems",
		"l": "0"
	  },
	  {
		"airline_code": "6I",
		"airline_3lc": "",
		"iata_nr": "248",
		"name": "AIR ALSIE",
		"l": "0"
	  }
	]


	return {
		run: run
	}

}());
