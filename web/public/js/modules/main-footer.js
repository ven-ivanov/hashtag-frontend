;(function(){

	var footerMenuItems = $('.footerMenu-item a');
	$('.footerMenu').on('click', 'a', function(e) {
		e.preventDefault();

		footerMenuItems.removeClass('active');
		$(this).addClass('active');

		$('.headerMenu').removeClass('headerMenu-visible');
	})



	var lastTweetsTemplate = _.template($('[data-template="main-footer__last-tweets"]').html());
	$.get(App.baseUrl + '/api/lastTweets', function(data){

		var tweets = [];

		_.each(data, function(tweet){
			tweets.push(processTweetLinks(tweet));
		})

		$('.main-footer__last-tweets').html(lastTweetsTemplate({tweets: tweets}));

		var activeTweetIndex = 0;
		var footerLastTweets = $('.main-footer__tweet');
		setInterval(function(){
			$(footerLastTweets[activeTweetIndex]).removeClass('active');

			if(activeTweetIndex+1 === footerLastTweets.length){
				activeTweetIndex = 0;
			} else {
				activeTweetIndex++;
			}

			setTimeout(function(){
				$(footerLastTweets[activeTweetIndex]).addClass('active');
			}, 500)
		}, 5000)
		$(footerLastTweets[activeTweetIndex]).addClass('active');
	})


	function processTweetLinks(text) {
		var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i;
		text = text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
		exp = /(^|\s)#(\w+)/g;
		text = text.replace(exp, "$1<a href='http://search.twitter.com/search?q=%23$2' target='_blank'>#$2</a>");
		exp = /(^|\s)@(\w+)/g;
		text = text.replace(exp, "$1<a href='http://www.twitter.com/$2' target='_blank'>@$2</a>");
		return text;
	}


})();