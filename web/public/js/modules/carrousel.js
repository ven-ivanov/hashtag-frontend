App.Modules['carrousel'] = (function(){

	"use strict";

	function run(){
		var effect = 'fade';

		$(".carrousel").slidesjs({
			play : {
				auto : true,
				interval : 5000,
				pauseOnHover : false,
				effect : effect
			},
			navigation : {
				effect : effect
			},
			effect : {
				fade : {
					speed : 500
				},
				slide : {
					speed : 800
				}
			},
			callback : {
				loaded : function(number) {
					setCurrentSlideClass(number);

					$('.carrousel-item .carrousel-item-image').click(function(event){
						$(document).trigger("setDestination", $(event.currentTarget).attr('data-destination') );
					})
				},
				complete : function(number) {
					setCurrentSlideClass(number);
				}
			}
		});

		function setCurrentSlideClass(number) {
			$('.carrousel').addClass('is-active');
			$('.carrousel-item').removeClass('active').eq(number - 1).addClass('active');
		}
	}

	return {
		run: run
	}

}());