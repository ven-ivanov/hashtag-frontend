App.Modules['mosaic'] = (function(){

	'use strict';


	function run() {

		$('.search-tool-fieldset-drive').on('click', function(){
			$(this).removeClass('search-tool-fieldset-drive--date-highlighted')
		})

		// $('.mosaic').on('click', '.mosaic__item a', function(e) {
		// 	e.preventDefault();

		// 	var $el = $(this);

		// 	$('.mosaic__car-detail').addClass('is-active');
		// });

		$('.main').on('click', '.car-detail__close', function(e) {
			e.preventDefault();

			close()
		});

		$('.main').on('click', '.car-detail__select', function(e) {
			e.preventDefault();

			$('#drive-category').val($(e.currentTarget).data('type'))

			close()
		});

		function close(){
			$('.mosaic__car-detail').removeClass('is-active');

			setTimeout(function(){
				$('.mosaic__car-detail').remove()
			}, 500);
		}

		//  _              _ _   _
		// | |_ ___   ___ | | |_(_)_ __
		// | __/ _ \ / _ \| | __| | '_ \
		// | || (_) | (_) | | |_| | |_) |
		//  \__\___/ \___/|_|\__|_| .__/
		//                        |_|

		var tooltip = $('.tooltip-follow'),
			positionTop = 100,
			positionLeft = 0,
			clicked = false;

		$('.mosaic').on('mouseenter mouseleave mousemove click', function (event) {
			positionTop = event.pageY - $(this).offset().top >= 100 ? event.pageY - $(this).offset().top : 100;
			positionLeft = event.pageX - $(this).offset().left;

			if ((event.type == 'mouseenter' || event.type == 'mousemove') && !clicked ) {
				var $that = $(this);
				tooltip.addClass('is-active').css({ top : positionTop, left : positionLeft });
			} else if (event.type == 'mouseleave') {
				tooltip.removeClass('is-active')
			} else {
				tooltip.removeClass('is-active')
				clicked = true;
			}
		});

		var scrollCounter = 0;

		$(".mosaic__items-container").mCustomScrollbar({
			scrollInertia : 300,
			setTop : '200%',
			callbacks : {
				onScrollStart:function(){
					scrollCounter++

					if (scrollCounter > 6) {
						tooltip.removeClass('is-active')
						clicked = true;
					}
				}
			}
		})

		setTimeout(function(){
			$(".mosaic__items-container").mCustomScrollbar('scrollTo', 'top',{ scrollInertia : 1500 });
		}, 1000);


		// $(".mosaic__items-container").each(function(index, container){
		// 	setTimeout(function(){
		// 		$(container).mCustomScrollbar('scrollTo', 'top',{ scrollInertia : 1500 });
		// 	}, 1000 + index * 150)
		// })

	}


	return {
		run : run
	}

}());
