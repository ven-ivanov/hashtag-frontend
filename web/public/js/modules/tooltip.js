App.Modules['tooltip'] = (function () {

	'use strict';

	function run(){
		var template = '<div class="tooltip"></div>',
			tooltip = $(template).appendTo('body');

		$(document).on('mouseenter mouseleave', '.has-tooltip', function (event) {
			if (event.type == 'mouseenter') {
				var $that = $(this),
					target = { top : $that.offset().top, left : $that.offset().left, message : $that.data('text') };

				if(target.message.length)
					tooltip.addClass('tooltip--is-active').text(target.message).css({ top : target.top - 30, left : target.left - 5 });
			} else {
				tooltip.removeClass('tooltip--is-active')
			}
		});
	}

	return {
		run: run
	}

}());