App.Modules['share'] = (function () {
	'use strict';

	function run(){

		var share = {
			init : {
				twitter : function () {
					if(window.twttr){
						twttr.ready(function (twttr) {
							twttr.events.bind('tweet', function (event) {
								console.log('tweet', event);
							});
						});
					}
				}
			},
			click : function () {
				var $that = $(this),
					network = $that.data('network');

				switch (network) {
					case 'facebook' : share.on.facebook(); break;
					case 'twitter' : return; break;
					case 'email' : share.on.email(); return; break;
				}

				return false;
			},
			on : {
				facebook : function () {
					FB.ui({
						method: 'feed',
						name: 'Facebook Dialogs',
						link: 'https://developers.facebook.com/docs/dialogs/',
						picture: 'http://fbrell.com/f8.jpg',
						caption: 'Reference Documentation',
						description: 'Dialogs provide a simple, consistent interface for applications to interface with users.'
					}, function(response) {
						if (response && response.post_id) {
							console.log('Post was published.');
						} else {
							console.log('Post was not published.');
						}
					});
				},
				email : function () {
					console.log('@TODO');
				}
			}
		}

		$('.bt').on('click', share.click);

		setTimeout(function () {
			share.init.twitter();
		}, 100);

	}

	return {
		run : run
	}

}());
