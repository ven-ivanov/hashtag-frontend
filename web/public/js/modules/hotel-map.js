App.Modules['hotel-map'] = (function(){

	'use strict';


	/**
	 * MapView
	 */

	var mapView;

	function MapView() {

		var scope = this;

		scope.currentZIndex = 300;
		scope.myLatlng = null;
		scope.latlngbounds = null;
		scope.map = null;
		scope.markerCluster = null;

		scope.currency = App.selectedCurrency;

		scope.windowMap = [];
		scope.windowMapOpened = null;
		scope.markers = [];

		// scope.icon  = App.staticPath + '/img/modules/results/marker-active.png';
		scope.icon  = 'http://www.housetrip.com/images/v4/map/marker_inactive.svg';
		scope.activeIcon = App.staticPath + '/img/modules/results/marker.png';

		// google.maps.event.addDomListener(window, 'load', function(){
		scope.init();
		// });

		scope.callbacks = {
			onMouseOverMarker: function(){},
			onMouseLeaveMarker: function(){}
		}

	}


	MapView.prototype.init = function() {

		var scope = this;

		// scope.myLatlng = new google.maps.LatLng(46.073231 , 7.382813);
		var mapStyle = [{"featureType":"water","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"landscape","stylers":[{"color":"#f2f2f2"}]},{"featureType":"road","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]}];

		var mapOptions = { zoom: 8, styles: mapStyle};

		scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

	}


	MapView.prototype.render = function(points, callbacks) {

		var scope = this;

		scope.callbacks = callbacks;
		scope.latlngbounds = new google.maps.LatLngBounds();

		for(var i = 0, t = points.length; i < t; i++) {
			// if(points[i].latitude != 0 && points[i].latitude != null && points[i].longitude != 0 && points[i].longitude != null)
			scope.setPoint(points[i], i);
		}

		// scope.markerCluster = new MarkerClusterer(scope.map, scope.markers, {gridSize: 2});
		scope.map.fitBounds(scope.latlngbounds);

	}



	MapView.prototype.filter = function(points) {

		var scope = this,
			markersApproved = [],
			markersRemoved = [];

		scope.closeWindowMap();

		scope.latlngbounds = new google.maps.LatLngBounds();

		if(scope.markers.length) {

			_.each(scope.markers, function(marker, i){
				var hasId = _.where(points, { hotelId: marker.id });

				if(!hasId.length) {

					if(marker.getMap() != null) marker.setMap(null);
					// markersRemoved.push(marker);
				} else {
					if(marker.getMap() == null) marker.setMap(scope.map);
					// markersApproved.push(marker);
					scope.latlngbounds.extend(marker.position);
				}
			});

			// if(scope.markerCluster) {
			// 	scope.markerCluster.clearMarkers();
			// 	scope.markerCluster.addMarkers(markersApproved);
				scope.map.setZoom(8);
			// }

			scope.map.fitBounds(scope.latlngbounds);

			if(markersApproved.length == 1) {
				scope.setMarker(markersApproved[0].id)
			}
		}

	}


	MapView.prototype.setPoint = function(point, index) {

		var scope = this,

			contentBox = '<figure> \
							<img src="'+ point.picture +'" width="250"> \
							<figcaption>'+ point.title +'</figcaption> \
						</figure> \
						<p>from '+ scope.currency +' '+ point.startPrice +'</p>',

			marker = new google.maps.Marker({
							position: new google.maps.LatLng(point.latitude, point.longitude),
							title: point.title,
							icon: scope.icon,
							animation: google.maps.Animation.FADE,
							map: scope.map
						}),

			myOptions = {
					content: contentBox,
					pixelOffset: new google.maps.Size(-132, -80),
					infoBoxClearance: new google.maps.Size(43, 40)
				};


		scope.windowMap[index] = new InfoBox(myOptions);
		scope.windowMap[index].marker = marker;
		scope.windowMap[index].id = point.hotelId;
		scope.windowMap[index].info = point;

		scope.windowMap[index].listener = google.maps.event.addListener(marker, 'click', function (e) {
			$('#map').trigger('setMarker', point.hotelId)
			// scope.openWindowMap(index, scope.windowMap[index]);
		});

		scope.windowMap[index].listener = google.maps.event.addListener(marker, 'mouseover', function (e) {
			// marker.setZIndex(++scope.currentZIndex)
			$('#map').trigger('overMarker', point.hotelId)
			scope.setMarker(point.hotelId)
		});

		scope.windowMap[index].listener = google.maps.event.addListener(marker, 'mouseout', function (e) {
			// marker.setZIndex(++scope.currentZIndex)
			$('#map').trigger('outMarker', point.hotelId)
			scope.closeWindowMap()
		});

		scope.markers[index] = marker;
		scope.markers[index].id = point.hotelId;

		scope.latlngbounds.extend(marker.position);

	}


	MapView.prototype.setMarker = function(id) {
		var scope = this,
			point = _.where(scope.windowMap, { id : id });

		if(id != scope.windowMapOpened && point.length > 0) {
			point = point[0];

			if(point.marker.getPosition().k != 0 && point.marker.getPosition().b != 0){
				scope.closeWindowMap();
				scope.openWindowMap(id, point);
			}
		}
	}

	MapView.prototype.openWindowMap = function(index, point) {
		var scope = this;

		scope.closeWindowMap();

		// point.marker.setIcon(scope.activeIcon);
		point.marker.setZIndex(++scope.currentZIndex);
		// point.open(scope.map, point.marker);
		scope.windowMapOpened = point.id;

		scope.topMarker = new google.maps.Marker({
			position: point.marker.getPosition(),
			flat: true,
			map: scope.map,
			icon: {
				url: scope.activeIcon,
				anchor: new google.maps.Point(17, 58)
			},
			zIndex: ++scope.currentZIndex
		})

		// scope.latlngbounds = new google.maps.LatLngBounds();
		// scope.latlngbounds.extend(point.marker.position);
		scope.map.fitBounds(scope.latlngbounds);
		// scope.map.setZoom(16);
	}


	MapView.prototype.closeWindowMap = function() {
		var scope = this,
			point = _.where(scope.windowMap, { id: scope.windowMapOpened });

		if(point.length) {
			point = point[0];
			// point.close();
			// point.marker.setIcon(scope.icon);
			scope.windowMapOpened = null

			if(scope.topMarker) {
				scope.topMarker.setMap(null)
				scope.topMarker = null;
			}
		}



	}





	function run() {
		mapView = new MapView();
	}

	function render(points) {
		var scope = this;

		if(mapView.map) {
			mapView.render(points);
		} else {
			setTimeout(function() {
				render.apply(scope, [points]);
			}, 100);
		}
	}

	function filter(points) {
		mapView.filter(points);
	}


	function setHotel(id) {
		mapView.setMarker(id);
	}



	return {
		run: run,
		render: render,
		filter: filter,
		setHotel: setHotel
	}

}());