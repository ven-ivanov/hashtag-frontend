App.Modules['sign-in'] = (function () {

	'use strict';

	function run(){
		$('.btn--create-account').on('click', function () {
			$('.btn--create-account-mobile').addClass('closed');
			$('.sign-up__form').addClass('open').find('.input-text').eq(0).focus();

			return false;
		});
	}

	return {
		run: run
	}

}());