App.Modules['autocomplete_hotel'] = (function(){

	// "use strict";

	var KEY_CODES = {
		'ENTER': 13,
		'UP': 38,
		'DOWN': 40,
		'ESC': 27,
		'BACKSPACE': 8,
		'DELETE': 46
	}


	// function run(){
	// 	var $container = $('<ul />').addClass('input-autocomplete--results');
	// 	var templateItem = _.template('<li class="<%=(active) ? "input-autocomplete--results--active" : ""%>"><%=text%></li>');
	// 	var $inputs = $('.input-autocomplete--hotel');
	// 	var $input;
	// 	var $tool;
	// 	var data = {};

	// 	$inputs.on('focus', function(){
	// 		$input = $(this);
	// 		$tool = $input.closest('.search-tool');
	// 		$container.appendTo($input.parent());
	// 	})

	// 	$inputs.on('blur', function(){
	// 		var next;

	// 		if(data.activeIndex !== undefined && data.response){
	// 			$input.val(data.response[data.activeIndex].text)

	// 			next = $input.closest('.search-tool-fieldset').find('.input-autocomplete').filter(function(index, elm){
	// 				return $(elm).val().length <= 0
	// 			})
	// 		}

	// 		data = {}
	// 		$container = $container.empty().detach();
	// 		$tool.removeClass('search-tool--searching');
	// 		$input.removeClass('input-autocomplete--thinking')
	// 		if(request) request.abort();

	// 	})


	// 	$inputs.on('keydown', function(e){

	// 		switch (e.keyCode) {
	// 			case KEY_CODES.UP : {

	// 				if(data.activeIndex === undefined){
	// 					data.activeIndex = 0;
	// 				} else {
	// 					data.activeIndex = Math.max(0, data.activeIndex - 1);
	// 				}
	// 				break;
	// 			}

	// 			case KEY_CODES.DOWN : {
	// 				if(data.activeIndex === undefined){
	// 					data.activeIndex = 0;
	// 				} else {
	// 					data.activeIndex = Math.min(data.response.length-1, data.activeIndex + 1);
	// 				}

	// 				break;
	// 			}

	// 			case KEY_CODES.ESC : {
	// 				data.activeIndex = undefined;
	// 				$(e.target).trigger('blur');

	// 				break;
	// 			}

	// 			case KEY_CODES.ENTER : {
	// 				e.preventDefault();
	// 				$(e.target).trigger('blur');

	// 				break;
	// 			}

	// 			case KEY_CODES.BACKSPACE || KEY_CODES.DELETE : {
	// 				fetch();

	// 				break;
	// 			}
	// 		}

	// 		render();
	// 	})

	// 	$inputs.on('keypress', function(e){
	// 		fetch();
	// 	})


	// 	$container.on('mouseenter', 'li', function(e){
	// 		var previousIndex = data.activeIndex;
	// 		data.activeIndex = $(this).index();

	// 		if(previousIndex !== data.activeIndex) render();
	// 	})

	// 	$container.on('mouseleave', 'li', function(e){
	// 		data.activeIndex = undefined;
	// 		render();
	// 	})


	// 	var timeout;
	// 	var request;

	// 	function fetch(){

	// 		clearTimeout(timeout);

	// 		timeout = setTimeout(function(){
	// 			var val = $input.val();

	// 			if(val.length > 2) {

	// 				$input.addClass('input-autocomplete--thinking')

	// 				request = $.getJSON(App.baseUrl + '/api/autocomplete?loc=' + encodeURIComponent($input.val()), function(response){
	// 					console.log(response);
	// 					response = response.rows;

	// 					if(response.length) {
	// 						data.response = response;
	// 						render();
	// 					}

	// 					$input.removeClass('input-autocomplete--thinking')
	// 				})

	// 				render();

	// 			}

	// 		}, 300)

	// 	}

	// 	//jQueryUI Autocomplete
	// 	function levenshtein (a, b) {
	// 		var i;
	// 		var j;
	// 		var cost;
	// 		var d = new Array();
	// 		if (a.length == 0) {
	// 			return b.length;
	// 		}
	// 		if (b.length == 0) {
	// 			return a.length;
	// 		}
	// 		for (i = 0; i <= a.length; i++) {
	// 			d[i] = new Array();
	// 			d[i][0] = i;
	// 		}
	// 		for (j = 0; j <= b.length; j++) {
	// 			d[0][j] = j;
	// 		}
	// 		for (i = 1; i <= a.length; i++) {
	// 			for (j = 1; j <= b.length; j++) {
	// 				if (a.charAt(i - 1) == b.charAt(j - 1)) {
	// 					cost = 0;
	// 				} else {
	// 					cost = 1;
	// 				}
	// 				d[i][j] = Math.min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost);
	// 				if (i > 1 && j > 1 && a.charAt(i - 1) == b.charAt(j - 2) && a.charAt(i - 2) == b.charAt(j - 1)) {
	// 					d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);
	// 				}
	// 			}
	// 		}
	// 		return d[a.length][b.length];
	// 	}
	// 	///////

	// 	function render(){
	// 		if(!data.response) return;

	// 		if(data.response.length) {
	// 			$tool.addClass('search-tool--searching');
	// 		} else {
	// 			$tool.removeClass('search-tool--searching');
	// 		}

	// 		var html = ''
	// 		_.each(data.response, function(result, index){
	// 			html += templateItem(_.extend({}, {active: (index === data.activeIndex)}, result));
	// 		})
	// 		$container.html(html)
	// 	}

	// }


	return {
		run: function(){
			var searchInput = $('.input-autocomplete--hotel');
			searchInput.geocomplete({
			  blur: true,
			  types: ['geocode'],
			  details: "form"
			});
		}
	}

}());
