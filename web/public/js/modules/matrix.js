App.Modules['matrix'] = (function(){

	'use strict';

	function run(){

		$('.matrix')
			.on('mouseout', 'td', function(e){
				$('.matrix__cell--highlighted').removeClass('matrix__cell--highlighted');
			})
			.on('mouseover', 'td', function(e){
				var column = $(this).index() + 1;
				var row = $(this).parent().index() + 1;
				$('.matrix tr:nth-child(-n+'+row+') td:nth-child('+column+')').addClass('matrix__cell--highlighted');
				$('.matrix tr:nth-child('+row+') td:nth-child(-n+'+column+')').addClass('matrix__cell--highlighted');
			})

	}

	return {
		run : run
	}


}());