App.Modules['deals-map'] = (function(){

	'use strict';


	/**
	 * MapView
	 */

	var mapView;
	var mapStyle=[{featureType:"water",elementType:"all",stylers:[{hue:"#7fc8ed"},{saturation:55},{lightness:-6},{visibility:"on"}]},{featureType:"landscape",elementType:"all",stylers:[{hue:"#ffffff"},{saturation:-100},{lightness:100},{visibility:"on"}]},{featureType:"road",elementType:"all",stylers:[{hue:"#ffffff"},{saturation:-100},{lightness:100},{visibility:"on"}]},{featureType:"administrative",elementType:"all",stylers:[{hue:"#ffffff"},{saturation:0},{lightness:100},{visibility:"off"}]}];
	var points = [];

	function MapView(data) {

		var scope = this;

		scope.myLatlng = null;
		scope.latlngbounds = null;
		scope.map = null;

		scope.windowMap = [];
		scope.windowMapOpened = null;
		scope.markers = [];

		scope.icon  = App.staticPath + '/img/modules/results/marker.png';
 		scope.activeIcon = App.staticPath + '/img/modules/results/marker-active.png';

 		scope.currency = data.currency;
 		scope.points = data.deals;

		google.maps.event.addDomListener(window, 'load', function(){
			scope.init();
		});

	}


	MapView.prototype.init = function() {
		var scope = this;

		scope.myLatlng = new google.maps.LatLng(-25.363882,131.044922);

		var mapOptions = {
				zoom: 15,
				center: scope.myLatlng,
				disableDefaultUI: true,
				zoomControl: false,
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.LARGE,
					position: google.maps.ControlPosition.LEFT_CENTER
				},
				mapTypeControlOptions: {
					mapTypeIds: [ 'Styled']
				},
				mapTypeId: 'Styled'
			};

		scope.map = new google.maps.Map(document.getElementById('map-deals'), mapOptions);

		// events
		// google.maps.event.addListener(scope.map, "zoom_changed", function () {
		// 	scope.getZoomChange();
		// });

		// map style
		scope.map.mapTypes.set('Styled', new google.maps.StyledMapType(mapStyle, { name: 'Styled' }));

		scope.getPoints();
	}


	MapView.prototype.getPoints = function() {

		var scope = this;

		scope.latlngbounds = new google.maps.LatLngBounds();

		_.each(scope.points, function(point, i){
			scope.setPoint(point, i);
		});

		scope.markerCluster = new MarkerClusterer(scope.map, scope.markers);
		scope.map.fitBounds(scope.latlngbounds);

	}


	MapView.prototype.setPoint = function(point, index) {

		var scope = this,

			contentBox = '<figure> \
							<img src="'+ point.picture +'" width="250"> \
							<figcaption>'+ point.title +'</figcaption> \
						</figure> \
						<p>from '+ scope.currency +' '+ point.price +'</p>',

			marker = new google.maps.Marker({
							position: new google.maps.LatLng(point.lat, point.lng),
							title: point.title,
							icon: scope.icon,
							animation: google.maps.Animation.FADE
						}),

			myOptions = {
					content: contentBox,
					pixelOffset: new google.maps.Size(-132, -80),
					infoBoxClearance: new google.maps.Size(43, 40)
				},

			windowMap = new InfoBox(myOptions);

		marker.filters = point.filters;

		scope.markers[index] = marker;
		scope.markers[index].id = point.id;

		scope.markers[index].windowMap = new InfoBox(myOptions);
		scope.markers[index].windowMap.id = point.hotelId;
		scope.markers[index].windowMap.info = point;
		scope.markers[index].windowMap.active = false;

		scope.markers[index].windowMap.listener = google.maps.event.addListener(marker, 'click', function (e) {
			scope.openWindowMap(index, scope.markers[index]);
		});


		scope.latlngbounds.extend(marker.position);
	}


	MapView.prototype.openWindowMap = function(index, marker) {
		var scope = this;

		scope.closeWindowMap();

		marker.setIcon(scope.activeIcon);
		marker.windowMap.open(scope.map, marker);
		marker.windowMap.active = true;

		scope.latlngbounds = new google.maps.LatLngBounds();
		scope.latlngbounds.extend(marker.position);
		scope.map.fitBounds(scope.latlngbounds);
		scope.map.setZoom(16);
	}


	MapView.prototype.closeWindowMap = function() {
		var scope = this;

		_.each(scope.markers, function(marker) {
			if(marker.windowMap.active) {
				marker.windowMap.active = false;
				marker.windowMap.close();
				marker.setIcon(scope.icon);
			}
		})

		// 	point = _.where(scope.windowMap, { id: scope.windowMapOpened });

		// if(point.length) {
		// 	point = point[0];
		// 	point.close();
		// 	point.marker.setIcon(scope.icon);
		// }

	}

	MapView.prototype.filter = function(filtersSelected, filters) {

		var scope = this,
			markersApproved = [],
			markersRemoved = [];

		scope.closeWindowMap();

		scope.latlngbounds = new google.maps.LatLngBounds();

		if(scope.markers.length) {

				_.each(scope.markers, function(marker, i){

					if(filtersSelected.length) {

						var hasFilter = false;

						_.each(marker.filters, function(filter) {
							if(filtersSelected.indexOf(filter) >= 0) {
								hasFilter = true;
							}
						});

						if(!hasFilter) {
							markersRemoved.push(marker);
						} else {
							markersApproved.push(marker);
							scope.latlngbounds.extend(marker.position);
						}

					} else {
						markersApproved.push(marker);
						scope.latlngbounds.extend(marker.position);
					}
				});


			if(scope.markerCluster) {
				scope.markerCluster.clearMarkers();
				scope.markerCluster.addMarkers(markersApproved);
				scope.map.setZoom(14);
			}


			scope.map.fitBounds(scope.latlngbounds);

			if(markersApproved.length == 1) {
				scope.openWindowMap(markersApproved[0].id)
			}
		}

	}





	function run(data) {
		$('.results-filter--deals').on('click', '.results-filter-compact__btn-map', function(e){
			e.preventDefault();
			$(this).toggleClass('results-filter-compact__btn-map--active');
			$('.results-map').toggleClass('results-map--active');
		});

		mapView = new MapView(data);
	}

	function filter(filtersSelected, filters) {
		mapView.filter(filtersSelected, filters);
	}

	return {
		run: run,
		filter: filter
	}

}());