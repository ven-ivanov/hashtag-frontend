App.Modules['modal'] = (function () {

	'use strict';

	function run(){
		// App.Modules['internal-tabs'].run();

		var modal = null;

		$(document).on('click', '.btn-modal', function(e) {
			e.preventDefault();

			var $el = $(e.target);
			modal = typeof $el.attr('href') === 'undefined' ? $el.attr('data-href') : $el.attr('href');

			if(modal == '#') {
				close();
			} else {
				open(modal);
			}

		});


		$('.modal, .modal .btn-modal').on('click', function(e) {
			if( $(e.target).hasClass('modal') || $(e.target).attr('href') == '#') {
				e.preventDefault();
				close();
			}

		});


		function open (modal) {
			close();
			$(modal).addClass('modal--active');

			setTimeout(function () {
				// center();
			});
		}

		function close () {
			if($('.modal.modal--active').length) $('.modal.modal--active').removeClass('modal--active');
		}

		function center () {
			$('.modal__content').each(function () {
				var $el = $(this),
					height = $el.height();
				$el.css('margin-top', height / 2 * -1);
			});
		}

		$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				close();
			}
		});
	}

	return {
		run: run
	}

}());
