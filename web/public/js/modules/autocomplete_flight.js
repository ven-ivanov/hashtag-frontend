App.Modules['autocomplete_flight'] = (function(){

	"use strict";

	var KEY_CODES = {
		'ENTER': 13,
		'UP': 38,
		'DOWN': 40,
		'ESC': 27,
		'BACKSPACE': 8,
		'DELETE': 46
	}


	function run(){
		var $container = $('<ul />').addClass('input-autocomplete--results');
		var templateItem = _.template('<li class="<%=(active) ? "input-autocomplete--results--active" : ""%>"><%=label%> <strong>(<%=value%>)</strong></li>');
		var $inputs = $('.input-autocomplete--flight');
		var $input;
		var $tool;
		var data = {};

		$(document).on('setDestination', function(e, destination){
			$input = $('.search-tool .search-tool__tab.active').find('.input-icon--landing .input-autocomplete--flight')
			$tool = $input.closest('.search-tool');
			$container.appendTo($input.parent());

			$input.val(destination).trigger("focus");

			fetch()
		})

		$inputs.on('focus', function(){
			$input = $(this);
			$tool = $input.closest('.search-tool');
			$container.appendTo($input.parent());
		})

		$inputs.on('blur', function(){
			var next;
			var className;
			var multicityIndex = $input.closest('.search-tool-fieldset-multicity').find('.multicity-fieldset').index($input.closest('.multicity-fieldset'));

			if(data.response){
				if(data.activeIndex == undefined)
					data.activeIndex = 0;

				$input.val(data.response[data.activeIndex].label)
				$input.prev().val(data.response[data.activeIndex].value)

				next = $input.closest('.search-tool-fieldset').find('.input-autocomplete--flight').filter(function(index, elm){
					return $(elm).val().length <= 0
				})

				if($input.closest('.input-icon').hasClass('input-icon--takeoff')) {
					className = '.input-icon--takeoff';
				} else {
					className = '.input-icon--landing';
				}

				// if(multicityIndex < 1) {
				// 	_.each($(className).not($input.closest(className)), function(elm){

				// 		if($(elm).closest('.search-tool-fieldset-multicity').length && $(elm).closest('.search-tool-fieldset-multicity').find(className).index($(elm)) > 0)
				// 			return false;

				// 		$(elm)
				// 			.find('input:eq(0)').val(data.response[data.activeIndex].value).end()
				// 			.find('input:eq(1)').val(data.response[data.activeIndex].label);
				// 	})
				// }

			}

			data = {}
			$container = $container.empty().detach();
			$tool.removeClass('search-tool--searching');
			$input.removeClass('input-autocomplete--thinking')
			if(request) request.abort();
			if(next) next.eq(0).focus();

		})


		$inputs.on('keydown', function(e){

			switch (e.keyCode) {
				case KEY_CODES.UP : {

					if(data.activeIndex === undefined){
						data.activeIndex = 0;
					} else {
						data.activeIndex = Math.max(0, data.activeIndex - 1);
					}
					break;
				}

				case KEY_CODES.DOWN : {
					if(data.activeIndex === undefined){
						data.activeIndex = 0;
					} else {
						data.activeIndex = Math.min(data.response.length-1, data.activeIndex + 1);
					}

					break;
				}

				case KEY_CODES.ESC : {
					data.activeIndex = undefined;
					$(e.target).trigger('blur');

					break;
				}

				case KEY_CODES.ENTER : {
					e.preventDefault();
					$(e.target).trigger('blur');

					break;
				}

				case KEY_CODES.BACKSPACE || KEY_CODES.DELETE : {
					fetch();

					break;
				}
			}

			render();
		})

		$inputs.on('keypress input', function(e){
			fetch();
		})


		$container.on('mouseenter', 'li', function(e){
			var previousIndex = data.activeIndex;
			data.activeIndex = $(this).index();

			if(previousIndex !== data.activeIndex) render();
		})

		$container.on('mouseleave', 'li', function(e){
			data.activeIndex = undefined;
			render();
		})


		var timeout;
		var request;

		function fetch(){

			clearTimeout(timeout);

			timeout = setTimeout(function(){
				var val = $input.val().trim();
				val = normalize(val);

				if(val.length > 2) {
					// $input.addClass('input-autocomplete--thinking')

					// request = $.getJSON(App.baseUrl + '/api/autocomplete?loc=' + encodeURIComponent($input.val()), function(response){
					// 	if(response.length) {
					// 		data.response = response;
					// 		render();
					// 	}

					// 	$input.removeClass('input-autocomplete--thinking')
					// })

					//jQueryUI Autocomplete
					var term = val.toUpperCase();
					var results = $.grep(airports, function(value) {
						return (value.value == term) || (value.label.toUpperCase().indexOf(term) >= 0);
					});
					if (results.length > 1) {
						var distances = [];
						var top25 = [];
						for (var i = 0; i < results.length; i++) {
							var dValue = levenshtein(term, results[i].value);
							var dLabel = levenshtein(term, results[i].label);
							if($.inArray( dValue + '-' + dLabel, distances ) == -1){
								distances.push(dValue + '-' + dLabel);
								top25[dValue + '-' + dLabel] = results[i];
							}
						}
						results = [];
						distances = distances.sort();
						for (var i = 0; i < (distances.length < 25 - 1 ? distances.length : 25); i++) {
							results.push(top25[distances[i]]);
						}
					}
					/////

					data.response = results;

					render();

				}

			}, 100)

		}

		//jQueryUI Autocomplete
		function levenshtein (a, b) {
			var i;
			var j;
			var cost;
			var d = new Array();
			if (a.length == 0) {
				return b.length;
			}
			if (b.length == 0) {
				return a.length;
			}
			for (i = 0; i <= a.length; i++) {
				d[i] = new Array();
				d[i][0] = i;
			}
			for (j = 0; j <= b.length; j++) {
				d[0][j] = j;
			}
			for (i = 1; i <= a.length; i++) {
				for (j = 1; j <= b.length; j++) {
					if (a.charAt(i - 1) == b.charAt(j - 1)) {
						cost = 0;
					} else {
						cost = 1;
					}
					d[i][j] = Math.min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cost);
					if (i > 1 && j > 1 && a.charAt(i - 1) == b.charAt(j - 2) && a.charAt(i - 2) == b.charAt(j - 1)) {
						d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);
					}
				}
			}
			return d[a.length][b.length];
		}
		///////

		function render(){
			if(!data.response) return;

			if(data.response.length) {
				$tool.addClass('search-tool--searching');
			} else {
				$tool.removeClass('search-tool--searching');
			}

			var html = ''
			_.each(data.response, function(result, index){
				result.label = result.label.replace(/ \(([A-Z]{3})\)/, "");
				html += templateItem(_.extend({}, {active: (index === data.activeIndex)}, result));
			})
			$container.html(html)
		}

	}


	var airports = [{
			"label": "El Arish, Egypt - Al Arish (AAC)",
			"value": "AAC",
			"domestic": 0
		}, {
			"label": "Annaba, Algeria - Les Salines Airport (AAE)",
			"value": "AAE",
			"domestic": 0
		}, {
			"label": "Aachen, Germany - Maastricht (AAH)",
			"value": "AAH",
			"domestic": 0
		}, {
			"label": "Aalborg, Denmark - Aalborg (AAL)",
			"value": "AAL",
			"domestic": 0
		}, {
			"label": "Mala Mala, South Africa - Mala Mala Airport (AAM)",
			"value": "AAM",
			"domestic": 0
		}, {
			"label": "Al Ain, United Arab Emirates - Al Ain (AAN)",
			"value": "AAN",
			"domestic": 0
		}, {
			"label": "Anapa, Russia - Anapa (AAQ)",
			"value": "AAQ",
			"domestic": 0
		}, {
			"label": "Aarhus, Denmark - Tirstrup (AAR)",
			"value": "AAR",
			"domestic": 0
		}, {
			"label": "Altay, China - Altay Airport (AAT)",
			"value": "AAT",
			"domestic": 0
		}, {
			"label": "Araxa, Brazil - Araxa (AAX)",
			"value": "AAX",
			"domestic": 0
		}, {
			"label": "Al Ghaydah, Yemen - Al Ghaydah Airport (AAY)",
			"value": "AAY",
			"domestic": 0
		}, {
			"label": "Abakan, Russia - Abakan Airport (ABA)",
			"value": "ABA",
			"domestic": 0
		}, {
			"label": "Albacete, Spain - Los Llanos (ABC)",
			"value": "ABC",
			"domestic": 0
		}, {
			"label": "Abadan, Iran - Abadan Airport (ABD)",
			"value": "ABD",
			"domestic": 0
		}, {
			"label": "Allentown, United States - Lehigh Valley International Airport (ABE)",
			"value": "ABE",
			"domestic": 1
		}, {
			"label": "Abilene, United States - Abilene Municipal Airport (ABI)",
			"value": "ABI",
			"domestic": 1
		}, {
			"label": "Abidjan, Cote D\'Ivoire - F.H. Boigny (ABJ)",
			"value": "ABJ",
			"domestic": 0
		}, {
			"label": "Kabri Dar, Ethiopia - Kabri Dar Airport (ABK)",
			"value": "ABK",
			"domestic": 0
		}, {
			"label": "Ambler, United States - Ambler Airport (ABL)",
			"value": "ABL",
			"domestic": 1
		}, {
			"label": "Bamaga, Australia - Bamaga Airport (ABM)",
			"value": "ABM",
			"domestic": 0
		}, {
			"label": "Albuquerque, United States - Albuquerque (ABQ)",
			"value": "ABQ",
			"domestic": 1
		}, {
			"label": "Aberdeen, United States - Aberdeen Municipal Airport (ABR)",
			"value": "ABR",
			"domestic": 1
		}, {
			"label": "Abu Simbel, Egypt - Abu Simbel Airport (ABS)",
			"value": "ABS",
			"domestic": 0
		}, {
			"label": "Al-Baha, Saudi Arabia - Al-Aqiq Airport (ABT)",
			"value": "ABT",
			"domestic": 0
		}, {
			"label": "Abuja, Nigeria - Abuja International (ABV)",
			"value": "ABV",
			"domestic": 0
		}, {
			"label": "Albury, Australia - Albury (ABX)",
			"value": "ABX",
			"domestic": 0
		}, {
			"label": "Albany, GA, United States - Albany (ABY)",
			"value": "ABY",
			"domestic": 1
		}, {
			"label": "Aberdeen, United Kingdom - Dyce (ABZ)",
			"value": "ABZ",
			"domestic": 0
		}, {
			"label": "Acapulco, Mexico - Alvarez International Airport (ACA)",
			"value": "ACA",
			"domestic": 0
		}, {
			"label": "Accra, Ghana - Kotoka (ACC)",
			"value": "ACC",
			"domestic": 0
		}, {
			"label": "Lanzarote, Spain - Lanzarote (ACE)",
			"value": "ACE",
			"domestic": 0
		}, {
			"label": "Altenrhein, Switzerland - Altenrhein Airport (ACH)",
			"value": "ACH",
			"domestic": 0
		}, {
			"label": "Alderney, United Kingdom - The Blaye Airport (ACI)",
			"value": "ACI",
			"domestic": 0
		}, {
			"label": "Nantucket, United States - Nantucket Memorial Airport (ACK)",
			"value": "ACK",
			"domestic": 1
		}, {
			"label": "Sahand, Iran - Sahand Airport (ACP)",
			"value": "ACP",
			"domestic": 0
		}, {
			"label": "Araracuara, Colombia - Araracuara Airport (ACR)",
			"value": "ACR",
			"domestic": 0
		}, {
			"label": "Waco, United States - Waco Municipal Airport (ACT)",
			"value": "ACT",
			"domestic": 1
		}, {
			"label": "Arcata, United States - Arcata Airport (ACV)",
			"value": "ACV",
			"domestic": 1
		}, {
			"label": "Xingyi, China - Xingyi Airport (ACX)",
			"value": "ACX",
			"domestic": 0
		}, {
			"label": "Atlantic City, United States - Atlantic City (ACY)",
			"value": "ACY",
			"domestic": 1
		}, {
			"label": "Zabol, Iran - Zabol Airport (ACZ)",
			"value": "ACZ",
			"domestic": 0
		}, {
			"label": "Adana, Turkey - Adana (ADA)",
			"value": "ADA",
			"domestic": 0
		}, {
			"label": "Izmir, Turkey - Adnan Menderes (ADB)",
			"value": "ADB",
			"domestic": 0
		}, {
			"label": "Addis Ababa, Ethiopia - Bole (ADD)",
			"value": "ADD",
			"domestic": 0
		}, {
			"label": "Aden, Yemen - Aden Intl (ADE)",
			"value": "ADE",
			"domestic": 0
		}, {
			"label": "Adiyaman, Turkey - Adiyaman Airport (ADF)",
			"value": "ADF",
			"domestic": 0
		}, {
			"label": "Amman, Jordan - Amman Civil-Marka (ADJ)",
			"value": "ADJ",
			"domestic": 0
		}, {
			"label": "Adak, United States - Adak Airport (ADK)",
			"value": "ADK",
			"domestic": 1
		}, {
			"label": "Adelaide, Australia - Adelaide (ADL)",
			"value": "ADL",
			"domestic": 0
		}, {
			"label": "Ardmore, United States - Ardmore Mncpl (ADM)",
			"value": "ADM",
			"domestic": 1
		}, {
			"label": "Kodiak, United States - Kodiak Airport (ADQ)",
			"value": "ADQ",
			"domestic": 1
		}, {
			"label": "Ada, United States - Ada Mncpl (ADT)",
			"value": "ADT",
			"domestic": 1
		}, {
			"label": "Ardabil, Iran - Ardabil Airport (ADU)",
			"value": "ADU",
			"domestic": 0
		}, {
			"label": "San Andres Island, Colombia - San Andres (ADZ)",
			"value": "ADZ",
			"domestic": 0
		}, {
			"label": "Baise, China - Baise Airport (AEB)",
			"value": "AEB",
			"domestic": 0
		}, {
			"label": "Albert Lea, United States - Albert Lea (AEL)",
			"value": "AEL",
			"domestic": 1
		}, {
			"label": "Buenos Aires, Argentina - Jorge Newbery Airport (AEP)",
			"value": "AEP",
			"domestic": 0
		}, {
			"label": "Adler/Sochi, Russia - Adler/Sochi Airport (AER)",
			"value": "AER",
			"domestic": 0
		}, {
			"label": "Aalesund, Norway - Vigra (AES)",
			"value": "AES",
			"domestic": 0
		}, {
			"label": "Allakaket, United States - Allakaket Airport (AET)",
			"value": "AET",
			"domestic": 1
		}, {
			"label": "Alexandria, LA, United States - Alexandria International Airport (AEX)",
			"value": "AEX",
			"domestic": 1
		}, {
			"label": "Akureyri, Iceland - Akureyri (AEY)",
			"value": "AEY",
			"domestic": 0
		}, {
			"label": "San Rafael, Argentina - San Rafael Airport (AFA)",
			"value": "AFA",
			"domestic": 0
		}, {
			"label": "Alta Floresta, Brazil - Alta Floresta Airport (AFL)",
			"value": "AFL",
			"domestic": 0
		}, {
			"label": "Zarafshan, Uzbekistan - Zarafshan Airport (AFS)",
			"value": "AFS",
			"domestic": 0
		}, {
			"label": "Afutara, Solomon Islands - Afutara Aerodrome (AFT)",
			"value": "AFT",
			"domestic": 0
		}, {
			"label": "Sabzevar, Iran - Sabzevar Airport (AFZ)",
			"value": "AFZ",
			"domestic": 0
		}, {
			"label": "Agadir, Morocco - Agadir Almassira (AGA)",
			"value": "AGA",
			"domestic": 0
		}, {
			"label": "Agen, France - Agen La Garenne (AGF)",
			"value": "AGF",
			"domestic": 0
		}, {
			"label": "Helsingborg, Sweden - Angelholm (AGH)",
			"value": "AGH",
			"domestic": 0
		}, {
			"label": "Wanigela, Papua New Guinea - Wanigela Airport (AGL)",
			"value": "AGL",
			"domestic": 0
		}, {
			"label": "Tasiilaq, Greenland - Tasiilaq Airport (AGM)",
			"value": "AGM",
			"domestic": 0
		}, {
			"label": "Angoon, United States - Angoon Airport (AGN)",
			"value": "AGN",
			"domestic": 1
		}, {
			"label": "Malaga, Spain - Malaga (AGP)",
			"value": "AGP",
			"domestic": 0
		}, {
			"label": "Agra, India - Kheria (AGR)",
			"value": "AGR",
			"domestic": 0
		}, {
			"label": "Augusta, GA, United States - Bush Field (AGS)",
			"value": "AGS",
			"domestic": 1
		}, {
			"label": "Augusta, ME, United States - Daniel Field (AUG)",
			"value": "AGS",
			"domestic": 1
		}, {
			"label": "Ciudad Del Este, Paraguay - Alejo Garcia Airport (AGT)",
			"value": "AGT",
			"domestic": 0
		}, {
			"label": "Aguascalientes, Mexico - Aguascalients Airport (AGU)",
			"value": "AGU",
			"domestic": 0
		}, {
			"label": "Agatti Island, India - Agatti Island Airport (AGX)",
			"value": "AGX",
			"domestic": 0
		}, {
			"label": "Abha, Saudi Arabia - Abha (AHB)",
			"value": "AHB",
			"domestic": 0
		}, {
			"label": "Ahe, French Polynesia and Tahiti - Ahe Airport (AHE)",
			"value": "AHE",
			"domestic": 0
		}, {
			"label": "Athens, GA, United States - Athens Airport (AHN)",
			"value": "AHN",
			"domestic": 1
		}, {
			"label": "Alghero, Italy - Fertilia (AHO)",
			"value": "AHO",
			"domestic": 0
		}, {
			"label": "Ahuas, Honduras - Ahuas Airport (AHS)",
			"value": "AHS",
			"domestic": 0
		}, {
			"label": "Al Hoceima, Morocco - Charif Al Idrissi (AHU)",
			"value": "AHU",
			"domestic": 0
		}, {
			"label": "Alliance, United States - Alliance Municipal Airport (AIA)",
			"value": "AIA",
			"domestic": 1
		}, {
			"label": "Airok, Marshall Islands - Airok Airport (AIC)",
			"value": "AIC",
			"domestic": 0
		}, {
			"label": "Anderson, United States - Anderson Mncpl (AID)",
			"value": "AID",
			"domestic": 1
		}, {
			"label": "Ailuk Island, Marshall Islands - Ailuk Island Airport (AIM)",
			"value": "AIM",
			"domestic": 0
		}, {
			"label": "Wainwright, United States - Wainwright Airport (AIN)",
			"value": "AIN",
			"domestic": 1
		}, {
			"label": "Aitutaki, Cook Islands - Aitutaki (AIT)",
			"value": "AIT",
			"domestic": 0
		}, {
			"label": "Atiu Island, Cook Islands - Atiu Island Airport (AIU)",
			"value": "AIU",
			"domestic": 0
		}, {
			"label": "Lake Ozark, United States - Lee C. Fine (AIZ)",
			"value": "AIZ",
			"domestic": 1
		}, {
			"label": "Ajaccio, France - Campo Dell Oro (AJA)",
			"value": "AJA",
			"domestic": 0
		}, {
			"label": "Jouf, Saudi Arabia - Jouf Airport (AJF)",
			"value": "AJF",
			"domestic": 0
		}, {
			"label": "Agri, Turkey - Agri Airport (AJI)",
			"value": "AJI",
			"domestic": 0
		}, {
			"label": "Aizawl, India - Aizawl Airport (AJL)",
			"value": "AJL",
			"domestic": 0
		}, {
			"label": "Anjouan, Comoros - Ouani Airport (AJN)",
			"value": "AJN",
			"domestic": 0
		}, {
			"label": "Arvidsjaur, Sweden - Arvidsjaur (AJR)",
			"value": "AJR",
			"domestic": 0
		}, {
			"label": "Aracaju, Brazil - Santa Maria (AJU)",
			"value": "AJU",
			"domestic": 0
		}, {
			"label": "Ankang, China - Ankang Airport (AKA)",
			"value": "AKA",
			"domestic": 0
		}, {
			"label": "Atka, United States - Atka Airport (AKB)",
			"value": "AKB",
			"domestic": 1
		}, {
			"label": "Kufrah, Libya - Kufrah Airport (AKF)",
			"value": "AKF",
			"domestic": 0
		}, {
			"label": "Anguganak, Papua New Guinea - Anguganak Airport (AKG)",
			"value": "AKG",
			"domestic": 0
		}, {
			"label": "Akiak, United States - Akiak Airport (AKI)",
			"value": "AKI",
			"domestic": 1
		}, {
			"label": "Asahikawa, Japan - Asahikawa (AKJ)",
			"value": "AKJ",
			"domestic": 0
		}, {
			"label": "Auckland, New Zealand - Auckland International Airport (AKL)",
			"value": "AKL",
			"domestic": 0
		}, {
			"label": "King Salmon, United States - King Salmon Airport (AKN)",
			"value": "AKN",
			"domestic": 1
		}, {
			"label": "Anaktuvuk, United States - Anaktuvuk Airport (AKP)",
			"value": "AKP",
			"domestic": 1
		}, {
			"label": "Auki, Solomon Islands - Gwaunaru\'u Airport (AKS)",
			"value": "AKS",
			"domestic": 0
		}, {
			"label": "Aksu, China - Aksu Airport (AKU)",
			"value": "AKU",
			"domestic": 0
		}, {
			"label": "Akulivik, Canada - Akulivik Airport (AKV)",
			"value": "AKV",
			"domestic": 0
		}, {
			"label": "Aktyubinsk, Kazakhstan - Aktyubinsk Airport (AKX)",
			"value": "AKX",
			"domestic": 0
		}, {
			"label": "Sittwe, Myanmar (Burma) - Civil Airport (AKY)",
			"value": "AKY",
			"domestic": 0
		}, {
			"label": "Almaty, Kazakhstan - Almaty (ALA)",
			"value": "ALA",
			"domestic": 0
		}, {
			"label": "Albany, NY, United States - Albany (ALB)",
			"value": "ALB",
			"domestic": 1
		}, {
			"label": "Alicante, Spain - Alicante (ALC)",
			"value": "ALC",
			"domestic": 0
		}, {
			"label": "Alpine, United States - Alpine Mncpl (ALE)",
			"value": "ALE",
			"domestic": 1
		}, {
			"label": "Alta, Norway - Alta (ALF)",
			"value": "ALF",
			"domestic": 0
		}, {
			"label": "Algiers, Algeria - Houari Boumediene (ALG)",
			"value": "ALG",
			"domestic": 0
		}, {
			"label": "Albany, Australia - Albany (ALH)",
			"value": "ALH",
			"domestic": 0
		}, {
			"label": "Albenga, Italy - Albenga Airport (ALL)",
			"value": "ALL",
			"domestic": 0
		}, {
			"label": "Alamogordo, United States - Alamogordo Mncpl (ALM)",
			"value": "ALM",
			"domestic": 1
		}, {
			"label": "Waterloo, United States - Waterloo Airport (ALO)",
			"value": "ALO",
			"domestic": 1
		}, {
			"label": "Aleppo, Syria - Nejrab (ALP)",
			"value": "ALP",
			"domestic": 0
		}, {
			"label": "Alamosa, United States - San Luis Valley Regional Airport (ALS)",
			"value": "ALS",
			"domestic": 1
		}, {
			"label": "Andorra La Vella, Andorra - Andorra La Vella (ALV)",
			"value": "ALV",
			"domestic": 0
		}, {
			"label": "Walla Walla, United States - Walla Walla Airport (ALW)",
			"value": "ALW",
			"domestic": 1
		}, {
			"label": "Amarillo, United States - Amarillo (AMA)",
			"value": "AMA",
			"domestic": 1
		}, {
			"label": "Ahmedabad, India - Ahmedabad (AMD)",
			"value": "AMD",
			"domestic": 0
		}, {
			"label": "Arba Mintch, Ethiopia - Arba Mintch Airport (AMH)",
			"value": "AMH",
			"domestic": 0
		}, {
			"label": "Mataram, Indonesia - Selaparang (AMI)",
			"value": "AMI",
			"domestic": 0
		}, {
			"label": "Amman, Jordan - Queen Alia Intl (AMM)",
			"value": "AMM",
			"domestic": 0
		}, {
			"label": "Ambon, Indonesia - Pattimura Airport (AMQ)",
			"value": "AMQ",
			"domestic": 0
		}, {
			"label": "Amsterdam, Netherlands - Amsterdam-Schiphol Airport (AMS)",
			"value": "AMS",
			"domestic": 0
		}, {
			"label": "Ames, United States - Ames Mncpl (AMW)",
			"value": "AMW",
			"domestic": 1
		}, {
			"label": "Ambatomainty, Madagascar - Ambatomainty Airport (AMY)",
			"value": "AMY",
			"domestic": 0
		}, {
			"label": "Anniston, United States - Anniston Cty (ANB)",
			"value": "ANB",
			"domestic": 1
		}, {
			"label": "Anchorage, United States - Anchorage (ANC)",
			"value": "ANC",
			"domestic": 1
		}, {
			"label": "Anderson, United States - Anderson County (AND)",
			"value": "AND",
			"domestic": 1
		}, {
			"label": "Angers, France - Marce (ANE)",
			"value": "ANE",
			"domestic": 0
		}, {
			"label": "Antofagasta, Chile and Easter Island - Cerro Moreno (ANF)",
			"value": "ANF",
			"domestic": 0
		}, {
			"label": "Angouleme, France - Brie Champniers (ANG)",
			"value": "ANG",
			"domestic": 0
		}, {
			"label": "Aniak, United States - Aniak Airport (ANI)",
			"value": "ANI",
			"domestic": 1
		}, {
			"label": "Ankara, Turkey - Etimesgut (ANK)",
			"value": "ANK",
			"domestic": 0
		}, {
			"label": "Antalaha, Madagascar - Antsirabato (ANM)",
			"value": "ANM",
			"domestic": 0
		}, {
			"label": "Antwerp, Belgium - Deurne (ANR)",
			"value": "ANR",
			"domestic": 0
		}, {
			"label": "Andahuaylas, Peru - Andahuaylas Airport (ANS)",
			"value": "ANS",
			"domestic": 0
		}, {
			"label": "Antigua, Antigua and Barbuda - V.C. Bird International Airport (ANU)",
			"value": "ANU",
			"domestic": 0
		}, {
			"label": "Anvik, United States - Anvik Airport (ANV)",
			"value": "ANV",
			"domestic": 1
		}, {
			"label": "Ainsworth, United States - Ainsworth (ANW)",
			"value": "ANW",
			"domestic": 1
		}, {
			"label": "Andenes, Norway - Andenes Airport (ANX)",
			"value": "ANX",
			"domestic": 0
		}, {
			"label": "Altenburg, Germany - Altenburg (AOC)",
			"value": "AOC",
			"domestic": 0
		}, {
			"label": "Lima, United States - Allen County (AOH)",
			"value": "AOH",
			"domestic": 1
		}, {
			"label": "Ancona, Italy - Falconara (AOI)",
			"value": "AOI",
			"domestic": 0
		}, {
			"label": "Aomori, Japan - Aomori (AOJ)",
			"value": "AOJ",
			"domestic": 0
		}, {
			"label": "Karpathos, Greece - Karpathos Airport (AOK)",
			"value": "AOK",
			"domestic": 0
		}, {
			"label": "Altoona, United States - Blair Cty (AOO)",
			"value": "AOO",
			"domestic": 1
		}, {
			"label": "Alor Setar, Malaysia - Sultan Abdul Halim (AOR)",
			"value": "AOR",
			"domestic": 0
		}, {
			"label": "Amook, United States - Amook Airport (AOS)",
			"value": "AOS",
			"domestic": 1
		}, {
			"label": "Aosta, Italy - Aosta (AOT)",
			"value": "AOT",
			"domestic": 0
		}, {
			"label": "Napa, United States - Napa County (APC)",
			"value": "APC",
			"domestic": 1
		}, {
			"label": "Naples, United States - Naples Airport (APF)",
			"value": "APF",
			"domestic": 1
		}, {
			"label": "Nampula, Mozambique - Nampula Airport (APL)",
			"value": "APL",
			"domestic": 0
		}, {
			"label": "Alpena, United States - Alpena County Regional Airport (APN)",
			"value": "APN",
			"domestic": 1
		}, {
			"label": "Apartado, Colombia - Apartado Airport (APO)",
			"value": "APO",
			"domestic": 0
		}, {
			"label": "Apple Valley, United States - Apple Valley (APV)",
			"value": "APV",
			"domestic": 1
		}, {
			"label": "Apia, Samoa - Faleolo (APW)",
			"value": "APW",
			"domestic": 0
		}, {
			"label": "Araraquara, Brazil - Araraquara Airport (AQA)",
			"value": "AQA",
			"domestic": 0
		}, {
			"label": "Qaisumah, Saudi Arabia - Qaisumah Airport (AQI)",
			"value": "AQI",
			"domestic": 0
		}, {
			"label": "Aqaba, Jordan - King Hussein Intl (AQJ)",
			"value": "AQJ",
			"domestic": 0
		}, {
			"label": "Arequipa, Peru - Rodriguez Ballon (AQP)",
			"value": "AQP",
			"domestic": 0
		}, {
			"label": "Ann Arbor, United States - Ann Arbor (ARB)",
			"value": "ARB",
			"domestic": 1
		}, {
			"label": "Arctic Village, United States - Arctic Village Airport (ARC)",
			"value": "ARC",
			"domestic": 1
		}, {
			"label": "Arkhangelsk, Russia - Arkhangelsk (ARH)",
			"value": "ARH",
			"domestic": 0
		}, {
			"label": "Arica, Chile and Easter Island - Chacalluta (ARI)",
			"value": "ARI",
			"domestic": 0
		}, {
			"label": "Arusha, Tanzania - Arusha (ARK)",
			"value": "ARK",
			"domestic": 0
		}, {
			"label": "Armidale, Australia - Armidale (ARM)",
			"value": "ARM",
			"domestic": 0
		}, {
			"label": "Stockholm, Sweden - Arlanda Airport (ARN)",
			"value": "ARN",
			"domestic": 0
		}, {
			"label": "Watertown, United States - Watertown (ART)",
			"value": "ART",
			"domestic": 1
		}, {
			"label": "Aracatuba, Brazil - Aracatuba (ARU)",
			"value": "ARU",
			"domestic": 0
		}, {
			"label": "Minocqua, United States - Noble F. Lee (ARV)",
			"value": "ARV",
			"domestic": 1
		}, {
			"label": "Arad, Romania - Arad (ARW)",
			"value": "ARW",
			"domestic": 0
		}, {
			"label": "Asbury Park, United States - Asbury Park (ARX)",
			"value": "ARX",
			"domestic": 1
		}, {
			"label": "Assab, Eritrea - Assab Airport (ASA)",
			"value": "ASA",
			"domestic": 0
		}, {
			"label": "Ashgabat, Turkmenistan - Ashgabat (ASB)",
			"value": "ASB",
			"domestic": 0
		}, {
			"label": "Andros Town, Bahamas - Andros (ASD)",
			"value": "ASD",
			"domestic": 0
		}, {
			"label": "Aspen, United States - Aspen Airport (ASE)",
			"value": "ASE",
			"domestic": 1
		}, {
			"label": "Astrakhan, Russia - Astrakhan (ASF)",
			"value": "ASF",
			"domestic": 0
		}, {
			"label": "Amami O Shima, Japan - Amami O Shima Airport (ASJ)",
			"value": "ASJ",
			"domestic": 0
		}, {
			"label": "Asmara, Eritrea - Asmara Intl (ASM)",
			"value": "ASM",
			"domestic": 0
		}, {
			"label": "Asosa, Ethiopia - Asosa Airport (ASO)",
			"value": "ASO",
			"domestic": 0
		}, {
			"label": "Alice Springs, Australia - Alice Spring (ASP)",
			"value": "ASP",
			"domestic": 0
		}, {
			"label": "Kayseri, Turkey - Erkilet (ASR)",
			"value": "ASR",
			"domestic": 0
		}, {
			"label": "Astoria, United States - Astoria (AST)",
			"value": "AST",
			"domestic": 1
		}, {
			"label": "Asuncion, Paraguay - Silvio Pettirossi (ASU)",
			"value": "ASU",
			"domestic": 0
		}, {
			"label": "Amboseli, Kenya - Amboseli (ASV)",
			"value": "ASV",
			"domestic": 0
		}, {
			"label": "Aswan, Egypt - Aswan-Daraw (ASW)",
			"value": "ASW",
			"domestic": 0
		}, {
			"label": "Ashland, United States - Ashland (ASX)",
			"value": "ASX",
			"domestic": 1
		}, {
			"label": "Arthur\'s Town, Bahamas - Arthurs Town (ATC)",
			"value": "ATC",
			"domestic": 0
		}, {
			"label": "Atoifi, Solomon Islands - Atoifi Airport (ATD)",
			"value": "ATD",
			"domestic": 0
		}, {
			"label": "Athens, Greece - Athinai Airport (ATH)",
			"value": "ATH",
			"domestic": 0
		}, {
			"label": "Atqasuk, United States - Atqasuk Airport (ATK)",
			"value": "ATK",
			"domestic": 1
		}, {
			"label": "Atlanta, United States - Atlanta (ATL)",
			"value": "ATL",
			"domestic": 1
		}, {
			"label": "Altamira, Brazil - Altamira Airport (ATM)",
			"value": "ATM",
			"domestic": 0
		}, {
			"label": "Namatanai, Papua New Guinea - Namatanai Airport (ATN)",
			"value": "ATN",
			"domestic": 0
		}, {
			"label": "Aitape, Papua New Guinea - Aitape (ATP)",
			"value": "ATP",
			"domestic": 0
		}, {
			"label": "Amritsar, India - Raja Sansi (ATQ)",
			"value": "ATQ",
			"domestic": 0
		}, {
			"label": "Atmautluak, United States - Atmautluak Airport (ATT)",
			"value": "ATT",
			"domestic": 1
		}, {
			"label": "Appleton, United States - Outagamie County Airport (ATW)",
			"value": "ATW",
			"domestic": 1
		}, {
			"label": "Watertown, United States - Watertown Airport (ATY)",
			"value": "ATY",
			"domestic": 1
		}, {
			"label": "Assiut, Egypt - Assiut Airport (ATZ)",
			"value": "ATZ",
			"domestic": 0
		}, {
			"label": "Oranjestad, Aruba - Reina Beatrix Airport (AUA)",
			"value": "AUA",
			"domestic": 0
		}, {
			"label": "Arauca, Colombia - Arauca Airport (AUC)",
			"value": "AUC",
			"domestic": 0
		}, {
			"label": "Auxerre, France - Auxerre Branches (AUF)",
			"value": "AUF",
			"domestic": 0
		}, {
			"label": "Abu Dhabi, United Arab Emirates - Abu Dhabi Intl (AUH)",
			"value": "AUH",
			"domestic": 0
		}, {
			"label": "Alakanuk, United States - Alakanuk Airport (AUK)",
			"value": "AUK",
			"domestic": 1
		}, {
			"label": "Aur Island, Marshall Islands - Aur Island Airport (AUL)",
			"value": "AUL",
			"domestic": 0
		}, {
			"label": "Auburn, United States - Auburn-Opelika (AUO)",
			"value": "AUO",
			"domestic": 1
		}, {
			"label": "Atuona, French Polynesia and Tahiti - Atuona (AUQ)",
			"value": "AUQ",
			"domestic": 0
		}, {
			"label": "Aurillac, France - Tronquieres (AUR)",
			"value": "AUR",
			"domestic": 0
		}, {
			"label": "Austin, United States - Austin (AUS)",
			"value": "AUS",
			"domestic": 1
		}, {
			"label": "Aurukun Mission, Australia - Aurukun Mission Airport (AUU)",
			"value": "AUU",
			"domestic": 0
		}, {
			"label": "Wausau, United States - Wausau (AUW)",
			"value": "AUW",
			"domestic": 1
		}, {
			"label": "Araguaina, Brazil - Araguaina Airport (AUX)",
			"value": "AUX",
			"domestic": 0
		}, {
			"label": "Aneityum, Vanuatu - Aneityum Airport (AUY)",
			"value": "AUY",
			"domestic": 0
		}, {
			"label": "An Shun, China - An Shun/Huang Guo Shu Airport (AVA)",
			"value": "AVA",
			"domestic": 0
		}, {
			"label": "Ciego de Avila, Cuba - Maximo Gomez (AVI)",
			"value": "AVI",
			"domestic": 0
		}, {
			"label": "Arvaikheer, Mongolia - Arvaikheer Airport (AVK)",
			"value": "AVK",
			"domestic": 0
		}, {
			"label": "Asheville, United States - Asheville Municipal Airport (AVL)",
			"value": "AVL",
			"domestic": 1
		}, {
			"label": "Avignon, France - Avignon/Caumont (AVN)",
			"value": "AVN",
			"domestic": 0
		}, {
			"label": "Wilkes-Barre, United States - Wilkes-Barre International Airport (AVP)",
			"value": "AVP",
			"domestic": 1
		}, {
			"label": "Avu Avu, Solomon Islands - Avu Avu Airport (AVU)",
			"value": "AVU",
			"domestic": 0
		}, {
			"label": "Melbourne, Australia - Avalon (AVV)",
			"value": "AVV",
			"domestic": 0
		}, {
			"label": "Catalina Island, United States - Avalon Bay (AVX)",
			"value": "AVX",
			"domestic": 1
		}, {
			"label": "Aniwa, Vanuatu - Aniwa Airport (AWD)",
			"value": "AWD",
			"domestic": 0
		}, {
			"label": "Ahwaz, Iran - Ahwaz Airport (AWZ)",
			"value": "AWZ",
			"domestic": 0
		}, {
			"label": "Anguilla, Anguilla - Wallblake Airport (AXA)",
			"value": "AXA",
			"domestic": 0
		}, {
			"label": "Alexandria Bay, United States - Alexandria Bay Mncpl (AXB)",
			"value": "AXB",
			"domestic": 1
		}, {
			"label": "Alexandroupolis, Greece - Demokritos (AXD)",
			"value": "AXD",
			"domestic": 0
		}, {
			"label": "Armenia, Colombia - El Eden Airport (AXM)",
			"value": "AXM",
			"domestic": 0
		}, {
			"label": "Spring Point, Bahamas - Springpoint Airport (AXP)",
			"value": "AXP",
			"domestic": 0
		}, {
			"label": "Arutua, French Polynesia and Tahiti - Arutua Airport (AXR)",
			"value": "AXR",
			"domestic": 0
		}, {
			"label": "Altus, United States - Altus Mncpl (AXS)",
			"value": "AXS",
			"domestic": 1
		}, {
			"label": "Akita, Japan - Akita (AXT)",
			"value": "AXT",
			"domestic": 0
		}, {
			"label": "Axum, Ethiopia - Axum Airport (AXU)",
			"value": "AXU",
			"domestic": 0
		}, {
			"label": "Angel Fire, United States - Angel Fire Mncpl (AXX)",
			"value": "AXX",
			"domestic": 1
		}, {
			"label": "Ayacucho, Peru - Yanamilla Airport (AYP)",
			"value": "AYP",
			"domestic": 0
		}, {
			"label": "Ayers Rock, Australia - Ayers Rock Connellan (AYQ)",
			"value": "AYQ",
			"domestic": 0
		}, {
			"label": "Waycross, United States - Waycross-Ware County (AYS)",
			"value": "AYS",
			"domestic": 1
		}, {
			"label": "Antalya, Turkey - Antalya (AYT)",
			"value": "AYT",
			"domestic": 0
		}, {
			"label": "Mesa, United States - Phoenix-Mesa Gateway Airport (AZA)",
			"value": "AZA",
			"domestic": 1
		}, {
			"label": "Yazd, Iran - Yazd Airport (AZD)",
			"value": "AZD",
			"domestic": 0
		}, {
			"label": "Andizhan, Uzbekistan - Andizhan Airport (AZN)",
			"value": "AZN",
			"domestic": 0
		}, {
			"label": "Kalamazoo, United States - Battle Creek International Airport (AZO)",
			"value": "AZO",
			"domestic": 1
		}, {
			"label": "Adrar, Algeria - Adrar Airport (AZR)",
			"value": "AZR",
			"domestic": 0
		}, {
			"label": "Samana, Dominican Republic - Samana Intl (AZS)",
			"value": "AZS",
			"domestic": 0
		}, {
			"label": "Baguio, Philippines - Loakan (BAG)",
			"value": "BAG",
			"domestic": 0
		}, {
			"label": "Bahrain, Bahrain - Bahrain Intl (BAH)",
			"value": "BAH",
			"domestic": 0
		}, {
			"label": "Batman, Turkey - Batman Airport (BAL)",
			"value": "BAL",
			"domestic": 0
		}, {
			"label": "Battle Mountain, United States - Lander County (BAM)",
			"value": "BAM",
			"domestic": 1
		}, {
			"label": "Barranquilla, Colombia - E Cortissoz Airport (BAQ)",
			"value": "BAQ",
			"domestic": 0
		}, {
			"label": "Balalae, Solomon Islands - Balalae Airport (BAS)",
			"value": "BAS",
			"domestic": 0
		}, {
			"label": "Bauru, Brazil - Bauru (BAU)",
			"value": "BAU",
			"domestic": 0
		}, {
			"label": "Baotou, China - Baotou (BAV)",
			"value": "BAV",
			"domestic": 0
		}, {
			"label": "Barnaul, Russia - Barnaul Airport (BAX)",
			"value": "BAX",
			"domestic": 0
		}, {
			"label": "Baia Mare, Romania - Baia Mare (BAY)",
			"value": "BAY",
			"domestic": 0
		}, {
			"label": "Barbelos, Brazil - Barbelos Airport (BAZ)",
			"value": "BAZ",
			"domestic": 0
		}, {
			"label": "Balmaceda, Chile and Easter Island - Teniente Vidal Airport (BBA)",
			"value": "BBA",
			"domestic": 0
		}, {
			"label": "Bhubaneswar, India - Bhubaneswar (BBI)",
			"value": "BBI",
			"domestic": 0
		}, {
			"label": "Kasane, Botswana - Kasane (BBK)",
			"value": "BBK",
			"domestic": 0
		}, {
			"label": "Bario, Malaysia - Bario Airport (BBN)",
			"value": "BBN",
			"domestic": 0
		}, {
			"label": "Berbera, Somalia - Berbera Airport (BBO)",
			"value": "BBO",
			"domestic": 0
		}, {
			"label": "Bucharest, Romania - Baneasa (BBU)",
			"value": "BBU",
			"domestic": 0
		}, {
			"label": "Blue Bell, United States - Wings Field (BBX)",
			"value": "BBX",
			"domestic": 1
		}, {
			"label": "Baracoa, Cuba - Baracua (BCA)",
			"value": "BCA",
			"domestic": 0
		}, {
			"label": "Blacksburg, United States - Virginia Tech (BCB)",
			"value": "BCB",
			"domestic": 1
		}, {
			"label": "Bacolod, Philippines - Bacolod Airport (BCD)",
			"value": "BCD",
			"domestic": 0
		}, {
			"label": "Bryce Canyon, United States - Bryce Canyon Mncpl (BCE)",
			"value": "BCE",
			"domestic": 1
		}, {
			"label": "Barcaldine, Australia - Barcaldine Airport (BCI)",
			"value": "BCI",
			"domestic": 0
		}, {
			"label": "Barra Colorado, Costa Rica - Barra Colorado Airport (BCL)",
			"value": "BCL",
			"domestic": 0
		}, {
			"label": "Bacau, Romania - Bacau (BCM)",
			"value": "BCM",
			"domestic": 0
		}, {
			"label": "Barcelona, Spain - Barcelona Airport (BCN)",
			"value": "BCN",
			"domestic": 0
		}, {
			"label": "Jinka, Ethiopia - Jinka Airport (BCO)",
			"value": "BCO",
			"domestic": 0
		}, {
			"label": "Bambu, Papua New Guinea - Bambu Airport (BCP)",
			"value": "BCP",
			"domestic": 0
		}, {
			"label": "Benguera Island, Mozambique - Benguera Island Airport (BCW)",
			"value": "BCW",
			"domestic": 0
		}, {
			"label": "Hamilton, Bermuda - Bermuda International Airport (BDA)",
			"value": "BDA",
			"domestic": 0
		}, {
			"label": "Bundaberg, Australia - Bundaberg (BDB)",
			"value": "BDB",
			"domestic": 0
		}, {
			"label": "Badu Island, Australia - Badu Island Airport (BDD)",
			"value": "BDD",
			"domestic": 0
		}, {
			"label": "Blanding, United States - Blanding Mncpl (BDG)",
			"value": "BDG",
			"domestic": 1
		}, {
			"label": "Bandar Lengeh, Iran - Bandar Lengeh Airport (BDH)",
			"value": "BDH",
			"domestic": 0
		}, {
			"label": "Banjarmasin, Indonesia - Sjamsudin Noor Airport (BDJ)",
			"value": "BDJ",
			"domestic": 0
		}, {
			"label": "Hartford, United States - Hartford (BDL)",
			"value": "BDL",
			"domestic": 1
		}, {
			"label": "Bandung, Indonesia - Husein Sastranegara (BDO)",
			"value": "BDO",
			"domestic": 0
		}, {
			"label": "Bhadrapur, Nepal - Bhadrapur Airport (BDP)",
			"value": "BDP",
			"domestic": 0
		}, {
			"label": "Vadodara, India - Vadodara (BDQ)",
			"value": "BDQ",
			"domestic": 0
		}, {
			"label": "Bridgeport, United States - Sikorsky Memorial (BDR)",
			"value": "BDR",
			"domestic": 1
		}, {
			"label": "Brindisi, Italy - Papola Casale (BDS)",
			"value": "BDS",
			"domestic": 0
		}, {
			"label": "Gbadolite, Democratic Republic of Congo - Gbadolite Airport (BDT)",
			"value": "BDT",
			"domestic": 0
		}, {
			"label": "Bardufoss, Norway - Bardufoss (BDU)",
			"value": "BDU",
			"domestic": 0
		}, {
			"label": "Benbecula, United Kingdom - Benbecula Airport (BEB)",
			"value": "BEB",
			"domestic": 0
		}, {
			"label": "Bedford, United States - Hanscom Field (BED)",
			"value": "BED",
			"domestic": 1
		}, {
			"label": "Bluefields, Nicaragua - Bluefields Airport (BEF)",
			"value": "BEF",
			"domestic": 0
		}, {
			"label": "Belgrade, Serbia - Beograd (BEG)",
			"value": "BEG",
			"domestic": 0
		}, {
			"label": "Benton Harbor, United States - Ross Field (BEH)",
			"value": "BEH",
			"domestic": 1
		}, {
			"label": "Beica, Ethiopia - Beica Airport (BEI)",
			"value": "BEI",
			"domestic": 0
		}, {
			"label": "Berau, Indonesia - Berau Airport (BEJ)",
			"value": "BEJ",
			"domestic": 0
		}, {
			"label": "Belem, Brazil - Val de Cans (BEL)",
			"value": "BEL",
			"domestic": 0
		}, {
			"label": "Benghazi, Libya - Benina (BEN)",
			"value": "BEN",
			"domestic": 0
		}, {
			"label": "Bellary, India - Bellary Airport (BEP)",
			"value": "BEP",
			"domestic": 0
		}, {
			"label": "Berlin, Germany - All Airports (BER)",
			"value": "BER",
			"domestic": 0
		}, {
			"label": "Brest, France - Guipavas (BES)",
			"value": "BES",
			"domestic": 0
		}, {
			"label": "Bethel, United States - Bethel Airport (BET)",
			"value": "BET",
			"domestic": 1
		}, {
			"label": "Bedourie, Australia - Bedourie Airport (BEU)",
			"value": "BEU",
			"domestic": 0
		}, {
			"label": "Beira, Mozambique - Beira Airport (BEW)",
			"value": "BEW",
			"domestic": 0
		}, {
			"label": "Beirut, Lebanon - Beirut Intl (BEY)",
			"value": "BEY",
			"domestic": 0
		}, {
			"label": "Bradford, United States - Bradford-Regl (BFD)",
			"value": "BFD",
			"domestic": 1
		}, {
			"label": "Scottsbluff, United States - Western Nebraska Regional Airport (BFF)",
			"value": "BFF",
			"domestic": 1
		}, {
			"label": "Bakersfield, United States - Meadows Field (BFL)",
			"value": "BFL",
			"domestic": 1
		}, {
			"label": "Bloemfontein, South Africa - Bloemfontein Intl (BFN)",
			"value": "BFN",
			"domestic": 0
		}, {
			"label": "Pinas Bay, Panama - Bahia Pinas Airport (BFQ)",
			"value": "BFQ",
			"domestic": 0
		}, {
			"label": "Bedford, United States - Virgil Grissom Mncpl (BFR)",
			"value": "BFR",
			"domestic": 1
		}, {
			"label": "Belfast, United Kingdom - Belfast Intl (BFS)",
			"value": "BFS",
			"domestic": 0
		}, {
			"label": "Bengbu, China - Bengbu (BFU)",
			"value": "BFU",
			"domestic": 0
		}, {
			"label": "Buri Ram, Thailand - Buri Ram Airport (BFV)",
			"value": "BFV",
			"domestic": 0
		}, {
			"label": "Bucaramanga, Colombia - Palo Negro (BGA)",
			"value": "BGA",
			"domestic": 0
		}, {
			"label": "Braganca, Portugal - Braganca (BGC)",
			"value": "BGC",
			"domestic": 0
		}, {
			"label": "Borger, United States - Hutchinson County (BGD)",
			"value": "BGD",
			"domestic": 1
		}, {
			"label": "Bangui, Central African Republic - Bangui (BGF)",
			"value": "BGF",
			"domestic": 0
		}, {
			"label": "Bridgetown, Barbados - Grantley Adams International Airport (BGI)",
			"value": "BGI",
			"domestic": 0
		}, {
			"label": "Binghamton, United States - Binghamton Airport (BGM)",
			"value": "BGM",
			"domestic": 1
		}, {
			"label": "Bergen, Norway - Flesland (BGO)",
			"value": "BGO",
			"domestic": 0
		}, {
			"label": "Bangor, United States - Bangor International Airport (BGR)",
			"value": "BGR",
			"domestic": 1
		}, {
			"label": "Baghdad, Iraq - Baghdad Intl (BGW)",
			"value": "BGW",
			"domestic": 0
		}, {
			"label": "Milan, Italy - Orio Al Serio (BGY)",
			"value": "BGY",
			"domestic": 0
		}, {
			"label": "Bar Harbor, United States - Bar Harbor (BHB)",
			"value": "BHB",
			"domestic": 1
		}, {
			"label": "Belfast, United Kingdom - Belfast City (BHD)",
			"value": "BHD",
			"domestic": 0
		}, {
			"label": "Blenheim, New Zealand - Blenheim (BHE)",
			"value": "BHE",
			"domestic": 0
		}, {
			"label": "Brus Laguna, Honduras - Brus Laguna Airport (BHG)",
			"value": "BHG",
			"domestic": 0
		}, {
			"label": "Bisha, Saudi Arabia - Bisha Airport (BHH)",
			"value": "BHH",
			"domestic": 0
		}, {
			"label": "Bahia Blanca, Argentina - Comandante Airport (BHI)",
			"value": "BHI",
			"domestic": 0
		}, {
			"label": "Bhuj, India - Rudra Mata Airport (BHJ)",
			"value": "BHJ",
			"domestic": 0
		}, {
			"label": "Bukhara, Uzbekistan - Bukhara (BHK)",
			"value": "BHK",
			"domestic": 0
		}, {
			"label": "Birmingham, United States - Birmingham (BHM)",
			"value": "BHM",
			"domestic": 1
		}, {
			"label": "Bhopal, India - Bhopal (BHO)",
			"value": "BHO",
			"domestic": 0
		}, {
			"label": "Broken Hill, Australia - Broken Hill (BHQ)",
			"value": "BHQ",
			"domestic": 0
		}, {
			"label": "Bharatpur, Nepal - Bharatpur Airport (BHR)",
			"value": "BHR",
			"domestic": 0
		}, {
			"label": "Bathurst, Australia - Raglan (BHS)",
			"value": "BHS",
			"domestic": 0
		}, {
			"label": "Bhavnagar, India - Bhavnagar Airport (BHU)",
			"value": "BHU",
			"domestic": 0
		}, {
			"label": "Bahawalpur, Pakistan - Bahawalpur Airport (BHV)",
			"value": "BHV",
			"domestic": 0
		}, {
			"label": "Birmingham, United Kingdom - Birmingham International Airport (BHX)",
			"value": "BHX",
			"domestic": 0
		}, {
			"label": "Beihai, China - Beihai (BHY)",
			"value": "BHY",
			"domestic": 0
		}, {
			"label": "Belo Horizonte, Brazil - All Airports (BHZ)",
			"value": "BHZ",
			"domestic": 0
		}, {
			"label": "Bastia, France - Poretta (BIA)",
			"value": "BIA",
			"domestic": 0
		}, {
			"label": "Block Island, United States - Block Island State (BID)",
			"value": "BID",
			"domestic": 1
		}, {
			"label": "Beatrice, United States - Beatrice Municipal (BIE)",
			"value": "BIE",
			"domestic": 1
		}, {
			"label": "Bishop, United States - Bishop (BIH)",
			"value": "BIH",
			"domestic": 1
		}, {
			"label": "Bikini Atoll, Marshall Islands - Enyu Airfield (BII)",
			"value": "BII",
			"domestic": 0
		}, {
			"label": "Biak, Indonesia - Mokmer Airport (BIK)",
			"value": "BIK",
			"domestic": 0
		}, {
			"label": "Billings, United States - Billings Airport (BIL)",
			"value": "BIL",
			"domestic": 1
		}, {
			"label": "Bimini, Bahamas - Bimini International Airport (BIM)",
			"value": "BIM",
			"domestic": 0
		}, {
			"label": "Bilbao, Spain - Bilbao (BIO)",
			"value": "BIO",
			"domestic": 0
		}, {
			"label": "Biarritz, France - Biarritz/Parme (BIQ)",
			"value": "BIQ",
			"domestic": 0
		}, {
			"label": "Biratnagar, Nepal - Biratnagar Airport (BIR)",
			"value": "BIR",
			"domestic": 0
		}, {
			"label": "Bismarck, United States - Bismarck Airport (BIS)",
			"value": "BIS",
			"domestic": 1
		}, {
			"label": "Bojnord, Iran - Bojnord Airport (BJB)",
			"value": "BJB",
			"domestic": 0
		}, {
			"label": "Broomfield, United States - Jeffco (BJC)",
			"value": "BJC",
			"domestic": 1
		}, {
			"label": "Batsfjord, Norway - Batsfjord Airport (BJF)",
			"value": "BJF",
			"domestic": 0
		}, {
			"label": "Bemidji, United States - Bemidji Mncpl (BJI)",
			"value": "BJI",
			"domestic": 1
		}, {
			"label": "Banjul, Gambia - Yundum Intl (BJL)",
			"value": "BJL",
			"domestic": 0
		}, {
			"label": "Bujumbura, Burundi - Bujumbura Intl (BJM)",
			"value": "BJM",
			"domestic": 0
		}, {
			"label": "Bahar Dar, Ethiopia - Bahar Dar Airport (BJR)",
			"value": "BJR",
			"domestic": 0
		}, {
			"label": "Beijing, China - All Airports (BJS)",
			"value": "BJS",
			"domestic": 0
		}, {
			"label": "Bodrum, Turkey - Milas (BJV)",
			"value": "BJV",
			"domestic": 0
		}, {
			"label": "Leon, Mexico - Del Bajio Airport (BJX)",
			"value": "BJX",
			"domestic": 0
		}, {
			"label": "Badajoz, Spain - Talaveral La Real (BJZ)",
			"value": "BJZ",
			"domestic": 0
		}, {
			"label": "Buckland, United States - Buckland Airport (BKC)",
			"value": "BKC",
			"domestic": 1
		}, {
			"label": "Baker City, United States - Baker City Mncpl (BKE)",
			"value": "BKE",
			"domestic": 1
		}, {
			"label": "Branson, United States - Branson Airport (BKG)",
			"value": "BKG",
			"domestic": 1
		}, {
			"label": "Kota Kinabalu, Malaysia - Kota Kinabalu (BKI)",
			"value": "BKI",
			"domestic": 0
		}, {
			"label": "Bangkok, Thailand - Bangkok International Airport (BKK)",
			"value": "BKK",
			"domestic": 0
		}, {
			"label": "Bakalalan, Malaysia - Bakalalan Airport (BKM)",
			"value": "BKM",
			"domestic": 0
		}, {
			"label": "Bamako, Mali - Bamako (BKO)",
			"value": "BKO",
			"domestic": 0
		}, {
			"label": "Bengkulu, Indonesia - Padangkemiling Airport (BKS)",
			"value": "BKS",
			"domestic": 0
		}, {
			"label": "Beckley, United States - Beckley (BKW)",
			"value": "BKW",
			"domestic": 1
		}, {
			"label": "Bukoba, Tanzania - Bukoba Airport (BKZ)",
			"value": "BKZ",
			"domestic": 0
		}, {
			"label": "Barcelona, Venezuela - Gen J A Anzoategui Airport (BLA)",
			"value": "BLA",
			"domestic": 0
		}, {
			"label": "Boulder City, United States - Boulder City Airport (BLD)",
			"value": "BLD",
			"domestic": 1
		}, {
			"label": "Borlange, Sweden - Dala (BLE)",
			"value": "BLE",
			"domestic": 0
		}, {
			"label": "Bluefield, United States - Mercer County Airport (BLF)",
			"value": "BLF",
			"domestic": 1
		}, {
			"label": "Belaga, Malaysia - Belaga Airport (BLG)",
			"value": "BLG",
			"domestic": 0
		}, {
			"label": "Blythe, United States - Blythe (BLH)",
			"value": "BLH",
			"domestic": 1
		}, {
			"label": "Bellingham, United States - Bellingham Airport (BLI)",
			"value": "BLI",
			"domestic": 1
		}, {
			"label": "Batna, Algeria - Batna Airport (BLJ)",
			"value": "BLJ",
			"domestic": 0
		}, {
			"label": "Blackpool, United Kingdom - Blackpool (BLK)",
			"value": "BLK",
			"domestic": 0
		}, {
			"label": "Billund, Denmark - Billund (BLL)",
			"value": "BLL",
			"domestic": 0
		}, {
			"label": "Bologna, Italy - Guglielmo Marconi (BLQ)",
			"value": "BLQ",
			"domestic": 0
		}, {
			"label": "Bangalore, India - Hindustan (BLR)",
			"value": "BLR",
			"domestic": 0
		}, {
			"label": "Blackwater, Australia - Blackwater Airport (BLT)",
			"value": "BLT",
			"domestic": 0
		}, {
			"label": "Belleville, United States - Scott AFB/MidAmerica (BLV)",
			"value": "BLV",
			"domestic": 1
		}, {
			"label": "Blantyre, Malawi - Chileka (BLZ)",
			"value": "BLZ",
			"domestic": 0
		}, {
			"label": "Stockholm, Sweden - Bromma Airport (BMA)",
			"value": "BMA",
			"domestic": 0
		}, {
			"label": "Belo, Madagascar - Belo Airport (BMD)",
			"value": "BMD",
			"domestic": 0
		}, {
			"label": "Broome, Australia - Broome (BME)",
			"value": "BME",
			"domestic": 0
		}, {
			"label": "Bloomington, IL, United States - Bloomington-Normal Airport (BMI)",
			"value": "BMI",
			"domestic": 1
		}, {
			"label": "Borkum, Germany - Borkum (BMK)",
			"value": "BMK",
			"domestic": 0
		}, {
			"label": "Bhamo, Myanmar (Burma) - Bhamo Airport (BMO)",
			"value": "BMO",
			"domestic": 0
		}, {
			"label": "Brampton Island, Australia - Brampton Is (BMP)",
			"value": "BMP",
			"domestic": 0
		}, {
			"label": "Bima, Indonesia - Bima Airport (BMU)",
			"value": "BMU",
			"domestic": 0
		}, {
			"label": "Banmethuot, Vietnam - Phung-Duc Airport (BMV)",
			"value": "BMV",
			"domestic": 0
		}, {
			"label": "Bordj Badji Mokhtar, Algeria - Bordj Badji Mokhtar Airport (BMW)",
			"value": "BMW",
			"domestic": 0
		}, {
			"label": "Belep Island, New Caledonia - Belep Island Airport (BMY)",
			"value": "BMY",
			"domestic": 0
		}, {
			"label": "Nashville, United States - Nashville (BNA)",
			"value": "BNA",
			"domestic": 1
		}, {
			"label": "Bandar Abbas, Iran - Bandar Abbas Airport (BND)",
			"value": "BND",
			"domestic": 0
		}, {
			"label": "Brisbane, Australia - Brisbane Intl (BNE)",
			"value": "BNE",
			"domestic": 0
		}, {
			"label": "Benin City, Nigeria - Benin City Airport (BNI)",
			"value": "BNI",
			"domestic": 0
		}, {
			"label": "Ballina, Australia - Ballina Byron Gtwy (BNK)",
			"value": "BNK",
			"domestic": 0
		}, {
			"label": "Bronnoysund, Norway - Bronnoy (BNN)",
			"value": "BNN",
			"domestic": 0
		}, {
			"label": "Barinas, Venezuela - Barinas Airport (BNS)",
			"value": "BNS",
			"domestic": 0
		}, {
			"label": "Blumenau, Brazil - Blumenau (BNU)",
			"value": "BNU",
			"domestic": 0
		}, {
			"label": "Banja Luka, Bosnia and Herzegovina - Banja Luka Airport (BNX)",
			"value": "BNX",
			"domestic": 0
		}, {
			"label": "Bellona, Solomon Islands - Bellona Airport (BNY)",
			"value": "BNY",
			"domestic": 0
		}, {
			"label": "Bora-Bora, French Polynesia and Tahiti - Motu-mute (BOB)",
			"value": "BOB",
			"domestic": 0
		}, {
			"label": "Bocas del Toro, Panama - Bocas del Toro (BOC)",
			"value": "BOC",
			"domestic": 0
		}, {
			"label": "Bordeaux, France - Bordeaux (BOD)",
			"value": "BOD",
			"domestic": 0
		}, {
			"label": "Bogota, Colombia - Eldorado Airport (BOG)",
			"value": "BOG",
			"domestic": 0
		}, {
			"label": "Bournemouth, United Kingdom - Bournemouth (BOH)",
			"value": "BOH",
			"domestic": 0
		}, {
			"label": "Boise, United States - Boise (BOI)",
			"value": "BOI",
			"domestic": 1
		}, {
			"label": "Bourgas, Bulgaria - Bourgas (BOJ)",
			"value": "BOJ",
			"domestic": 0
		}, {
			"label": "Mumbai, India - Chhatrapati Shivaji (BOM)",
			"value": "BOM",
			"domestic": 0
		}, {
			"label": "Bonaire, Bonaire - Flamingo International Airport (BON)",
			"value": "BON",
			"domestic": 0
		}, {
			"label": "Bodo, Norway - Bodo (BOO)",
			"value": "BOO",
			"domestic": 0
		}, {
			"label": "Belfort, France - Fontaine (BOR)",
			"value": "BOR",
			"domestic": 0
		}, {
			"label": "Boston, United States - Boston (BOS)",
			"value": "BOS",
			"domestic": 1
		}, {
			"label": "Bourges, France - Bourges (BOU)",
			"value": "BOU",
			"domestic": 0
		}, {
			"label": "Bobo Dioulasso, Burkina Faso - Borgo (BOY)",
			"value": "BOY",
			"domestic": 0
		}, {
			"label": "Balikpapan, Indonesia - Sepinggan (BPN)",
			"value": "BPN",
			"domestic": 0
		}, {
			"label": "Porto Seguro, Brazil - Porto Seguro Mncpl (BPS)",
			"value": "BPS",
			"domestic": 0
		}, {
			"label": "Beaumont, United States - Jefferson County Airport (BPT)",
			"value": "BPT",
			"domestic": 1
		}, {
			"label": "Bangda, China - Bangda Airport (BPX)",
			"value": "BPX",
			"domestic": 0
		}, {
			"label": "Besalampy, Madagascar - Besalampy Airport (BPY)",
			"value": "BPY",
			"domestic": 0
		}, {
			"label": "Busselton, Australia - Busselton (BQB)",
			"value": "BQB",
			"domestic": 0
		}, {
			"label": "Brunswick, United States - Glynco Jetport (BQK)",
			"value": "BQK",
			"domestic": 1
		}, {
			"label": "Boulia, Australia - Boulia Airport (BQL)",
			"value": "BQL",
			"domestic": 0
		}, {
			"label": "Aguadilla, Puerto Rico - Aguadilla (BQN)",
			"value": "BQN",
			"domestic": 0
		}, {
			"label": "Blagoveschensk, Russia - Blagoveschensk Airport (BQS)",
			"value": "BQS",
			"domestic": 0
		}, {
			"label": "Brest, Belarus - Brest (BQT)",
			"value": "BQT",
			"domestic": 0
		}, {
			"label": "San Carlos de Bariloche, Argentina - DeBariloche Intl (BRC)",
			"value": "BRC",
			"domestic": 0
		}, {
			"label": "Brainerd, United States - Crow Wing County Airport (BRD)",
			"value": "BRD",
			"domestic": 1
		}, {
			"label": "Bremen, Germany - Bremen (BRE)",
			"value": "BRE",
			"domestic": 0
		}, {
			"label": "Bari, Italy - Palese (BRI)",
			"value": "BRI",
			"domestic": 0
		}, {
			"label": "Burlington, IA, United States - Burlington Airport (BRL)",
			"value": "BRL",
			"domestic": 1
		}, {
			"label": "Barquisimeto, Venezuela - Barquisimeto (BRM)",
			"value": "BRM",
			"domestic": 0
		}, {
			"label": "Berne, Switzerland - Belp (BRN)",
			"value": "BRN",
			"domestic": 0
		}, {
			"label": "Brownsville, United States - South Padre Island International Airport (BRO)",
			"value": "BRO",
			"domestic": 1
		}, {
			"label": "Brno, Czech Republic - Turany (BRQ)",
			"value": "BRQ",
			"domestic": 0
		}, {
			"label": "Barra, United Kingdom - North Bay (BRR)",
			"value": "BRR",
			"domestic": 0
		}, {
			"label": "Bristol, United Kingdom - Bristol Intl (BRS)",
			"value": "BRS",
			"domestic": 0
		}, {
			"label": "Brussels, Belgium - National Airport (BRU)",
			"value": "BRU",
			"domestic": 0
		}, {
			"label": "Bremerhaven, Germany - Bremerhaven (BRV)",
			"value": "BRV",
			"domestic": 0
		}, {
			"label": "Barrow, United States - Wiley Post (BRW)",
			"value": "BRW",
			"domestic": 1
		}, {
			"label": "Barahona, Dominican Republic - Maria Montez Intl (BRX)",
			"value": "BRX",
			"domestic": 0
		}, {
			"label": "Bossaso, Somalia - Bossaso Airport (BSA)",
			"value": "BSA",
			"domestic": 0
		}, {
			"label": "Brasilia, Brazil - Brasilia Intl (BSB)",
			"value": "BSB",
			"domestic": 0
		}, {
			"label": "Bahia Solano, Colombia - Bahia Solano Airport (BSC)",
			"value": "BSC",
			"domestic": 0
		}, {
			"label": "Baoshan, China - Baoshan (BSD)",
			"value": "BSD",
			"domestic": 0
		}, {
			"label": "Brighton, United Kingdom - Brighton Airport (BSH)",
			"value": "BSH",
			"domestic": 0
		}, {
			"label": "Bairnsdale, Australia - Bairnsdale (BSJ)",
			"value": "BSJ",
			"domestic": 0
		}, {
			"label": "Biskra, Algeria - Biskra Airport (BSK)",
			"value": "BSK",
			"domestic": 0
		}, {
			"label": "Basel/Mulhouse, Switzerland - EuroAirport Swiss (BSL)",
			"value": "BSL",
			"domestic": 0
		}, {
			"label": "Basco, Philippines - Basco Airport (BSO)",
			"value": "BSO",
			"domestic": 0
		}, {
			"label": "Basra, Iraq - Basra International Airport (BSR)",
			"value": "BSR",
			"domestic": 0
		}, {
			"label": "Bassein, Myanmar (Burma) - Bassein Airport (BSX)",
			"value": "BSX",
			"domestic": 0
		}, {
			"label": "Batam, Indonesia - Hang Nadim (BTH)",
			"value": "BTH",
			"domestic": 0
		}, {
			"label": "Barter Island, United States - Barter Island Airport (BTI)",
			"value": "BTI",
			"domestic": 1
		}, {
			"label": "Banda Aceh, Indonesia - Blang Bintang Airport (BTJ)",
			"value": "BTJ",
			"domestic": 0
		}, {
			"label": "Bratsk, Russia - Bratsk Airport (BTK)",
			"value": "BTK",
			"domestic": 0
		}, {
			"label": "Battle Creek, United States - WK Kellogg Regl (BTL)",
			"value": "BTL",
			"domestic": 1
		}, {
			"label": "Butte, United States - Butte Airport (BTM)",
			"value": "BTM",
			"domestic": 1
		}, {
			"label": "Butler, United States - Graham Field (BTP)",
			"value": "BTP",
			"domestic": 1
		}, {
			"label": "Baton Rouge, United States - Ryan Airport (BTR)",
			"value": "BTR",
			"domestic": 1
		}, {
			"label": "Bratislava, Slovakia - Ivanka (BTS)",
			"value": "BTS",
			"domestic": 0
		}, {
			"label": "Bettles, United States - Bettles Airport (BTT)",
			"value": "BTT",
			"domestic": 1
		}, {
			"label": "Bintulu, Malaysia - Bintulu (BTU)",
			"value": "BTU",
			"domestic": 0
		}, {
			"label": "Burlington, VT, United States - Burlington (BTV)",
			"value": "BTV",
			"domestic": 1
		}, {
			"label": "Bursa, Turkey - Bursa (BTZ)",
			"value": "BTZ",
			"domestic": 0
		}, {
			"label": "Buka, Papua New Guinea - Buka Airport (BUA)",
			"value": "BUA",
			"domestic": 0
		}, {
			"label": "Burketown, Australia - Burketown Airport (BUC)",
			"value": "BUC",
			"domestic": 0
		}, {
			"label": "Budapest, Hungary - Ferihegy (BUD)",
			"value": "BUD",
			"domestic": 0
		}, {
			"label": "Buenos Aires, Argentina - All Airports (BUE)",
			"value": "BUE",
			"domestic": 0
		}, {
			"label": "Buffalo, United States - Buffalo (BUF)",
			"value": "BUF",
			"domestic": 1
		}, {
			"label": "Bucharest, Romania - All Airports (BUH)",
			"value": "BUH",
			"domestic": 0
		}, {
			"label": "Buenaventura, Colombia - Buenaventura Airport (BUN)",
			"value": "BUN",
			"domestic": 0
		}, {
			"label": "Burao, Somalia - Burao Airport (BUO)",
			"value": "BUO",
			"domestic": 0
		}, {
			"label": "Bulawayo, Zimbabwe - Bulawayo (BUQ)",
			"value": "BUQ",
			"domestic": 0
		}, {
			"label": "Burbank, United States - Burbank (BUR)",
			"value": "BUR",
			"domestic": 1
		}, {
			"label": "Batumi, Georgia - Batumi Airport (BUS)",
			"value": "BUS",
			"domestic": 0
		}, {
			"label": "Bunbury, Australia - Bunbury (BUY)",
			"value": "BUY",
			"domestic": 0
		}, {
			"label": "Bushehr, Iran - Bushehr Airport (BUZ)",
			"value": "BUZ",
			"domestic": 0
		}, {
			"label": "Paris, France - Tille (BVA)",
			"value": "BVA",
			"domestic": 0
		}, {
			"label": "Boa Vista, Brazil - Boa Vista Airport (BVB)",
			"value": "BVB",
			"domestic": 0
		}, {
			"label": "Boa Vista, Cape Verde - Rabil Airport (BVC)",
			"value": "BVC",
			"domestic": 0
		}, {
			"label": "Brive-la-Gaillarde, France - Laroche (BVE)",
			"value": "BVE",
			"domestic": 0
		}, {
			"label": "Berlevag, Norway - Berlevag Airport (BVG)",
			"value": "BVG",
			"domestic": 0
		}, {
			"label": "Vilhena, Brazil - Vilhena Airport (BVH)",
			"value": "BVH",
			"domestic": 0
		}, {
			"label": "Birdsville, Australia - Birdsville Airport (BVI)",
			"value": "BVI",
			"domestic": 0
		}, {
			"label": "Batesville, United States - Batesville Mncpl (BVX)",
			"value": "BVX",
			"domestic": 1
		}, {
			"label": "Bhairawa, Nepal - Bhairawa Airport (BWA)",
			"value": "BWA",
			"domestic": 0
		}, {
			"label": "Brownwood, United States - Brownwood Mncpl (BWD)",
			"value": "BWD",
			"domestic": 1
		}, {
			"label": "Braunschweig, Germany - Braunschweig (BWE)",
			"value": "BWE",
			"domestic": 0
		}, {
			"label": "Bowling Green, United States - Warren Cty (BWG)",
			"value": "BWG",
			"domestic": 1
		}, {
			"label": "Baltimore, United States - Baltimore (BWI)",
			"value": "BWI",
			"domestic": 1
		}, {
			"label": "Bol, Croatia - Bol (BWK)",
			"value": "BWK",
			"domestic": 0
		}, {
			"label": "Bandar Seri Begawan, Brunei - Brunei Intl (BWN)",
			"value": "BWN",
			"domestic": 0
		}, {
			"label": "Burnie, Australia - Burnie Wynyard (BWT)",
			"value": "BWT",
			"domestic": 0
		}, {
			"label": "Bankstown, Australia - Bankstown (BWU)",
			"value": "BWU",
			"domestic": 0
		}, {
			"label": "Bam, Iran - Bam Airport (BXR)",
			"value": "BXR",
			"domestic": 0
		}, {
			"label": "Borrego Springs, United States - Borrego Springs (BXS)",
			"value": "BXS",
			"domestic": 1
		}, {
			"label": "Butuan, Philippines - Butuan Airport (BXU)",
			"value": "BXU",
			"domestic": 0
		}, {
			"label": "Borama, Somalia - Borama Airport (BXX)",
			"value": "BXX",
			"domestic": 0
		}, {
			"label": "Boundary, United States - Boundary Airport (BYA)",
			"value": "BYA",
			"domestic": 1
		}, {
			"label": "Bouake, Cote D\'Ivoire - Bouake (BYK)",
			"value": "BYK",
			"domestic": 0
		}, {
			"label": "Bayamo, Cuba - C.M. de Cespedes Airport (BYM)",
			"value": "BYM",
			"domestic": 0
		}, {
			"label": "Bayankhongor, Mongolia - Bayankhongor Airport (BYN)",
			"value": "BYN",
			"domestic": 0
		}, {
			"label": "Bayreuth, Germany - Bindlacher Berg (BYU)",
			"value": "BYU",
			"domestic": 0
		}, {
			"label": "Bazaruto Island, Mozambique - Bazaruto Island Airport (BZB)",
			"value": "BZB",
			"domestic": 0
		}, {
			"label": "Belize City, Belize - Philip S.W. Goldson International Airport (BZE)",
			"value": "BZE",
			"domestic": 0
		}, {
			"label": "Bydgoszcz, Poland - Bydgoszcz (BZG)",
			"value": "BZG",
			"domestic": 0
		}, {
			"label": "Barisal, Bangladesh - Barisal Airport (BZL)",
			"value": "BZL",
			"domestic": 0
		}, {
			"label": "Bozeman, United States - Bozeman (BZN)",
			"value": "BZN",
			"domestic": 1
		}, {
			"label": "Bolzano, Italy - Bolzano/Bozen (BZO)",
			"value": "BZO",
			"domestic": 0
		}, {
			"label": "Beziers, France - Beziers Vias (BZR)",
			"value": "BZR",
			"domestic": 0
		}, {
			"label": "Brazzaville, Republic of Congo - Maya Maya (BZV)",
			"value": "BZV",
			"domestic": 0
		}, {
			"label": "Cabinda, Angola - Cabinda Airport (CAB)",
			"value": "CAB",
			"domestic": 0
		}, {
			"label": "Cascavel, Brazil - Cascavel Mncpl (CAC)",
			"value": "CAC",
			"domestic": 0
		}, {
			"label": "Cadillac, United States - County (CAD)",
			"value": "CAD",
			"domestic": 1
		}, {
			"label": "Columbia, SC, United States - Columbia (CAE)",
			"value": "CAE",
			"domestic": 1
		}, {
			"label": "Carauari, Brazil - Carauari Airport (CAF)",
			"value": "CAF",
			"domestic": 0
		}, {
			"label": "Cagliari, Italy - Elmas (CAG)",
			"value": "CAG",
			"domestic": 0
		}, {
			"label": "Ca Mau, Vietnam - Ca Mau Airport (CAH)",
			"value": "CAH",
			"domestic": 0
		}, {
			"label": "Cairo, Egypt - Cairo Intl (CAI)",
			"value": "CAI",
			"domestic": 0
		}, {
			"label": "Akron, United States - Fulton International Airport (CAK)",
			"value": "CAK",
			"domestic": 1
		}, {
			"label": "Campbeltown, United Kingdom - Machrihanish Airport (CAL)",
			"value": "CAL",
			"domestic": 0
		}, {
			"label": "Guangzhou, China - Baiyun (CAN)",
			"value": "CAN",
			"domestic": 0
		}, {
			"label": "Cap Haitien, Haiti - Cap Haitien Airport (CAP)",
			"value": "CAP",
			"domestic": 0
		}, {
			"label": "Casablanca, Morocco - Anfa (CAS)",
			"value": "CAS",
			"domestic": 0
		}, {
			"label": "Cat Island, Bahamas - Cat Island Airport (CAT)",
			"value": "CAT",
			"domestic": 0
		}, {
			"label": "Campos, Brazil - Bartolomeu Lisandro Airport (CAW)",
			"value": "CAW",
			"domestic": 0
		}, {
			"label": "Carlisle, United Kingdom - Carlisle (CAX)",
			"value": "CAX",
			"domestic": 0
		}, {
			"label": "Cayenne, French Guiana - Rochambeau (CAY)",
			"value": "CAY",
			"domestic": 0
		}, {
			"label": "Cochabamba, Bolivia - J Wilsterman (CBB)",
			"value": "CBB",
			"domestic": 0
		}, {
			"label": "Cumberland, United States - Wiley Ford Airport (CBE)",
			"value": "CBE",
			"domestic": 1
		}, {
			"label": "Cambridge, United States - Cambridge (CBG)",
			"value": "CBG",
			"domestic": 1
		}, {
			"label": "Bechar, Algeria - Leger Airport (CBH)",
			"value": "CBH",
			"domestic": 0
		}, {
			"label": "Cape Barren Island, Australia - Cape Barren Island Airport (CBI)",
			"value": "CBI",
			"domestic": 0
		}, {
			"label": "Ciudad Bolivar, Venezuela - Ciudad Bolivar Airport (CBL)",
			"value": "CBL",
			"domestic": 0
		}, {
			"label": "Cotabato, Philippines - Awang Airport (CBO)",
			"value": "CBO",
			"domestic": 0
		}, {
			"label": "Coimbra, Portugal - Coimbra (CBP)",
			"value": "CBP",
			"domestic": 0
		}, {
			"label": "Calabar, Nigeria - Calabar Airport (CBQ)",
			"value": "CBQ",
			"domestic": 0
		}, {
			"label": "Canberra, Australia - Canberra (CBR)",
			"value": "CBR",
			"domestic": 0
		}, {
			"label": "Catumbela, Angola - Catumbela Airport (CBT)",
			"value": "CBT",
			"domestic": 0
		}, {
			"label": "Cayo Coco, Cuba - Cayo Coco Airport (CCC)",
			"value": "CCC",
			"domestic": 0
		}, {
			"label": "Carcassonne, France - Salvaza (CCF)",
			"value": "CCF",
			"domestic": 0
		}, {
			"label": "Kozhikode, India - Kozhikode (CCJ)",
			"value": "CCJ",
			"domestic": 0
		}, {
			"label": "Cocos (Keeling) Islands, Australia - Cocos Islands Airport (CCK)",
			"value": "CCK",
			"domestic": 0
		}, {
			"label": "Concepcion, Chile and Easter Island - Carriel Sur (CCP)",
			"value": "CCP",
			"domestic": 0
		}, {
			"label": "Concord, United States - Buchanan Field (CCR)",
			"value": "CCR",
			"domestic": 1
		}, {
			"label": "Caracas, Venezuela - Simon Bolivar Airport (CCS)",
			"value": "CCS",
			"domestic": 0
		}, {
			"label": "Kolkata, India - Subhas Chandra Bose (CCU)",
			"value": "CCU",
			"domestic": 0
		}, {
			"label": "Craig Cove, Vanuatu - Craig Cove Airport (CCV)",
			"value": "CCV",
			"domestic": 0
		}, {
			"label": "Chub Cay, Bahamas - Chub Cay (CCZ)",
			"value": "CCZ",
			"domestic": 0
		}, {
			"label": "Cold Bay, United States - Cold Bay Airport (CDB)",
			"value": "CDB",
			"domestic": 1
		}, {
			"label": "Cedar City, United States - Cedar City Airport (CDC)",
			"value": "CDC",
			"domestic": 1
		}, {
			"label": "Paris, France - De Gaulle (CDG)",
			"value": "CDG",
			"domestic": 0
		}, {
			"label": "Camden, United States - Harrell Fld (CDH)",
			"value": "CDH",
			"domestic": 1
		}, {
			"label": "Cedar Key, United States - Lewis (CDK)",
			"value": "CDK",
			"domestic": 1
		}, {
			"label": "Chadron, United States - Chadron Municipal Airport (CDR)",
			"value": "CDR",
			"domestic": 1
		}, {
			"label": "Cordova, United States - Mudhole Smith Airport (CDV)",
			"value": "CDV",
			"domestic": 1
		}, {
			"label": "Caldwell, United States - Caldwell Wright Airport (CDW)",
			"value": "CDW",
			"domestic": 1
		}, {
			"label": "Cebu, Philippines - Cebu Intl (CEB)",
			"value": "CEB",
			"domestic": 0
		}, {
			"label": "Crescent City, United States - Mc Namara Field (CEC)",
			"value": "CEC",
			"domestic": 1
		}, {
			"label": "Ceduna, Australia - Ceduna (CED)",
			"value": "CED",
			"domestic": 0
		}, {
			"label": "Cherepovets, Russia - Cherepovets Airport (CEE)",
			"value": "CEE",
			"domestic": 0
		}, {
			"label": "Chester, United Kingdom - Harwarden (CEG)",
			"value": "CEG",
			"domestic": 0
		}, {
			"label": "Chiang Rai, Thailand - Chiang Rai (CEI)",
			"value": "CEI",
			"domestic": 0
		}, {
			"label": "Chelyabinsk, Russia - Chelyabinsk (CEK)",
			"value": "CEK",
			"domestic": 0
		}, {
			"label": "Central, United States - Central (CEM)",
			"value": "CEM",
			"domestic": 1
		}, {
			"label": "Ciudad Obregon, Mexico - Ciudad Obregon (CEN)",
			"value": "CEN",
			"domestic": 0
		}, {
			"label": "Cannes, France - Mandelieu (CEQ)",
			"value": "CEQ",
			"domestic": 0
		}, {
			"label": "Cherbourg, France - Maupertus (CER)",
			"value": "CER",
			"domestic": 0
		}, {
			"label": "Cessnock, Australia - Cessnock (CES)",
			"value": "CES",
			"domestic": 0
		}, {
			"label": "Murray, United States - Murray Calloway Cty (CEY)",
			"value": "CEY",
			"domestic": 1
		}, {
			"label": "Cortez, United States - Cortez Municipal Airport (CEZ)",
			"value": "CEZ",
			"domestic": 1
		}, {
			"label": "Cabo Frio, Brazil - Cabo Frio (CFB)",
			"value": "CFB",
			"domestic": 0
		}, {
			"label": "Clermont-Ferrand, France - Aulnat (CFE)",
			"value": "CFE",
			"domestic": 0
		}, {
			"label": "Cienfuegos, Cuba - Cienfeugos Mncpl (CFG)",
			"value": "CFG",
			"domestic": 0
		}, {
			"label": "Chlef, Algeria - Abou Bakr Belkaid Airport (CFK)",
			"value": "CFK",
			"domestic": 0
		}, {
			"label": "Donegal, Ireland - Donegal (CFN)",
			"value": "CFN",
			"domestic": 0
		}, {
			"label": "Caen, France - Carpiquet (CFR)",
			"value": "CFR",
			"domestic": 0
		}, {
			"label": "Coffs Harbour, Australia - Coffs Harbour (CFS)",
			"value": "CFS",
			"domestic": 0
		}, {
			"label": "Kerkyra, Greece - I Kapodistrias (CFU)",
			"value": "CFU",
			"domestic": 0
		}, {
			"label": "Craig, United States - Craig Mncpl (CGA)",
			"value": "CGA",
			"domestic": 1
		}, {
			"label": "Cuiaba, Brazil - Marechal Rondon (CGB)",
			"value": "CGB",
			"domestic": 0
		}, {
			"label": "Changde, China - Changde (CGD)",
			"value": "CGD",
			"domestic": 0
		}, {
			"label": "Sao Paulo, Brazil - Congonhas (CGH)",
			"value": "CGH",
			"domestic": 0
		}, {
			"label": "Cape Girardeau, United States - Cape Girardeau Mncpl (CGI)",
			"value": "CGI",
			"domestic": 1
		}, {
			"label": "Jakarta, Indonesia - Soekarno-Hatta Intl (CGK)",
			"value": "CGK",
			"domestic": 0
		}, {
			"label": "Camiguin, Philippines - Mambajao Airport (CGM)",
			"value": "CGM",
			"domestic": 0
		}, {
			"label": "Cologne, Germany - Koln/Bonn (CGN)",
			"value": "CGN",
			"domestic": 0
		}, {
			"label": "Zhengzhou, China - Zhengzhou (CGO)",
			"value": "CGO",
			"domestic": 0
		}, {
			"label": "Chittagong, Bangladesh - Patenga (CGP)",
			"value": "CGP",
			"domestic": 0
		}, {
			"label": "Changchun, China - Changchun (CGQ)",
			"value": "CGQ",
			"domestic": 0
		}, {
			"label": "Campo Grande, Brazil - Campo Grande (CGR)",
			"value": "CGR",
			"domestic": 0
		}, {
			"label": "Cagayan De Oro, Philippines - Lumbia Airport (CGY)",
			"value": "CGY",
			"domestic": 0
		}, {
			"label": "Casa Grande, United States - Casa Grande Mncpl (CGZ)",
			"value": "CGZ",
			"domestic": 1
		}, {
			"label": "Chattanooga, United States - Lovell Field (CHA)",
			"value": "CHA",
			"domestic": 1
		}, {
			"label": "Christchurch, New Zealand - Christchurch Intl (CHC)",
			"value": "CHC",
			"domestic": 0
		}, {
			"label": "Chandler, United States - Williams AFB (CHD)",
			"value": "CHD",
			"domestic": 1
		}, {
			"label": "Chicago, United States - All Airports (CHI)",
			"value": "CHI",
			"domestic": 1
		}, {
			"label": "Chickasha, United States - Chickasha Mncpl (CHK)",
			"value": "CHK",
			"domestic": 1
		}, {
			"label": "Challis, United States - Challis Mncpl (CHL)",
			"value": "CHL",
			"domestic": 1
		}, {
			"label": "Charlottesville, United States - Albemarle Airport (CHO)",
			"value": "CHO",
			"domestic": 1
		}, {
			"label": "Chania, Greece - Chania-Souda (CHQ)",
			"value": "CHQ",
			"domestic": 0
		}, {
			"label": "Chateauroux, France - Chateauroux (CHR)",
			"value": "CHR",
			"domestic": 0
		}, {
			"label": "Charleston, United States - Charleston (CHS)",
			"value": "CHS",
			"domestic": 1
		}, {
			"label": "Chatham Island, New Zealand - Karewa Airport (CHT)",
			"value": "CHT",
			"domestic": 0
		}, {
			"label": "Chuathbaluk, United States - Chuathbaluk Airport (CHU)",
			"value": "CHU",
			"domestic": 1
		}, {
			"label": "Changuinola, Panama - Changuinola Airport (CHX)",
			"value": "CHX",
			"domestic": 0
		}, {
			"label": "Choiseul Bay, Solomon Islands - Choiseul Bay Airport (CHY)",
			"value": "CHY",
			"domestic": 0
		}, {
			"label": "Rome, Italy - Ciampino (CIA)",
			"value": "CIA",
			"domestic": 0
		}, {
			"label": "Catalina Island, United States - Airport in the Sky (CIB)",
			"value": "CIB",
			"domestic": 1
		}, {
			"label": "Chico, United States - Chico Airport (CIC)",
			"value": "CIC",
			"domestic": 1
		}, {
			"label": "Cedar Rapids, United States - Cedar Rapids (CID)",
			"value": "CID",
			"domestic": 1
		}, {
			"label": "Chifeng, China - Chifeng Airport (CIF)",
			"value": "CIF",
			"domestic": 0
		}, {
			"label": "Changzhi, China - Changzhi Airport (CIH)",
			"value": "CIH",
			"domestic": 0
		}, {
			"label": "Cobija, Bolivia - E. Beltram Airport (CIJ)",
			"value": "CIJ",
			"domestic": 0
		}, {
			"label": "Chalkyitsik, United States - Chalkyitsik Airport (CIK)",
			"value": "CIK",
			"domestic": 1
		}, {
			"label": "Chipata, Zambia - Chipata Airport (CIP)",
			"value": "CIP",
			"domestic": 0
		}, {
			"label": "Shimkent, Kazakhstan - Shimkent Airport (CIT)",
			"value": "CIT",
			"domestic": 0
		}, {
			"label": "Sault Ste Marie, United States - Chippewa Cty (CIU)",
			"value": "CIU",
			"domestic": 1
		}, {
			"label": "Canouan Island, St. Vincent and the Grenadines - Canouan Island Mncpl (CIW)",
			"value": "CIW",
			"domestic": 0
		}, {
			"label": "Chiclayo, Peru - Coronel Ruiz (CIX)",
			"value": "CIX",
			"domestic": 0
		}, {
			"label": "Coari, Brazil - Coari Airport (CIZ)",
			"value": "CIZ",
			"domestic": 0
		}, {
			"label": "Cajamarca, Peru - Cajamarca Airport (CJA)",
			"value": "CJA",
			"domestic": 0
		}, {
			"label": "Coimbatore, India - Peelamedu (CJB)",
			"value": "CJB",
			"domestic": 0
		}, {
			"label": "Calama, Chile and Easter Island - El Loa (CJC)",
			"value": "CJC",
			"domestic": 0
		}, {
			"label": "Cheongju, South Korea - Cheongju Airport (CJJ)",
			"value": "CJJ",
			"domestic": 0
		}, {
			"label": "Chitral, Pakistan - Chitral Airport (CJL)",
			"value": "CJL",
			"domestic": 0
		}, {
			"label": "El Cajon, United States - Gillespie Field (CJN)",
			"value": "CJN",
			"domestic": 1
		}, {
			"label": "Ciudad Juarez, Mexico - A Gonzalez Intl (CJS)",
			"value": "CJS",
			"domestic": 0
		}, {
			"label": "Jeju, South Korea - Jeju (CJU)",
			"value": "CJU",
			"domestic": 0
		}, {
			"label": "Clarksburg, United States - Benedum Airport (CKB)",
			"value": "CKB",
			"domestic": 1
		}, {
			"label": "Crooked Creek, United States - Crooked Creek Airport (CKD)",
			"value": "CKD",
			"domestic": 1
		}, {
			"label": "Chongqing, China - Chongqing (CKG)",
			"value": "CKG",
			"domestic": 0
		}, {
			"label": "Carajas, Brazil - Carajas Airport (CKS)",
			"value": "CKS",
			"domestic": 0
		}, {
			"label": "Clarksville, United States - Outlaw Fld (CKV)",
			"value": "CKV",
			"domestic": 1
		}, {
			"label": "Chicken, United States - Chicken Airport (CKX)",
			"value": "CKX",
			"domestic": 1
		}, {
			"label": "Conakry, Guinea - Conakry (CKY)",
			"value": "CKY",
			"domestic": 0
		}, {
			"label": "Canakkale, Turkey - Canakkale Airport (CKZ)",
			"value": "CKZ",
			"domestic": 0
		}, {
			"label": "Carlsbad, CA, United States - Carlsbad Airport (CLD)",
			"value": "CLD",
			"domestic": 1
		}, {
			"label": "Cleveland, United States - Cleveland (CLE)",
			"value": "CLE",
			"domestic": 1
		}, {
			"label": "Cluj-Napoca, Romania - Cluj (CLJ)",
			"value": "CLJ",
			"domestic": 0
		}, {
			"label": "College Station, United States - Easterwood Field (CLL)",
			"value": "CLL",
			"domestic": 1
		}, {
			"label": "Port Angeles, United States - Fairchild International Airport (CLM)",
			"value": "CLM",
			"domestic": 1
		}, {
			"label": "Cali, Colombia - Alfonso B. Aragon Airport (CLO)",
			"value": "CLO",
			"domestic": 0
		}, {
			"label": "Colima, Mexico - Colima Mncpl (CLQ)",
			"value": "CLQ",
			"domestic": 0
		}, {
			"label": "Charlotte, United States - Charlotte (CLT)",
			"value": "CLT",
			"domestic": 1
		}, {
			"label": "Calvi, France - Ste Catherine (CLY)",
			"value": "CLY",
			"domestic": 0
		}, {
			"label": "Cunnamulla, Australia - Cunnamulla Airport (CMA)",
			"value": "CMA",
			"domestic": 0
		}, {
			"label": "Colombo, Sri Lanka - Bandaranaike (CMB)",
			"value": "CMB",
			"domestic": 0
		}, {
			"label": "Cootamundra, Australia - Cootamundra (CMD)",
			"value": "CMD",
			"domestic": 0
		}, {
			"label": "Ciudad Del Carmen, Mexico - Ciudad Del Carmen (CME)",
			"value": "CME",
			"domestic": 0
		}, {
			"label": "Chambery, France - Chambery (CMF)",
			"value": "CMF",
			"domestic": 0
		}, {
			"label": "Corumba, Brazil - Corumba Internacional Airport (CMG)",
			"value": "CMG",
			"domestic": 0
		}, {
			"label": "Columbus, OH, United States - Columbus (CMH)",
			"value": "CMH",
			"domestic": 1
		}, {
			"label": "Champaign, United States - Willard University Airport (CMI)",
			"value": "CMI",
			"domestic": 1
		}, {
			"label": "Chi Mei, Taiwan - Chi Mei Airport (CMJ)",
			"value": "CMJ",
			"domestic": 0
		}, {
			"label": "Casablanca, Morocco - Mohamed V (CMN)",
			"value": "CMN",
			"domestic": 0
		}, {
			"label": "Colmar, France - Colmar-Houssen (CMR)",
			"value": "CMR",
			"domestic": 0
		}, {
			"label": "Kundiawa, Papua New Guinea - Chimbu Airport (CMU)",
			"value": "CMU",
			"domestic": 0
		}, {
			"label": "Camaguey, Cuba - Ignacio Agramonte (CMW)",
			"value": "CMW",
			"domestic": 0
		}, {
			"label": "Hancock, United States - Houghton County Airport (CMX)",
			"value": "CMX",
			"domestic": 1
		}, {
			"label": "Coconut Island, Australia - Coconut Island Airport (CNC)",
			"value": "CNC",
			"domestic": 0
		}, {
			"label": "Constanta, Romania - Kogalniceanu (CND)",
			"value": "CND",
			"domestic": 0
		}, {
			"label": "Belo Horizonte, Brazil - Tancredo Neves (CNF)",
			"value": "CNF",
			"domestic": 0
		}, {
			"label": "Cloncurry, Australia - Cloncurry Airport (CNJ)",
			"value": "CNJ",
			"domestic": 0
		}, {
			"label": "Carlsbad, NM, United States - Carlsbad Cavern City (CNM)",
			"value": "CNM",
			"domestic": 1
		}, {
			"label": "Neerlerit Inaat, Greenland - Neerlerit Inaat Airport (CNP)",
			"value": "CNP",
			"domestic": 0
		}, {
			"label": "Corrientes, Argentina - Camba Punta Airport (CNQ)",
			"value": "CNQ",
			"domestic": 0
		}, {
			"label": "Cairns, Australia - Cairns (CNS)",
			"value": "CNS",
			"domestic": 0
		}, {
			"label": "Chiang Mai, Thailand - Chiang Mai Intl (CNX)",
			"value": "CNX",
			"domestic": 0
		}, {
			"label": "Moab, United States - Canyonlands Field Airport (CNY)",
			"value": "CNY",
			"domestic": 1
		}, {
			"label": "Cody, United States - Yellowstone Regional Airport (COD)",
			"value": "COD",
			"domestic": 1
		}, {
			"label": "Coeur d\'Alene, United States - Coeur d\'Alene (COE)",
			"value": "COE",
			"domestic": 1
		}, {
			"label": "Coonabarabran, Australia - Coonabarabran (COJ)",
			"value": "COJ",
			"domestic": 0
		}, {
			"label": "Kochi, India - Kochi Intl (COK)",
			"value": "COK",
			"domestic": 0
		}, {
			"label": "Cotonou, Benin - Cotonou (COO)",
			"value": "COO",
			"domestic": 0
		}, {
			"label": "Choibalsan, Mongolia - Choibalsan Airport (COQ)",
			"value": "COQ",
			"domestic": 0
		}, {
			"label": "Cordoba, Argentina - Pajas Blancas (COR)",
			"value": "COR",
			"domestic": 0
		}, {
			"label": "Colorado Springs, United States - Colorado Springs (COS)",
			"value": "COS",
			"domestic": 1
		}, {
			"label": "Columbia, MO, United States - Columbia (COU)",
			"value": "COU",
			"domestic": 1
		}, {
			"label": "Covilha, Portugal - Covilha (COV)",
			"value": "COV",
			"domestic": 0
		}, {
			"label": "San Martin de los Andes, Argentina - Chapelco (CPC)",
			"value": "CPC",
			"domestic": 0
		}, {
			"label": "Coober Pedy, Australia - Coober Pedy (CPD)",
			"value": "CPD",
			"domestic": 0
		}, {
			"label": "Campeche, Mexico - Campeche Intl (CPE)",
			"value": "CPE",
			"domestic": 0
		}, {
			"label": "Copenhagen, Denmark - Copenhagen Airport (CPH)",
			"value": "CPH",
			"domestic": 0
		}, {
			"label": "Copiapo, Chile and Easter Island - Desierto De Atacama Airport (CPO)",
			"value": "CPO",
			"domestic": 0
		}, {
			"label": "Campinas, Brazil - Campinas Intl (CPQ)",
			"value": "CPQ",
			"domestic": 0
		}, {
			"label": "Casper, United States - Casper Airport (CPR)",
			"value": "CPR",
			"domestic": 1
		}, {
			"label": "Cape Town, South Africa - Cape Town Intl (CPT)",
			"value": "CPT",
			"domestic": 0
		}, {
			"label": "Campina Grande, Brazil - Joao Suassuna Airport (CPV)",
			"value": "CPV",
			"domestic": 0
		}, {
			"label": "Culebra Island, Puerto Rico - Culebra (CPX)",
			"value": "CPX",
			"domestic": 0
		}, {
			"label": "Shahre-Kord, Iran - Shahre-Kord Airport (CQD)",
			"value": "CQD",
			"domestic": 0
		}, {
			"label": "Calais, France - Calais (CQF)",
			"value": "CQF",
			"domestic": 0
		}, {
			"label": "Craiova, Romania - Craiova (CRA)",
			"value": "CRA",
			"domestic": 0
		}, {
			"label": "Comodoro Rivadavia, Argentina - Comodoro Rivadavia (CRD)",
			"value": "CRD",
			"domestic": 0
		}, {
			"label": "Crooked Island, Bahamas - Crooked Island Airport (CRI)",
			"value": "CRI",
			"domestic": 0
		}, {
			"label": "Luzon Island, Philippines - Clark Field (CRK)",
			"value": "CRK",
			"domestic": 0
		}, {
			"label": "Brussels, Belgium - Brussels South (CRL)",
			"value": "CRL",
			"domestic": 0
		}, {
			"label": "Catarman, Philippines - National Airport (CRM)",
			"value": "CRM",
			"domestic": 0
		}, {
			"label": "Corpus Christi, United States - Corpus Christi (CRP)",
			"value": "CRP",
			"domestic": 1
		}, {
			"label": "Carriacou Island, Grenada - Lauriston (CRU)",
			"value": "CRU",
			"domestic": 0
		}, {
			"label": "Crotone, Italy - Crotone (CRV)",
			"value": "CRV",
			"domestic": 0
		}, {
			"label": "Charleston, United States - Yeager Airport (CRW)",
			"value": "CRW",
			"domestic": 1
		}, {
			"label": "Corinth, United States - Roscoe Turner Field (CRX)",
			"value": "CRX",
			"domestic": 1
		}, {
			"label": "Columbus, GA, United States - Metropolitan Area (CSG)",
			"value": "CSG",
			"domestic": 1
		}, {
			"label": "Solovetsky, Russia - Solovetsky Airport (CSH)",
			"value": "CSH",
			"domestic": 0
		}, {
			"label": "Cap Skirring, Senegal - Cap Skirring Airport (CSK)",
			"value": "CSK",
			"domestic": 0
		}, {
			"label": "San Luis Obispo, United States - Metropolitan (CSL)",
			"value": "CSL",
			"domestic": 1
		}, {
			"label": "Carson City, United States - Carson City Mncpl (CSN)",
			"value": "CSN",
			"domestic": 1
		}, {
			"label": "Creston, United States - Creston Mncpl (CSQ)",
			"value": "CSQ",
			"domestic": 1
		}, {
			"label": "Changsha, China - Changsha (CSX)",
			"value": "CSX",
			"domestic": 0
		}, {
			"label": "Catania, Italy - Fontanarossa (CTA)",
			"value": "CTA",
			"domestic": 0
		}, {
			"label": "Catamarca, Argentina - Choya (CTC)",
			"value": "CTC",
			"domestic": 0
		}, {
			"label": "Chitre, Panama - Chitre (CTD)",
			"value": "CTD",
			"domestic": 0
		}, {
			"label": "Carti, Panama - Carti Airport (CTE)",
			"value": "CTE",
			"domestic": 0
		}, {
			"label": "Cartagena, Colombia - Rafael Nunez (CTG)",
			"value": "CTG",
			"domestic": 0
		}, {
			"label": "Charleville, Australia - Charleville Airport (CTL)",
			"value": "CTL",
			"domestic": 0
		}, {
			"label": "Chetumal, Mexico - Chetumal (CTM)",
			"value": "CTM",
			"domestic": 0
		}, {
			"label": "Cooktown, Australia - Cooktown Airport (CTN)",
			"value": "CTN",
			"domestic": 0
		}, {
			"label": "Sapporo, Japan - Chitose (CTS)",
			"value": "CTS",
			"domestic": 0
		}, {
			"label": "Chengdu, China - Chengdu (CTU)",
			"value": "CTU",
			"domestic": 0
		}, {
			"label": "Cottonwood, United States - Cottonwood Mncpl (CTW)",
			"value": "CTW",
			"domestic": 1
		}, {
			"label": "Cucuta, Colombia - Camilo Daza (CUC)",
			"value": "CUC",
			"domestic": 0
		}, {
			"label": "Caloundra, Australia - Caloundra (CUD)",
			"value": "CUD",
			"domestic": 0
		}, {
			"label": "Cuenca, Ecuador - Mariscal Lamar (CUE)",
			"value": "CUE",
			"domestic": 0
		}, {
			"label": "Cuneo, Italy - Levaldigi (CUF)",
			"value": "CUF",
			"domestic": 0
		}, {
			"label": "Caye Caulker, Belize - Caye Caulker (CUK)",
			"value": "CUK",
			"domestic": 0
		}, {
			"label": "Culiacan, Mexico - Culiacan (CUL)",
			"value": "CUL",
			"domestic": 0
		}, {
			"label": "Cumana, Venezuela - Cumana (CUM)",
			"value": "CUM",
			"domestic": 0
		}, {
			"label": "Cancun, Mexico - Cancun Airport (CUN)",
			"value": "CUN",
			"domestic": 0
		}, {
			"label": "Carupano, Venezuela - Carupano Airport (CUP)",
			"value": "CUP",
			"domestic": 0
		}, {
			"label": "Coen, Australia - Coen Airport (CUQ)",
			"value": "CUQ",
			"domestic": 0
		}, {
			"label": "Willemstad, Curacao - Aeropuerto Hato (CUR)",
			"value": "CUR",
			"domestic": 0
		}, {
			"label": "Chihuahua, Mexico - Chihuahua (CUU)",
			"value": "CUU",
			"domestic": 0
		}, {
			"label": "Cuzco, Peru - Velazco Astete (CUZ)",
			"value": "CUZ",
			"domestic": 0
		}, {
			"label": "Courchevel, France - Courchevel (CVF)",
			"value": "CVF",
			"domestic": 0
		}, {
			"label": "Cincinnati, United States - Cincinnati (CVG)",
			"value": "CVG",
			"domestic": 1
		}, {
			"label": "Cuernavaca, Mexico - Cuernavaca (CVJ)",
			"value": "CVJ",
			"domestic": 0
		}, {
			"label": "Ciudad Victoria, Mexico - Ciudad Victoria (CVM)",
			"value": "CVM",
			"domestic": 0
		}, {
			"label": "Clovis, United States - Clovis Mncpl (CVN)",
			"value": "CVN",
			"domestic": 1
		}, {
			"label": "Corvallis, United States - Corvallis Mncpl (CVO)",
			"value": "CVO",
			"domestic": 1
		}, {
			"label": "Carnarvon, Australia - Carnarvon Airport (CVQ)",
			"value": "CVQ",
			"domestic": 0
		}, {
			"label": "Coventry, United Kingdom - Baginton (CVT)",
			"value": "CVT",
			"domestic": 0
		}, {
			"label": "Corvo Island (Azores), Portugal - Corvo Island (Azores) Airport (CVU)",
			"value": "CVU",
			"domestic": 0
		}, {
			"label": "Wausau, United States - Central Wisconsin Airport (CWA)",
			"value": "CWA",
			"domestic": 1
		}, {
			"label": "Curitiba, Brazil - Afonso Pena (CWB)",
			"value": "CWB",
			"domestic": 0
		}, {
			"label": "Chernovtsy, Ukraine - Chernovtsy (CWC)",
			"value": "CWC",
			"domestic": 0
		}, {
			"label": "Clinton, United States - Clinton Mncpl (CWI)",
			"value": "CWI",
			"domestic": 1
		}, {
			"label": "Cardiff, United Kingdom - Cardiff-Wales (CWL)",
			"value": "CWL",
			"domestic": 0
		}, {
			"label": "Cowra, Australia - Cowra (CWT)",
			"value": "CWT",
			"domestic": 0
		}, {
			"label": "Corowa, Australia - Corowa (CWW)",
			"value": "CWW",
			"domestic": 0
		}, {
			"label": "Coxs Bazar, Bangladesh - Coxs Bazar Airport (CXB)",
			"value": "CXB",
			"domestic": 0
		}, {
			"label": "Vancouver, Canada - Coal Harbour (CXH)",
			"value": "CXH",
			"domestic": 0
		}, {
			"label": "Christmas Island, Kiribati - Christmas Island (CXI)",
			"value": "CXI",
			"domestic": 0
		}, {
			"label": "Caxias do Sul, Brazil - Campo dos Bugres (CXJ)",
			"value": "CXJ",
			"domestic": 0
		}, {
			"label": "Calexico, United States - Calexico Intl (CXL)",
			"value": "CXL",
			"domestic": 1
		}, {
			"label": "Cilacap, Indonesia - Tunggul Wulung Airport (CXP)",
			"value": "CXP",
			"domestic": 0
		}, {
			"label": "Cayman Brac, Cayman Islands - Gerrard-Smith (CYB)",
			"value": "CYB",
			"domestic": 0
		}, {
			"label": "Chefornak, United States - Chefornak Sea Plane Base (CYF)",
			"value": "CYF",
			"domestic": 1
		}, {
			"label": "Chiayi, Taiwan - Chiayi (CYI)",
			"value": "CYI",
			"domestic": 0
		}, {
			"label": "Cayo Largo, Cuba - Cayo Largo Mncpl (CYO)",
			"value": "CYO",
			"domestic": 0
		}, {
			"label": "Calbayog, Philippines - Calbayog Airport (CYP)",
			"value": "CYP",
			"domestic": 0
		}, {
			"label": "Colonia, Uruguay - Colonia (CYR)",
			"value": "CYR",
			"domestic": 0
		}, {
			"label": "Cheyenne, United States - Cheyenne Airport (CYS)",
			"value": "CYS",
			"domestic": 1
		}, {
			"label": "Chichen Itza, Mexico - Chichen Itza Mncpl (CZA)",
			"value": "CZA",
			"domestic": 0
		}, {
			"label": "Coro, Venezuela - Coro (CZE)",
			"value": "CZE",
			"domestic": 0
		}, {
			"label": "Corozal, Belize - Corozal Mncpl (CZH)",
			"value": "CZH",
			"domestic": 0
		}, {
			"label": "Corazon De Jesus, Panama - Corazon De Jesus Airport (CZJ)",
			"value": "CZJ",
			"domestic": 0
		}, {
			"label": "Constantine, Algeria - Ain El Bey Airport (CZL)",
			"value": "CZL",
			"domestic": 0
		}, {
			"label": "Cozumel, Mexico - Cozumel Airport (CZM)",
			"value": "CZM",
			"domestic": 0
		}, {
			"label": "Chisana, United States - Chisana Field (CZN)",
			"value": "CZN",
			"domestic": 1
		}, {
			"label": "Cruzeiro Do Sul, Brazil - Campo Internacional Airport (CZS)",
			"value": "CZS",
			"domestic": 0
		}, {
			"label": "Corozal, Colombia - Corozal Airport (CZU)",
			"value": "CZU",
			"domestic": 0
		}, {
			"label": "Changzhou, China - Changzhou (CZX)",
			"value": "CZX",
			"domestic": 0
		}, {
			"label": "Daytona Beach, United States - Daytona Beach Regional Airport (DAB)",
			"value": "DAB",
			"domestic": 1
		}, {
			"label": "Dhaka, Bangladesh - Zia Intl (DAC)",
			"value": "DAC",
			"domestic": 0
		}, {
			"label": "Da Nang, Vietnam - Da Nang (DAD)",
			"value": "DAD",
			"domestic": 0
		}, {
			"label": "Dallas, United States - Love (DAL)",
			"value": "DAL",
			"domestic": 1
		}, {
			"label": "Damascus, Syria - Damascus Intl (DAM)",
			"value": "DAM",
			"domestic": 0
		}, {
			"label": "Danville, United States - Danville Mncpl (DAN)",
			"value": "DAN",
			"domestic": 1
		}, {
			"label": "Dar es Salaam, Tanzania - Dar es Salaam Intl (DAR)",
			"value": "DAR",
			"domestic": 0
		}, {
			"label": "Datong, China - Datong (DAT)",
			"value": "DAT",
			"domestic": 0
		}, {
			"label": "Daru, Papua New Guinea - Daru Airport (DAU)",
			"value": "DAU",
			"domestic": 0
		}, {
			"label": "David, Panama - Enrique Malek (DAV)",
			"value": "DAV",
			"domestic": 0
		}, {
			"label": "Daxian, China - Daxian Airport (DAX)",
			"value": "DAX",
			"domestic": 0
		}, {
			"label": "Dayton, United States - Dayton (DAY)",
			"value": "DAY",
			"domestic": 1
		}, {
			"label": "Dalbandin, Pakistan - Dalbandin Airport (DBA)",
			"value": "DBA",
			"domestic": 0
		}, {
			"label": "Dabaa City, Egypt - Alalamain International Airport (DBB)",
			"value": "DBB",
			"domestic": 0
		}, {
			"label": "Dubbo, Australia - Dubbo (DBO)",
			"value": "DBO",
			"domestic": 0
		}, {
			"label": "Dubuque, United States - Dubuque Municipal Airport (DBQ)",
			"value": "DBQ",
			"domestic": 1
		}, {
			"label": "Dubrovnik, Croatia - Dubrovnik (DBV)",
			"value": "DBV",
			"domestic": 0
		}, {
			"label": "Washington, D.C., United States - National (DCA)",
			"value": "DCA",
			"domestic": 1
		}, {
			"label": "Castres, France - Mazamet (DCM)",
			"value": "DCM",
			"domestic": 0
		}, {
			"label": "Dodge City, United States - Dodge City Regional Airport (DDC)",
			"value": "DDC",
			"domestic": 1
		}, {
			"label": "Dandong, China - Dandong (DDG)",
			"value": "DDG",
			"domestic": 0
		}, {
			"label": "Dera Ghazi Khan, Pakistan - Dera Ghazi Khan Airport (DEA)",
			"value": "DEA",
			"domestic": 0
		}, {
			"label": "Decatur, United States - Decatur (DEC)",
			"value": "DEC",
			"domestic": 1
		}, {
			"label": "Dehra Dun, India - Jolly Grant (DED)",
			"value": "DED",
			"domestic": 0
		}, {
			"label": "Decorah, United States - Decorah Mncpl (DEH)",
			"value": "DEH",
			"domestic": 1
		}, {
			"label": "New Delhi, India - Indira Gandhi International Airport (DEL)",
			"value": "DEL",
			"domestic": 0
		}, {
			"label": "Dembidollo, Ethiopia - Dembidollo Airport (DEM)",
			"value": "DEM",
			"domestic": 0
		}, {
			"label": "Denver, United States - Denver (DEN)",
			"value": "DEN",
			"domestic": 1
		}, {
			"label": "Deir Ezzor, Syria - Al Jafrah (DEZ)",
			"value": "DEZ",
			"domestic": 0
		}, {
			"label": "Dallas, United States - Ft. Worth (DFW)",
			"value": "DFW",
			"domestic": 1
		}, {
			"label": "Dangriga, Belize - Dangriga (DGA)",
			"value": "DGA",
			"domestic": 0
		}, {
			"label": "Mudgee, Australia - Mudgee (DGE)",
			"value": "DGE",
			"domestic": 0
		}, {
			"label": "Durango, Mexico - Guadalupe Victoria (DGO)",
			"value": "DGO",
			"domestic": 0
		}, {
			"label": "Durango, Mexico - Guadalupe Victoria Airport (DGO)",
			"value": "DGO",
			"domestic": 0
		}, {
			"label": "Dumaguete, Philippines - Dumaguete (DGT)",
			"value": "DGT",
			"domestic": 0
		}, {
			"label": "Douglas, United States - Converse County (DGW)",
			"value": "DGW",
			"domestic": 1
		}, {
			"label": "Dothan, United States - Dothan Airport (DHN)",
			"value": "DHN",
			"domestic": 1
		}, {
			"label": "Dibrugarh, India - Chabua Airport (DIB)",
			"value": "DIB",
			"domestic": 0
		}, {
			"label": "Antsiranana, Madagascar - Antsiranana/Arrachart Airport (DIE)",
			"value": "DIE",
			"domestic": 0
		}, {
			"label": "Diqing, China - Diqing (DIG)",
			"value": "DIG",
			"domestic": 0
		}, {
			"label": "Dijon, France - Dijon/Longvic (DIJ)",
			"value": "DIJ",
			"domestic": 0
		}, {
			"label": "Dickinson, United States - Dickinson Municipal Airport (DIK)",
			"value": "DIK",
			"domestic": 1
		}, {
			"label": "Dili, East Timor - Comoro Airport (DIL)",
			"value": "DIL",
			"domestic": 0
		}, {
			"label": "Dien Bien Phu, Vietnam - Dien Bien Airport (DIN)",
			"value": "DIN",
			"domestic": 0
		}, {
			"label": "Dire Dawa, Ethiopia - Aba Tenna D Yilma Airport (DIR)",
			"value": "DIR",
			"domestic": 0
		}, {
			"label": "Diu, India - Diu Airport (DIU)",
			"value": "DIU",
			"domestic": 0
		}, {
			"label": "Diyarbakir, Turkey - Kaplaner (DIY)",
			"value": "DIY",
			"domestic": 0
		}, {
			"label": "Jambi, Indonesia - Slt Taha Syarifudin (DJB)",
			"value": "DJB",
			"domestic": 0
		}, {
			"label": "Jerba Island, Tunisia - Melita (DJE)",
			"value": "DJE",
			"domestic": 0
		}, {
			"label": "Djanet, Algeria - Inedbirenne Airport (DJG)",
			"value": "DJG",
			"domestic": 0
		}, {
			"label": "Jayapura, Indonesia - Sentani Airport (DJJ)",
			"value": "DJJ",
			"domestic": 0
		}, {
			"label": "Delta Junction, United States - Delta Junction Mncpl (DJN)",
			"value": "DJN",
			"domestic": 1
		}, {
			"label": "Dunk Island, Australia - Dunk Island (DKI)",
			"value": "DKI",
			"domestic": 0
		}, {
			"label": "Dakar, Senegal - Yoff (DKR)",
			"value": "DKR",
			"domestic": 0
		}, {
			"label": "Douala, Cameroon - Douala (DLA)",
			"value": "DLA",
			"domestic": 0
		}, {
			"label": "Dalian, China - Dalian (DLC)",
			"value": "DLC",
			"domestic": 0
		}, {
			"label": "Geilo, Norway - Dagali (DLD)",
			"value": "DLD",
			"domestic": 0
		}, {
			"label": "Dillingham, United States - Dillingham Municipal Airport (DLG)",
			"value": "DLG",
			"domestic": 1
		}, {
			"label": "Duluth, United States - Duluth International Airport (DLH)",
			"value": "DLH",
			"domestic": 1
		}, {
			"label": "Da Lat, Vietnam - Lienkhang (DLI)",
			"value": "DLI",
			"domestic": 0
		}, {
			"label": "Dillon, United States - Dillon County (DLL)",
			"value": "DLL",
			"domestic": 1
		}, {
			"label": "Dalaman, Turkey - Dalaman (DLM)",
			"value": "DLM",
			"domestic": 0
		}, {
			"label": "The Dalles, United States - The Dalles Mncpl (DLS)",
			"value": "DLS",
			"domestic": 1
		}, {
			"label": "Dali, China - Dali (DLU)",
			"value": "DLU",
			"domestic": 0
		}, {
			"label": "Dillons Bay, Vanuatu - Dillons Bay Airport (DLY)",
			"value": "DLY",
			"domestic": 0
		}, {
			"label": "Dalanzadgad, Mongolia - Dalanzadgad Airport (DLZ)",
			"value": "DLZ",
			"domestic": 0
		}, {
			"label": "Dzhambyl, Kazakhstan - Dzhambyl Airport (DMB)",
			"value": "DMB",
			"domestic": 0
		}, {
			"label": "Doomadgee, Australia - Doomadgee Airport (DMD)",
			"value": "DMD",
			"domestic": 0
		}, {
			"label": "Moscow, Russia - Domodedovo Airport (DME)",
			"value": "DME",
			"domestic": 0
		}, {
			"label": "Dammam, Saudi Arabia - King Fahad Intl (DMM)",
			"value": "DMM",
			"domestic": 0
		}, {
			"label": "Sedalia, United States - Sedalia (DMO)",
			"value": "DMO",
			"domestic": 1
		}, {
			"label": "Dimapur, India - Dimapur Airport (DMU)",
			"value": "DMU",
			"domestic": 0
		}, {
			"label": "Dundee, United Kingdom - Dundee (DND)",
			"value": "DND",
			"domestic": 0
		}, {
			"label": "Dunhuang, China - Dunhuang (DNH)",
			"value": "DNH",
			"domestic": 0
		}, {
			"label": "Dnepropetrovsk, Ukraine - Dnepropetrovsk (DNK)",
			"value": "DNK",
			"domestic": 0
		}, {
			"label": "Dinard, France - Pleurtuit (DNR)",
			"value": "DNR",
			"domestic": 0
		}, {
			"label": "Danville, United States - Vermilion Cty (DNV)",
			"value": "DNV",
			"domestic": 1
		}, {
			"label": "Denizli, Turkey - Cardak (DNZ)",
			"value": "DNZ",
			"domestic": 0
		}, {
			"label": "Dongola, Sudan - Dongola Airport (DOG)",
			"value": "DOG",
			"domestic": 0
		}, {
			"label": "Doha, Qatar - Doha Free Zone (DOH)",
			"value": "DOH",
			"domestic": 0
		}, {
			"label": "Doha, Qatar - Doha Intl (DOH)",
			"value": "DOH",
			"domestic": 0
		}, {
			"label": "Donetsk, Ukraine - Donetsk (DOK)",
			"value": "DOK",
			"domestic": 0
		}, {
			"label": "Deauville, France - Saint Gatien (DOL)",
			"value": "DOL",
			"domestic": 0
		}, {
			"label": "Roseau, Dominica - Melville Hall Airport (DOM)",
			"value": "DOM",
			"domestic": 0
		}, {
			"label": "Dolpa, Nepal - Dolpa Airport (DOP)",
			"value": "DOP",
			"domestic": 0
		}, {
			"label": "Dourados, Brazil - Dourados Airport (DOU)",
			"value": "DOU",
			"domestic": 0
		}, {
			"label": "Dongying, China - Dongying Airport (DOY)",
			"value": "DOY",
			"domestic": 0
		}, {
			"label": "Dieppe, France - Dieppe (DPE)",
			"value": "DPE",
			"domestic": 0
		}, {
			"label": "Dipolog, Philippines - Dipolog Airport (DPL)",
			"value": "DPL",
			"domestic": 0
		}, {
			"label": "Devonport, Australia - Devonport (DPO)",
			"value": "DPO",
			"domestic": 0
		}, {
			"label": "Denpasar, Indonesia - Ngurah Rai (DPS)",
			"value": "DPS",
			"domestic": 0
		}, {
			"label": "Derby, Australia - Derby (DRB)",
			"value": "DRB",
			"domestic": 0
		}, {
			"label": "Drummond Island, United States - Drummond Island Mncp (DRE)",
			"value": "DRE",
			"domestic": 1
		}, {
			"label": "Deering, United States - Deering Airport (DRG)",
			"value": "DRG",
			"domestic": 1
		}, {
			"label": "Durango, United States - La Plata Airport (DRO)",
			"value": "DRO",
			"domestic": 1
		}, {
			"label": "Dresden, Germany - Dresden (DRS)",
			"value": "DRS",
			"domestic": 0
		}, {
			"label": "Del Rio, United States - Del Rio Intl (DRT)",
			"value": "DRT",
			"domestic": 1
		}, {
			"label": "Darwin, Australia - Darwin (DRW)",
			"value": "DRW",
			"domestic": 0
		}, {
			"label": "Doncaster, United Kingdom - Sheffield (DSA)",
			"value": "DSA",
			"domestic": 0
		}, {
			"label": "Destin, United States - Destin-Ft Walton Bch (DSI)",
			"value": "DSI",
			"domestic": 1
		}, {
			"label": "Des Moines, United States - Des Moines Airport (DSM)",
			"value": "DSM",
			"domestic": 1
		}, {
			"label": "Delta, United States - Delta Mncpl (DTA)",
			"value": "DTA",
			"domestic": 1
		}, {
			"label": "Datadawai, Indonesia - Datadawai Airport (DTD)",
			"value": "DTD",
			"domestic": 0
		}, {
			"label": "Death Valley, United States - Death Valley (DTH)",
			"value": "DTH",
			"domestic": 1
		}, {
			"label": "Detroit Lakes, United States - Detroit Lakes (DTL)",
			"value": "DTL",
			"domestic": 1
		}, {
			"label": "Dortmund, Germany - Dortmund Wickede (DTM)",
			"value": "DTM",
			"domestic": 0
		}, {
			"label": "Detroit, United States - All Airports (DTT)",
			"value": "DTT",
			"domestic": 1
		}, {
			"label": "Detroit, United States - Detroit Metro (DTW)",
			"value": "DTW",
			"domestic": 1
		}, {
			"label": "Dublin, Ireland - Dublin Airport (DUB)",
			"value": "DUB",
			"domestic": 0
		}, {
			"label": "Dunedin, New Zealand - Dunedin (DUD)",
			"value": "DUD",
			"domestic": 0
		}, {
			"label": "Dundo, Angola - Dundo Airport (DUE)",
			"value": "DUE",
			"domestic": 0
		}, {
			"label": "Douglas, United States - Bisbee-Douglas Intl (DUG)",
			"value": "DUG",
			"domestic": 1
		}, {
			"label": "Du Bois, United States - Jefferson Cty (DUJ)",
			"value": "DUJ",
			"domestic": 1
		}, {
			"label": "Duncan, Canada - Quamichan Lake (DUQ)",
			"value": "DUQ",
			"domestic": 0
		}, {
			"label": "Durban, South Africa - Durban Intl (DUR)",
			"value": "DUR",
			"domestic": 0
		}, {
			"label": "Dusseldorf, Germany - Dusseldorf Airport (DUS)",
			"value": "DUS",
			"domestic": 0
		}, {
			"label": "Dutch Harbor, United States - Emergency Field (DUT)",
			"value": "DUT",
			"domestic": 1
		}, {
			"label": "Devils Lake, United States - Devils Lake Mncpl (DVL)",
			"value": "DVL",
			"domestic": 1
		}, {
			"label": "Davenport, United States - Davenport Mncpl (DVN)",
			"value": "DVN",
			"domestic": 1
		}, {
			"label": "Davao, Philippines - Mati (DVO)",
			"value": "DVO",
			"domestic": 0
		}, {
			"label": "Soalala, Madagascar - Soalala Airport (DWB)",
			"value": "DWB",
			"domestic": 0
		}, {
			"label": "Dawadmi, Saudi Arabia - Dawadmi Airport (DWD)",
			"value": "DWD",
			"domestic": 0
		}, {
			"label": "Dubai, United Arab Emirates - Dubai (DXB)",
			"value": "DXB",
			"domestic": 0
		}, {
			"label": "Danbury, United States - Danbury Mncpl (DXR)",
			"value": "DXR",
			"domestic": 1
		}, {
			"label": "Dayong, China - Dayong Airport (DYG)",
			"value": "DYG",
			"domestic": 0
		}, {
			"label": "Anadyr, Russia - Anadyr Airport (DYR)",
			"value": "DYR",
			"domestic": 0
		}, {
			"label": "Dushanbe, Tajikistan - Dushanbe (DYU)",
			"value": "DYU",
			"domestic": 0
		}, {
			"label": "Mayotte, France - Dzaoudzi Airport (DZA)",
			"value": "DZA",
			"domestic": 0
		}, {
			"label": "Zhezkazgan, Kazakhstan - Zhezhazgan Airport (DZN)",
			"value": "DZN",
			"domestic": 0
		}, {
			"label": "Eagle, United States - Eagle Airport (EAA)",
			"value": "EAA",
			"domestic": 1
		}, {
			"label": "Emae, Vanuatu - Emae Airport (EAE)",
			"value": "EAE",
			"domestic": 0
		}, {
			"label": "Kwajalein Atoll, Marshall Islands - Elenak Airport (EAL)",
			"value": "EAL",
			"domestic": 0
		}, {
			"label": "Najran, Saudi Arabia - Nejran (EAM)",
			"value": "EAM",
			"domestic": 0
		}, {
			"label": "Kearney, United States - Kearney Municipal Airport (EAR)",
			"value": "EAR",
			"domestic": 1
		}, {
			"label": "San Sebastian, Spain - San Sebastian (EAS)",
			"value": "EAS",
			"domestic": 0
		}, {
			"label": "Wenatchee, United States - Pangborn Field (EAT)",
			"value": "EAT",
			"domestic": 1
		}, {
			"label": "Eau Claire, United States - Eau Claire Airport (EAU)",
			"value": "EAU",
			"domestic": 1
		}, {
			"label": "Elba Island, Italy - Marina Di Campo (EBA)",
			"value": "EBA",
			"domestic": 0
		}, {
			"label": "Entebbe, Uganda - Entebbe (EBB)",
			"value": "EBB",
			"domestic": 0
		}, {
			"label": "El Obeid, Sudan - El Obeid Airport (EBD)",
			"value": "EBD",
			"domestic": 0
		}, {
			"label": "Esbjerg, Denmark - Esbjerg (EBJ)",
			"value": "EBJ",
			"domestic": 0
		}, {
			"label": "Erbil, Iraq - Erbil International Airport (EBL)",
			"value": "EBL",
			"domestic": 0
		}, {
			"label": "Ebon, Marshall Islands - Ebon Airport (EBO)",
			"value": "EBO",
			"domestic": 0
		}, {
			"label": "St Etienne, France - Boutheon (EBU)",
			"value": "EBU",
			"domestic": 0
		}, {
			"label": "East Tawas, United States - East Tawas Mncpl (ECA)",
			"value": "ECA",
			"domestic": 1
		}, {
			"label": "Echuca, Australia - Echuca (ECH)",
			"value": "ECH",
			"domestic": 0
		}, {
			"label": "Ercan, Cyprus - Ercan Airport (ECN)",
			"value": "ECN",
			"domestic": 0
		}, {
			"label": "Panama City Beach, United States - Panama City Beach (ECP)",
			"value": "ECP",
			"domestic": 1
		}, {
			"label": "Edna Bay, United States - Edna Bay Airport (EDA)",
			"value": "EDA",
			"domestic": 1
		}, {
			"label": "Edinburgh, United Kingdom - Edinburgh (EDI)",
			"value": "EDI",
			"domestic": 0
		}, {
			"label": "Edremit/Korfez, Turkey - Edremit/Korfez Airport (EDO)",
			"value": "EDO",
			"domestic": 0
		}, {
			"label": "Edward River, Australia - Edward River Airport (EDR)",
			"value": "EDR",
			"domestic": 0
		}, {
			"label": "Eek, United States - Eek Airport (EEK)",
			"value": "EEK",
			"domestic": 1
		}, {
			"label": "Keene, United States - Dillant-Hopkins (EEN)",
			"value": "EEN",
			"domestic": 1
		}, {
			"label": "Kefalonia, Greece - Argostoli (EFL)",
			"value": "EFL",
			"domestic": 0
		}, {
			"label": "Bergerac, France - Roumaniere (EGC)",
			"value": "EGC",
			"domestic": 0
		}, {
			"label": "Vail, United States - Vail/Eagle Airport (EGE)",
			"value": "EGE",
			"domestic": 1
		}, {
			"label": "Sege, Solomon Islands - Sege Airport (EGM)",
			"value": "EGM",
			"domestic": 0
		}, {
			"label": "Belgorod, Russia - Belgorod Airport (EGO)",
			"value": "EGO",
			"domestic": 0
		}, {
			"label": "Eagle Pass, United States - Maverick County (EGP)",
			"value": "EGP",
			"domestic": 1
		}, {
			"label": "Egilsstadir, Iceland - Egilsstadir (EGS)",
			"value": "EGS",
			"domestic": 0
		}, {
			"label": "Eagle River, United States - Eagle River Union (EGV)",
			"value": "EGV",
			"domestic": 1
		}, {
			"label": "Egegik, United States - Egegik Airport (EGX)",
			"value": "EGX",
			"domestic": 1
		}, {
			"label": "Eniseysk, Russia - Eniseysk Airport (EIE)",
			"value": "EIE",
			"domestic": 0
		}, {
			"label": "Eindhoven, Netherlands - Eindhoven (EIN)",
			"value": "EIN",
			"domestic": 0
		}, {
			"label": "Tortola, British Virgin Islands - Beef Island Airport (EIS)",
			"value": "EIS",
			"domestic": 0
		}, {
			"label": "Barrancabermeja, Colombia - Variguies Airport (EJA)",
			"value": "EJA",
			"domestic": 0
		}, {
			"label": "Wedjh, Saudi Arabia - Wedjh Airport (EJH)",
			"value": "EJH",
			"domestic": 0
		}, {
			"label": "Mili Atoll, Marshall Islands - Enijet Airport (EJT)",
			"value": "EJT",
			"domestic": 0
		}, {
			"label": "Elkhart, United States - Elkhart Mncpl (EKI)",
			"value": "EKI",
			"domestic": 1
		}, {
			"label": "Elkins, United States - Elkins (EKN)",
			"value": "EKN",
			"domestic": 1
		}, {
			"label": "Elko, United States - Elko (EKO)",
			"value": "EKO",
			"domestic": 1
		}, {
			"label": "Eskilstuna, Sweden - Eskilstuna (EKT)",
			"value": "EKT",
			"domestic": 0
		}, {
			"label": "Elizabethtown, United States - Elizabethtown (EKX)",
			"value": "EKX",
			"domestic": 1
		}, {
			"label": "Elcho Island, Australia - Elcho Island Airport (ELC)",
			"value": "ELC",
			"domestic": 0
		}, {
			"label": "El Dorado, United States - Goodwin Fld (ELD)",
			"value": "ELD",
			"domestic": 1
		}, {
			"label": "El Real, Panama - El Real Airport (ELE)",
			"value": "ELE",
			"domestic": 0
		}, {
			"label": "El Fasher, Sudan - El Fasher Airport (ELF)",
			"value": "ELF",
			"domestic": 0
		}, {
			"label": "North Eleuthera, Bahamas - North Eleuthera International Airport (ELH)",
			"value": "ELH",
			"domestic": 0
		}, {
			"label": "Elim, United States - Elim Airport (ELI)",
			"value": "ELI",
			"domestic": 1
		}, {
			"label": "Elmira, United States - Elmira Regional Airport (ELM)",
			"value": "ELM",
			"domestic": 1
		}, {
			"label": "El Paso, United States - El Paso (ELP)",
			"value": "ELP",
			"domestic": 1
		}, {
			"label": "Gassim, Saudi Arabia - Gassim Airport (ELQ)",
			"value": "ELQ",
			"domestic": 0
		}, {
			"label": "East London, South Africa - East London (ELS)",
			"value": "ELS",
			"domestic": 0
		}, {
			"label": "El Oued, Algeria - Guemar Airport (ELU)",
			"value": "ELU",
			"domestic": 0
		}, {
			"label": "Elfin Cove, United States - Elfin Cove Sea Plane Base (ELV)",
			"value": "ELV",
			"domestic": 1
		}, {
			"label": "Ely, United States - Yelland Field (ELY)",
			"value": "ELY",
			"domestic": 1
		}, {
			"label": "Wellsville, United States - Wellsville Mncpl (ELZ)",
			"value": "ELZ",
			"domestic": 1
		}, {
			"label": "Nottingham, NGM, United Kingdom - East Midlands (EMA)",
			"value": "EMA",
			"domestic": 0
		}, {
			"label": "Emerald, Australia - Emerald Airport (EMD)",
			"value": "EMD",
			"domestic": 0
		}, {
			"label": "Emden, Germany - Emden (EME)",
			"value": "EME",
			"domestic": 0
		}, {
			"label": "Emmonak, United States - Emmonak Airport (EMK)",
			"value": "EMK",
			"domestic": 1
		}, {
			"label": "Emporia, United States - Emporia Mncpl (EMP)",
			"value": "EMP",
			"domestic": 1
		}, {
			"label": "Kenai, United States - Kenai Mncpl (ENA)",
			"value": "ENA",
			"domestic": 1
		}, {
			"label": "Nancy, France - Essey (ENC)",
			"value": "ENC",
			"domestic": 0
		}, {
			"label": "Ende, Indonesia - Ende Airport (ENE)",
			"value": "ENE",
			"domestic": 0
		}, {
			"label": "Enontekio, Finland - Enontekio (ENF)",
			"value": "ENF",
			"domestic": 0
		}, {
			"label": "Enshi, China - Enshi (ENH)",
			"value": "ENH",
			"domestic": 0
		}, {
			"label": "El Nido, Philippines - El Nido Airport (ENI)",
			"value": "ENI",
			"domestic": 0
		}, {
			"label": "Enschede, Netherlands - Twente (ENS)",
			"value": "ENS",
			"domestic": 0
		}, {
			"label": "Enewetak Island, Marshall Islands - Enewetak Island Airport (ENT)",
			"value": "ENT",
			"domestic": 0
		}, {
			"label": "Enugu, Nigeria - Enugu (ENU)",
			"value": "ENU",
			"domestic": 0
		}, {
			"label": "Wendover, United States - Wendover Mncpl (ENV)",
			"value": "ENV",
			"domestic": 1
		}, {
			"label": "Yan\'an, China - Yan\'an Airport (ENY)",
			"value": "ENY",
			"domestic": 0
		}, {
			"label": "Medellin, Colombia - Enriq Olaya Herrera (EOH)",
			"value": "EOH",
			"domestic": 0
		}, {
			"label": "Eday, United Kingdom - Eday Airport (EOI)",
			"value": "EOI",
			"domestic": 0
		}, {
			"label": "Keokuk, United States - Keokuk Mncpl (EOK)",
			"value": "EOK",
			"domestic": 1
		}, {
			"label": "Neosho, United States - Neosho Mncpl (EOS)",
			"value": "EOS",
			"domestic": 1
		}, {
			"label": "Elorza, Venezuela - Elorza Airport (EOZ)",
			"value": "EOZ",
			"domestic": 0
		}, {
			"label": "Epinal, France - Mirecourt (EPL)",
			"value": "EPL",
			"domestic": 0
		}, {
			"label": "Esperance, Australia - Esperance (EPR)",
			"value": "EPR",
			"domestic": 0
		}, {
			"label": "Samana, Dominican Republic - El Portillo (EPS)",
			"value": "EPS",
			"domestic": 0
		}, {
			"label": "Parnu, Estonia - Parnu (EPU)",
			"value": "EPU",
			"domestic": 0
		}, {
			"label": "Esquel, Argentina - Esquel Mncpl (EQS)",
			"value": "EQS",
			"domestic": 0
		}, {
			"label": "Erzincan, Turkey - Erzincan Airport (ERC)",
			"value": "ERC",
			"domestic": 0
		}, {
			"label": "Erfurt, Germany - Erfurt (ERF)",
			"value": "ERF",
			"domestic": 0
		}, {
			"label": "Errachidia, Morocco - Errachidia Airport (ERH)",
			"value": "ERH",
			"domestic": 0
		}, {
			"label": "Erie, United States - Erie International Airport (ERI)",
			"value": "ERI",
			"domestic": 1
		}, {
			"label": "Eirunepe, Brazil - Eirunepe Airport (ERN)",
			"value": "ERN",
			"domestic": 0
		}, {
			"label": "Windhoek, Namibia - Eros (ERS)",
			"value": "ERS",
			"domestic": 0
		}, {
			"label": "Erzurum, Turkey - Erzurum (ERZ)",
			"value": "ERZ",
			"domestic": 0
		}, {
			"label": "Ankara, Turkey - Esenboga (ESB)",
			"value": "ESB",
			"domestic": 0
		}, {
			"label": "Escanaba, United States - Delta County Airport (ESC)",
			"value": "ESC",
			"domestic": 1
		}, {
			"label": "Eastsound, United States - Orcas Island (ESD)",
			"value": "ESD",
			"domestic": 1
		}, {
			"label": "Ensenada, Mexico - El Cipresese (ESE)",
			"value": "ESE",
			"domestic": 0
		}, {
			"label": "Shoreham By Sea, United Kingdom - Shoreham Airport (ESH)",
			"value": "ESH",
			"domestic": 0
		}, {
			"label": "Esmeraldas, Ecuador - Esmeraldas Airport (ESM)",
			"value": "ESM",
			"domestic": 0
		}, {
			"label": "Easton, United States - Easton Mncpl (ESN)",
			"value": "ESN",
			"domestic": 1
		}, {
			"label": "El Salvador, Chile and Easter Island - El Salvador Airport (ESR)",
			"value": "ESR",
			"domestic": 0
		}, {
			"label": "Essen, Germany - Essen Airport (ESS)",
			"value": "ESS",
			"domestic": 0
		}, {
			"label": "Essaouira, Morocco - Essaouira (ESU)",
			"value": "ESU",
			"domestic": 0
		}, {
			"label": "Eilat, Israel - Elat (ETH)",
			"value": "ETH",
			"domestic": 0
		}, {
			"label": "Metz/Nancy, France - Metz-Nancy-Lorraine Airport (ETZ)",
			"value": "ETZ",
			"domestic": 0
		}, {
			"label": "Eua, Tonga - Kaufana Airport (EUA)",
			"value": "EUA",
			"domestic": 0
		}, {
			"label": "Eugene, United States - Eugene (EUG)",
			"value": "EUG",
			"domestic": 1
		}, {
			"label": "Laayoune, Morocco - Hassan I Airport (EUN)",
			"value": "EUN",
			"domestic": 0
		}, {
			"label": "St Eustatius, Curacao - Roosevelt Fld (EUX)",
			"value": "EUX",
			"domestic": 0
		}, {
			"label": "Harstad-Narvik, Norway - Evenes Airport (EVE)",
			"value": "EVE",
			"domestic": 0
		}, {
			"label": "Sveg, Sweden - Sveg Airport (EVG)",
			"value": "EVG",
			"domestic": 0
		}, {
			"label": "Eveleth, United States - Eveleth-Virginia (EVM)",
			"value": "EVM",
			"domestic": 1
		}, {
			"label": "Yerevan, Armenia - Yerevan (EVN)",
			"value": "EVN",
			"domestic": 0
		}, {
			"label": "Evansville, United States - Dress Regional Airport (EVV)",
			"value": "EVV",
			"domestic": 1
		}, {
			"label": "Evanston, United States - Evanston Mncpl (EVW)",
			"value": "EVW",
			"domestic": 1
		}, {
			"label": "New Bedford, United States - New Bedford Mncpl (EWB)",
			"value": "EWB",
			"domestic": 1
		}, {
			"label": "New Bern, United States - Simmons Nott Airport (EWN)",
			"value": "EWN",
			"domestic": 1
		}, {
			"label": "Newark, United States - Newark (EWR)",
			"value": "EWR",
			"domestic": 1
		}, {
			"label": "Excursion Inlet, United States - Excursion Inlet Sea Plane Base (EXI)",
			"value": "EXI",
			"domestic": 1
		}, {
			"label": "Exeter, United Kingdom - Exeter (EXT)",
			"value": "EXT",
			"domestic": 0
		}, {
			"label": "El Yopal, Colombia - El Yopal Airport (EYP)",
			"value": "EYP",
			"domestic": 0
		}, {
			"label": "Key West, United States - Key West International Airport (EYW)",
			"value": "EYW",
			"domestic": 1
		}, {
			"label": "Buenos Aires, Argentina - Ministro Pistarini Airport (EZE)",
			"value": "EZE",
			"domestic": 0
		}, {
			"label": "Elazig, Turkey - Elazig Airport (EZS)",
			"value": "EZS",
			"domestic": 0
		}, {
			"label": "Farnborough, United Kingdom - Farnborough (FAB)",
			"value": "FAB",
			"domestic": 0
		}, {
			"label": "Faroe Islands, Denmark - Vagar Airport (FAE)",
			"value": "FAE",
			"domestic": 0
		}, {
			"label": "Fairbanks, United States - Fairbanks (FAI)",
			"value": "FAI",
			"domestic": 1
		}, {
			"label": "Fajardo, Puerto Rico - Puerto Real Seaplane (FAJ)",
			"value": "FAJ",
			"domestic": 0
		}, {
			"label": "Farsund, Norway - Lista (FAN)",
			"value": "FAN",
			"domestic": 0
		}, {
			"label": "Faro, Portugal - Faro (FAO)",
			"value": "FAO",
			"domestic": 0
		}, {
			"label": "Fargo, United States - Hector Field (FAR)",
			"value": "FAR",
			"domestic": 1
		}, {
			"label": "Fresno, United States - Fresno (FAT)",
			"value": "FAT",
			"domestic": 1
		}, {
			"label": "Fakarava, French Polynesia and Tahiti - Fakarava Airport (FAV)",
			"value": "FAV",
			"domestic": 0
		}, {
			"label": "Fayetteville, United States - Fayetteville (FAY)",
			"value": "FAY",
			"domestic": 1
		}, {
			"label": "Faribault, United States - Faribault Mncpl (FBL)",
			"value": "FBL",
			"domestic": 1
		}, {
			"label": "Lubumbashi, Democratic Republic of Congo - Luano (FBM)",
			"value": "FBM",
			"domestic": 0
		}, {
			"label": "Friday Harbor, United States - Friday Harbor SPB (FBS)",
			"value": "FBS",
			"domestic": 1
		}, {
			"label": "Kalispell, United States - Glacier National Park Airport (FCA)",
			"value": "FCA",
			"domestic": 1
		}, {
			"label": "Rome, Italy - Fiumicino Airport (FCO)",
			"value": "FCO",
			"domestic": 0
		}, {
			"label": "Forde, Norway - Bringeland (FDE)",
			"value": "FDE",
			"domestic": 0
		}, {
			"label": "Fort de France, Martinique - Lamentin (FDF)",
			"value": "FDF",
			"domestic": 0
		}, {
			"label": "Friedrichshafen, Germany - Friedrichshafen (FDH)",
			"value": "FDH",
			"domestic": 0
		}, {
			"label": "Frederick, United States - Frederick Mncpl (FDK)",
			"value": "FDK",
			"domestic": 1
		}, {
			"label": "Fergana, Uzbekistan - Fergana Airport (FEG)",
			"value": "FEG",
			"domestic": 0
		}, {
			"label": "Fernando de Noronha, Brazil - Fernando De Noronha Airport (FEN)",
			"value": "FEN",
			"domestic": 0
		}, {
			"label": "Fez, Morocco - Sais (FEZ)",
			"value": "FEZ",
			"domestic": 0
		}, {
			"label": "Fergus Falls, United States - Fergus Falls Mncpl (FFM)",
			"value": "FFM",
			"domestic": 1
		}, {
			"label": "Frankfort, United States - Capitol City (FFT)",
			"value": "FFT",
			"domestic": 1
		}, {
			"label": "Fort Huachuca/Sierra Vista, United States - Fort Huachuca/Sierra Vista Municipal Airport (FHU)",
			"value": "FHU",
			"domestic": 1
		}, {
			"label": "Kinshasa, Democratic Republic of Congo - N\'Djili (FIH)",
			"value": "FIH",
			"domestic": 0
		}, {
			"label": "Al-Fujairah, United Arab Emirates - Fujairah Intl (FJR)",
			"value": "FJR",
			"domestic": 0
		}, {
			"label": "Karlsruhe, Germany - Soellingen (FKB)",
			"value": "FKB",
			"domestic": 0
		}, {
			"label": "Kisangani, Democratic Republic of Congo - Kisangani Airport (FKI)",
			"value": "FKI",
			"domestic": 0
		}, {
			"label": "Franklin, United States - Chess-Lamberton (FKL)",
			"value": "FKL",
			"domestic": 1
		}, {
			"label": "Fukushima, Japan - Fukushima (FKS)",
			"value": "FKS",
			"domestic": 0
		}, {
			"label": "Florencia, Colombia - Capitolio Airport (FLA)",
			"value": "FLA",
			"domestic": 0
		}, {
			"label": "Flagstaff, United States - Pulliam Field (FLG)",
			"value": "FLG",
			"domestic": 1
		}, {
			"label": "Fort Lauderdale, United States - Ft. Lauderdale (FLL)",
			"value": "FLL",
			"domestic": 1
		}, {
			"label": "Ft Lauderdale, United States - Ft. Lauderdale (FLL)",
			"value": "FLL",
			"domestic": 1
		}, {
			"label": "Florianopolis, Brazil - Hercilio Luz (FLN)",
			"value": "FLN",
			"domestic": 0
		}, {
			"label": "Florence, United States - Florence Airport (FLO)",
			"value": "FLO",
			"domestic": 1
		}, {
			"label": "Florence, Italy - Peretola Airport (FLR)",
			"value": "FLR",
			"domestic": 0
		}, {
			"label": "Flinders Island, Australia - Flinders Island Airport (FLS)",
			"value": "FLS",
			"domestic": 0
		}, {
			"label": "Flores Island (Azores), Portugal - Santa Cruz Airport (FLW)",
			"value": "FLW",
			"domestic": 0
		}, {
			"label": "Formosa, Argentina - El Pucu Airport (FMA)",
			"value": "FMA",
			"domestic": 0
		}, {
			"label": "Kalemie, Democratic Republic of Congo - Kalemie Airport (FMI)",
			"value": "FMI",
			"domestic": 0
		}, {
			"label": "Memmingen, Germany - Memmingen Airport (FMM)",
			"value": "FMM",
			"domestic": 0
		}, {
			"label": "Farmington, United States - Farmington Municipal Airport (FMN)",
			"value": "FMN",
			"domestic": 1
		}, {
			"label": "Muenster, Germany - Muenster (FMO)",
			"value": "FMO",
			"domestic": 0
		}, {
			"label": "Fort Madison, United States - Fort Madison Mncpl (FMS)",
			"value": "FMS",
			"domestic": 1
		}, {
			"label": "Fort Myers, FL, United States - SW Florida Intl (RSW)",
			"value": "FMY",
			"domestic": 1
		}, {
			"label": "Freetown, Sierra Leone - Lungi Intl (FNA)",
			"value": "FNA",
			"domestic": 0
		}, {
			"label": "Neubrandenburg, Germany - Neubrandenburg (FNB)",
			"value": "FNB",
			"domestic": 0
		}, {
			"label": "Funchal, Portugal - Funchal (FNC)",
			"value": "FNC",
			"domestic": 0
		}, {
			"label": "Nimes, France - Garons (FNI)",
			"value": "FNI",
			"domestic": 0
		}, {
			"label": "Ft Collins, United States - Fort Collins/Loveland (FNL)",
			"value": "FNL",
			"domestic": 1
		}, {
			"label": "Flint, United States - Bishop Airport (FNT)",
			"value": "FNT",
			"domestic": 1
		}, {
			"label": "Fort Bragg, United States - Little River-Mendoci (FOB)",
			"value": "FOB",
			"domestic": 1
		}, {
			"label": "Fuzhou, China - Fuzhou (FOC)",
			"value": "FOC",
			"domestic": 0
		}, {
			"label": "Fort Dodge, United States - Fort Dodge Mncpl (FOD)",
			"value": "FOD",
			"domestic": 1
		}, {
			"label": "Foggia, Italy - Gino Lisa (FOG)",
			"value": "FOG",
			"domestic": 0
		}, {
			"label": "Fortaleza, Brazil - Pinto Martins (FOR)",
			"value": "FOR",
			"domestic": 0
		}, {
			"label": "Freeport, Bahamas - Freeport International Airport (FPO)",
			"value": "FPO",
			"domestic": 0
		}, {
			"label": "Fort Pierce, United States - Saint Lucie Cty (FPR)",
			"value": "FPR",
			"domestic": 1
		}, {
			"label": "Frankfurt, Germany - Frankfurt International Airport (FRA)",
			"value": "FRA",
			"domestic": 0
		}, {
			"label": "Forbes, Australia - Forbes (FRB)",
			"value": "FRB",
			"domestic": 0
		}, {
			"label": "Franca, Brazil - Franca (FRC)",
			"value": "FRC",
			"domestic": 0
		}, {
			"label": "Friday Harbor, United States - Friday Harbor (FRD)",
			"value": "FRD",
			"domestic": 1
		}, {
			"label": "Fera Island, Solomon Islands - Fera Island Airport (FRE)",
			"value": "FRE",
			"domestic": 0
		}, {
			"label": "Farmingdale, United States - Republic (FRG)",
			"value": "FRG",
			"domestic": 1
		}, {
			"label": "French Lick, United States - French Lick Mncpl (FRH)",
			"value": "FRH",
			"domestic": 1
		}, {
			"label": "Fregate Island, Seychelles - Fregate Is. (FRK)",
			"value": "FRK",
			"domestic": 0
		}, {
			"label": "Forli, Italy - Luigi Ridolfi (FRL)",
			"value": "FRL",
			"domestic": 0
		}, {
			"label": "Fairmont, United States - Fairmont Mncpl (FRM)",
			"value": "FRM",
			"domestic": 1
		}, {
			"label": "Floro, Norway - Flora (FRO)",
			"value": "FRO",
			"domestic": 0
		}, {
			"label": "Flores, Guatemala - Santa Elena (FRS)",
			"value": "FRS",
			"domestic": 0
		}, {
			"label": "Bishkek, Kyrgyzstan - Manas (FRU)",
			"value": "FRU",
			"domestic": 0
		}, {
			"label": "Francistown, Botswana - Francistown (FRW)",
			"value": "FRW",
			"domestic": 0
		}, {
			"label": "Figari, France - Sud Corse (FSC)",
			"value": "FSC",
			"domestic": 0
		}, {
			"label": "Sioux Falls, United States - Sioux Falls Regional Airport (Jo Foss Field) (FSD)",
			"value": "FSD",
			"domestic": 1
		}, {
			"label": "Ft Smith, United States - Fort Smith Municipal Airport (FSM)",
			"value": "FSM",
			"domestic": 1
		}, {
			"label": "St-Pierre, Dominica - St-Pierre (FSP)",
			"value": "FSP",
			"domestic": 0
		}, {
			"label": "Futuna Island, Vanuatu - Futuna Airport (FTA)",
			"value": "FTA",
			"domestic": 0
		}, {
			"label": "El Calafate, Argentina - El Calafate (FTE)",
			"value": "FTE",
			"domestic": 0
		}, {
			"label": "Fort Dauphin, Madagascar - Marillac Airport (FTU)",
			"value": "FTU",
			"domestic": 0
		}, {
			"label": "Ft Worth, United States - Meacham  Field (FTW)",
			"value": "FTW",
			"domestic": 1
		}, {
			"label": "Fuerteventura, Spain - Fuerteventura (FUE)",
			"value": "FUE",
			"domestic": 0
		}, {
			"label": "Fukue, Japan - Fukue Airport (FUJ)",
			"value": "FUJ",
			"domestic": 0
		}, {
			"label": "Fukuoka, Japan - Fukuoka (FUK)",
			"value": "FUK",
			"domestic": 0
		}, {
			"label": "Fullerton, United States - Fullerton (FUL)",
			"value": "FUL",
			"domestic": 1
		}, {
			"label": "Funafuti Atoll, Tuvalu - Funafuti Atoll Intl (FUN)",
			"value": "FUN",
			"domestic": 0
		}, {
			"label": "Futuna Island, Wallis and Futuna - Futuna Island Airport (FUT)",
			"value": "FUT",
			"domestic": 0
		}, {
			"label": "Ft Wayne, United States - Fort Wayne Municipal Airport/Baer Field (FWA)",
			"value": "FWA",
			"domestic": 1
		}, {
			"label": "Fort Yukon, United States - Fort Yukon Airport (FYU)",
			"value": "FYU",
			"domestic": 1
		}, {
			"label": "Gadsden, United States - Gadsden Mncpl (GAD)",
			"value": "GAD",
			"domestic": 1
		}, {
			"label": "Gaithersburg, United States - Montgomery Cty (GAI)",
			"value": "GAI",
			"domestic": 1
		}, {
			"label": "Yamagata, Japan - Junmachi (GAJ)",
			"value": "GAJ",
			"domestic": 0
		}, {
			"label": "Galena, United States - Galena (GAL)",
			"value": "GAL",
			"domestic": 1
		}, {
			"label": "Gambell, United States - Gambell Airport (GAM)",
			"value": "GAM",
			"domestic": 1
		}, {
			"label": "Gan Island, Republic of Maldives - Gan/Seenu Airport (GAN)",
			"value": "GAN",
			"domestic": 0
		}, {
			"label": "Guantanamo, Cuba - Los Canos Airport (GAO)",
			"value": "GAO",
			"domestic": 0
		}, {
			"label": "Guwahati, India - Borjhar (GAU)",
			"value": "GAU",
			"domestic": 0
		}, {
			"label": "Gamba, Gabon - Gamba Airport (GAX)",
			"value": "GAX",
			"domestic": 0
		}, {
			"label": "Gaya, India - Gaya Airport (GAY)",
			"value": "GAY",
			"domestic": 0
		}, {
			"label": "Great Bend, United States - Great Bend Municipal Airport (GBD)",
			"value": "GBD",
			"domestic": 1
		}, {
			"label": "Gaborone, Botswana - Sir Seretse Khama (GBE)",
			"value": "GBE",
			"domestic": 0
		}, {
			"label": "Galesburg, United States - Galesburg Mncpl (GBG)",
			"value": "GBG",
			"domestic": 1
		}, {
			"label": "Marie Galante, St. Barthelemy - Marie Galante (GBJ)",
			"value": "GBJ",
			"domestic": 0
		}, {
			"label": "Great Barrington, United States - Great Barrington (GBR)",
			"value": "GBR",
			"domestic": 1
		}, {
			"label": "Gorgon, Iran - Gorgon Airport (GBT)",
			"value": "GBT",
			"domestic": 0
		}, {
			"label": "Gillette, United States - Gillette-Campbell County Airport (GCC)",
			"value": "GCC",
			"domestic": 1
		}, {
			"label": "Guernsey, United Kingdom - Guernsey (GCI)",
			"value": "GCI",
			"domestic": 0
		}, {
			"label": "Garden City, United States - Garden City Regional Airport (GCK)",
			"value": "GCK",
			"domestic": 1
		}, {
			"label": "Grand Cayman Island, Cayman Islands - Owen Roberts International Airport (GCM)",
			"value": "GCM",
			"domestic": 0
		}, {
			"label": "Grand Canyon, United States - Grand Canyon (GCN)",
			"value": "GCN",
			"domestic": 1
		}, {
			"label": "Gode/Iddidole, Ethiopia - Gode/Iddidole Airport (GDE)",
			"value": "GDE",
			"domestic": 0
		}, {
			"label": "Guadalajara, Mexico - Miguel Hidal Airport (GDL)",
			"value": "GDL",
			"domestic": 0
		}, {
			"label": "Gdansk, Poland - Rebiechowo (GDN)",
			"value": "GDN",
			"domestic": 0
		}, {
			"label": "Guasdualito, Venezuela - Vare Maria Airport (GDO)",
			"value": "GDO",
			"domestic": 0
		}, {
			"label": "Gondar, Ethiopia - Gondar (GDQ)",
			"value": "GDQ",
			"domestic": 0
		}, {
			"label": "Grand Turk, Turks and Caicos - Grand Turk (GDT)",
			"value": "GDT",
			"domestic": 0
		}, {
			"label": "Glendive, United States - Dawson Community (GDV)",
			"value": "GDV",
			"domestic": 1
		}, {
			"label": "Magadan, Russia - Magadan Airport (GDX)",
			"value": "GDX",
			"domestic": 0
		}, {
			"label": "Noumea, New Caledonia - Magenta (GEA)",
			"value": "GEA",
			"domestic": 0
		}, {
			"label": "Nicosia-South, Cyprus - Gecitkale (GEC)",
			"value": "GEC",
			"domestic": 0
		}, {
			"label": "Georgetown, United States - Sussex County (GED)",
			"value": "GED",
			"domestic": 1
		}, {
			"label": "Spokane, United States - Spokane (GEG)",
			"value": "GEG",
			"domestic": 1
		}, {
			"label": "Georgetown, Guyana - Cheddi Jagan Intl (GEO)",
			"value": "GEO",
			"domestic": 0
		}, {
			"label": "Nueva Gerona, Cuba - Rafael Cabrera Airport (GER)",
			"value": "GER",
			"domestic": 0
		}, {
			"label": "General Santos, Philippines - Buayan Airport (GES)",
			"value": "GES",
			"domestic": 0
		}, {
			"label": "Geraldton, Australia - Geraldton (GET)",
			"value": "GET",
			"domestic": 0
		}, {
			"label": "Gallivare, Sweden - Gallivare (GEV)",
			"value": "GEV",
			"domestic": 0
		}, {
			"label": "Geelong, Australia - Geelong (GEX)",
			"value": "GEX",
			"domestic": 0
		}, {
			"label": "Griffith, Australia - Griffith (GFF)",
			"value": "GFF",
			"domestic": 0
		}, {
			"label": "Grand Forks, United States - Grand Forks Airport (GFK)",
			"value": "GFK",
			"domestic": 1
		}, {
			"label": "Glens Falls, United States - Warren Cty (GFL)",
			"value": "GFL",
			"domestic": 1
		}, {
			"label": "Grafton, Australia - Grafton (GFN)",
			"value": "GFN",
			"domestic": 0
		}, {
			"label": "Georgetown, United States - Georgetown County (GGE)",
			"value": "GGE",
			"domestic": 1
		}, {
			"label": "Longview, United States - Gregg County Airport (GGG)",
			"value": "GGG",
			"domestic": 1
		}, {
			"label": "Gobernador Gregores, Argentina - Gobernador Gregores Airport (GGS)",
			"value": "GGS",
			"domestic": 0
		}, {
			"label": "George Town, Bahamas - Exuma International Airport (GGT)",
			"value": "GGT",
			"domestic": 0
		}, {
			"label": "Glasgow, United States - Glasgow Intl (GGW)",
			"value": "GGW",
			"domestic": 1
		}, {
			"label": "Ghardaia, Algeria - Noumerate (GHA)",
			"value": "GHA",
			"domestic": 0
		}, {
			"label": "Governor\'s Harbour, Bahamas - Governor\'s Harbour (GHB)",
			"value": "GHB",
			"domestic": 0
		}, {
			"label": "Garachine, Panama - Garachine Airport (GHE)",
			"value": "GHE",
			"domestic": 0
		}, {
			"label": "Gibraltar, Gibraltar - North Front (GIB)",
			"value": "GIB",
			"domestic": 0
		}, {
			"label": "Boigu Island, Australia - Boigu Island Airport (GIC)",
			"value": "GIC",
			"domestic": 0
		}, {
			"label": "Winter Haven, United States - Gilbert Field (GIF)",
			"value": "GIF",
			"domestic": 1
		}, {
			"label": "Rio de Janeiro, Brazil - Internacional Airport (GIG)",
			"value": "GIG",
			"domestic": 0
		}, {
			"label": "Gilgit, Pakistan - Gilgit (GIL)",
			"value": "GIL",
			"domestic": 0
		}, {
			"label": "Gisborne, New Zealand - Gisborne (GIS)",
			"value": "GIS",
			"domestic": 0
		}, {
			"label": "Jazan, Saudi Arabia - Jazan Airport (GIZ)",
			"value": "GIZ",
			"domestic": 0
		}, {
			"label": "Guanaja Island, Honduras - Guanaja Airport (GJA)",
			"value": "GJA",
			"domestic": 0
		}, {
			"label": "Jijel, Algeria - Jijel Airport (GJL)",
			"value": "GJL",
			"domestic": 0
		}, {
			"label": "Grand Junction, United States - Walker Field (GJT)",
			"value": "GJT",
			"domestic": 1
		}, {
			"label": "Goroka, Papua New Guinea - Goroka (GKA)",
			"value": "GKA",
			"domestic": 0
		}, {
			"label": "Great Keppel Island, Australia - Great Keppel Is (GKL)",
			"value": "GKL",
			"domestic": 0
		}, {
			"label": "Gatlinburg, United States - Gatlinburg Mncpl (GKT)",
			"value": "GKT",
			"domestic": 1
		}, {
			"label": "Glasgow, United Kingdom - Glasgow International Airport (GLA)",
			"value": "GLA",
			"domestic": 0
		}, {
			"label": "Goodland, United States - Renner Fld (GLD)",
			"value": "GLD",
			"domestic": 1
		}, {
			"label": "Golfito, Costa Rica - Golfito (GLF)",
			"value": "GLF",
			"domestic": 0
		}, {
			"label": "Greenville, MS United States - Greenville Airport (GLH)",
			"value": "GLH",
			"domestic": 1
		}, {
			"label": "Glen Innes, Australia - Glen Innes (GLI)",
			"value": "GLI",
			"domestic": 0
		}, {
			"label": "Galcaio, Somalia - Galcaio Airport (GLK)",
			"value": "GLK",
			"domestic": 0
		}, {
			"label": "Gloucester, United Kingdom - Gloucestershire (GLO)",
			"value": "GLO",
			"domestic": 0
		}, {
			"label": "Gaylord, United States - Otsego County (GLR)",
			"value": "GLR",
			"domestic": 1
		}, {
			"label": "Galveston, United States - Scholes Fld (GLS)",
			"value": "GLS",
			"domestic": 1
		}, {
			"label": "Gladstone, Australia - Gladstone (GLT)",
			"value": "GLT",
			"domestic": 0
		}, {
			"label": "Golovin, United States - Golovin Airport (GLV)",
			"value": "GLV",
			"domestic": 1
		}, {
			"label": "Gemena, Democratic Republic of Congo - Gemena Airport (GMA)",
			"value": "GMA",
			"domestic": 0
		}, {
			"label": "Gambela, Ethiopia - Gambela Airport (GMB)",
			"value": "GMB",
			"domestic": 0
		}, {
			"label": "Gasmata Island, Papua New Guinea - Gasmata Island Airport (GMI)",
			"value": "GMI",
			"domestic": 0
		}, {
			"label": "Seoul, South Korea - Gimpo (GMP)",
			"value": "GMP",
			"domestic": 0
		}, {
			"label": "Gambier Island, French Polynesia and Tahiti - Gambier Island Airport (GMR)",
			"value": "GMR",
			"domestic": 0
		}, {
			"label": "San Sebastian La Gomera, Spain - La Gomera (GMZ)",
			"value": "GMZ",
			"domestic": 0
		}, {
			"label": "St. George\'s, Grenada - Point Saline International Airport (GND)",
			"value": "GND",
			"domestic": 0
		}, {
			"label": "Ghent, Belgium - Ghent (GNE)",
			"value": "GNE",
			"domestic": 0
		}, {
			"label": "Green Island, Taiwan - Green Island Airport (GNI)",
			"value": "GNI",
			"domestic": 0
		}, {
			"label": "Goodnews Bay, United States - Goodnews Bay Airport (GNU)",
			"value": "GNU",
			"domestic": 1
		}, {
			"label": "Gainesville, United States - J R Alison Municipal Airport (GNV)",
			"value": "GNV",
			"domestic": 1
		}, {
			"label": "Genoa, Italy - Cristoforo Colombo (GOA)",
			"value": "GOA",
			"domestic": 0
		}, {
			"label": "Nuuk, Greenland - Nuuk (GOH)",
			"value": "GOH",
			"domestic": 0
		}, {
			"label": "Goa, India - Dabolim (GOI)",
			"value": "GOI",
			"domestic": 0
		}, {
			"label": "Nizhniy Novgorod, Russia - Nizhniy Novgorod (GOJ)",
			"value": "GOJ",
			"domestic": 0
		}, {
			"label": "Gold Beach, United States - Gold Beach Mncpl (GOL)",
			"value": "GOL",
			"domestic": 1
		}, {
			"label": "Goma, Democratic Republic of Congo - Goma Airport (GOM)",
			"value": "GOM",
			"domestic": 0
		}, {
			"label": "New London, United States - Groton-New London (GON)",
			"value": "GON",
			"domestic": 1
		}, {
			"label": "Goondiwindi, Australia - Goondiwindi (GOO)",
			"value": "GOO",
			"domestic": 0
		}, {
			"label": "Gorakhpur, India - Gorakhpur Airport (GOP)",
			"value": "GOP",
			"domestic": 0
		}, {
			"label": "Golmud, China - Golmud Airport (GOQ)",
			"value": "GOQ",
			"domestic": 0
		}, {
			"label": "Gore, Ethiopia - Gore Airport (GOR)",
			"value": "GOR",
			"domestic": 0
		}, {
			"label": "Gothenburg, Sweden - Landvetter Airport (GOT)",
			"value": "GOT",
			"domestic": 0
		}, {
			"label": "Garoua, Cameroon - Garoua Airport (GOU)",
			"value": "GOU",
			"domestic": 0
		}, {
			"label": "Gove, Australia - Nhulunbuy Airport (GOV)",
			"value": "GOV",
			"domestic": 0
		}, {
			"label": "Patras, Greece - Patras-Araxos (GPA)",
			"value": "GPA",
			"domestic": 0
		}, {
			"label": "Guapi, Colombia - Guapi Airport (GPI)",
			"value": "GPI",
			"domestic": 0
		}, {
			"label": "Galapagos Islands, Ecuador - Baltra Airport (GPS)",
			"value": "GPS",
			"domestic": 0
		}, {
			"label": "Gulfport, United States - Gulfport/Biloxi Airport (GPT)",
			"value": "GPT",
			"domestic": 1
		}, {
			"label": "Green Bay, United States - Green Bay (GRB)",
			"value": "GRB",
			"domestic": 1
		}, {
			"label": "Greenwood, United States - County (GRD)",
			"value": "GRD",
			"domestic": 1
		}, {
			"label": "Grand Island, United States - Central Nebraska Regional Airport (GRI)",
			"value": "GRI",
			"domestic": 1
		}, {
			"label": "George, South Africa - George (GRJ)",
			"value": "GRJ",
			"domestic": 0
		}, {
			"label": "Killeen, United States - Killeen (GRK)",
			"value": "GRK",
			"domestic": 1
		}, {
			"label": "Grand Marais, United States - Grand Marais (GRM)",
			"value": "GRM",
			"domestic": 1
		}, {
			"label": "Gerona, Spain - Costa Brava (GRO)",
			"value": "GRO",
			"domestic": 0
		}, {
			"label": "Groningen, Netherlands - Eelde (GRQ)",
			"value": "GRQ",
			"domestic": 0
		}, {
			"label": "Grand Rapids, United States - Grand Rapids (GRR)",
			"value": "GRR",
			"domestic": 1
		}, {
			"label": "Grosseto, Italy - Baccarini (GRS)",
			"value": "GRS",
			"domestic": 0
		}, {
			"label": "Sao Paulo, Brazil - Guarulhos International Airport (GRU)",
			"value": "GRU",
			"domestic": 0
		}, {
			"label": "Graciosa Island (Azores), Portugal - Graciosa Island Airport (GRW)",
			"value": "GRW",
			"domestic": 0
		}, {
			"label": "Granada, Spain - Granada (GRX)",
			"value": "GRX",
			"domestic": 0
		}, {
			"label": "Grimsey, Iceland - Grimsey Airport (GRY)",
			"value": "GRY",
			"domestic": 0
		}, {
			"label": "Graz, Austria - Thalerhof (GRZ)",
			"value": "GRZ",
			"domestic": 0
		}, {
			"label": "Gothenburg, Sweden - Saeve Airport (GSE)",
			"value": "GSE",
			"domestic": 0
		}, {
			"label": "Goshen, United States - Goshen (GSH)",
			"value": "GSH",
			"domestic": 1
		}, {
			"label": "Gheshm, Iran - Gheshm Airport (GSM)",
			"value": "GSM",
			"domestic": 0
		}, {
			"label": "Greensboro, United States - Greensboro (GSO)",
			"value": "GSO",
			"domestic": 1
		}, {
			"label": "Greenville, SC, United States - Greenville-Spartanburg Airport (GSP)",
			"value": "GSP",
			"domestic": 1
		}, {
			"label": "Gustavus, United States - Gustavus Airport (GST)",
			"value": "GST",
			"domestic": 1
		}, {
			"label": "Gatokae, Solomon Islands - Gatokae Aerodrom (GTA)",
			"value": "GTA",
			"domestic": 0
		}, {
			"label": "Groote Island, Australia - Alyangula Airport (GTE)",
			"value": "GTE",
			"domestic": 0
		}, {
			"label": "Great Falls, United States - Great Falls International Airport (GTF)",
			"value": "GTF",
			"domestic": 1
		}, {
			"label": "Gorontalo, Indonesia - Tolotio Airport (GTO)",
			"value": "GTO",
			"domestic": 0
		}, {
			"label": "Columbus, MS, United States - Golden Triangle Regional Airport (GTR)",
			"value": "GTR",
			"domestic": 1
		}, {
			"label": "Zlin, Czech Republic - Holesov (GTW)",
			"value": "GTW",
			"domestic": 0
		}, {
			"label": "Guatemala City, Guatemala - La Aurora Airport (GUA)",
			"value": "GUA",
			"domestic": 0
		}, {
			"label": "Guerrero Negro, Mexico - Guerrero Negro (GUB)",
			"value": "GUB",
			"domestic": 0
		}, {
			"label": "Gunnison, United States - Gunnison Airport (GUC)",
			"value": "GUC",
			"domestic": 1
		}, {
			"label": "Gulf Shores, United States - Jack Edwards (GUF)",
			"value": "GUF",
			"domestic": 1
		}, {
			"label": "Gunnedah, Australia - Gunnedah (GUH)",
			"value": "GUH",
			"domestic": 0
		}, {
			"label": "Goulburn, Australia - Goulburn (GUL)",
			"value": "GUL",
			"domestic": 0
		}, {
			"label": "Agana, Guam - A.B. Won Pat Intl (GUM)",
			"value": "GUM",
			"domestic": 0
		}, {
			"label": "Gallup, United States - Senator Clark (GUP)",
			"value": "GUP",
			"domestic": 1
		}, {
			"label": "Alotau, Papua New Guinea - Gurney (GUR)",
			"value": "GUR",
			"domestic": 0
		}, {
			"label": "Atyrau, Kazakhstan - Atyrau Airport (GUW)",
			"value": "GUW",
			"domestic": 0
		}, {
			"label": "Geneva, Switzerland - Geneve-Cointrin Airport (GVA)",
			"value": "GVA",
			"domestic": 0
		}, {
			"label": "Green River, Papua New Guinea - Green River Airport (GVI)",
			"value": "GVI",
			"domestic": 0
		}, {
			"label": "Governador Valadares, Brazil - Governador Valadares Airport (GVR)",
			"value": "GVR",
			"domestic": 0
		}, {
			"label": "Gavle, Sweden - Sandviken (GVX)",
			"value": "GVX",
			"domestic": 0
		}, {
			"label": "Gwadar, Pakistan - Gwadar Airport (GWD)",
			"value": "GWD",
			"domestic": 0
		}, {
			"label": "Gweru, Zimbabwe - Gweru (GWE)",
			"value": "GWE",
			"domestic": 0
		}, {
			"label": "Gwalior, India - Gwalior (GWL)",
			"value": "GWL",
			"domestic": 0
		}, {
			"label": "Greenwood, United States - Leflore (GWO)",
			"value": "GWO",
			"domestic": 1
		}, {
			"label": "Westerland, Germany - Westerland-Sylt (GWT)",
			"value": "GWT",
			"domestic": 0
		}, {
			"label": "Galway, Ireland - Carnmore (GWY)",
			"value": "GWY",
			"domestic": 0
		}, {
			"label": "Seiyun, Yemen - Seiyun Airport (GXF)",
			"value": "GXF",
			"domestic": 0
		}, {
			"label": "Negage, Angola - Negage Airport (GXG)",
			"value": "GXG",
			"domestic": 0
		}, {
			"label": "Greeley, United States - Weld County (GXY)",
			"value": "GXY",
			"domestic": 1
		}, {
			"label": "Guayaramerin, Bolivia - Guayaramerin Airport (GYA)",
			"value": "GYA",
			"domestic": 0
		}, {
			"label": "Baku, Azerbaijan - Heydar Aliyev Intl (GYD)",
			"value": "GYD",
			"domestic": 0
		}, {
			"label": "Guayaquil, Ecuador - Simon Bolivar Airport (GYE)",
			"value": "GYE",
			"domestic": 0
		}, {
			"label": "Argyle, Australia - Argyle Airport (GYL)",
			"value": "GYL",
			"domestic": 0
		}, {
			"label": "Guaymas, Mexico - Guaymas (GYM)",
			"value": "GYM",
			"domestic": 0
		}, {
			"label": "Goiania, Brazil - Santa Genoveva (GYN)",
			"value": "GYN",
			"domestic": 0
		}, {
			"label": "Guang Yuan, China - Guang Yuan Airport (GYS)",
			"value": "GYS",
			"domestic": 0
		}, {
			"label": "Gary, United States - Gary Regl (GYY)",
			"value": "GYY",
			"domestic": 1
		}, {
			"label": "Victoria, Malta - Gozo_Heliport (GZM)",
			"value": "GZM",
			"domestic": 0
		}, {
			"label": "Gizo, Solomon Islands - Gizo Airport (GZO)",
			"value": "GZO",
			"domestic": 0
		}, {
			"label": "Gaziantep, Turkey - Gaziantep (GZT)",
			"value": "GZT",
			"domestic": 0
		}, {
			"label": "Hasvik, Norway - Hasvik Airport (HAA)",
			"value": "HAA",
			"domestic": 0
		}, {
			"label": "Hachijo Jima, Japan - Hachijo Jima Airport (HAC)",
			"value": "HAC",
			"domestic": 0
		}, {
			"label": "Halmstad, Sweden - Halmstad (HAD)",
			"value": "HAD",
			"domestic": 0
		}, {
			"label": "Moroni, Comoros - Prince Said Ibrahim In Airport (HAH)",
			"value": "HAH",
			"domestic": 0
		}, {
			"label": "Hanover, Germany - Hanover (HAJ)",
			"value": "HAJ",
			"domestic": 0
		}, {
			"label": "Haikou, China - Haikou (HAK)",
			"value": "HAK",
			"domestic": 0
		}, {
			"label": "Hamburg, Germany - Hamburg (HAM)",
			"value": "HAM",
			"domestic": 0
		}, {
			"label": "Hanoi, Vietnam - Noibai (HAN)",
			"value": "HAN",
			"domestic": 0
		}, {
			"label": "Hanimaadhoo, Republic of Maldives - Hanimaadhoo Airport (HAQ)",
			"value": "HAQ",
			"domestic": 0
		}, {
			"label": "Harrisburg, United States - Skyport (HAR)",
			"value": "HAR",
			"domestic": 1
		}, {
			"label": "Hail, Saudi Arabia - Hail Airport (HAS)",
			"value": "HAS",
			"domestic": 0
		}, {
			"label": "Haugesund, Norway - Haugesund (HAU)",
			"value": "HAU",
			"domestic": 0
		}, {
			"label": "Havana, Cuba - Jose Marti Intl (HAV)",
			"value": "HAV",
			"domestic": 0
		}, {
			"label": "Hobart, Australia - Hobart International (HBA)",
			"value": "HBA",
			"domestic": 0
		}, {
			"label": "Alexandria, United States - Borg El Arab (HBE)",
			"value": "HBE",
			"domestic": 1
		}, {
			"label": "Hubli, India - Hubli Airport (HBX)",
			"value": "HBX",
			"domestic": 0
		}, {
			"label": "Big Spring, United States - Big Spring Mncpl (HCA)",
			"value": "HCA",
			"domestic": 1
		}, {
			"label": "Hengchun, Taiwan - Hengchun (HCN)",
			"value": "HCN",
			"domestic": 0
		}, {
			"label": "Halls Creek, Australia - Halls Creek (HCQ)",
			"value": "HCQ",
			"domestic": 0
		}, {
			"label": "Holy Cross, United States - Holy Cross Airport (HCR)",
			"value": "HCR",
			"domestic": 1
		}, {
			"label": "Heidelberg, Germany - Heidelberg (HDB)",
			"value": "HDB",
			"domestic": 0
		}, {
			"label": "Heringsdorf, Germany - Heringsdorf (HDF)",
			"value": "HDF",
			"domestic": 0
		}, {
			"label": "Hamadan, Iran - Hamadan Airport (HDM)",
			"value": "HDM",
			"domestic": 0
		}, {
			"label": "Hayden, United States - Hayden (HDN)",
			"value": "HDN",
			"domestic": 1
		}, {
			"label": "Hoedspruit, South Africa - Hoedspruit (HDS)",
			"value": "HDS",
			"domestic": 0
		}, {
			"label": "Hat Yai, Thailand - Hat Yai (HDY)",
			"value": "HDY",
			"domestic": 0
		}, {
			"label": "Herat, Afghanistan - Herat Airport (HEA)",
			"value": "HEA",
			"domestic": 0
		}, {
			"label": "Heho, Myanmar (Burma) - Heho Airport (HEH)",
			"value": "HEH",
			"domestic": 0
		}, {
			"label": "Heide, Germany - Heide-Buesum Airport (HEI)",
			"value": "HEI",
			"domestic": 0
		}, {
			"label": "Heihe, China - Heihe Airport (HEK)",
			"value": "HEK",
			"domestic": 0
		}, {
			"label": "Helsinki, Finland - Helsinki-Vantaa Airport (HEL)",
			"value": "HEL",
			"domestic": 0
		}, {
			"label": "Heraklion, Greece - N. Kazantzakis (HER)",
			"value": "HER",
			"domestic": 0
		}, {
			"label": "Hermiston, United States - State (HES)",
			"value": "HES",
			"domestic": 1
		}, {
			"label": "Hohhot, China - Hohhot (HET)",
			"value": "HET",
			"domestic": 0
		}, {
			"label": "Natchez, United States - Hardy-Anders (HEZ)",
			"value": "HEZ",
			"domestic": 1
		}, {
			"label": "Haifa, Israel - Haifa (HFA)",
			"value": "HFA",
			"domestic": 0
		}, {
			"label": "Hartford, United States - Hartford-Brainard (HFD)",
			"value": "HFD",
			"domestic": 1
		}, {
			"label": "Hefei, China - Hefei (HFE)",
			"value": "HFE",
			"domestic": 0
		}, {
			"label": "Hofn, Iceland - Hornafjordur (HFN)",
			"value": "HFN",
			"domestic": 0
		}, {
			"label": "Hagfors, Sweden - Hagfors Airport (HFS)",
			"value": "HFS",
			"domestic": 0
		}, {
			"label": "Hammerfest, Norway - Hammerfest (HFT)",
			"value": "HFT",
			"domestic": 0
		}, {
			"label": "Hargeisa, Somalia - Hargeisa Airport (HGA)",
			"value": "HGA",
			"domestic": 0
		}, {
			"label": "Hughenden, Australia - Hughenden Airport (HGD)",
			"value": "HGD",
			"domestic": 0
		}, {
			"label": "Hangzhou, China - Hangzhou (HGH)",
			"value": "HGH",
			"domestic": 0
		}, {
			"label": "Helgoland, Germany - Helgoland (HGL)",
			"value": "HGL",
			"domestic": 0
		}, {
			"label": "Mae Hong Son, Thailand - Mae Hong Son (HGN)",
			"value": "HGN",
			"domestic": 0
		}, {
			"label": "Hagerstown, United States - Wash. County Regional Airport (HGR)",
			"value": "HGR",
			"domestic": 1
		}, {
			"label": "Mount Hagen, Papua New Guinea - Kagamuga (HGU)",
			"value": "HGU",
			"domestic": 0
		}, {
			"label": "Huanghua, China - Changsha Huanghua Airport (HHA)",
			"value": "HHA",
			"domestic": 0
		}, {
			"label": "Hilton Head, United States - Hilton Head (HHH)",
			"value": "HHH",
			"domestic": 1
		}, {
			"label": "Frankfurt, Germany - Hahn (HHN)",
			"value": "HHN",
			"domestic": 0
		}, {
			"label": "Hua Hin, Thailand - Hua Hin (HHQ)",
			"value": "HHQ",
			"domestic": 0
		}, {
			"label": "Hibbing, United States - Hibbing-Chisholm (HIB)",
			"value": "HIB",
			"domestic": 1
		}, {
			"label": "Horn Island, Australia - Horn Island Airport (HID)",
			"value": "HID",
			"domestic": 0
		}, {
			"label": "Lake Havasu City, United States - Lake Havasu City (HII)",
			"value": "HII",
			"domestic": 1
		}, {
			"label": "Hiroshima, Japan - Hiroshima (HIJ)",
			"value": "HIJ",
			"domestic": 0
		}, {
			"label": "Shillavo, Ethiopia - Shillavo Airport (HIL)",
			"value": "HIL",
			"domestic": 0
		}, {
			"label": "Jinju, South Korea - Sacheon Airport (HIN)",
			"value": "HIN",
			"domestic": 0
		}, {
			"label": "Hillsboro, United States - Hillsboro (HIO)",
			"value": "HIO",
			"domestic": 1
		}, {
			"label": "Honiara, Solomon Islands - Henderson Intl (HIR)",
			"value": "HIR",
			"domestic": 0
		}, {
			"label": "Hayman Island, Australia - Hayman Island Airport (HIS)",
			"value": "HIS",
			"domestic": 0
		}, {
			"label": "Zhi Jiang, China - Zhi Jiang Airport (HJJ)",
			"value": "HJJ",
			"domestic": 0
		}, {
			"label": "Khajuraho, India - Khajuraho (HJR)",
			"value": "HJR",
			"domestic": 0
		}, {
			"label": "Healy Lake, United States - Healy Lake Airport (HKB)",
			"value": "HKB",
			"domestic": 1
		}, {
			"label": "Hakodate, Japan - Hakodate (HKD)",
			"value": "HKD",
			"domestic": 0
		}, {
			"label": "Hong Kong, China - Hong Kong International Airport (HKG)",
			"value": "HKG",
			"domestic": 0
		}, {
			"label": "Hokitika, New Zealand - Hokitika (HKK)",
			"value": "HKK",
			"domestic": 0
		}, {
			"label": "Hoskins, Papua New Guinea - Hoskins Airport (HKN)",
			"value": "HKN",
			"domestic": 0
		}, {
			"label": "Phuket, Thailand - Phuket Intl (HKT)",
			"value": "HKT",
			"domestic": 0
		}, {
			"label": "Hickory, United States - Hickory Airport (HKY)",
			"value": "HKY",
			"domestic": 1
		}, {
			"label": "Lanseria, South Africa - Lanseria Airport (HLA)",
			"value": "HLA",
			"domestic": 0
		}, {
			"label": "Hailar, China - Hailar Airport (HLD)",
			"value": "HLD",
			"domestic": 0
		}, {
			"label": "Hultsfred, Sweden - Hultsfred (HLF)",
			"value": "HLF",
			"domestic": 0
		}, {
			"label": "Ulanhot, China - Ulanhot Airport (HLH)",
			"value": "HLH",
			"domestic": 0
		}, {
			"label": "Holland, United States - Park Township (HLM)",
			"value": "HLM",
			"domestic": 1
		}, {
			"label": "Helena, United States - Helena Airport (HLN)",
			"value": "HLN",
			"domestic": 1
		}, {
			"label": "Jakarta, Indonesia - Halim Perdana Kusuma (HLP)",
			"value": "HLP",
			"domestic": 0
		}, {
			"label": "Hamilton, Australia - Hamilton (HLT)",
			"value": "HLT",
			"domestic": 0
		}, {
			"label": "Hluhluwe, South Africa - Hluhluwe (HLW)",
			"value": "HLW",
			"domestic": 0
		}, {
			"label": "Hamilton, New Zealand - Hamilton (HLZ)",
			"value": "HLZ",
			"domestic": 0
		}, {
			"label": "Khanty-Mansiysk, Russia - Khanty-Mansiysk (HMA)",
			"value": "HMA",
			"domestic": 0
		}, {
			"label": "Hassi Messaoud, Algeria - Oued Irara Airport (HME)",
			"value": "HME",
			"domestic": 0
		}, {
			"label": "Hermosillo, Mexico - Gen Pesqueira Garcia Airport (HMO)",
			"value": "HMO",
			"domestic": 0
		}, {
			"label": "Hamar, Norway - Hamar (HMR)",
			"value": "HMR",
			"domestic": 0
		}, {
			"label": "Hemet, United States - Hemet-Ryan (HMT)",
			"value": "HMT",
			"domestic": 1
		}, {
			"label": "Hemavan, Sweden - Hemavan (HMV)",
			"value": "HMV",
			"domestic": 0
		}, {
			"label": "Hanamaki, Japan - Hanamaki (HNA)",
			"value": "HNA",
			"domestic": 0
		}, {
			"label": "Tokyo, Japan - Haneda Airport (HND)",
			"value": "HND",
			"domestic": 0
		}, {
			"label": "Hoonah, United States - Hoonah Airport (HNH)",
			"value": "HNH",
			"domestic": 1
		}, {
			"label": "Honolulu, United States - Honolulu (HNL)",
			"value": "HNL",
			"domestic": 1
		}, {
			"label": "Hana, United States - Hana (HNM)",
			"value": "HNM",
			"domestic": 1
		}, {
			"label": "Haines, United States - Haines (HNS)",
			"value": "HNS",
			"domestic": 1
		}, {
			"label": "Hobbs, United States - Lea Cty (HOB)",
			"value": "HOB",
			"domestic": 1
		}, {
			"label": "Hodeidah, Yemen - Hodeidah Airport (HOD)",
			"value": "HOD",
			"domestic": 0
		}, {
			"label": "Houeisay, Laos - Houeisay Airport (HOE)",
			"value": "HOE",
			"domestic": 0
		}, {
			"label": "Alahsa, Saudi Arabia - Alahsa (HOF)",
			"value": "HOF",
			"domestic": 0
		}, {
			"label": "Holguin, Cuba - Frank Pais (HOG)",
			"value": "HOG",
			"domestic": 0
		}, {
			"label": "Hao Island, French Polynesia and Tahiti - Hao Island Airport (HOI)",
			"value": "HOI",
			"domestic": 0
		}, {
			"label": "Homer, United States - Homer (HOM)",
			"value": "HOM",
			"domestic": 1
		}, {
			"label": "Huron, United States - Howes (HON)",
			"value": "HON",
			"domestic": 1
		}, {
			"label": "Hof, Germany - Hof-Pirk (HOQ)",
			"value": "HOQ",
			"domestic": 0
		}, {
			"label": "Horta, Portugal - Horta (HOR)",
			"value": "HOR",
			"domestic": 0
		}, {
			"label": "Hot Springs, United States - Memorial Fld (HOT)",
			"value": "HOT",
			"domestic": 1
		}, {
			"label": "Houston, United States - Hobby (HOU)",
			"value": "HOU",
			"domestic": 1
		}, {
			"label": "Orsta, Norway - Hovden (HOV)",
			"value": "HOV",
			"domestic": 0
		}, {
			"label": "Ha\'Apai, Tonga - Salote Pilolevu Airport (HPA)",
			"value": "HPA",
			"domestic": 0
		}, {
			"label": "Hooper Bay, United States - Hooper Bay Airport (HPB)",
			"value": "HPB",
			"domestic": 1
		}, {
			"label": "Haiphong, Vietnam - Haiphong (HPH)",
			"value": "HPH",
			"domestic": 0
		}, {
			"label": "White Plains, United States - Westchester County (HPN)",
			"value": "HPN",
			"domestic": 1
		}, {
			"label": "Lihue, United States - Princeville (HPV)",
			"value": "HPV",
			"domestic": 1
		}, {
			"label": "Hoquiam, United States - Bowerman (HQM)",
			"value": "HQM",
			"domestic": 1
		}, {
			"label": "Harbin, China - Harbin (HRB)",
			"value": "HRB",
			"domestic": 0
		}, {
			"label": "Harare, Zimbabwe - Harare (HRE)",
			"value": "HRE",
			"domestic": 0
		}, {
			"label": "Hurghada, Egypt - Hurghada (HRG)",
			"value": "HRG",
			"domestic": 0
		}, {
			"label": "Kharkov, Ukraine - Kharkov (HRK)",
			"value": "HRK",
			"domestic": 0
		}, {
			"label": "Harlingen, United States - Harlingen (HRL)",
			"value": "HRL",
			"domestic": 1
		}, {
			"label": "Harrison, United States - Boone Cty (HRO)",
			"value": "HRO",
			"domestic": 1
		}, {
			"label": "Shaoguan, China - Shaoguan (HSC)",
			"value": "HSC",
			"domestic": 0
		}, {
			"label": "Saga, Japan - Saga Airport (HSG)",
			"value": "HSG",
			"domestic": 0
		}, {
			"label": "Hastings, United States - Hastings Mncpl (HSI)",
			"value": "HSI",
			"domestic": 1
		}, {
			"label": "Huslia, United States - Huslia Airport (HSL)",
			"value": "HSL",
			"domestic": 1
		}, {
			"label": "Horsham, Australia - Horsham (HSM)",
			"value": "HSM",
			"domestic": 0
		}, {
			"label": "Zhoushan, China - Zhoushan (HSN)",
			"value": "HSN",
			"domestic": 0
		}, {
			"label": "Hot Springs, United States - Ingalls Field (HSP)",
			"value": "HSP",
			"domestic": 1
		}, {
			"label": "Homestead, United States - Homestead Mncpl (HST)",
			"value": "HST",
			"domestic": 1
		}, {
			"label": "Huntsville, United States - Madison County Airport (HSV)",
			"value": "HSV",
			"domestic": 1
		}, {
			"label": "Hsinchu, Taiwan - Hsinchu (HSZ)",
			"value": "HSZ",
			"domestic": 0
		}, {
			"label": "Chita, Russia - Chita Airport (HTA)",
			"value": "HTA",
			"domestic": 0
		}, {
			"label": "Hatanga, Russia - Hatanga Airport (HTG)",
			"value": "HTG",
			"domestic": 0
		}, {
			"label": "Hamilton Island, Australia - Hamilton Is (HTI)",
			"value": "HTI",
			"domestic": 0
		}, {
			"label": "Hotan, China - Hotan Airport (HTN)",
			"value": "HTN",
			"domestic": 0
		}, {
			"label": "East Hampton, United States - East Hampton (HTO)",
			"value": "HTO",
			"domestic": 1
		}, {
			"label": "Huntington, United States - Tri-State/Milton Airport (HTS)",
			"value": "HTS",
			"domestic": 1
		}, {
			"label": "Humboldt, United States - Humboldt (HUD)",
			"value": "HUD",
			"domestic": 1
		}, {
			"label": "Terre Haute, United States - Hulman Fld (HUF)",
			"value": "HUF",
			"domestic": 1
		}, {
			"label": "Huahine, French Polynesia and Tahiti - Huahine (HUH)",
			"value": "HUH",
			"domestic": 0
		}, {
			"label": "Hue, Vietnam - Hue (HUI)",
			"value": "HUI",
			"domestic": 0
		}, {
			"label": "Houlton, United States - Houlton Intl (HUL)",
			"value": "HUL",
			"domestic": 1
		}, {
			"label": "Houma, United States - Terrebonne (HUM)",
			"value": "HUM",
			"domestic": 1
		}, {
			"label": "Hualien, Taiwan - Hualien (HUN)",
			"value": "HUN",
			"domestic": 0
		}, {
			"label": "Hughes, United States - Hughes Municipal Airport (HUS)",
			"value": "HUS",
			"domestic": 1
		}, {
			"label": "Hutchinson, United States - Hutchinson Mncpl (HUT)",
			"value": "HUT",
			"domestic": 1
		}, {
			"label": "Hudiksvall, Sweden - Hudiksvall (HUV)",
			"value": "HUV",
			"domestic": 0
		}, {
			"label": "Huatulco, Mexico - Huatulco Airport (HUX)",
			"value": "HUX",
			"domestic": 0
		}, {
			"label": "Humberside, United Kingdom - Humberside Airport (HUY)",
			"value": "HUY",
			"domestic": 0
		}, {
			"label": "Huizhou, China - Huizhou (HUZ)",
			"value": "HUZ",
			"domestic": 0
		}, {
			"label": "Hervey Bay, Australia - Hervey Bay (HVB)",
			"value": "HVB",
			"domestic": 0
		}, {
			"label": "Khovd, Mongolia - Khovd Airport (HVD)",
			"value": "HVD",
			"domestic": 0
		}, {
			"label": "Honningsvag, Norway - Valan (HVG)",
			"value": "HVG",
			"domestic": 0
		}, {
			"label": "New Haven, United States - New Haven Airport (HVN)",
			"value": "HVN",
			"domestic": 1
		}, {
			"label": "Havre, United States - City Cty (HVR)",
			"value": "HVR",
			"domestic": 1
		}, {
			"label": "Hay, Australia - Hay (HXX)",
			"value": "HXX",
			"domestic": 0
		}, {
			"label": "Hyannis, United States - Barnstable Airport (HYA)",
			"value": "HYA",
			"domestic": 1
		}, {
			"label": "Hyderabad, India - Begumpet (HYD)",
			"value": "HYD",
			"domestic": 0
		}, {
			"label": "Hayfields, Papua New Guinea - Hayfields Airport (HYF)",
			"value": "HYF",
			"domestic": 0
		}, {
			"label": "Hydaburg, United States - Hydaburg Sea Plane Base (HYG)",
			"value": "HYG",
			"domestic": 1
		}, {
			"label": "Hollis, United States - Hollis Sea Plane Base (HYL)",
			"value": "HYL",
			"domestic": 1
		}, {
			"label": "Huangyan, China - Huangyan Airport (HYN)",
			"value": "HYN",
			"domestic": 0
		}, {
			"label": "Hayward, United States - Hayward Mncpl (HYR)",
			"value": "HYR",
			"domestic": 1
		}, {
			"label": "Hays, United States - Hays Regional Airport (HYS)",
			"value": "HYS",
			"domestic": 1
		}, {
			"label": "Hanzhong, China - Hanzhong Airport (HZG)",
			"value": "HZG",
			"domestic": 0
		}, {
			"label": "Husavik, Iceland - Husavik (HZK)",
			"value": "HZK",
			"domestic": 0
		}, {
			"label": "Igarka, Russia - Igarka Airport (IAA)",
			"value": "IAA",
			"domestic": 0
		}, {
			"label": "Washington, D.C., United States - Dulles (IAD)",
			"value": "IAD",
			"domestic": 1
		}, {
			"label": "Niagara Falls, United States - Niagara Falls Intl (IAG)",
			"value": "IAG",
			"domestic": 1
		}, {
			"label": "Houston, United States - Bush (IAH)",
			"value": "IAH",
			"domestic": 1
		}, {
			"label": "In Amenas, Algeria - In Amenas Airport (IAM)",
			"value": "IAM",
			"domestic": 0
		}, {
			"label": "Kiana, United States - Bob Barker Memorial Airport (IAN)",
			"value": "IAN",
			"domestic": 1
		}, {
			"label": "Iasi, Romania - Iasi (IAS)",
			"value": "IAS",
			"domestic": 0
		}, {
			"label": "Ibadan, Nigeria - Ibadan (IBA)",
			"value": "IBA",
			"domestic": 0
		}, {
			"label": "Ibague, Colombia - Ibague (IBE)",
			"value": "IBE",
			"domestic": 0
		}, {
			"label": "Indigo Bay Lodge, Mozambique - Indigo Bay Lodge (IBL)",
			"value": "IBL",
			"domestic": 0
		}, {
			"label": "Ibiza, Spain - Ibiza (IBZ)",
			"value": "IBZ",
			"domestic": 0
		}, {
			"label": "Cicia, Fiji - Cicia Airport (ICI)",
			"value": "ICI",
			"domestic": 0
		}, {
			"label": "Seoul, South Korea - Incheon (ICN)",
			"value": "ICN",
			"domestic": 0
		}, {
			"label": "Wichita, United States - Wichita (ICT)",
			"value": "ICT",
			"domestic": 1
		}, {
			"label": "Idaho Falls, United States - Fanning Field (IDA)",
			"value": "IDA",
			"domestic": 1
		}, {
			"label": "Santa Isabel do Morro, Brazil - Santa Isabel do Morro Airport (IDO)",
			"value": "IDO",
			"domestic": 0
		}, {
			"label": "Indore, India - Indore (IDR)",
			"value": "IDR",
			"domestic": 0
		}, {
			"label": "Zielona Gora, Poland - Babimost Airport (IEG)",
			"value": "IEG",
			"domestic": 0
		}, {
			"label": "Kiev, Ukraine - Zhulhany (IEV)",
			"value": "IEV",
			"domestic": 0
		}, {
			"label": "Isafjordur, Iceland - Isafjordur (IFJ)",
			"value": "IFJ",
			"domestic": 0
		}, {
			"label": "Isfahan, Iran - Isfahan (IFN)",
			"value": "IFN",
			"domestic": 0
		}, {
			"label": "Ivano-Frankovsk, Ukraine - Ivano-Frankovsk Airport (IFO)",
			"value": "IFO",
			"domestic": 0
		}, {
			"label": "Bullhead City, United States - Laughlin/Bullhead (IFP)",
			"value": "IFP",
			"domestic": 1
		}, {
			"label": "Inagua, Bahamas - Inagua Airport (IGA)",
			"value": "IGA",
			"domestic": 0
		}, {
			"label": "Igiugig, United States - Igiugig Airport (IGG)",
			"value": "IGG",
			"domestic": 1
		}, {
			"label": "Kingman, United States - Kingman Airport (IGM)",
			"value": "IGM",
			"domestic": 1
		}, {
			"label": "Iguazu, Argentina - Iguazu Intl (IGR)",
			"value": "IGR",
			"domestic": 0
		}, {
			"label": "Iguassu Falls, Brazil - Cataratas (IGU)",
			"value": "IGU",
			"domestic": 0
		}, {
			"label": "Ilaam, Iran - Ilaam Airport (IIL)",
			"value": "IIL",
			"domestic": 0
		}, {
			"label": "Nissan Island, Papua New Guinea - Nissan Island Airport (IIS)",
			"value": "IIS",
			"domestic": 0
		}, {
			"label": "Tehran, Iran - Imam Khomeini International Airport (IKA)",
			"value": "IKA",
			"domestic": 0
		}, {
			"label": "Wilkesboro, United States - Wilkes County (IKB)",
			"value": "IKB",
			"domestic": 1
		}, {
			"label": "Nikolski, United States - Nikolski Air Force Station (IKO)",
			"value": "IKO",
			"domestic": 1
		}, {
			"label": "Irkutsk, Russia - Irkutsk (IKT)",
			"value": "IKT",
			"domestic": 0
		}, {
			"label": "Killeen, United States - Killeen Municipal Airport (ILE)",
			"value": "ILE",
			"domestic": 1
		}, {
			"label": "Wilmington, United States - Greater Wilmington Airport (ILG)",
			"value": "ILG",
			"domestic": 1
		}, {
			"label": "Iliamna, United States - Iliamna Airport (ILI)",
			"value": "ILI",
			"domestic": 1
		}, {
			"label": "Willmar, United States - Willmar (ILL)",
			"value": "ILL",
			"domestic": 1
		}, {
			"label": "Wilmington, United States - New Hanover County Airport (ILM)",
			"value": "ILM",
			"domestic": 1
		}, {
			"label": "Wilmington, United States - Clinton Field (ILN)",
			"value": "ILN",
			"domestic": 1
		}, {
			"label": "Iloilo, Philippines - Mandurriao (ILO)",
			"value": "ILO",
			"domestic": 0
		}, {
			"label": "Ile des Pins, New Caledonia - Ile des Pins (ILP)",
			"value": "ILP",
			"domestic": 0
		}, {
			"label": "Islay, United Kingdom - Glenegedale Airport (ILY)",
			"value": "ILY",
			"domestic": 0
		}, {
			"label": "Zilina, Slovakia - Zilina Airport (ILZ)",
			"value": "ILZ",
			"domestic": 0
		}, {
			"label": "Imphal, India - Imphal Municipal Airport (IMF)",
			"value": "IMF",
			"domestic": 0
		}, {
			"label": "Simikot, Nepal - Simikot Airport (IMK)",
			"value": "IMK",
			"domestic": 0
		}, {
			"label": "Imperatriz, Brazil - Imperatriz Airport (IMP)",
			"value": "IMP",
			"domestic": 0
		}, {
			"label": "Iron Mountain, United States - Ford Airport (IMT)",
			"value": "IMT",
			"domestic": 1
		}, {
			"label": "Yinchuan, China - Yinchuan (INC)",
			"value": "INC",
			"domestic": 0
		}, {
			"label": "Indianapolis, United States - Indianapolis (IND)",
			"value": "IND",
			"domestic": 1
		}, {
			"label": "Inhambane, Mozambique - Inhambane Airport (INH)",
			"value": "INH",
			"domestic": 0
		}, {
			"label": "Nis, Serbia - Nis Airport (INI)",
			"value": "INI",
			"domestic": 0
		}, {
			"label": "International Falls, United States - International Falls (INL)",
			"value": "INL",
			"domestic": 1
		}, {
			"label": "Innsbruck, Austria - Kranebitten (INN)",
			"value": "INN",
			"domestic": 0
		}, {
			"label": "Winston-Salem, United States - Smith Reynolds (INT)",
			"value": "INT",
			"domestic": 1
		}, {
			"label": "Nauru Island, Nauru - Nauru Island International Airport (INU)",
			"value": "INU",
			"domestic": 0
		}, {
			"label": "Inverness, United Kingdom - Inverness (INV)",
			"value": "INV",
			"domestic": 0
		}, {
			"label": "Winslow, United States - Winslow Mncpl (INW)",
			"value": "INW",
			"domestic": 1
		}, {
			"label": "In Salah, Algeria - In Salah Airport (INZ)",
			"value": "INZ",
			"domestic": 0
		}, {
			"label": "Ioannina, Greece - Ioannina (IOA)",
			"value": "IOA",
			"domestic": 0
		}, {
			"label": "Isle of Man, United Kingdom - Ronaldsway (IOM)",
			"value": "IOM",
			"domestic": 0
		}, {
			"label": "Ilheus, Brazil - Ilheus Eduardo Gomes (IOS)",
			"value": "IOS",
			"domestic": 0
		}, {
			"label": "Iowa City, United States - Iowa City Mncpl (IOW)",
			"value": "IOW",
			"domestic": 1
		}, {
			"label": "Ipota, Vanuatu - Ipota Airport (IPA)",
			"value": "IPA",
			"domestic": 0
		}, {
			"label": "Easter Island, Chile and Easter Island - Mataveri International Airport (IPC)",
			"value": "IPC",
			"domestic": 0
		}, {
			"label": "Ipoh, Malaysia - Ipoh (IPH)",
			"value": "IPH",
			"domestic": 0
		}, {
			"label": "Ipiales, Colombia - San Luis Airport (IPI)",
			"value": "IPI",
			"domestic": 0
		}, {
			"label": "Imperial, United States - Imperial County Airport (IPL)",
			"value": "IPL",
			"domestic": 1
		}, {
			"label": "Ipatinga, Brazil - Usiminas Airport (IPN)",
			"value": "IPN",
			"domestic": 0
		}, {
			"label": "Williamsport, United States - Williamsport (IPT)",
			"value": "IPT",
			"domestic": 1
		}, {
			"label": "Qiemo, China - Qiemo Airport (IQM)",
			"value": "IQM",
			"domestic": 0
		}, {
			"label": "Qingyang, China - Qingyang Airport (IQN)",
			"value": "IQN",
			"domestic": 0
		}, {
			"label": "Iquique, Chile and Easter Island - Cavancha (IQQ)",
			"value": "IQQ",
			"domestic": 0
		}, {
			"label": "Iquitos, Peru - C.F. Secada (IQT)",
			"value": "IQT",
			"domestic": 0
		}, {
			"label": "Kirakira, Solomon Islands - Kirakira Airport (IRA)",
			"value": "IRA",
			"domestic": 0
		}, {
			"label": "Circle, United States - Circle City Airport (IRC)",
			"value": "IRC",
			"domestic": 1
		}, {
			"label": "Lockhart River, Australia - Lockhart River Airport (IRG)",
			"value": "IRG",
			"domestic": 0
		}, {
			"label": "La Rioja, Argentina - La Rioja Mncpl (IRJ)",
			"value": "IRJ",
			"domestic": 0
		}, {
			"label": "Kirksville, United States - Kirksville Mnpl (IRK)",
			"value": "IRK",
			"domestic": 1
		}, {
			"label": "Isiro, Democratic Republic of Congo - Matari (IRP)",
			"value": "IRP",
			"domestic": 0
		}, {
			"label": "Mount Isa, Australia - Mt Isa (ISA)",
			"value": "ISA",
			"domestic": 0
		}, {
			"label": "Islamabad/Rawalpindi, Pakistan - Islamabad Intl (ISB)",
			"value": "ISB",
			"domestic": 0
		}, {
			"label": "Isles of Scilly, United Kingdom - St Marys (ISC)",
			"value": "ISC",
			"domestic": 0
		}, {
			"label": "Ishigaki, Japan - Ishigaki (ISG)",
			"value": "ISG",
			"domestic": 0
		}, {
			"label": "Isla Mujeres, Mexico - Isla Mujeres Mncpl (ISJ)",
			"value": "ISJ",
			"domestic": 0
		}, {
			"label": "Kissimmee, United States - Kissimmee Mncpl (ISM)",
			"value": "ISM",
			"domestic": 1
		}, {
			"label": "Williston, United States - Sloulin Field International Airport (ISN)",
			"value": "ISN",
			"domestic": 1
		}, {
			"label": "Kinston, United States - Kinston (ISO)",
			"value": "ISO",
			"domestic": 1
		}, {
			"label": "Islip, United States - Long Island MacArthur (ISP)",
			"value": "ISP",
			"domestic": 1
		}, {
			"label": "Wiscasset, United States - Wiscasset Mncpl (ISS)",
			"value": "ISS",
			"domestic": 1
		}, {
			"label": "Istanbul, Turkey - Ataturk Airport (IST)",
			"value": "IST",
			"domestic": 0
		}, {
			"label": "Sulaymaniyah, Iraq - Sulaymaniyah International Airport (ISU)",
			"value": "ISU",
			"domestic": 0
		}, {
			"label": "Wisconsin Rapids, United States - Alexander Fld (ISW)",
			"value": "ISW",
			"domestic": 1
		}, {
			"label": "Itaituba, Brazil - Itaituba Airport (ITB)",
			"value": "ITB",
			"domestic": 0
		}, {
			"label": "Ithaca, United States - Tompkins County Airport (ITH)",
			"value": "ITH",
			"domestic": 1
		}, {
			"label": "Osaka, Japan - Itami (ITM)",
			"value": "ITM",
			"domestic": 0
		}, {
			"label": "Hilo, United States - Hilo International Airport (ITO)",
			"value": "ITO",
			"domestic": 1
		}, {
			"label": "Niue Island, Niue - Hanan Airport (IUE)",
			"value": "IUE",
			"domestic": 0
		}, {
			"label": "Invercargill, New Zealand - Invercargill (IVC)",
			"value": "IVC",
			"domestic": 0
		}, {
			"label": "Ivalo, Finland - Ivalo (IVL)",
			"value": "IVL",
			"domestic": 0
		}, {
			"label": "Inverell, Australia - Inverell (IVR)",
			"value": "IVR",
			"domestic": 0
		}, {
			"label": "Ironwood, United States - Gogebic and Iron Country Airport (IWD)",
			"value": "IWD",
			"domestic": 1
		}, {
			"label": "Iwami, Japan - Iwami Airport (IWJ)",
			"value": "IWJ",
			"domestic": 0
		}, {
			"label": "Agartala, India - Singerbhil Airport (IXA)",
			"value": "IXA",
			"domestic": 0
		}, {
			"label": "Bagdogra, India - Bagdogra Airport (IXB)",
			"value": "IXB",
			"domestic": 0
		}, {
			"label": "Chandigarh, India - Chandigarh (IXC)",
			"value": "IXC",
			"domestic": 0
		}, {
			"label": "Allahabad, India - Bamrauli (IXD)",
			"value": "IXD",
			"domestic": 0
		}, {
			"label": "Mangalore, India - Bajpe (IXE)",
			"value": "IXE",
			"domestic": 0
		}, {
			"label": "Belgaum, India - Sambre Airport (IXG)",
			"value": "IXG",
			"domestic": 0
		}, {
			"label": "Lilabari, India - Lilabari Airport (IXI)",
			"value": "IXI",
			"domestic": 0
		}, {
			"label": "Jammu, India - Satwari (IXJ)",
			"value": "IXJ",
			"domestic": 0
		}, {
			"label": "Leh, India - Leh Airport (IXL)",
			"value": "IXL",
			"domestic": 0
		}, {
			"label": "Madurai, India - Madurai (IXM)",
			"value": "IXM",
			"domestic": 0
		}, {
			"label": "Pathankot, India - Pathankot Airport (IXP)",
			"value": "IXP",
			"domestic": 0
		}, {
			"label": "Ranchi, India - Ranchi (IXR)",
			"value": "IXR",
			"domestic": 0
		}, {
			"label": "Silchar, India - Kumbhirgram Airport (IXS)",
			"value": "IXS",
			"domestic": 0
		}, {
			"label": "Aurangabad, India - Chikkalthana (IXU)",
			"value": "IXU",
			"domestic": 0
		}, {
			"label": "Kandla, India - Kandla Airport (IXY)",
			"value": "IXY",
			"domestic": 0
		}, {
			"label": "Port Blair, India - Port Blair (IXZ)",
			"value": "IXZ",
			"domestic": 0
		}, {
			"label": "Inyokern, United States - Kern County Airport (IYK)",
			"value": "IYK",
			"domestic": 1
		}, {
			"label": "Izmir, Turkey - All Airports (IZM)",
			"value": "IZM",
			"domestic": 0
		}, {
			"label": "Izumo, Japan - Izumo Airport (IZO)",
			"value": "IZO",
			"domestic": 0
		}, {
			"label": "Jalalabad, Afghanistan - Jalalabad Airport (JAA)",
			"value": "JAA",
			"domestic": 0
		}, {
			"label": "Jabiru, Australia - Jabiru (JAB)",
			"value": "JAB",
			"domestic": 0
		}, {
			"label": "Jackson, WY, United States - Jackson Hole (JAC)",
			"value": "JAC",
			"domestic": 1
		}, {
			"label": "Jacobabad, Pakistan - Jacobabad Airport (JAG)",
			"value": "JAG",
			"domestic": 0
		}, {
			"label": "Jaipur, India - Sanganeer (JAI)",
			"value": "JAI",
			"domestic": 0
		}, {
			"label": "Jalapa, Mexico - Jalapa Mncpl (JAL)",
			"value": "JAL",
			"domestic": 0
		}, {
			"label": "Jackson, MS, United States - Thomson Field (JAN)",
			"value": "JAN",
			"domestic": 1
		}, {
			"label": "Jacquinot Bay, Papua New Guinea - Jacquinot Bay Airport (JAQ)",
			"value": "JAQ",
			"domestic": 0
		}, {
			"label": "Jabot, Marshall Islands - Jabot Airport (JAT)",
			"value": "JAT",
			"domestic": 0
		}, {
			"label": "Ilulissat, Greenland - Ilulissat (JAV)",
			"value": "JAV",
			"domestic": 0
		}, {
			"label": "Jacksonville, United States - Jacksonville (JAX)",
			"value": "JAX",
			"domestic": 1
		}, {
			"label": "Jonesboro, United States - Jonesboro Mncpl (JBR)",
			"value": "JBR",
			"domestic": 1
		}, {
			"label": "Qasigiannguit, Greenland - Qasigiannguit Airport (JCH)",
			"value": "JCH",
			"domestic": 0
		}, {
			"label": "Julia Creek, Australia - Julia Creek Airport (JCK)",
			"value": "JCK",
			"domestic": 0
		}, {
			"label": "Ceuta, Spain - Ceuta Heliport (JCU)",
			"value": "JCU",
			"domestic": 0
		}, {
			"label": "John Day, United States - John Day (JDA)",
			"value": "JDA",
			"domestic": 1
		}, {
			"label": "Juiz de Fora, Brazil - Francisco De Assis Airport (JDF)",
			"value": "JDF",
			"domestic": 0
		}, {
			"label": "Jodhpur, India - Jodhpur (JDH)",
			"value": "JDH",
			"domestic": 0
		}, {
			"label": "Juazeiro Do Norte, Brazil - Regional Do Cariri Airport (JDO)",
			"value": "JDO",
			"domestic": 0
		}, {
			"label": "Jingdezhen, China - Jingdezhen Airport (JDZ)",
			"value": "JDZ",
			"domestic": 0
		}, {
			"label": "Jeddah, Saudi Arabia - King Abdulaziz Intl (JED)",
			"value": "JED",
			"domestic": 0
		}, {
			"label": "Jefferson City, United States - Jefferson City Mem (JEF)",
			"value": "JEF",
			"domestic": 1
		}, {
			"label": "Aasiaat, Greenland - Aasiaat (JEG)",
			"value": "JEG",
			"domestic": 0
		}, {
			"label": "Jeh, Marshall Islands - Jeh Airport (JEJ)",
			"value": "JEJ",
			"domestic": 0
		}, {
			"label": "Jersey, United Kingdom - Jersey (JER)",
			"value": "JER",
			"domestic": 0
		}, {
			"label": "New York City, United States - John F Kennedy International (JFK)",
			"value": "JFK",
			"domestic": 1
		}, {
			"label": "Paamiut, Greenland - Paamiut Airport (JFR)",
			"value": "JFR",
			"domestic": 0
		}, {
			"label": "Jamnagar, India - Govardhanpur (JGA)",
			"value": "JGA",
			"domestic": 0
		}, {
			"label": "Jiayuguan, China - Jiayuguan Airport (JGN)",
			"value": "JGN",
			"domestic": 0
		}, {
			"label": "Qeqertarsuaq, Greenland - Qeqertarsuaq Airport (JGO)",
			"value": "JGO",
			"domestic": 0
		}, {
			"label": "Gronnedal, Greenland - Gronnedal Heliport (JGR)",
			"value": "JGR",
			"domestic": 0
		}, {
			"label": "Ji\'An, China - Ji An/Jing Gang Shan Airport (JGS)",
			"value": "JGS",
			"domestic": 0
		}, {
			"label": "Johor Bahru, Malaysia - Sultan Ismail Intl (JHB)",
			"value": "JHB",
			"domestic": 0
		}, {
			"label": "Jinghong, China - Gasa Airport (JHG)",
			"value": "JHG",
			"domestic": 0
		}, {
			"label": "Kapalua, United States - Kapalua West Maui (JHM)",
			"value": "JHM",
			"domestic": 1
		}, {
			"label": "Sisimiut, Greenland - Sisimiut (JHS)",
			"value": "JHS",
			"domestic": 0
		}, {
			"label": "Jamestown, NY, United States - Chautauqua Cty (JHW)",
			"value": "JHW",
			"domestic": 1
		}, {
			"label": "Cambridge, United States - Hyatt Regency-H/P (JHY)",
			"value": "JHY",
			"domestic": 1
		}, {
			"label": "Djibouti, Djibouti - Ambouli (JIB)",
			"value": "JIB",
			"domestic": 0
		}, {
			"label": "Jijiga, Ethiopia - Jigiga Airport (JIJ)",
			"value": "JIJ",
			"domestic": 0
		}, {
			"label": "Ikaria Island, Greece - Ikaria Airport (JIK)",
			"value": "JIK",
			"domestic": 0
		}, {
			"label": "Jilin, China - Jilin (JIL)",
			"value": "JIL",
			"domestic": 0
		}, {
			"label": "Jimma, Ethiopia - Jimma Airport (JIM)",
			"value": "JIM",
			"domestic": 0
		}, {
			"label": "Jiujiang, China - Jiujiang (JIU)",
			"value": "JIU",
			"domestic": 0
		}, {
			"label": "Jinjiang, China - Jinjiang Airport (JJN)",
			"value": "JJN",
			"domestic": 0
		}, {
			"label": "Qaqortoq, Greenland - Qaqortoq Heliport (JJU)",
			"value": "JJU",
			"domestic": 0
		}, {
			"label": "Jonkoping, Sweden - Axamo (JKG)",
			"value": "JKG",
			"domestic": 0
		}, {
			"label": "Chios, Greece - Chios (JKH)",
			"value": "JKH",
			"domestic": 0
		}, {
			"label": "Kalymnos Island, Greece - Kalymnos Airport (JKL)",
			"value": "JKL",
			"domestic": 0
		}, {
			"label": "Jakarta, Indonesia - All Airports (JKT)",
			"value": "JKT",
			"domestic": 0
		}, {
			"label": "Joplin, United States - Joplin Airport (JLN)",
			"value": "JLN",
			"domestic": 1
		}, {
			"label": "Jabalpur, India - Jabalpur Airport (JLR)",
			"value": "JLR",
			"domestic": 0
		}, {
			"label": "Mykonos, Greece - Mikonos (JMK)",
			"value": "JMK",
			"domestic": 0
		}, {
			"label": "Jamestown, MD, United States - Jamestown Mncpl (JMS)",
			"value": "JMS",
			"domestic": 1
		}, {
			"label": "Jiamusi, China - Jiamusi Airport (JMU)",
			"value": "JMU",
			"domestic": 0
		}, {
			"label": "Johannesburg, South Africa - Johannesburg Intl (JNB)",
			"value": "JNB",
			"domestic": 0
		}, {
			"label": "Nanortalik, Greenland - Nanortalik Airport (JNN)",
			"value": "JNN",
			"domestic": 0
		}, {
			"label": "Narsaq, Greenland - Narsaq H/P (JNS)",
			"value": "JNS",
			"domestic": 0
		}, {
			"label": "Juneau, United States - Juneau (JNU)",
			"value": "JNU",
			"domestic": 1
		}, {
			"label": "Naxos, Greece - Naxos (JNX)",
			"value": "JNX",
			"domestic": 0
		}, {
			"label": "Jinzhou, China - Jinzhou (JNZ)",
			"value": "JNZ",
			"domestic": 0
		}, {
			"label": "Joensuu, Finland - Joensuu (JOE)",
			"value": "JOE",
			"domestic": 0
		}, {
			"label": "Yogyakarta, Indonesia - Adisutjipto (JOG)",
			"value": "JOG",
			"domestic": 0
		}, {
			"label": "Joinville, Brazil - Cubatao (JOI)",
			"value": "JOI",
			"domestic": 0
		}, {
			"label": "Jolo, Philippines - Jolo Airport (JOL)",
			"value": "JOL",
			"domestic": 0
		}, {
			"label": "Joliet, United States - Joliet Mncpl (JOT)",
			"value": "JOT",
			"domestic": 1
		}, {
			"label": "Joao Pessoa, Brazil - Castro Pinto (JPA)",
			"value": "JPA",
			"domestic": 0
		}, {
			"label": "Ji-Parana, Brazil - Ji-Parana Airport (JPR)",
			"value": "JPR",
			"domestic": 0
		}, {
			"label": "Qaarsut, Greenland - Qaarsut Airport (JQA)",
			"value": "JQA",
			"domestic": 0
		}, {
			"label": "Jaque, Panama - Jaque Airport (JQE)",
			"value": "JQE",
			"domestic": 0
		}, {
			"label": "Jorhat, India - Rowriah Airport (JRH)",
			"value": "JRH",
			"domestic": 0
		}, {
			"label": "Kilimanjaro, Tanzania - Kilimanjaro (JRO)",
			"value": "JRO",
			"domestic": 0
		}, {
			"label": "Jerusalem, Israel - Jerusalem (JRS)",
			"value": "JRS",
			"domestic": 0
		}, {
			"label": "Jaisalmer, India - Jaisalmer (JSA)",
			"value": "JSA",
			"domestic": 0
		}, {
			"label": "Sitia, Greece - Sitia (JSH)",
			"value": "JSH",
			"domestic": 0
		}, {
			"label": "Skiathos, Greece - Skiathos (JSI)",
			"value": "JSI",
			"domestic": 0
		}, {
			"label": "Jessore, Bangladesh - Jessore Airport (JSR)",
			"value": "JSR",
			"domestic": 0
		}, {
			"label": "Johnstown, United States - Cambria Cty (JST)",
			"value": "JST",
			"domestic": 1
		}, {
			"label": "Maniitsoq, Greenland - Maniitsoq (JSU)",
			"value": "JSU",
			"domestic": 0
		}, {
			"label": "Syros Island, Greece - Syros Island Airport (JSY)",
			"value": "JSY",
			"domestic": 0
		}, {
			"label": "Thira, Greece - Santorini (JTR)",
			"value": "JTR",
			"domestic": 0
		}, {
			"label": "Astypalaia Island, Greece - Astypalaia Airport (JTY)",
			"value": "JTY",
			"domestic": 0
		}, {
			"label": "Juba, Sudan - Juba Airport (JUB)",
			"value": "JUB",
			"domestic": 0
		}, {
			"label": "Juist, Germany - Juist (JUI)",
			"value": "JUI",
			"domestic": 0
		}, {
			"label": "Jujuy, Argentina - El Cadillal (JUJ)",
			"value": "JUJ",
			"domestic": 0
		}, {
			"label": "Juliaca, Peru - Juliaca (JUL)",
			"value": "JUL",
			"domestic": 0
		}, {
			"label": "Jumla, Nepal - Jumla Airport (JUM)",
			"value": "JUM",
			"domestic": 0
		}, {
			"label": "Upernavik, Greenland - Upernavik Heliport (JUV)",
			"value": "JUV",
			"domestic": 0
		}, {
			"label": "Zhejiang, China - Juzhou (JUZ)",
			"value": "JUZ",
			"domestic": 0
		}, {
			"label": "Ankavandra, Madagascar - Ankavandra Airport (JVA)",
			"value": "JVA",
			"domestic": 0
		}, {
			"label": "Janesville, United States - Rock Cty (JVL)",
			"value": "JVL",
			"domestic": 1
		}, {
			"label": "Jackson, United States - Reynolds Mnpl (JXN)",
			"value": "JXN",
			"domestic": 1
		}, {
			"label": "Jiroft, Iran - Jiroft Airport (JYR)",
			"value": "JYR",
			"domestic": 0
		}, {
			"label": "Jyvaskyla, Finland - Jyvaskyla (JYV)",
			"value": "JYV",
			"domestic": 0
		}, {
			"label": "Song Pan, China - Jiu Zhai Huang Long Airport (JZH)",
			"value": "JZH",
			"domestic": 0
		}, {
			"label": "Kariba, Zimbabwe - Kariba (KAB)",
			"value": "KAB",
			"domestic": 0
		}, {
			"label": "Kameshli, Syria - Kameshli Airport (KAC)",
			"value": "KAC",
			"domestic": 0
		}, {
			"label": "Kaduna, Nigeria - Kaduna Airport (KAD)",
			"value": "KAD",
			"domestic": 0
		}, {
			"label": "Kake, United States - Kake Sea Plane Base (KAE)",
			"value": "KAE",
			"domestic": 1
		}, {
			"label": "Kajaani, Finland - Kajaani (KAJ)",
			"value": "KAJ",
			"domestic": 0
		}, {
			"label": "Kaltag, United States - Kaltag Airport (KAL)",
			"value": "KAL",
			"domestic": 1
		}, {
			"label": "Kano, Nigeria - Aminu Kano International Airport (KAN)",
			"value": "KAN",
			"domestic": 0
		}, {
			"label": "Kuusamo, Finland - Kuusamo (KAO)",
			"value": "KAO",
			"domestic": 0
		}, {
			"label": "Kaitaia, New Zealand - Kaitaia (KAT)",
			"value": "KAT",
			"domestic": 0
		}, {
			"label": "Kawthaung, Myanmar (Burma) - Kawthaung Airport (KAW)",
			"value": "KAW",
			"domestic": 0
		}, {
			"label": "Kalbarri, Australia - Kalbarri (KAX)",
			"value": "KAX",
			"domestic": 0
		}, {
			"label": "Birch Creek, United States - Birch Creek Airport (KBC)",
			"value": "KBC",
			"domestic": 1
		}, {
			"label": "Kings Canyon, Australia - Kings Canyon (KBJ)",
			"value": "KBJ",
			"domestic": 0
		}, {
			"label": "Kabul, Afghanistan - Khwaja Rawash (KBL)",
			"value": "KBL",
			"domestic": 0
		}, {
			"label": "Kiev, Ukraine - Borispol (KBP)",
			"value": "KBP",
			"domestic": 0
		}, {
			"label": "Kota Bharu, Malaysia - Pengkalan Chepa (KBR)",
			"value": "KBR",
			"domestic": 0
		}, {
			"label": "Kaben, Marshall Islands - Kaben Airport (KBT)",
			"value": "KBT",
			"domestic": 0
		}, {
			"label": "Kotabaru, Indonesia - Kotabaru Airport (KBU)",
			"value": "KBU",
			"domestic": 0
		}, {
			"label": "Krabi, Thailand - Krabi (KBV)",
			"value": "KBV",
			"domestic": 0
		}, {
			"label": "Kaikoura, New Zealand - Kaikoura (KBZ)",
			"value": "KBZ",
			"domestic": 0
		}, {
			"label": "Kuqa, China - Kuqa Airport (KCA)",
			"value": "KCA",
			"domestic": 0
		}, {
			"label": "Coffman Cove, United States - Coffman Cove Sea Plane Base (KCC)",
			"value": "KCC",
			"domestic": 1
		}, {
			"label": "Chignik, United States - Fisheries Airport (KCG)",
			"value": "KCG",
			"domestic": 1
		}, {
			"label": "Kuching, Malaysia - Kuching (KCH)",
			"value": "KCH",
			"domestic": 0
		}, {
			"label": "Chignik, United States - Lagoon Airport (KCL)",
			"value": "KCL",
			"domestic": 1
		}, {
			"label": "Kahramanmaras, Turkey - Kahramanmaras Airport (KCM)",
			"value": "KCM",
			"domestic": 0
		}, {
			"label": "Chignik, United States - Chignik Lake Airport (KCQ)",
			"value": "KCQ",
			"domestic": 1
		}, {
			"label": "Kochi, Japan - Kochi (KCZ)",
			"value": "KCZ",
			"domestic": 0
		}, {
			"label": "Kendari, Indonesia - Wolter Monginsidi Airport (KDI)",
			"value": "KDI",
			"domestic": 0
		}, {
			"label": "Kardla, Estonia - Kardla Airport (KDL)",
			"value": "KDL",
			"domestic": 0
		}, {
			"label": "Kaadedhdhoo, Republic of Maldives - Kaadedhdhoo Airport (KDM)",
			"value": "KDM",
			"domestic": 0
		}, {
			"label": "Kadhdhoo, Republic of Maldives - Kadhdhoo Airport (KDO)",
			"value": "KDO",
			"domestic": 0
		}, {
			"label": "Skardu, Pakistan - Skardu (KDU)",
			"value": "KDU",
			"domestic": 0
		}, {
			"label": "Kandavu, Fiji - Kandavu Airport (KDV)",
			"value": "KDV",
			"domestic": 0
		}, {
			"label": "Reykjavik, Iceland - Keflavik International Airport (KEF)",
			"value": "KEF",
			"domestic": 0
		}, {
			"label": "Kemerovo, Russia - Kemerovo (KEJ)",
			"value": "KEJ",
			"domestic": 0
		}, {
			"label": "Ekwok, United States - Ekwok Airport (KEK)",
			"value": "KEK",
			"domestic": 1
		}, {
			"label": "Kiel, Germany - Holtenau (KEL)",
			"value": "KEL",
			"domestic": 0
		}, {
			"label": "Kemi, Finland - Kemi (KEM)",
			"value": "KEM",
			"domestic": 0
		}, {
			"label": "Nepalganj, Nepal - Nepalganj Airport (KEP)",
			"value": "KEP",
			"domestic": 0
		}, {
			"label": "Kerman, Iran - Kerman Airport (KER)",
			"value": "KER",
			"domestic": 0
		}, {
			"label": "Keng Tung, Myanmar (Burma) - Keng Tung Airport (KET)",
			"value": "KET",
			"domestic": 0
		}, {
			"label": "Keewaywin, Canada - Keewaywin Airport (KEW)",
			"value": "KEW",
			"domestic": 0
		}, {
			"label": "Kananga, Democratic Republic of Congo - Kananga Airport (KGA)",
			"value": "KGA",
			"domestic": 0
		}, {
			"label": "Kingscote, Australia - Kingscote (KGC)",
			"value": "KGC",
			"domestic": 0
		}, {
			"label": "Kaliningrad, Russia - Khrabrovo (KGD)",
			"value": "KGD",
			"domestic": 0
		}, {
			"label": "Kagau, Solomon Islands - Kagau Airport (KGE)",
			"value": "KGE",
			"domestic": 0
		}, {
			"label": "Karaganda, Kazakhstan - Karaganda Airport (KGF)",
			"value": "KGF",
			"domestic": 0
		}, {
			"label": "Kalgoorlie, Australia - Kalgoorlie (KGI)",
			"value": "KGI",
			"domestic": 0
		}, {
			"label": "New Koliganek, United States - New Koliganek Airport (KGK)",
			"value": "KGK",
			"domestic": 1
		}, {
			"label": "Kigali, Rwanda - Gregoire Kayibanda (KGL)",
			"value": "KGL",
			"domestic": 0
		}, {
			"label": "Kogalym, Russia - Kogalym International Airport (KGP)",
			"value": "KGP",
			"domestic": 0
		}, {
			"label": "Kos, Greece - Kos (KGS)",
			"value": "KGS",
			"domestic": 0
		}, {
			"label": "Grayling, United States - Grayling Airport (KGX)",
			"value": "KGX",
			"domestic": 1
		}, {
			"label": "Khorramabad, Iran - Khorramabad Airport (KHD)",
			"value": "KHD",
			"domestic": 0
		}, {
			"label": "Kherson, Ukraine - Kherson (KHE)",
			"value": "KHE",
			"domestic": 0
		}, {
			"label": "Kashi, China - Kashi Airport (KHG)",
			"value": "KHG",
			"domestic": 0
		}, {
			"label": "Kaohsiung, Taiwan - Kaohsiung Intl (KHH)",
			"value": "KHH",
			"domestic": 0
		}, {
			"label": "Karachi, Pakistan - Quaid-E-Azam Intl (KHI)",
			"value": "KHI",
			"domestic": 0
		}, {
			"label": "Khamtis, Myanmar (Burma) - Khamtis Airport (KHM)",
			"value": "KHM",
			"domestic": 0
		}, {
			"label": "Nanchang, China - Nanchang (KHN)",
			"value": "KHN",
			"domestic": 0
		}, {
			"label": "Khasab, Oman - Khasab (KHS)",
			"value": "KHS",
			"domestic": 0
		}, {
			"label": "Khabarovsk, Russia - Novyy (KHV)",
			"value": "KHV",
			"domestic": 0
		}, {
			"label": "Khoy, Iran - Khoy Airport (KHY)",
			"value": "KHY",
			"domestic": 0
		}, {
			"label": "Kristianstad, Sweden - Kristianstad (KID)",
			"value": "KID",
			"domestic": 0
		}, {
			"label": "Kingfisher Lake, Canada - Kingfisher Lake Airport (KIF)",
			"value": "KIF",
			"domestic": 0
		}, {
			"label": "Kish Island, Iran - Kish Island Airport (KIH)",
			"value": "KIH",
			"domestic": 0
		}, {
			"label": "Niigata, Japan - Niigata (KIJ)",
			"value": "KIJ",
			"domestic": 0
		}, {
			"label": "Kimberley, South Africa - Kimberley (KIM)",
			"value": "KIM",
			"domestic": 0
		}, {
			"label": "Kingston, Jamaica - Norman Manley Airport (KIN)",
			"value": "KIN",
			"domestic": 0
		}, {
			"label": "Kili Island, Marshall Islands - Kili Island Airport (KIO)",
			"value": "KIO",
			"domestic": 0
		}, {
			"label": "Killarney, Ireland - Kerry Cty (KIR)",
			"value": "KIR",
			"domestic": 0
		}, {
			"label": "Kisumu, Kenya - Kisumu (KIS)",
			"value": "KIS",
			"domestic": 0
		}, {
			"label": "Kithira, Greece - Kithira Airport (KIT)",
			"value": "KIT",
			"domestic": 0
		}, {
			"label": "Chisinau, Moldova - Chisinau (KIV)",
			"value": "KIV",
			"domestic": 0
		}, {
			"label": "Kitwe, Zambia - Southdowns Airport (KIW)",
			"value": "KIW",
			"domestic": 0
		}, {
			"label": "Osaka, Japan - Kansai International Airport (KIX)",
			"value": "KIX",
			"domestic": 0
		}, {
			"label": "Krasnojarsk, Russia - Krasnojarsk (KJA)",
			"value": "KJA",
			"domestic": 0
		}, {
			"label": "Koyuk, United States - Koyuk Airport (KKA)",
			"value": "KKA",
			"domestic": 1
		}, {
			"label": "Kitui Bay, United States - Kitui Bay Sea Plane Base (KKB)",
			"value": "KKB",
			"domestic": 1
		}, {
			"label": "Khon Kaen, Thailand - Khon Kaen (KKC)",
			"value": "KKC",
			"domestic": 0
		}, {
			"label": "Kokoda, Papua New Guinea - Kokoda Airport (KKD)",
			"value": "KKD",
			"domestic": 0
		}, {
			"label": "Kerikeri, New Zealand - Kerikeri (KKE)",
			"value": "KKE",
			"domestic": 0
		}, {
			"label": "Kongiganak, United States - Kongiganak Airport (KKH)",
			"value": "KKH",
			"domestic": 1
		}, {
			"label": "Akiachak, United States - Akiachak Sea Plane Base (KKI)",
			"value": "KKI",
			"domestic": 1
		}, {
			"label": "Kitakyushu, Japan - Kita Kyushu (KKJ)",
			"value": "KKJ",
			"domestic": 0
		}, {
			"label": "Kirkenes, Norway - Hoeybuktmoen (KKN)",
			"value": "KKN",
			"domestic": 0
		}, {
			"label": "Kaukura Atoll, French Polynesia and Tahiti - Kaukura Atoll Airport (KKR)",
			"value": "KKR",
			"domestic": 0
		}, {
			"label": "Ekuk, United States - Ekuk Airport (KKU)",
			"value": "KKU",
			"domestic": 1
		}, {
			"label": "Kikaiga Shima, Japan - Kikaiga Shima Airport (KKX)",
			"value": "KKX",
			"domestic": 0
		}, {
			"label": "Kalskag, United States - Kalskag Municipal Airport (KLG)",
			"value": "KLG",
			"domestic": 1
		}, {
			"label": "Kolhapur, India - Kolhapur Airport (KLH)",
			"value": "KLH",
			"domestic": 0
		}, {
			"label": "Levelock, United States - Levelock Airport (KLL)",
			"value": "KLL",
			"domestic": 1
		}, {
			"label": "Kalibo, Philippines - Kalibo (KLO)",
			"value": "KLO",
			"domestic": 0
		}, {
			"label": "Kalmar, Sweden - Kalmar (KLR)",
			"value": "KLR",
			"domestic": 0
		}, {
			"label": "Klagenfurt, Austria - Klagenfurt (KLU)",
			"value": "KLU",
			"domestic": 0
		}, {
			"label": "Karlovy Vary, Czech Republic - Karlovy Vary (KLV)",
			"value": "KLV",
			"domestic": 0
		}, {
			"label": "Klawock, United States - Klawock Airport (KLW)",
			"value": "KLW",
			"domestic": 1
		}, {
			"label": "Kalamata, Greece - Kalamata (KLX)",
			"value": "KLX",
			"domestic": 0
		}, {
			"label": "Kerema, Papua New Guinea - Kerema Airport (KMA)",
			"value": "KMA",
			"domestic": 0
		}, {
			"label": "King Khalid Military City, Saudi Arabia - King Khalid Military City Airport (KMC)",
			"value": "KMC",
			"domestic": 0
		}, {
			"label": "Kunming, China - Kunming (KMG)",
			"value": "KMG",
			"domestic": 0
		}, {
			"label": "Miyazaki, Japan - Miyazaki (KMI)",
			"value": "KMI",
			"domestic": 0
		}, {
			"label": "Kumamoto, Japan - Kumamoto (KMJ)",
			"value": "KMJ",
			"domestic": 0
		}, {
			"label": "Manokotak, United States - Manokotak Sea Plane Base (KMO)",
			"value": "KMO",
			"domestic": 1
		}, {
			"label": "Keetmanshoop, Namibia - JGH Van Der Wath (KMP)",
			"value": "KMP",
			"domestic": 0
		}, {
			"label": "Komatsu, Japan - Komatsu (KMQ)",
			"value": "KMQ",
			"domestic": 0
		}, {
			"label": "Kumasi, Ghana - Kumasi Airport (KMS)",
			"value": "KMS",
			"domestic": 0
		}, {
			"label": "Kalemyo, Myanmar (Burma) - Kalemyo Airport (KMV)",
			"value": "KMV",
			"domestic": 0
		}, {
			"label": "Moser Bay, United States - Moser Bay Airport (KMY)",
			"value": "KMY",
			"domestic": 1
		}, {
			"label": "Vina del Mar, Chile and Easter Island - Vina Del Mar (KNA)",
			"value": "KNA",
			"domestic": 0
		}, {
			"label": "Kanab, United States - Kanab (KNB)",
			"value": "KNB",
			"domestic": 1
		}, {
			"label": "Kindu, Democratic Republic of Congo - Kindu Airport (KND)",
			"value": "KND",
			"domestic": 0
		}, {
			"label": "Kinmen, Taiwan - Shang-Yi Airport (KNH)",
			"value": "KNH",
			"domestic": 0
		}, {
			"label": "Kone, New Caledonia - Kone Airport (KNQ)",
			"value": "KNQ",
			"domestic": 0
		}, {
			"label": "King Island, Australia - King Island Airport (KNS)",
			"value": "KNS",
			"domestic": 0
		}, {
			"label": "Kanpur, India - Kanpur (KNU)",
			"value": "KNU",
			"domestic": 0
		}, {
			"label": "New Stuyahok, United States - New Stuyahok Airport (KNW)",
			"value": "KNW",
			"domestic": 1
		}, {
			"label": "Kununurra, Australia - Kununurra (KNX)",
			"value": "KNX",
			"domestic": 0
		}, {
			"label": "Kona, United States - Keahole Airport (KOA)",
			"value": "KOA",
			"domestic": 1
		}, {
			"label": "Koumac, New Caledonia - Koumac Airport (KOC)",
			"value": "KOC",
			"domestic": 0
		}, {
			"label": "Kupang, Indonesia - Eltari Airport (KOE)",
			"value": "KOE",
			"domestic": 0
		}, {
			"label": "Kirkwall, United Kingdom - Kirkwall (KOI)",
			"value": "KOI",
			"domestic": 0
		}, {
			"label": "Kagoshima, Japan - Kagoshima (KOJ)",
			"value": "KOJ",
			"domestic": 0
		}, {
			"label": "Kokkola, Finland - Kruunupyy (KOK)",
			"value": "KOK",
			"domestic": 0
		}, {
			"label": "Nakhon Phanom, Thailand - Nakhon Phanom Airport (KOP)",
			"value": "KOP",
			"domestic": 0
		}, {
			"label": "Sihanoukville, Cambodia - Sihanoukville Airport (KOS)",
			"value": "KOS",
			"domestic": 0
		}, {
			"label": "Kotlik, United States - Kotlik Airport (KOT)",
			"value": "KOT",
			"domestic": 1
		}, {
			"label": "Koulamoutou, Gabon - Koulamoutou Airport (KOU)",
			"value": "KOU",
			"domestic": 0
		}, {
			"label": "Ganzhou, China - Ganzhou Airport (KOW)",
			"value": "KOW",
			"domestic": 0
		}, {
			"label": "Olga Bay, United States - Olga Bay Sea Plane Base (KOY)",
			"value": "KOY",
			"domestic": 1
		}, {
			"label": "Ouzinkie, United States - Ouzinkie Sea Plane Base (KOZ)",
			"value": "KOZ",
			"domestic": 1
		}, {
			"label": "Point Baker, United States - Point Baker Sea Plane Base (KPB)",
			"value": "KPB",
			"domestic": 1
		}, {
			"label": "Port Clarence, United States - Port Clarence Airport (KPC)",
			"value": "KPC",
			"domestic": 1
		}, {
			"label": "Kipnuk, United States - Kipnuk Sea Plane Base (KPN)",
			"value": "KPN",
			"domestic": 1
		}, {
			"label": "P\'ohang, South Korea - Pohang (KPO)",
			"value": "KPO",
			"domestic": 0
		}, {
			"label": "Port Williams, United States - Port Williams Sea Plane Base (KPR)",
			"value": "KPR",
			"domestic": 1
		}, {
			"label": "Kempsey, Australia - Kempsey (KPS)",
			"value": "KPS",
			"domestic": 0
		}, {
			"label": "Perryville, United States - Perryville Sea Plane Base (KPV)",
			"value": "KPV",
			"domestic": 1
		}, {
			"label": "Port Bailey, United States - Port Bailey Sea Plane Base (KPY)",
			"value": "KPY",
			"domestic": 1
		}, {
			"label": "Akutan, United States - Akutan Airport (KQA)",
			"value": "KQA",
			"domestic": 1
		}, {
			"label": "Karumba, Australia - Karumba Airport (KRB)",
			"value": "KRB",
			"domestic": 0
		}, {
			"label": "Kramfors, Sweden - Kramfors (KRF)",
			"value": "KRF",
			"domestic": 0
		}, {
			"label": "Kikori, Papua New Guinea - Kikori Airport (KRI)",
			"value": "KRI",
			"domestic": 0
		}, {
			"label": "Krakow, Poland - John Paul II Balice (KRK)",
			"value": "KRK",
			"domestic": 0
		}, {
			"label": "Korla, China - Korla Airport (KRL)",
			"value": "KRL",
			"domestic": 0
		}, {
			"label": "Kiruna, Sweden - Kiruna (KRN)",
			"value": "KRN",
			"domestic": 0
		}, {
			"label": "Kurgan, Russia - Kurgan Airport (KRO)",
			"value": "KRO",
			"domestic": 0
		}, {
			"label": "Karup, Denmark - Karup Airport (KRP)",
			"value": "KRP",
			"domestic": 0
		}, {
			"label": "Krasnodar, Russia - Krasnodar (KRR)",
			"value": "KRR",
			"domestic": 0
		}, {
			"label": "Kristiansand, Norway - Kjevik (KRS)",
			"value": "KRS",
			"domestic": 0
		}, {
			"label": "Khartoum, Sudan - Civil (KRT)",
			"value": "KRT",
			"domestic": 0
		}, {
			"label": "Karamay, China - Karamay (KRY)",
			"value": "KRY",
			"domestic": 0
		}, {
			"label": "Kosrae, Caroline Islands, Micronesia - Kosrae Airport (KSA)",
			"value": "KSA",
			"domestic": 0
		}, {
			"label": "Kosice, Slovakia - Barca (KSC)",
			"value": "KSC",
			"domestic": 0
		}, {
			"label": "Karlstad, Sweden - Karlstad (KSD)",
			"value": "KSD",
			"domestic": 0
		}, {
			"label": "Kassel, Germany - Kassel (KSF)",
			"value": "KSF",
			"domestic": 0
		}, {
			"label": "Kermanshah, Iran - Kermanshah Airport (KSH)",
			"value": "KSH",
			"domestic": 0
		}, {
			"label": "Kasos Island, Greece - Kasos Island Airport (KSJ)",
			"value": "KSJ",
			"domestic": 0
		}, {
			"label": "Saint Mary\'s, United States - Saint Mary\'s Airport (KSM)",
			"value": "KSM",
			"domestic": 1
		}, {
			"label": "Kostanay, Kazakhstan - Kostanay Airport (KSN)",
			"value": "KSN",
			"domestic": 0
		}, {
			"label": "Kastoria, Greece - Aristoteles (KSO)",
			"value": "KSO",
			"domestic": 0
		}, {
			"label": "Karshi, Uzbekistan - Karshi Airport (KSQ)",
			"value": "KSQ",
			"domestic": 0
		}, {
			"label": "Kristiansund, Norway - Kvernberget (KSU)",
			"value": "KSU",
			"domestic": 0
		}, {
			"label": "Kars, Turkey - Kars Airport (KSY)",
			"value": "KSY",
			"domestic": 0
		}, {
			"label": "Kotlas, Russia - Kotlas Airport (KSZ)",
			"value": "KSZ",
			"domestic": 0
		}, {
			"label": "Karratha, Australia - Karratha (KTA)",
			"value": "KTA",
			"domestic": 0
		}, {
			"label": "Thorne Bay, United States - Thorne Bay Airport (KTB)",
			"value": "KTB",
			"domestic": 1
		}, {
			"label": "Kerteh, Malaysia - Kerteh (KTE)",
			"value": "KTE",
			"domestic": 0
		}, {
			"label": "Ketapang, Indonesia - Ketapang Airport (KTG)",
			"value": "KTG",
			"domestic": 0
		}, {
			"label": "Kathmandu, Nepal - Tribhuvan (KTM)",
			"value": "KTM",
			"domestic": 0
		}, {
			"label": "Ketchikan, United States - Ketchikan International Airport (KTN)",
			"value": "KTN",
			"domestic": 1
		}, {
			"label": "Kingston, United States - Tinson (KTP)",
			"value": "KTP",
			"domestic": 1
		}, {
			"label": "Katherine, Australia - Tindal (KTR)",
			"value": "KTR",
			"domestic": 0
		}, {
			"label": "Teller Mission, United States - Brevig Mission Airport (KTS)",
			"value": "KTS",
			"domestic": 1
		}, {
			"label": "Kittila, Finland - Kittila (KTT)",
			"value": "KTT",
			"domestic": 0
		}, {
			"label": "Kota, India - Kota (KTU)",
			"value": "KTU",
			"domestic": 0
		}, {
			"label": "Katowice, Poland - Pyrzowice (KTW)",
			"value": "KTW",
			"domestic": 0
		}, {
			"label": "Kuantan, Malaysia - Kuantan (KUA)",
			"value": "KUA",
			"domestic": 0
		}, {
			"label": "Kudat, Malaysia - Kudat Airport (KUD)",
			"value": "KUD",
			"domestic": 0
		}, {
			"label": "Samara, Russia - Samara (KUF)",
			"value": "KUF",
			"domestic": 0
		}, {
			"label": "Kubin Island, Australia - Kubin Island Airport (KUG)",
			"value": "KUG",
			"domestic": 0
		}, {
			"label": "Kushiro, Japan - Kushiro (KUH)",
			"value": "KUH",
			"domestic": 0
		}, {
			"label": "Kasigluk, United States - Kasigluk Airport (KUK)",
			"value": "KUK",
			"domestic": 1
		}, {
			"label": "Kuala Lumpur, Malaysia - Kuala Lumpur Intl (KUL)",
			"value": "KUL",
			"domestic": 0
		}, {
			"label": "Yakushima, Japan - Yakushima Airport (KUM)",
			"value": "KUM",
			"domestic": 0
		}, {
			"label": "Kaunas, Lithuania - Kaunas (KUN)",
			"value": "KUN",
			"domestic": 0
		}, {
			"label": "Kuopio, Finland - Kuopio (KUO)",
			"value": "KUO",
			"domestic": 0
		}, {
			"label": "Kulusuk, Greenland - Kulusuk Airport (KUS)",
			"value": "KUS",
			"domestic": 0
		}, {
			"label": "Kulu, India - Bhuntar (KUU)",
			"value": "KUU",
			"domestic": 0
		}, {
			"label": "Gunsan, South Korea - Gunsan Airport (KUV)",
			"value": "KUV",
			"domestic": 0
		}, {
			"label": "Kamusi, Papua New Guinea - Kamusi Airport (KUY)",
			"value": "KUY",
			"domestic": 0
		}, {
			"label": "Kavala, Greece - Megas Alexandros (KVA)",
			"value": "KVA",
			"domestic": 0
		}, {
			"label": "Skovde, Sweden - Skovde (KVB)",
			"value": "KVB",
			"domestic": 0
		}, {
			"label": "Gyandzha, Azerbaijan - Gyandzha Airport (KVD)",
			"value": "KVD",
			"domestic": 0
		}, {
			"label": "Kavieng, Papua New Guinea - Kavieng Airport (KVG)",
			"value": "KVG",
			"domestic": 0
		}, {
			"label": "Kivalina, United States - Kivalina Airport (KVL)",
			"value": "KVL",
			"domestic": 1
		}, {
			"label": "Kavalerovo, Russia - Kavalerovo Airport (KVR)",
			"value": "KVR",
			"domestic": 0
		}, {
			"label": "Kwajalein, Marshall Islands - Kwajalein Airport (KWA)",
			"value": "KWA",
			"domestic": 0
		}, {
			"label": "Guiyang, China - Guiyang (KWE)",
			"value": "KWE",
			"domestic": 0
		}, {
			"label": "Waterfall, United States - Waterfall Sea Plane Base (KWF)",
			"value": "KWF",
			"domestic": 1
		}, {
			"label": "Kuwait, Kuwait - Kuwait Intl (KWI)",
			"value": "KWI",
			"domestic": 0
		}, {
			"label": "Gwangju, South Korea - Gwangju Airport (KWJ)",
			"value": "KWJ",
			"domestic": 0
		}, {
			"label": "Kwigillingok, United States - Kwigillingok Airport (KWK)",
			"value": "KWK",
			"domestic": 1
		}, {
			"label": "Guilin, China - Guilin (KWL)",
			"value": "KWL",
			"domestic": 0
		}, {
			"label": "Kowanyama, Australia - Kowanyama Airport (KWM)",
			"value": "KWM",
			"domestic": 0
		}, {
			"label": "Quinhagak, United States - Kwinhagak Airport (KWN)",
			"value": "KWN",
			"domestic": 1
		}, {
			"label": "Kawito, Papua New Guinea - Kawito Airport (KWO)",
			"value": "KWO",
			"domestic": 0
		}, {
			"label": "West Point, United States - Village Sea Plane Base (KWP)",
			"value": "KWP",
			"domestic": 1
		}, {
			"label": "Kwethluk, United States - Kwethluk Airport (KWT)",
			"value": "KWT",
			"domestic": 1
		}, {
			"label": "Kolwezi, Democratic Republic of Congo - Kolwezi Airport (KWZ)",
			"value": "KWZ",
			"domestic": 0
		}, {
			"label": "Kasaan, United States - Kasaan Sea Plane Base (KXA)",
			"value": "KXA",
			"domestic": 1
		}, {
			"label": "Koro, Fiji - Koro Airport (KXF)",
			"value": "KXF",
			"domestic": 0
		}, {
			"label": "Komsomolsk Na Amure, Russia - Komsomolsk Na Amure (KXK)",
			"value": "KXK",
			"domestic": 0
		}, {
			"label": "Konya, Turkey - Konya (KYA)",
			"value": "KYA",
			"domestic": 0
		}, {
			"label": "Orchid Island, Taiwan - Orchid Island Airport (KYD)",
			"value": "KYD",
			"domestic": 0
		}, {
			"label": "Karluk, United States - Karluk Airport (KYK)",
			"value": "KYK",
			"domestic": 1
		}, {
			"label": "Kyaukpyu, Myanmar (Burma) - Kyaukpyu Airport (KYP)",
			"value": "KYP",
			"domestic": 0
		}, {
			"label": "Koyukuk, United States - Koyukuk Airport (KYU)",
			"value": "KYU",
			"domestic": 1
		}, {
			"label": "Kyzyl, Russia - Kyzyl Airport (KYZ)",
			"value": "KYZ",
			"domestic": 0
		}, {
			"label": "Zachar Bay, United States - Zachar Bay Sea Plane Base (KZB)",
			"value": "KZB",
			"domestic": 1
		}, {
			"label": "Kozani, Greece - Philippos (KZI)",
			"value": "KZI",
			"domestic": 0
		}, {
			"label": "Kazan, Russia - Kazan (KZN)",
			"value": "KZN",
			"domestic": 0
		}, {
			"label": "Kzyl-Orda, Kazakhstan - Kzyl-Orda Airport (KZO)",
			"value": "KZO",
			"domestic": 0
		}, {
			"label": "Kastelorizo, Greece - Kastelorizo Airport (KZS)",
			"value": "KZS",
			"domestic": 0
		}, {
			"label": "Lamar, United States - Lamar Mncpl (LAA)",
			"value": "LAA",
			"domestic": 1
		}, {
			"label": "Luanda, Angola - 4 De Fevereiro (LAD)",
			"value": "LAD",
			"domestic": 0
		}, {
			"label": "Lae, Papua New Guinea - Nadzab (LAE)",
			"value": "LAE",
			"domestic": 0
		}, {
			"label": "Lannion, France - Servel (LAI)",
			"value": "LAI",
			"domestic": 0
		}, {
			"label": "Aklavik, Canada - Aklavik Airport (LAK)",
			"value": "LAK",
			"domestic": 0
		}, {
			"label": "Lakeland, United States - Lakeland Mncpl (LAL)",
			"value": "LAL",
			"domestic": 1
		}, {
			"label": "Los Alamos, United States - Los Alamos (LAM)",
			"value": "LAM",
			"domestic": 1
		}, {
			"label": "Lansing, United States - Capital City Airport (LAN)",
			"value": "LAN",
			"domestic": 1
		}, {
			"label": "Laoag, Philippines - Laoag (LAO)",
			"value": "LAO",
			"domestic": 0
		}, {
			"label": "La Paz, Mexico - Leon Airport (LAP)",
			"value": "LAP",
			"domestic": 0
		}, {
			"label": "Beida, Libya - La Braq Airport (LAQ)",
			"value": "LAQ",
			"domestic": 0
		}, {
			"label": "Laramie, United States - Laramie Regional Airport (LAR)",
			"value": "LAR",
			"domestic": 1
		}, {
			"label": "Las Vegas, United States - Las Vegas (LAS)",
			"value": "LAS",
			"domestic": 1
		}, {
			"label": "Lamu, Kenya - Lamu (LAU)",
			"value": "LAU",
			"domestic": 0
		}, {
			"label": "Lawton, United States - Lawton Municipal Airport (LAW)",
			"value": "LAW",
			"domestic": 1
		}, {
			"label": "Los Angeles, United States - LAX (LAX)",
			"value": "LAX",
			"domestic": 1
		}, {
			"label": "Ladysmith, South Africa - Ladysmith (LAY)",
			"value": "LAY",
			"domestic": 0
		}, {
			"label": "Leeds, United Kingdom - Leeds/Bradford (LBA)",
			"value": "LBA",
			"domestic": 0
		}, {
			"label": "Lubbock, United States - Lubbock (LBB)",
			"value": "LBB",
			"domestic": 1
		}, {
			"label": "Khudzhand, Tajikistan - Khudzhand Airport (LBD)",
			"value": "LBD",
			"domestic": 0
		}, {
			"label": "Latrobe, United States - Westmoreland Cty (LBE)",
			"value": "LBE",
			"domestic": 1
		}, {
			"label": "North Platte, United States - North Platte Regional Airport Lee Bird Field (LBF)",
			"value": "LBF",
			"domestic": 1
		}, {
			"label": "Albi, France - Le Sequestre (LBI)",
			"value": "LBI",
			"domestic": 0
		}, {
			"label": "Labuan Bajo, Indonesia - Mutiara Airport (LBJ)",
			"value": "LBJ",
			"domestic": 0
		}, {
			"label": "Liberal, United States - Liberal Municipal Airport (LBL)",
			"value": "LBL",
			"domestic": 1
		}, {
			"label": "Long Banga, Malaysia - Long Banga Airfield (LBP)",
			"value": "LBP",
			"domestic": 0
		}, {
			"label": "Labrea, Brazil - Labrea Airport (LBR)",
			"value": "LBR",
			"domestic": 0
		}, {
			"label": "Labasa, Fiji - Labasa (LBS)",
			"value": "LBS",
			"domestic": 0
		}, {
			"label": "Lumberton, United States - Lumberton Mncpl (LBT)",
			"value": "LBT",
			"domestic": 1
		}, {
			"label": "Labuan, Malaysia - Labuan (LBU)",
			"value": "LBU",
			"domestic": 0
		}, {
			"label": "Libreville, Gabon - Libreville (LBV)",
			"value": "LBV",
			"domestic": 0
		}, {
			"label": "Long Bawan, Indonesia - Long Bawan Airport (LBW)",
			"value": "LBW",
			"domestic": 0
		}, {
			"label": "La Baule, France - Montoir (LBY)",
			"value": "LBY",
			"domestic": 0
		}, {
			"label": "Larnaca, Cyprus - Larnaca Intl (LCA)",
			"value": "LCA",
			"domestic": 0
		}, {
			"label": "La Ceiba, Honduras - Goloson Intl (LCE)",
			"value": "LCE",
			"domestic": 0
		}, {
			"label": "La Coruna, Spain - La Coruna (LCG)",
			"value": "LCG",
			"domestic": 0
		}, {
			"label": "Lake Charles, United States - Lake Charles Municipal Airport (LCH)",
			"value": "LCH",
			"domestic": 1
		}, {
			"label": "Laconia, United States - Laconia Mncpl (LCI)",
			"value": "LCI",
			"domestic": 1
		}, {
			"label": "Lodz, Poland - Lodz Lublinek Airport (LCJ)",
			"value": "LCJ",
			"domestic": 0
		}, {
			"label": "La Chorrera, Colombia - La Chorrera Airport (LCR)",
			"value": "LCR",
			"domestic": 0
		}, {
			"label": "Lucca, Italy - Lucca (LCV)",
			"value": "LCV",
			"domestic": 0
		}, {
			"label": "Longyan, China - Liancheng Airport (LCX)",
			"value": "LCX",
			"domestic": 0
		}, {
			"label": "London, United Kingdom - London City Airport (LCY)",
			"value": "LCY",
			"domestic": 0
		}, {
			"label": "Londrina, Brazil - Londrina (LDB)",
			"value": "LDB",
			"domestic": 0
		}, {
			"label": "Lourdes, France - Tarbes Ossun Lourdes (LDE)",
			"value": "LDE",
			"domestic": 0
		}, {
			"label": "Leshukonskoye, Russia - Leshukonskoye Airport (LDG)",
			"value": "LDG",
			"domestic": 0
		}, {
			"label": "Lord Howe Island, Australia - Lord Howe Is (LDH)",
			"value": "LDH",
			"domestic": 0
		}, {
			"label": "Lindi, Tanzania - Kikwetu Airport (LDI)",
			"value": "LDI",
			"domestic": 0
		}, {
			"label": "Lidkoping, Sweden - Hovby (LDK)",
			"value": "LDK",
			"domestic": 0
		}, {
			"label": "Lamidanda, Nepal - Lamidanda Airport (LDN)",
			"value": "LDN",
			"domestic": 0
		}, {
			"label": "Lahad Datu, Malaysia - Lahad Datu Airport (LDU)",
			"value": "LDU",
			"domestic": 0
		}, {
			"label": "Londonderry, United Kingdom - Eglinton (LDY)",
			"value": "LDY",
			"domestic": 0
		}, {
			"label": "Learmonth, Australia - Learmonth Airport (LEA)",
			"value": "LEA",
			"domestic": 0
		}, {
			"label": "Lebanon, United States - Lebanon (LEB)",
			"value": "LEB",
			"domestic": 1
		}, {
			"label": "St Petersburg, Russia - Pulkovo (LED)",
			"value": "LED",
			"domestic": 0
		}, {
			"label": "Leesburg, United States - Leesburg (LEE)",
			"value": "LEE",
			"domestic": 1
		}, {
			"label": "Le Havre, France - Octeville (LEH)",
			"value": "LEH",
			"domestic": 0
		}, {
			"label": "Almeria, Spain - Almeria (LEI)",
			"value": "LEI",
			"domestic": 0
		}, {
			"label": "Leipzig, Germany - Leipzig/Halle (LEJ)",
			"value": "LEJ",
			"domestic": 0
		}, {
			"label": "Lake Evella, Australia - Lake Evella Airport (LEL)",
			"value": "LEL",
			"domestic": 0
		}, {
			"label": "Leon, Spain - Leon (LEN)",
			"value": "LEN",
			"domestic": 0
		}, {
			"label": "Lands End, United Kingdom - Lands End (LEQ)",
			"value": "LEQ",
			"domestic": 0
		}, {
			"label": "Leinster, Australia - Leinster Airport (LER)",
			"value": "LER",
			"domestic": 0
		}, {
			"label": "Leticia, Colombia - Gen. A.V. Cobo Airport (LET)",
			"value": "LET",
			"domestic": 0
		}, {
			"label": "Seo de Urgel, Spain - Aeroport De La Seu (LEU)",
			"value": "LEU",
			"domestic": 0
		}, {
			"label": "Bureta, Fiji - Levuka Airfield (LEV)",
			"value": "LEV",
			"domestic": 0
		}, {
			"label": "Lewiston, United States - Auburn (LEW)",
			"value": "LEW",
			"domestic": 1
		}, {
			"label": "Lexington, United States - Blue Grass Airport (LEX)",
			"value": "LEX",
			"domestic": 1
		}, {
			"label": "Lelystad, Netherlands - Lelystad (LEY)",
			"value": "LEY",
			"domestic": 0
		}, {
			"label": "Lamerd, Iran - Lamerd Airport (LFM)",
			"value": "LFM",
			"domestic": 0
		}, {
			"label": "Lafayette, LA, United States - Lafayette Regional Airport (LFT)",
			"value": "LFT",
			"domestic": 1
		}, {
			"label": "Lome, Togo - Lome (LFW)",
			"value": "LFW",
			"domestic": 0
		}, {
			"label": "New York City, United States - LaGuardia (LGA)",
			"value": "LGA",
			"domestic": 1
		}, {
			"label": "Long Beach, United States - Long Beach (LGB)",
			"value": "LGB",
			"domestic": 1
		}, {
			"label": "Liege, Belgium - Bierset (LGG)",
			"value": "LGG",
			"domestic": 0
		}, {
			"label": "Deadmans Cay, Bahamas - Deadmans Cay Airport (LGI)",
			"value": "LGI",
			"domestic": 0
		}, {
			"label": "Langkawi, Malaysia - Langkawi (LGK)",
			"value": "LGK",
			"domestic": 0
		}, {
			"label": "Long Lellang, Malaysia - Long Lellang Airport (LGL)",
			"value": "LGL",
			"domestic": 0
		}, {
			"label": "Langeoog, Germany - Langeoog (LGO)",
			"value": "LGO",
			"domestic": 0
		}, {
			"label": "Legaspi, Philippines - Legaspi (LGP)",
			"value": "LGP",
			"domestic": 0
		}, {
			"label": "Lago Agrio, Ecuador - Lago Agrio Airport (LGQ)",
			"value": "LGQ",
			"domestic": 0
		}, {
			"label": "Logan, United States - Logan-Cache (LGU)",
			"value": "LGU",
			"domestic": 1
		}, {
			"label": "London, United Kingdom - Gatwick (LGW)",
			"value": "LGW",
			"domestic": 0
		}, {
			"label": "Lahr, Germany - Lahr (LHA)",
			"value": "LHA",
			"domestic": 0
		}, {
			"label": "Lahore, Pakistan - Lahore (LHE)",
			"value": "LHE",
			"domestic": 0
		}, {
			"label": "London, United Kingdom - Heathrow (LHR)",
			"value": "LHR",
			"domestic": 0
		}, {
			"label": "Lanzhou, China - Lanzhou (LHW)",
			"value": "LHW",
			"domestic": 0
		}, {
			"label": "Lifou, New Caledonia - Lifou Airport (LIF)",
			"value": "LIF",
			"domestic": 0
		}, {
			"label": "Limoges, France - Bellegarde (LIG)",
			"value": "LIG",
			"domestic": 0
		}, {
			"label": "Lihue, United States - Kauai Island Airport (LIH)",
			"value": "LIH",
			"domestic": 1
		}, {
			"label": "Likiep, Marshall Islands - Likiep Airport (LIK)",
			"value": "LIK",
			"domestic": 0
		}, {
			"label": "Lille, France - Lesquin (LIL)",
			"value": "LIL",
			"domestic": 0
		}, {
			"label": "Lima, Peru - J Chavez International Airport (LIM)",
			"value": "LIM",
			"domestic": 0
		}, {
			"label": "Milan, Italy - Linate (LIN)",
			"value": "LIN",
			"domestic": 0
		}, {
			"label": "Limon, Costa Rica - Limon (LIO)",
			"value": "LIO",
			"domestic": 0
		}, {
			"label": "Liberia, Costa Rica - Daniel Oduber Quiros (LIR)",
			"value": "LIR",
			"domestic": 0
		}, {
			"label": "Lisbon, Portugal - Lisboa (LIS)",
			"value": "LIS",
			"domestic": 0
		}, {
			"label": "Little Rock, United States - Little Rock (LIT)",
			"value": "LIT",
			"domestic": 1
		}, {
			"label": "Loikaw, Myanmar (Burma) - Loikaw Airport (LIW)",
			"value": "LIW",
			"domestic": 0
		}, {
			"label": "Lijiang City, China - Lijiang (LJG)",
			"value": "LJG",
			"domestic": 0
		}, {
			"label": "Lake Jackson, United States - Brazoria County (LJN)",
			"value": "LJN",
			"domestic": 1
		}, {
			"label": "Ljubljana, Slovenia - Brnik (LJU)",
			"value": "LJU",
			"domestic": 0
		}, {
			"label": "Lakeba, Fiji - Lakeba Airport (LKB)",
			"value": "LKB",
			"domestic": 0
		}, {
			"label": "Long Akah, Malaysia - Long Akah Airport (LKH)",
			"value": "LKH",
			"domestic": 0
		}, {
			"label": "Lakselv, Norway - Banak (LKL)",
			"value": "LKL",
			"domestic": 0
		}, {
			"label": "Leknes, Norway - Leknes (LKN)",
			"value": "LKN",
			"domestic": 0
		}, {
			"label": "Lucknow, India - Amausi (LKO)",
			"value": "LKO",
			"domestic": 0
		}, {
			"label": "Lakeview, United States - Lakeview Mncpl (LKV)",
			"value": "LKV",
			"domestic": 1
		}, {
			"label": "Lulea, Sweden - Kallax (LLA)",
			"value": "LLA",
			"domestic": 0
		}, {
			"label": "Lalibela, Ethiopia - Lalibela Airport (LLI)",
			"value": "LLI",
			"domestic": 0
		}, {
			"label": "Alluitsup Paa, Greenland - Alluitsup Paa Airport (LLU)",
			"value": "LLU",
			"domestic": 0
		}, {
			"label": "Lilongwe, Malawi - Lilongwe Intl (LLW)",
			"value": "LLW",
			"domestic": 0
		}, {
			"label": "Lake Minchumina, United States - Lake Minchumina Airport (LMA)",
			"value": "LMA",
			"domestic": 1
		}, {
			"label": "Lamacarena, Colombia - Lamacarena Airport (LMC)",
			"value": "LMC",
			"domestic": 0
		}, {
			"label": "Le Mans, France - Arnage (LME)",
			"value": "LME",
			"domestic": 0
		}, {
			"label": "Lumi, Papua New Guinea - Lumi Airport (LMI)",
			"value": "LMI",
			"domestic": 0
		}, {
			"label": "Lae, Marshall Islands - Lae Airport (LML)",
			"value": "LML",
			"domestic": 0
		}, {
			"label": "Los Mochis, Mexico - Federal (LMM)",
			"value": "LMM",
			"domestic": 0
		}, {
			"label": "Limbang, Malaysia - Limbang Airport (LMN)",
			"value": "LMN",
			"domestic": 0
		}, {
			"label": "Lampedusa, Italy - Lampedusa (LMP)",
			"value": "LMP",
			"domestic": 0
		}, {
			"label": "Klamath Falls, United States - Kingsley Field (LMT)",
			"value": "LMT",
			"domestic": 1
		}, {
			"label": "Lamen Bay, Vanuatu - Lamen Bay Airport (LNB)",
			"value": "LNB",
			"domestic": 0
		}, {
			"label": "Lonorore, Vanuatu - Lonorore Airport (LNE)",
			"value": "LNE",
			"domestic": 0
		}, {
			"label": "Lincang, China - Lincang Airport (LNJ)",
			"value": "LNJ",
			"domestic": 0
		}, {
			"label": "Lincoln, United States - Lincoln Municipal Airport (LNK)",
			"value": "LNK",
			"domestic": 1
		}, {
			"label": "Leonora, Australia - Leonora Airport (LNO)",
			"value": "LNO",
			"domestic": 0
		}, {
			"label": "Wise, United States - Wise Mncpl (LNP)",
			"value": "LNP",
			"domestic": 1
		}, {
			"label": "Lancaster, United States - Lancaster Airport (LNS)",
			"value": "LNS",
			"domestic": 1
		}, {
			"label": "Lihir Island, Papua New Guinea - Lihir Island Airport (LNV)",
			"value": "LNV",
			"domestic": 0
		}, {
			"label": "Lana\'i City, United States - Lana\'i Airport (LNY)",
			"value": "LNY",
			"domestic": 1
		}, {
			"label": "Linz, Austria - Hoersching (LNZ)",
			"value": "LNZ",
			"domestic": 0
		}, {
			"label": "Longana, Vanuatu - Longana Airport (LOD)",
			"value": "LOD",
			"domestic": 0
		}, {
			"label": "Loei, Thailand - Loei Airport (LOE)",
			"value": "LOE",
			"domestic": 0
		}, {
			"label": "Loja, Ecuador - Loja Airport (LOH)",
			"value": "LOH",
			"domestic": 0
		}, {
			"label": "Lovelock, United States - Derby Field (LOL)",
			"value": "LOL",
			"domestic": 1
		}, {
			"label": "Lagos de Moreno, Mexico - Francisco P.V.y R (LOM)",
			"value": "LOM",
			"domestic": 0
		}, {
			"label": "London, United Kingdom - All Airports (LON)",
			"value": "LON",
			"domestic": 0
		}, {
			"label": "Lagos, Nigeria - Murtala Muhammed (LOS)",
			"value": "LOS",
			"domestic": 0
		}, {
			"label": "Monclova, Mexico - Monclova (LOV)",
			"value": "LOV",
			"domestic": 0
		}, {
			"label": "London, United States - Corbin-London (LOZ)",
			"value": "LOZ",
			"domestic": 1
		}, {
			"label": "Las Palmas, Spain - Gran Canaria (LPA)",
			"value": "LPA",
			"domestic": 0
		}, {
			"label": "La Paz, Bolivia - El Alto (LPB)",
			"value": "LPB",
			"domestic": 0
		}, {
			"label": "La Pedrera, Colombia - La Pedrera Airport (LPD)",
			"value": "LPD",
			"domestic": 0
		}, {
			"label": "Linkoping, Sweden - Linkoping/Saab (LPI)",
			"value": "LPI",
			"domestic": 0
		}, {
			"label": "Lipetsk, Russia - Lipetsk Airport (LPK)",
			"value": "LPK",
			"domestic": 0
		}, {
			"label": "Liverpool, United Kingdom - John Lennon (LPL)",
			"value": "LPL",
			"domestic": 0
		}, {
			"label": "Lamap, Vanuatu - Lamap Airport (LPM)",
			"value": "LPM",
			"domestic": 0
		}, {
			"label": "Lappeenranta, Finland - Lappeenranta (LPP)",
			"value": "LPP",
			"domestic": 0
		}, {
			"label": "Luang Prabang, Laos - Luang Prabang (LPQ)",
			"value": "LPQ",
			"domestic": 0
		}, {
			"label": "Lopez Island, United States - Lopez Island Mncpl (LPS)",
			"value": "LPS",
			"domestic": 1
		}, {
			"label": "Lampang, Thailand - Lampang Airport (LPT)",
			"value": "LPT",
			"domestic": 0
		}, {
			"label": "Long Apung, Indonesia - Long Apung Airport (LPU)",
			"value": "LPU",
			"domestic": 0
		}, {
			"label": "Liepaya, Latvia - Liepaya (LPX)",
			"value": "LPX",
			"domestic": 0
		}, {
			"label": "Le Puy-en-Velay, France - Loudes (LPY)",
			"value": "LPY",
			"domestic": 0
		}, {
			"label": "Puerto Leguizamo, Colombia - Puerto Leguizamo Airport (LQM)",
			"value": "LQM",
			"domestic": 0
		}, {
			"label": "Larisa, Greece - Larisa (LRA)",
			"value": "LRA",
			"domestic": 0
		}, {
			"label": "Laredo, United States - Laredo International Airport (LRD)",
			"value": "LRD",
			"domestic": 1
		}, {
			"label": "Longreach, Australia - Longreach (LRE)",
			"value": "LRE",
			"domestic": 0
		}, {
			"label": "La Rochelle, France - Laleu (LRH)",
			"value": "LRH",
			"domestic": 0
		}, {
			"label": "La Romana, Dominican Republic - La Romana Airport (LRM)",
			"value": "LRM",
			"domestic": 0
		}, {
			"label": "Lar, Iran - Lar Airport (LRR)",
			"value": "LRR",
			"domestic": 0
		}, {
			"label": "Leros, Greece - Leros (LRS)",
			"value": "LRS",
			"domestic": 0
		}, {
			"label": "Lorient, France - Lann Bihoue (LRT)",
			"value": "LRT",
			"domestic": 0
		}, {
			"label": "Las Cruces, United States - Las Cruces-Crawford (LRU)",
			"value": "LRU",
			"domestic": 1
		}, {
			"label": "Los Roques Islands, Venezuela - Los Roques (LRV)",
			"value": "LRV",
			"domestic": 0
		}, {
			"label": "Losuia, Papua New Guinea - Losuia Airport (LSA)",
			"value": "LSA",
			"domestic": 0
		}, {
			"label": "Lordsburg, United States - Lordsburg (LSB)",
			"value": "LSB",
			"domestic": 1
		}, {
			"label": "La Serena, Chile and Easter Island - La Florida (LSC)",
			"value": "LSC",
			"domestic": 0
		}, {
			"label": "La Crosse, United States - La Crosse Municipal Airport (LSE)",
			"value": "LSE",
			"domestic": 1
		}, {
			"label": "Lashio, Myanmar (Burma) - Lashio Airport (LSH)",
			"value": "LSH",
			"domestic": 0
		}, {
			"label": "Lerwick, United Kingdom - Sumburgh (LSI)",
			"value": "LSI",
			"domestic": 0
		}, {
			"label": "Las Piedras, Venezuela - Josefa Camejo Airport (LSP)",
			"value": "LSP",
			"domestic": 0
		}, {
			"label": "Terre de Haut, St. Barthelemy - Terre de Haut (LSS)",
			"value": "LSS",
			"domestic": 0
		}, {
			"label": "Launceston, Australia - Launceston (LST)",
			"value": "LST",
			"domestic": 0
		}, {
			"label": "Lismore, Australia - Lismore (LSY)",
			"value": "LSY",
			"domestic": 0
		}, {
			"label": "Tzaneen, South Africa - Tzaneen (LTA)",
			"value": "LTA",
			"domestic": 0
		}, {
			"label": "Altai, Mongolia - Altai Airport (LTI)",
			"value": "LTI",
			"domestic": 0
		}, {
			"label": "Latakia, Syria - Latakia/Hmelmin (LTK)",
			"value": "LTK",
			"domestic": 0
		}, {
			"label": "London, United Kingdom - Luton (LTN)",
			"value": "LTN",
			"domestic": 0
		}, {
			"label": "Loreto, Mexico - Loreto (LTO)",
			"value": "LTO",
			"domestic": 0
		}, {
			"label": "Le Touquet, France - Le Touquet (LTQ)",
			"value": "LTQ",
			"domestic": 0
		}, {
			"label": "St Tropez, France - La Mole (LTT)",
			"value": "LTT",
			"domestic": 0
		}, {
			"label": "Lukla, Nepal - Lukla Airport (LUA)",
			"value": "LUA",
			"domestic": 0
		}, {
			"label": "Luderitz, Namibia - Luderitz (LUD)",
			"value": "LUD",
			"domestic": 0
		}, {
			"label": "Lugano, Switzerland - Lugano (LUG)",
			"value": "LUG",
			"domestic": 0
		}, {
			"label": "Ludhiana, India - Sahnewal (LUH)",
			"value": "LUH",
			"domestic": 0
		}, {
			"label": "Cincinnati, United States - Lunken Fld (LUK)",
			"value": "LUK",
			"domestic": 1
		}, {
			"label": "Luxi, China - Mangshi Airport (LUM)",
			"value": "LUM",
			"domestic": 0
		}, {
			"label": "Lusaka, Zambia - Lusaka (LUN)",
			"value": "LUN",
			"domestic": 0
		}, {
			"label": "Kalaupapa, United States - Kalaupapa Airport (LUP)",
			"value": "LUP",
			"domestic": 1
		}, {
			"label": "San Luis, Argentina - San Luis Airport (LUQ)",
			"value": "LUQ",
			"domestic": 0
		}, {
			"label": "Cape Lisburne, United States - Cape Lisburne Airport (LUR)",
			"value": "LUR",
			"domestic": 1
		}, {
			"label": "Luxembourg City, Luxembourg - Luxembourg (LUX)",
			"value": "LUX",
			"domestic": 0
		}, {
			"label": "Laval, France - Laval (LVA)",
			"value": "LVA",
			"domestic": 0
		}, {
			"label": "Lime Village, United States - Lime Village Airport (LVD)",
			"value": "LVD",
			"domestic": 1
		}, {
			"label": "Livingstone, Zambia - Livingstone (LVI)",
			"value": "LVI",
			"domestic": 0
		}, {
			"label": "Livermore, United States - Livermore (LVK)",
			"value": "LVK",
			"domestic": 1
		}, {
			"label": "Laverton, Australia - Laverton Airport (LVO)",
			"value": "LVO",
			"domestic": 0
		}, {
			"label": "Greenbrier, United States - Greenbrier (LWB)",
			"value": "LWB",
			"domestic": 1
		}, {
			"label": "Lawrence, United States - Lawrence Mncpl (LWC)",
			"value": "LWC",
			"domestic": 1
		}, {
			"label": "Lerwick, United Kingdom - Lerwick/Tingwall (LWK)",
			"value": "LWK",
			"domestic": 0
		}, {
			"label": "Wells, United States - Harriet Field (LWL)",
			"value": "LWL",
			"domestic": 1
		}, {
			"label": "Gyoumri, Armenia - Leninakan Airport (LWN)",
			"value": "LWN",
			"domestic": 0
		}, {
			"label": "Lviv, Ukraine - Lvov (LWO)",
			"value": "LWO",
			"domestic": 0
		}, {
			"label": "Lewiston, ID, United States - Nez Perce County Regional Airport (LWS)",
			"value": "LWS",
			"domestic": 1
		}, {
			"label": "Lewistown, United States - Lewistown Mncpl (LWT)",
			"value": "LWT",
			"domestic": 1
		}, {
			"label": "Lawas, Malaysia - Lawas Airport (LWY)",
			"value": "LWY",
			"domestic": 0
		}, {
			"label": "Lhasa, China - Lhasa (LXA)",
			"value": "LXA",
			"domestic": 0
		}, {
			"label": "Luang Namtha, Laos - Luang Namtha Airport (LXG)",
			"value": "LXG",
			"domestic": 0
		}, {
			"label": "Luxor, Egypt - Luxor (LXR)",
			"value": "LXR",
			"domestic": 0
		}, {
			"label": "Limnos, Greece - Limnos (LXS)",
			"value": "LXS",
			"domestic": 0
		}, {
			"label": "Leadville, United States - Lake County (LXV)",
			"value": "LXV",
			"domestic": 1
		}, {
			"label": "Luoyang, China - Luoyang (LYA)",
			"value": "LYA",
			"domestic": 0
		}, {
			"label": "Little Cayman, Cayman Islands - Little Cayman Mncpl (LYB)",
			"value": "LYB",
			"domestic": 0
		}, {
			"label": "Lycksele, Sweden - Lycksele (LYC)",
			"value": "LYC",
			"domestic": 0
		}, {
			"label": "Lianyungang, China - Lianyungang (LYG)",
			"value": "LYG",
			"domestic": 0
		}, {
			"label": "Lynchburg, United States - Lynchburg (LYH)",
			"value": "LYH",
			"domestic": 1
		}, {
			"label": "Linyi, China - Linyi Airport (LYI)",
			"value": "LYI",
			"domestic": 0
		}, {
			"label": "Faisalabad, Pakistan - Faisalabad (LYP)",
			"value": "LYP",
			"domestic": 0
		}, {
			"label": "Longyearbyen, Norway - Svalbard (LYR)",
			"value": "LYR",
			"domestic": 0
		}, {
			"label": "Lyon, France - Lyon Satolas Airport (LYS)",
			"value": "LYS",
			"domestic": 0
		}, {
			"label": "Ely, United States - Ely Mncpl (LYU)",
			"value": "LYU",
			"domestic": 1
		}, {
			"label": "Lydd, United Kingdom - Lydd International Airport (LYX)",
			"value": "LYX",
			"domestic": 0
		}, {
			"label": "Lazaro Cardenas, Mexico - Lazaro Cardenas Mncp (LZC)",
			"value": "LZC",
			"domestic": 0
		}, {
			"label": "Liuzhou, China - Liuzhou Airport (LZH)",
			"value": "LZH",
			"domestic": 0
		}, {
			"label": "Nangan, China - Nangan Airport (Matsu Islands) (LZN)",
			"value": "LZN",
			"domestic": 0
		}, {
			"label": "Luzhou, China - Luzhou Airport (LZO)",
			"value": "LZO",
			"domestic": 0
		}, {
			"label": "Lin Zhi, China - Lin Zhi Airport (LZY)",
			"value": "LZY",
			"domestic": 0
		}, {
			"label": "Chennai, India - Chennai (MAA)",
			"value": "MAA",
			"domestic": 0
		}, {
			"label": "Maraba, Brazil - Maraba Airport (MAB)",
			"value": "MAB",
			"domestic": 0
		}, {
			"label": "Madrid, Spain - Barajas Airport (MAD)",
			"value": "MAD",
			"domestic": 0
		}, {
			"label": "Midland, United States - Odessa Regional Airport (MAF)",
			"value": "MAF",
			"domestic": 1
		}, {
			"label": "Madang, Papua New Guinea - Madang (MAG)",
			"value": "MAG",
			"domestic": 0
		}, {
			"label": "Menorca, Spain - Menorca (MAH)",
			"value": "MAH",
			"domestic": 0
		}, {
			"label": "Majuro, Marshall Islands - Amata Kabua Intl (MAJ)",
			"value": "MAJ",
			"domestic": 0
		}, {
			"label": "Malakal, Sudan - Malakal Airport (MAK)",
			"value": "MAK",
			"domestic": 0
		}, {
			"label": "Matamoros, Mexico - Matamoros Intl (MAM)",
			"value": "MAM",
			"domestic": 0
		}, {
			"label": "Manchester, United Kingdom - Manchester International Airport (MAN)",
			"value": "MAN",
			"domestic": 0
		}, {
			"label": "Manaus, Brazil - Eduardo Gomes Intl (MAO)",
			"value": "MAO",
			"domestic": 0
		}, {
			"label": "Mae Sot, Thailand - Mae Sot (MAQ)",
			"value": "MAQ",
			"domestic": 0
		}, {
			"label": "Maracaibo, Venezuela - La Chinita Airport (MAR)",
			"value": "MAR",
			"domestic": 0
		}, {
			"label": "Manus Island, Papua New Guinea - Momote Airport (MAS)",
			"value": "MAS",
			"domestic": 0
		}, {
			"label": "Maupiti, French Polynesia and Tahiti - Maupiti Airport (MAU)",
			"value": "MAU",
			"domestic": 0
		}, {
			"label": "Maloelap, Marshall Islands - Maloelap Airport (MAV)",
			"value": "MAV",
			"domestic": 0
		}, {
			"label": "Mayaguez, Puerto Rico - Eugenio M. De Hostos Airport (MAZ)",
			"value": "MAZ",
			"domestic": 0
		}, {
			"label": "Mombasa, Kenya - Moi Intl (MBA)",
			"value": "MBA",
			"domestic": 0
		}, {
			"label": "Mmabatho, South Africa - Mmmabatho Intl (MBD)",
			"value": "MBD",
			"domestic": 0
		}, {
			"label": "Monbetsu, Japan - Monbetsu Airport (MBE)",
			"value": "MBE",
			"domestic": 0
		}, {
			"label": "Maryborough, Australia - Maryborough (MBH)",
			"value": "MBH",
			"domestic": 0
		}, {
			"label": "Montego Bay, Jamaica - Sangster International Airport (MBJ)",
			"value": "MBJ",
			"domestic": 0
		}, {
			"label": "Manistee, United States - Manistee Country Blacker Airport (MBL)",
			"value": "MBL",
			"domestic": 1
		}, {
			"label": "Saginaw, United States - Tri City Airport (MBS)",
			"value": "MBS",
			"domestic": 1
		}, {
			"label": "Masbate, Philippines - Masbate Airport (MBT)",
			"value": "MBT",
			"domestic": 0
		}, {
			"label": "Mbambanakira, Solomon Islands - Mbambanakira Airport (MBU)",
			"value": "MBU",
			"domestic": 0
		}, {
			"label": "Moorabbin, Australia - Moorabbin Airport (MBW)",
			"value": "MBW",
			"domestic": 0
		}, {
			"label": "Maribor, Slovenia - Maribor (MBX)",
			"value": "MBX",
			"domestic": 0
		}, {
			"label": "Maues, Brazil - Maues Airport (MBZ)",
			"value": "MBZ",
			"domestic": 0
		}, {
			"label": "McComb, United States - Pike County (MCB)",
			"value": "MCB",
			"domestic": 1
		}, {
			"label": "Mackinac Island, United States - Mackinac Island (MCD)",
			"value": "MCD",
			"domestic": 1
		}, {
			"label": "Merced, United States - Merced Municipal Airport (MCE)",
			"value": "MCE",
			"domestic": 1
		}, {
			"label": "McGrath, United States - Mc Grath Airport (MCG)",
			"value": "MCG",
			"domestic": 1
		}, {
			"label": "Machala, Ecuador - Gen. M. Serrano (MCH)",
			"value": "MCH",
			"domestic": 0
		}, {
			"label": "Kansas City, United States - Kansas City (MCI)",
			"value": "MCI",
			"domestic": 1
		}, {
			"label": "McCook, United States - Mc Cook Municipal Airport (MCK)",
			"value": "MCK",
			"domestic": 1
		}, {
			"label": "Monte Carlo, Monaco - H/P De Monte Carlo (MCM)",
			"value": "MCM",
			"domestic": 0
		}, {
			"label": "Macon, United States - Lewis B. Wilson Airport (MCN)",
			"value": "MCN",
			"domestic": 1
		}, {
			"label": "Orlando, United States - Orlando (MCO)",
			"value": "MCO",
			"domestic": 1
		}, {
			"label": "Macapa, Brazil - Macapa Internacional Airport (MCP)",
			"value": "MCP",
			"domestic": 0
		}, {
			"label": "Muscat, Oman - Seeb (MCT)",
			"value": "MCT",
			"domestic": 0
		}, {
			"label": "Montlucon, France - Gueret (Lepaud) (MCU)",
			"value": "MCU",
			"domestic": 0
		}, {
			"label": "Mcarthur River, Australia - Mcarthur River Airport (MCV)",
			"value": "MCV",
			"domestic": 0
		}, {
			"label": "Mason City, United States - Mason City Mncpl (MCW)",
			"value": "MCW",
			"domestic": 1
		}, {
			"label": "Makhachkala, Russia - Makhachkala Airport (MCX)",
			"value": "MCX",
			"domestic": 0
		}, {
			"label": "Maroochydore, Australia - Maroochydore (MCY)",
			"value": "MCY",
			"domestic": 0
		}, {
			"label": "Maceio, Brazil - Palmares (MCZ)",
			"value": "MCZ",
			"domestic": 0
		}, {
			"label": "Manado, Indonesia - Samratulangi (MDC)",
			"value": "MDC",
			"domestic": 0
		}, {
			"label": "Medellin, Colombia - Jose Marie Cordova Airport (MDE)",
			"value": "MDE",
			"domestic": 0
		}, {
			"label": "Mudanjiang, China - Mudanjiang (MDG)",
			"value": "MDG",
			"domestic": 0
		}, {
			"label": "Carbondale, United States - Southern Illinois (MDH)",
			"value": "MDH",
			"domestic": 1
		}, {
			"label": "Mbandaka, Democratic Republic of Congo - Mbandaka Airport (MDK)",
			"value": "MDK",
			"domestic": 0
		}, {
			"label": "Mandalay, Myanmar (Burma) - Annisaton (MDL)",
			"value": "MDL",
			"domestic": 0
		}, {
			"label": "Mar del Plata, Argentina - Mar del Plata (MDQ)",
			"value": "MDQ",
			"domestic": 0
		}, {
			"label": "Middle Caicos, Turks and Caicos - Middle Caicos Airport (MDS)",
			"value": "MDS",
			"domestic": 0
		}, {
			"label": "Harrisburg, United States - Harrisburg (MDT)",
			"value": "MDT",
			"domestic": 1
		}, {
			"label": "Mendi, Papua New Guinea - Mendi Airport (MDU)",
			"value": "MDU",
			"domestic": 0
		}, {
			"label": "Chicago, United States - Midway (MDW)",
			"value": "MDW",
			"domestic": 1
		}, {
			"label": "Mendoza, Argentina - El Plumerillo (MDZ)",
			"value": "MDZ",
			"domestic": 0
		}, {
			"label": "Macae, Brazil - Macae (MEA)",
			"value": "MEA",
			"domestic": 0
		}, {
			"label": "Manta, Ecuador - Manta Airport (MEC)",
			"value": "MEC",
			"domestic": 0
		}, {
			"label": "Madinah, Saudi Arabia - Mohammad Abdulaziz (MED)",
			"value": "MED",
			"domestic": 0
		}, {
			"label": "Mare, New Caledonia - Mare Airport (MEE)",
			"value": "MEE",
			"domestic": 0
		}, {
			"label": "Malange, Angola - Malange Airport (MEG)",
			"value": "MEG",
			"domestic": 0
		}, {
			"label": "Mehamn, Norway - Mehamn Mncpl (MEH)",
			"value": "MEH",
			"domestic": 0
		}, {
			"label": "Meridian, United States - Meridian (MEI)",
			"value": "MEI",
			"domestic": 1
		}, {
			"label": "Melbourne, Australia - Tullamarine Airport (MEL)",
			"value": "MEL",
			"domestic": 0
		}, {
			"label": "Memphis, United States - Memphis (MEM)",
			"value": "MEM",
			"domestic": 1
		}, {
			"label": "Manteo, United States - Dare Cty Regl (MEO)",
			"value": "MEO",
			"domestic": 1
		}, {
			"label": "Medan, Indonesia - Polonia (MES)",
			"value": "MES",
			"domestic": 0
		}, {
			"label": "Minden, United States - Douglas County (MEV)",
			"value": "MEV",
			"domestic": 1
		}, {
			"label": "Mexico City, Mexico - Juarez International Airport (MEX)",
			"value": "MEX",
			"domestic": 0
		}, {
			"label": "Meghauli, Nepal - Meghauli Airport (MEY)",
			"value": "MEY",
			"domestic": 0
		}, {
			"label": "Mafia Island, Tanzania - Mafia (MFA)",
			"value": "MFA",
			"domestic": 0
		}, {
			"label": "Mansfield, United States - Lahm Mncpl (MFD)",
			"value": "MFD",
			"domestic": 1
		}, {
			"label": "McAllen, United States - Miller International Airport (MFE)",
			"value": "MFE",
			"domestic": 1
		}, {
			"label": "Mesquite, United States - Mesquite Municipal (MFH)",
			"value": "MFH",
			"domestic": 1
		}, {
			"label": "Marshfield, United States - Marshfield Mncpl (MFI)",
			"value": "MFI",
			"domestic": 1
		}, {
			"label": "Moala, Fiji - Moala Airport (MFJ)",
			"value": "MFJ",
			"domestic": 0
		}, {
			"label": "Matsu, Taiwan - Matsu Airport (MFK)",
			"value": "MFK",
			"domestic": 0
		}, {
			"label": "Macao, China - Macau Airport (MFM)",
			"value": "MFM",
			"domestic": 0
		}, {
			"label": "Medford, United States - Jackson County Airport (MFR)",
			"value": "MFR",
			"domestic": 1
		}, {
			"label": "Mfuwe, Zambia - Mfuwe (MFU)",
			"value": "MFU",
			"domestic": 0
		}, {
			"label": "Managua, Nicaragua - Augusto C Sandino Airport (MGA)",
			"value": "MGA",
			"domestic": 0
		}, {
			"label": "Mount Gambier, Australia - Mt Gambier (MGB)",
			"value": "MGB",
			"domestic": 0
		}, {
			"label": "Michigan City, United States - Michigan City (MGC)",
			"value": "MGC",
			"domestic": 1
		}, {
			"label": "Maringa, Brazil - Maringa Regional (MGF)",
			"value": "MGF",
			"domestic": 0
		}, {
			"label": "Margate, South Africa - Margate Airport (MGH)",
			"value": "MGH",
			"domestic": 0
		}, {
			"label": "Montgomery, United States - Dannelly Field (MGM)",
			"value": "MGM",
			"domestic": 1
		}, {
			"label": "Mogadishu, Somalia - Mogadishu International Airport (MGQ)",
			"value": "MGQ",
			"domestic": 0
		}, {
			"label": "Moultrie, United States - Thomasville (MGR)",
			"value": "MGR",
			"domestic": 1
		}, {
			"label": "Mangaia Island, Cook Islands - Mangaia Island Airport (MGS)",
			"value": "MGS",
			"domestic": 0
		}, {
			"label": "Milingimbi, Australia - Milingimbi Airport (MGT)",
			"value": "MGT",
			"domestic": 0
		}, {
			"label": "Morgantown, United States - Morgantown Mncpl (MGW)",
			"value": "MGW",
			"domestic": 1
		}, {
			"label": "Myeik, Myanmar (Burma) - Myeik Airport (MGZ)",
			"value": "MGZ",
			"domestic": 0
		}, {
			"label": "Mashad, Iran - Mashad Airport (MHD)",
			"value": "MHD",
			"domestic": 0
		}, {
			"label": "Mitchell, United States - Mitchell (MHE)",
			"value": "MHE",
			"domestic": 1
		}, {
			"label": "Mannheim, Germany - Neuostheim (MHG)",
			"value": "MHG",
			"domestic": 0
		}, {
			"label": "Marsh Harbour, Bahamas - Marsh Harbour International Airport (MHH)",
			"value": "MHH",
			"domestic": 0
		}, {
			"label": "Manhattan, United States - Manhattan Regional Airport (MHK)",
			"value": "MHK",
			"domestic": 1
		}, {
			"label": "Minsk, Belarus - Minsk International 1 Airport (MHP)",
			"value": "MHP",
			"domestic": 0
		}, {
			"label": "Mariehamn, Finland - Mariehamn (MHQ)",
			"value": "MHQ",
			"domestic": 0
		}, {
			"label": "Manchester, United States - Manchester (MHT)",
			"value": "MHT",
			"domestic": 1
		}, {
			"label": "Mojave, United States - Mojave (MHV)",
			"value": "MHV",
			"domestic": 1
		}, {
			"label": "Manihiki Island, Cook Islands - Manihiki Island Airport (MHX)",
			"value": "MHX",
			"domestic": 0
		}, {
			"label": "Miami, United States - Miami (MIA)",
			"value": "MIA",
			"domestic": 1
		}, {
			"label": "Merida, Mexico - Rejon Airport (MID)",
			"value": "MID",
			"domestic": 0
		}, {
			"label": "Muncie, United States - Delaware Cty (MIE)",
			"value": "MIE",
			"domestic": 1
		}, {
			"label": "Monahans, United States - Roy Hurd Memorial (MIF)",
			"value": "MIF",
			"domestic": 1
		}, {
			"label": "Mian Yang, China - Mian Yang Airport (MIG)",
			"value": "MIG",
			"domestic": 0
		}, {
			"label": "Marilia, Brazil - Marilia (MII)",
			"value": "MII",
			"domestic": 0
		}, {
			"label": "Mili Island, Marshall Islands - Mili Island Airport (MIJ)",
			"value": "MIJ",
			"domestic": 0
		}, {
			"label": "Mikkeli, Finland - Mikkeli (MIK)",
			"value": "MIK",
			"domestic": 0
		}, {
			"label": "Milan, Italy - All Airports (MIL)",
			"value": "MIL",
			"domestic": 0
		}, {
			"label": "Merimbula, Australia - Merimbula (MIM)",
			"value": "MIM",
			"domestic": 0
		}, {
			"label": "Monastir, Tunisia - Habib Bourguiba Intl (MIR)",
			"value": "MIR",
			"domestic": 0
		}, {
			"label": "Misima Island, Papua New Guinea - Misima Island Airport (MIS)",
			"value": "MIS",
			"domestic": 0
		}, {
			"label": "Millville, United States - Millville Mncpl (MIV)",
			"value": "MIV",
			"domestic": 1
		}, {
			"label": "Marshalltown, United States - Marshalltown Mncpl (MIW)",
			"value": "MIW",
			"domestic": 1
		}, {
			"label": "Manja, Madagascar - Manja Airport (MJA)",
			"value": "MJA",
			"domestic": 0
		}, {
			"label": "Mejit Island, Marshall Islands - Mejit Island Airport (MJB)",
			"value": "MJB",
			"domestic": 0
		}, {
			"label": "Mohenjo Daro, Pakistan - Mohenjo Daro Airport (MJD)",
			"value": "MJD",
			"domestic": 0
		}, {
			"label": "Majkin, Marshall Islands - Majkin Airport (MJE)",
			"value": "MJE",
			"domestic": 0
		}, {
			"label": "Mosjoen, Norway - Kjaerstad (MJF)",
			"value": "MJF",
			"domestic": 0
		}, {
			"label": "Mitiga, Tripoli, Libya - Mitiga, Tripoli Airport (MJI)",
			"value": "MJI",
			"domestic": 0
		}, {
			"label": "Monkey Mia, Australia - Shark Bay Airport (MJK)",
			"value": "MJK",
			"domestic": 0
		}, {
			"label": "Mouila, Gabon - Mouila Airport (MJL)",
			"value": "MJL",
			"domestic": 0
		}, {
			"label": "Mbuji Mayi, Democratic Republic of Congo - Mbuji Mayi Airport (MJM)",
			"value": "MJM",
			"domestic": 0
		}, {
			"label": "Majunga, Madagascar - Amborovy Airport (MJN)",
			"value": "MJN",
			"domestic": 0
		}, {
			"label": "Mitilini, Greece - Mytilene (MJT)",
			"value": "MJT",
			"domestic": 0
		}, {
			"label": "Murcia, Spain - San Javier (MJV)",
			"value": "MJV",
			"domestic": 0
		}, {
			"label": "Mirnyj, Russia - Mirnyj Airport (MJZ)",
			"value": "MJZ",
			"domestic": 0
		}, {
			"label": "Milwaukee, United States - Milwaukee (MKE)",
			"value": "MKE",
			"domestic": 1
		}, {
			"label": "Muskegon, United States - Muskegon Airport (MKG)",
			"value": "MKG",
			"domestic": 1
		}, {
			"label": "Ho\'olehua, United States - Molokai Airport (MKK)",
			"value": "MKK",
			"domestic": 1
		}, {
			"label": "Jackson, United States - McKellar Fld (MKL)",
			"value": "MKL",
			"domestic": 1
		}, {
			"label": "Mukah, Malaysia - Mukah Airport (MKM)",
			"value": "MKM",
			"domestic": 0
		}, {
			"label": "Muskogee, United States - Davis Field (MKO)",
			"value": "MKO",
			"domestic": 1
		}, {
			"label": "Makemo, French Polynesia and Tahiti - Makemo Airport (MKP)",
			"value": "MKP",
			"domestic": 0
		}, {
			"label": "Merauke, Indonesia - Mopah Airport (MKQ)",
			"value": "MKQ",
			"domestic": 0
		}, {
			"label": "Meekatharra, Australia - Meekatharra Airport (MKR)",
			"value": "MKR",
			"domestic": 0
		}, {
			"label": "Mankato, United States - Mankato Mncpl (MKT)",
			"value": "MKT",
			"domestic": 1
		}, {
			"label": "Makokou, Gabon - Makokou Airport (MKU)",
			"value": "MKU",
			"domestic": 0
		}, {
			"label": "Manokwari, Indonesia - Rendani Airport (MKW)",
			"value": "MKW",
			"domestic": 0
		}, {
			"label": "Mackay, Australia - Mackay (MKY)",
			"value": "MKY",
			"domestic": 0
		}, {
			"label": "Malacca, Malaysia - Batu Berendum (MKZ)",
			"value": "MKZ",
			"domestic": 0
		}, {
			"label": "Valletta, Malta - Malta Intl (MLA)",
			"value": "MLA",
			"domestic": 0
		}, {
			"label": "Melbourne, United States - Melbourne International Airport (MLB)",
			"value": "MLB",
			"domestic": 1
		}, {
			"label": "McAlester, United States - McAlester (MLC)",
			"value": "MLC",
			"domestic": 1
		}, {
			"label": "Male, Republic of Maldives - Male International Airport (MLE)",
			"value": "MLE",
			"domestic": 0
		}, {
			"label": "Malang, Indonesia - Malang Airport (MLG)",
			"value": "MLG",
			"domestic": 0
		}, {
			"label": "Mulhouse, France - Mulhouse EuroAirport (MLH)",
			"value": "MLH",
			"domestic": 0
		}, {
			"label": "Moline, United States - Moline (MLI)",
			"value": "MLI",
			"domestic": 1
		}, {
			"label": "Morelia, Mexico - Morelia Airport (MLM)",
			"value": "MLM",
			"domestic": 0
		}, {
			"label": "Melilla, Spain - Melilla Airport (MLN)",
			"value": "MLN",
			"domestic": 0
		}, {
			"label": "Milos, Greece - Milos (MLO)",
			"value": "MLO",
			"domestic": 0
		}, {
			"label": "Miles City, United States - Miles City Mncpl (MLS)",
			"value": "MLS",
			"domestic": 1
		}, {
			"label": "Monroe, United States - Monroe Municipal Airport (MLU)",
			"value": "MLU",
			"domestic": 1
		}, {
			"label": "Monrovia, Liberia - Sprigg Payne (MLW)",
			"value": "MLW",
			"domestic": 0
		}, {
			"label": "Malatya, Turkey - Malatya Airport (MLX)",
			"value": "MLX",
			"domestic": 0
		}, {
			"label": "Manley Hot Springs, United States - Manley Hot Springs (MLY)",
			"value": "MLY",
			"domestic": 1
		}, {
			"label": "Malmo, Sweden - All Airports (MMA)",
			"value": "MMA",
			"domestic": 0
		}, {
			"label": "Memanbetsu, Japan - Memanbetsu Airport (MMB)",
			"value": "MMB",
			"domestic": 0
		}, {
			"label": "Teesside, United Kingdom - Durham Tees Valley (MME)",
			"value": "MME",
			"domestic": 0
		}, {
			"label": "Mount Magnet, Australia - Mount Magnet Airport (MMG)",
			"value": "MMG",
			"domestic": 0
		}, {
			"label": "Mammoth Lakes, United States - Mammoth Lakes (MMH)",
			"value": "MMH",
			"domestic": 1
		}, {
			"label": "Matsumoto, Japan - Matsumoto (MMJ)",
			"value": "MMJ",
			"domestic": 0
		}, {
			"label": "Murmansk, Russia - Murmansk (MMK)",
			"value": "MMK",
			"domestic": 0
		}, {
			"label": "Marshall, United States - Mnpl-Ryan Fld (MML)",
			"value": "MML",
			"domestic": 1
		}, {
			"label": "Morristown, United States - Morristown Mncpl (MMU)",
			"value": "MMU",
			"domestic": 1
		}, {
			"label": "Malmo, Sweden - Sturup (MMX)",
			"value": "MMX",
			"domestic": 0
		}, {
			"label": "Miyako, Japan - Hirara Airport (MMY)",
			"value": "MMY",
			"domestic": 0
		}, {
			"label": "Mana Island, Fiji - Mana Island Airstrip (MNF)",
			"value": "MNF",
			"domestic": 0
		}, {
			"label": "Maningrida, Australia - Maningrida Airport (MNG)",
			"value": "MNG",
			"domestic": 0
		}, {
			"label": "Olveston, Montserrat - Gerald\'s (MNI)",
			"value": "MNI",
			"domestic": 0
		}, {
			"label": "Mananjary, Madagascar - Mananjary Airport (MNJ)",
			"value": "MNJ",
			"domestic": 0
		}, {
			"label": "Manila, Philippines - Ninoy Aquino Intl (MNL)",
			"value": "MNL",
			"domestic": 0
		}, {
			"label": "Menominee, United States - Menominee Cty (MNM)",
			"value": "MNM",
			"domestic": 1
		}, {
			"label": "Minto, United States - Minto Airport (MNT)",
			"value": "MNT",
			"domestic": 1
		}, {
			"label": "Maulmyine, Myanmar (Burma) - Maulmyine Airport (MNU)",
			"value": "MNU",
			"domestic": 0
		}, {
			"label": "Manicore, Brazil - Manicore Airport (MNX)",
			"value": "MNX",
			"domestic": 0
		}, {
			"label": "Mono, Solomon Islands - Mono Airport (MNY)",
			"value": "MNY",
			"domestic": 0
		}, {
			"label": "Manassas, United States - Manassas/Davis Field (MNZ)",
			"value": "MNZ",
			"domestic": 1
		}, {
			"label": "Moa, Cuba - Orestes Acosta Airport (MOA)",
			"value": "MOA",
			"domestic": 0
		}, {
			"label": "Mobile, United States - Mobile Municipal Airport (MOB)",
			"value": "MOB",
			"domestic": 1
		}, {
			"label": "Montes Claros, Brazil - Montes Claros Airport (MOC)",
			"value": "MOC",
			"domestic": 0
		}, {
			"label": "Modesto, United States - Modesto Municipal Airport (MOD)",
			"value": "MOD",
			"domestic": 1
		}, {
			"label": "Maumere, Indonesia - Waioti Airport (MOF)",
			"value": "MOF",
			"domestic": 0
		}, {
			"label": "Mong Hsat, Myanmar (Burma) - Mong Hsat Airport (MOG)",
			"value": "MOG",
			"domestic": 0
		}, {
			"label": "Mitiaro Island, Cook Islands - Mitiaro Island Airport (MOI)",
			"value": "MOI",
			"domestic": 0
		}, {
			"label": "Molde, Norway - Aro (MOL)",
			"value": "MOL",
			"domestic": 0
		}, {
			"label": "Mount Cook, New Zealand - Mount Cook (MON)",
			"value": "MON",
			"domestic": 0
		}, {
			"label": "Moomba, Australia - Moomba Airport (MOO)",
			"value": "MOO",
			"domestic": 0
		}, {
			"label": "Morondava, Madagascar - Morondava Airport (MOQ)",
			"value": "MOQ",
			"domestic": 0
		}, {
			"label": "Minot, United States - Minot International Airport (MOT)",
			"value": "MOT",
			"domestic": 1
		}, {
			"label": "Mountain Village, United States - Mountain Village Airport (MOU)",
			"value": "MOU",
			"domestic": 1
		}, {
			"label": "Moranbah, Australia - Moranbah Airport (MOV)",
			"value": "MOV",
			"domestic": 0
		}, {
			"label": "Moscow, Russia - All Airports (MOW)",
			"value": "MOW",
			"domestic": 0
		}, {
			"label": "Moorea, French Polynesia and Tahiti - Temae (MOZ)",
			"value": "MOZ",
			"domestic": 0
		}, {
			"label": "Mpacha, Namibia - Mpacha Airport (MPA)",
			"value": "MPA",
			"domestic": 0
		}, {
			"label": "Caticlan, Philippines - Malay Airport (MPH)",
			"value": "MPH",
			"domestic": 0
		}, {
			"label": "Mamitupo, Panama - Mamitupo Airport (MPI)",
			"value": "MPI",
			"domestic": 0
		}, {
			"label": "Mokpo, South Korea - Mokpo Airport (MPK)",
			"value": "MPK",
			"domestic": 0
		}, {
			"label": "Montpellier, France - Mediterranee (MPL)",
			"value": "MPL",
			"domestic": 0
		}, {
			"label": "Maputo, Mozambique - Maputo Intl (MPM)",
			"value": "MPM",
			"domestic": 0
		}, {
			"label": "Mount Pleasant, Falkland Islands (U.K) - Mount Pleasant Airport (MPN)",
			"value": "MPN",
			"domestic": 0
		}, {
			"label": "Mulatupo, Panama - Mulatupo Airport (MPP)",
			"value": "MPP",
			"domestic": 0
		}, {
			"label": "Montpelier, United States - Edward F Knapp St (MPV)",
			"value": "MPV",
			"domestic": 1
		}, {
			"label": "Mariupol, Ukraine - Mariupol Airport (MPW)",
			"value": "MPW",
			"domestic": 0
		}, {
			"label": "Magnitogorsk, Russia - Magnitogorsk Airport (MQF)",
			"value": "MQF",
			"domestic": 0
		}, {
			"label": "Mildura, Australia - Mildura (MQL)",
			"value": "MQL",
			"domestic": 0
		}, {
			"label": "Mardin, Turkey - Mardin Airport (MQM)",
			"value": "MQM",
			"domestic": 0
		}, {
			"label": "Mo-i-Rana, Norway - Rossvoll (MQN)",
			"value": "MQN",
			"domestic": 0
		}, {
			"label": "Nelspruit, South Africa - Kruger Mpumalanga International Airport (MQP)",
			"value": "MQP",
			"domestic": 0
		}, {
			"label": "Mustique Island, St. Vincent and the Grenadines - Mustique (MQS)",
			"value": "MQS",
			"domestic": 0
		}, {
			"label": "Marquette, United States - Marquette (MQT)",
			"value": "MQT",
			"domestic": 1
		}, {
			"label": "Makale, Ethiopia - Makale Airport (MQX)",
			"value": "MQX",
			"domestic": 0
		}, {
			"label": "Margaret River, Australia - Margaret River (MQZ)",
			"value": "MQZ",
			"domestic": 0
		}, {
			"label": "Misurata, Libya - Misurata Airport (MRA)",
			"value": "MRA",
			"domestic": 0
		}, {
			"label": "Merida, Venezuela - Alberto Carnevalli (MRD)",
			"value": "MRD",
			"domestic": 0
		}, {
			"label": "Mara Lodges, Kenya - Mara Lodges Airport (MRE)",
			"value": "MRE",
			"domestic": 0
		}, {
			"label": "Marco Island, United States - Marco Island (MRK)",
			"value": "MRK",
			"domestic": 1
		}, {
			"label": "Masterton, New Zealand - Masterton (MRO)",
			"value": "MRO",
			"domestic": 0
		}, {
			"label": "Marseille, France - Marseille Airport (MRS)",
			"value": "MRS",
			"domestic": 0
		}, {
			"label": "Mauritius, Mauritius - SSR Intl (MRU)",
			"value": "MRU",
			"domestic": 0
		}, {
			"label": "Mineralnye Vody, Russia - Mineralnye Vody Airport (MRV)",
			"value": "MRV",
			"domestic": 0
		}, {
			"label": "Bandar Mahshahr, Iran - Mahshahr Airport (MRX)",
			"value": "MRX",
			"domestic": 0
		}, {
			"label": "Monterey, United States - Monterey Peninsula Airport (MRY)",
			"value": "MRY",
			"domestic": 1
		}, {
			"label": "Moree, Australia - Moree (MRZ)",
			"value": "MRZ",
			"domestic": 0
		}, {
			"label": "Muskrat Dam, Canada - Muskrat Dam Airport (MSA)",
			"value": "MSA",
			"domestic": 0
		}, {
			"label": "Misawa, Japan - Misawa Airport (MSJ)",
			"value": "MSJ",
			"domestic": 0
		}, {
			"label": "Muscle Shoals, United States - Muscle Shoals Airport (MSL)",
			"value": "MSL",
			"domestic": 1
		}, {
			"label": "Madison, United States - Madison (MSN)",
			"value": "MSN",
			"domestic": 1
		}, {
			"label": "Missoula, United States - Johnson-Bell Field (MSO)",
			"value": "MSO",
			"domestic": 1
		}, {
			"label": "Minneapolis, United States - Minneapolis/St. Paul (MSP)",
			"value": "MSP",
			"domestic": 1
		}, {
			"label": "Minsk, Belarus - Minsk 2 Intl (MSQ)",
			"value": "MSQ",
			"domestic": 0
		}, {
			"label": "Mus, Turkey - Mus Airport (MSR)",
			"value": "MSR",
			"domestic": 0
		}, {
			"label": "Massena, United States - Richard Fld (MSS)",
			"value": "MSS",
			"domestic": 1
		}, {
			"label": "Maastricht, Netherlands - Maastricht (MST)",
			"value": "MST",
			"domestic": 0
		}, {
			"label": "Maseru, Lesotho - Moshoeshoe Intl (MSU)",
			"value": "MSU",
			"domestic": 0
		}, {
			"label": "Monticello, United States - Sullivan Cty Intl (MSV)",
			"value": "MSV",
			"domestic": 1
		}, {
			"label": "Massawa, Eritrea - Massawa Airport (MSW)",
			"value": "MSW",
			"domestic": 0
		}, {
			"label": "New Orleans, United States - New Orleans (MSY)",
			"value": "MSY",
			"domestic": 1
		}, {
			"label": "Namibe, Angola - Namibe Airport (MSZ)",
			"value": "MSZ",
			"domestic": 0
		}, {
			"label": "Mizan Teferi, Ethiopia - Mizan Teferi Airport (MTF)",
			"value": "MTF",
			"domestic": 0
		}, {
			"label": "Marathon, United States - Marathon (MTH)",
			"value": "MTH",
			"domestic": 1
		}, {
			"label": "Montrose, United States - Montrose County Airport (MTJ)",
			"value": "MTJ",
			"domestic": 1
		}, {
			"label": "Maitland, Australia - Maitland (MTL)",
			"value": "MTL",
			"domestic": 0
		}, {
			"label": "Metlakatla, United States - Metlakatla Sea Plane Base (MTM)",
			"value": "MTM",
			"domestic": 1
		}, {
			"label": "Mattoon, United States - Coles Cty Memorial (MTO)",
			"value": "MTO",
			"domestic": 1
		}, {
			"label": "Monteria, Colombia - S. Jeronimo Airport (MTR)",
			"value": "MTR",
			"domestic": 0
		}, {
			"label": "Manzini, Swaziland - Matsapha Intl (MTS)",
			"value": "MTS",
			"domestic": 0
		}, {
			"label": "Minatitlan, Mexico - Minatitlan Airport (MTT)",
			"value": "MTT",
			"domestic": 0
		}, {
			"label": "Mota Lava, Vanuatu - Mota Lava Airport (MTV)",
			"value": "MTV",
			"domestic": 0
		}, {
			"label": "Manitowoc, United States - Manitowoc Mncpl (MTW)",
			"value": "MTW",
			"domestic": 1
		}, {
			"label": "Monterrey, Mexico - Gen Mariano Escobedo Airport (MTY)",
			"value": "MTY",
			"domestic": 0
		}, {
			"label": "Munda, Solomon Islands - Munda Airport (MUA)",
			"value": "MUA",
			"domestic": 0
		}, {
			"label": "Maun, Botswana - Maun (MUB)",
			"value": "MUB",
			"domestic": 0
		}, {
			"label": "Munich, Germany - Franz Josef Strauss Airport (MUC)",
			"value": "MUC",
			"domestic": 0
		}, {
			"label": "Kamuela, United States - Kamuela (MUE)",
			"value": "MUE",
			"domestic": 1
		}, {
			"label": "Mauke Island, Cook Islands - Mauke Island Airport (MUK)",
			"value": "MUK",
			"domestic": 0
		}, {
			"label": "Maturin, Venezuela - Quiriquire (MUN)",
			"value": "MUN",
			"domestic": 0
		}, {
			"label": "Marudi, Malaysia - Marudi Airport (MUR)",
			"value": "MUR",
			"domestic": 0
		}, {
			"label": "Multan, Pakistan - Multan (MUX)",
			"value": "MUX",
			"domestic": 0
		}, {
			"label": "Musoma, Tanzania - Musoma Airport (MUZ)",
			"value": "MUZ",
			"domestic": 0
		}, {
			"label": "Myvatn, Iceland - Myvatn (MVA)",
			"value": "MVA",
			"domestic": 0
		}, {
			"label": "Franceville, Gabon - Franceville/Mvengue (MVB)",
			"value": "MVB",
			"domestic": 0
		}, {
			"label": "Montevideo, Uruguay - Carrasco Airport (MVD)",
			"value": "MVD",
			"domestic": 0
		}, {
			"label": "Stowe, United States - Morrisville/Stowe (MVL)",
			"value": "MVL",
			"domestic": 1
		}, {
			"label": "Mitu, Colombia - Mitu Airport (MVP)",
			"value": "MVP",
			"domestic": 0
		}, {
			"label": "Mucuri, Brazil - Mucuri Airport (MVS)",
			"value": "MVS",
			"domestic": 0
		}, {
			"label": "Mataiva, French Polynesia and Tahiti - Mataiva Airport (MVT)",
			"value": "MVT",
			"domestic": 0
		}, {
			"label": "Martha\'s Vineyard, United States - Martha\'s Vineyard (MVY)",
			"value": "MVY",
			"domestic": 1
		}, {
			"label": "Masvingo, Zimbabwe - Masvingo (MVZ)",
			"value": "MVZ",
			"domestic": 0
		}, {
			"label": "Marion, United States - Williamson Cty (MWA)",
			"value": "MWA",
			"domestic": 1
		}, {
			"label": "Maewo, Vanuatu - Maewo Airport (MWF)",
			"value": "MWF",
			"domestic": 0
		}, {
			"label": "Moses Lake, United States - Grant Cty (MWH)",
			"value": "MWH",
			"domestic": 1
		}, {
			"label": "Maramoni, Papua New Guinea - Maramoni Airport (MWI)",
			"value": "MWI",
			"domestic": 0
		}, {
			"label": "Mountain, Nepal - Mountain Airport (MWP)",
			"value": "MWP",
			"domestic": 0
		}, {
			"label": "Magwe, Myanmar (Burma) - Magwe Airport (MWQ)",
			"value": "MWQ",
			"domestic": 0
		}, {
			"label": "Mwanza, Tanzania - Mwanza Airport (MWZ)",
			"value": "MWZ",
			"domestic": 0
		}, {
			"label": "Monticello, United States - Monticello/San Juan (MXC)",
			"value": "MXC",
			"domestic": 1
		}, {
			"label": "Moro, Papua New Guinea - Moro Airport (MXH)",
			"value": "MXH",
			"domestic": 0
		}, {
			"label": "Mexicali, Mexico - Mexicali Intl (MXL)",
			"value": "MXL",
			"domestic": 0
		}, {
			"label": "Morombe, Madagascar - Morombe Airport (MXM)",
			"value": "MXM",
			"domestic": 0
		}, {
			"label": "Morlaix, France - Morlaix-Ploujean (MXN)",
			"value": "MXN",
			"domestic": 0
		}, {
			"label": "Milan, Italy - Malpensa Airport (MXP)",
			"value": "MXP",
			"domestic": 0
		}, {
			"label": "Maintirano, Madagascar - Maintirano Airport (MXT)",
			"value": "MXT",
			"domestic": 0
		}, {
			"label": "Moron, Mongolia - Moron Airport (MXV)",
			"value": "MXV",
			"domestic": 0
		}, {
			"label": "Mandalgobi, Mongolia - Mandalgobi Airport (MXW)",
			"value": "MXW",
			"domestic": 0
		}, {
			"label": "Mora, Sweden - Mora (MXX)",
			"value": "MXX",
			"domestic": 0
		}, {
			"label": "Mei Xian, China - Mei Xian Airport (MXZ)",
			"value": "MXZ",
			"domestic": 0
		}, {
			"label": "Moruya, Australia - Moruya Airport (MYA)",
			"value": "MYA",
			"domestic": 0
		}, {
			"label": "Malindi, Kenya - Malindi (MYD)",
			"value": "MYD",
			"domestic": 0
		}, {
			"label": "Miyake Jima, Japan - Miyake Jima Airport (MYE)",
			"value": "MYE",
			"domestic": 0
		}, {
			"label": "Mayaguana, Bahamas - Mayaguana Airport (MYG)",
			"value": "MYG",
			"domestic": 0
		}, {
			"label": "Murray Island, Australia - Murray Island Airport (MYI)",
			"value": "MYI",
			"domestic": 0
		}, {
			"label": "Matsuyama, Japan - Matsuyama (MYJ)",
			"value": "MYJ",
			"domestic": 0
		}, {
			"label": "McCall, United States - McCall (MYL)",
			"value": "MYL",
			"domestic": 1
		}, {
			"label": "Mysore, India - Mysore (MYQ)",
			"value": "MYQ",
			"domestic": 0
		}, {
			"label": "Myrtle Beach, United States - Myrtle Beach Air Force Base (MYR)",
			"value": "MYR",
			"domestic": 1
		}, {
			"label": "Myitkyina, Myanmar (Burma) - Myitkyina Airport (MYT)",
			"value": "MYT",
			"domestic": 0
		}, {
			"label": "Mekoryuk, United States - Ellis Field (MYU)",
			"value": "MYU",
			"domestic": 1
		}, {
			"label": "Mtwara, Tanzania - Mtwara (MYW)",
			"value": "MYW",
			"domestic": 0
		}, {
			"label": "Miri, Malaysia - Miri (MYY)",
			"value": "MYY",
			"domestic": 0
		}, {
			"label": "Makung, Taiwan - Makung Airport (MZG)",
			"value": "MZG",
			"domestic": 0
		}, {
			"label": "Mopti, Mali - Mopti (MZI)",
			"value": "MZI",
			"domestic": 0
		}, {
			"label": "Manizales, Colombia - Santaguida Airport (MZL)",
			"value": "MZL",
			"domestic": 0
		}, {
			"label": "Metz, France - Frescaty (MZM)",
			"value": "MZM",
			"domestic": 0
		}, {
			"label": "Manzanillo, Cuba - Sierra Maestra Airport (MZO)",
			"value": "MZO",
			"domestic": 0
		}, {
			"label": "Mazatlan, Mexico - Gen Rafael Buelna Airport (MZT)",
			"value": "MZT",
			"domestic": 0
		}, {
			"label": "Mulu, Malaysia - Mulu Airport (MZV)",
			"value": "MZV",
			"domestic": 0
		}, {
			"label": "Narrabri, Australia - Narrabri (NAA)",
			"value": "NAA",
			"domestic": 0
		}, {
			"label": "Nagpur, India - Sonegaon Airport (NAG)",
			"value": "NAG",
			"domestic": 0
		}, {
			"label": "Nakhichevan, Azerbaijan - Nakhichevan Airport (NAJ)",
			"value": "NAJ",
			"domestic": 0
		}, {
			"label": "Nakhon Ratchasima, Thailand - Nakhon Ratchasima (NAK)",
			"value": "NAK",
			"domestic": 0
		}, {
			"label": "Nalchik, Russia - Nalchik Airport (NAL)",
			"value": "NAL",
			"domestic": 0
		}, {
			"label": "Nadi, Fiji - Nadi Intl (NAN)",
			"value": "NAN",
			"domestic": 0
		}, {
			"label": "Nanchong, China - Nanchong (NAO)",
			"value": "NAO",
			"domestic": 0
		}, {
			"label": "Naples, Italy - Naples Airport (NAP)",
			"value": "NAP",
			"domestic": 0
		}, {
			"label": "Qaanaaq, Greenland - Qaanaaq Airport (NAQ)",
			"value": "NAQ",
			"domestic": 0
		}, {
			"label": "Nassau, Bahamas - Nassau International Airport (NAS)",
			"value": "NAS",
			"domestic": 0
		}, {
			"label": "Natal, Brazil - Augusto Severo (NAT)",
			"value": "NAT",
			"domestic": 0
		}, {
			"label": "Nevsehir, Turkey - Nevsehir (NAV)",
			"value": "NAV",
			"domestic": 0
		}, {
			"label": "Narathiwat, Thailand - Narathiwat Airport (NAW)",
			"value": "NAW",
			"domestic": 0
		}, {
			"label": "Beijing, China - Nanyuan (NAY)",
			"value": "NAY",
			"domestic": 0
		}, {
			"label": "Star Habour, Solomon Islands - Nana Airport (NAZ)",
			"value": "NAZ",
			"domestic": 0
		}, {
			"label": "Nairobi, Kenya - Jomo Kenyatta Intl (NBO)",
			"value": "NBO",
			"domestic": 0
		}, {
			"label": "North Caicos, Turks and Caicos - North Caicos Mncpl (NCA)",
			"value": "NCA",
			"domestic": 0
		}, {
			"label": "Nice, France - Cote DAzur Airport (NCE)",
			"value": "NCE",
			"domestic": 0
		}, {
			"label": "Newcastle, United Kingdom - Newcastle (NCL)",
			"value": "NCL",
			"domestic": 0
		}, {
			"label": "New Chenega, United States - New Chenega Airport (NCN)",
			"value": "NCN",
			"domestic": 1
		}, {
			"label": "Newcastle, South Africa - Newcastle (NCS)",
			"value": "NCS",
			"domestic": 0
		}, {
			"label": "Nukus, Uzbekistan - Nukus Airport (NCU)",
			"value": "NCU",
			"domestic": 0
		}, {
			"label": "Annecy, France - Annecy-Meythet (NCY)",
			"value": "NCY",
			"domestic": 0
		}, {
			"label": "Nouadhibou, Mauritania - Nouadhibou Airport (NDB)",
			"value": "NDB",
			"domestic": 0
		}, {
			"label": "Qiqihar, China - Qiqihar (NDG)",
			"value": "NDG",
			"domestic": 0
		}, {
			"label": "N\'Djamena, Chad - N\'Djamena (NDJ)",
			"value": "NDJ",
			"domestic": 0
		}, {
			"label": "Namdrik Island, Marshall Islands - Namdrik Island Airport (NDK)",
			"value": "NDK",
			"domestic": 0
		}, {
			"label": "Nador, Morocco - Nador Airport (NDR)",
			"value": "NDR",
			"domestic": 0
		}, {
			"label": "Sanday, United Kingdom - Sanday Airport (NDY)",
			"value": "NDY",
			"domestic": 0
		}, {
			"label": "Necochea, Argentina - Necochea Airport (NEC)",
			"value": "NEC",
			"domestic": 0
		}, {
			"label": "Negril, Jamaica - Negril (NEG)",
			"value": "NEG",
			"domestic": 0
		}, {
			"label": "Neryungri, Russia - Neryungri Airport (NER)",
			"value": "NER",
			"domestic": 0
		}, {
			"label": "Nevis, St. Kitts and Nevis - Newcastle Airport (NEV)",
			"value": "NEV",
			"domestic": 0
		}, {
			"label": "Niuafo\'ou, Tonga - Mata\'aho Airport (NFO)",
			"value": "NFO",
			"domestic": 0
		}, {
			"label": "Young, Australia - Young (NGA)",
			"value": "NGA",
			"domestic": 0
		}, {
			"label": "Ningbo, China - Ningbo (NGB)",
			"value": "NGB",
			"domestic": 0
		}, {
			"label": "Anegada Island, British Virgin Islands - Anegada Island Mncpl (NGD)",
			"value": "NGD",
			"domestic": 0
		}, {
			"label": "Ngau Island, Fiji - Ngau Island Airport (NGI)",
			"value": "NGI",
			"domestic": 0
		}, {
			"label": "Nagoya, Japan - Central Japan Intl (NGO)",
			"value": "NGO",
			"domestic": 0
		}, {
			"label": "Nagasaki, Japan - Nagasaki (NGS)",
			"value": "NGS",
			"domestic": 0
		}, {
			"label": "Nha Trang, Vietnam - Nha-Trang (NHA)",
			"value": "NHA",
			"domestic": 0
		}, {
			"label": "Nuku Hiva, French Polynesia and Tahiti - Nuku Hiva Airport (NHV)",
			"value": "NHV",
			"domestic": 0
		}, {
			"label": "Nikolai, United States - Nikolai Airport (NIB)",
			"value": "NIB",
			"domestic": 1
		}, {
			"label": "Niamey, Niger - Niamey (NIM)",
			"value": "NIM",
			"domestic": 0
		}, {
			"label": "Nizhnevartovsk, Russia - Nizhnevartovsk Airport (NJC)",
			"value": "NJC",
			"domestic": 0
		}, {
			"label": "Nouakchott, Mauritania - Nouakchott (NKC)",
			"value": "NKC",
			"domestic": 0
		}, {
			"label": "Nanjing, China - Nanjing (NKG)",
			"value": "NKG",
			"domestic": 0
		}, {
			"label": "Naukiti, United States - Naukiti Airport (NKI)",
			"value": "NKI",
			"domestic": 1
		}, {
			"label": "Komaki, Japan - Komaki (NKM)",
			"value": "NKM",
			"domestic": 0
		}, {
			"label": "Nankina, Papua New Guinea - Nankina Airport (NKN)",
			"value": "NKN",
			"domestic": 0
		}, {
			"label": "Ndola, Zambia - Ndola (NLA)",
			"value": "NLA",
			"domestic": 0
		}, {
			"label": "Nuevo Laredo, Mexico - Quetzalcoatl Intl (NLD)",
			"value": "NLD",
			"domestic": 0
		}, {
			"label": "Niles, United States - Jerry Tyler Memorial (NLE)",
			"value": "NLE",
			"domestic": 1
		}, {
			"label": "Darnley Island, Australia - Darnley Island Airport (NLF)",
			"value": "NLF",
			"domestic": 0
		}, {
			"label": "Norfolk Island, Norfolk Islands - Norfolk Island Intl (NLK)",
			"value": "NLK",
			"domestic": 0
		}, {
			"label": "Namangan, Uzbekistan - Namangan Airport (NMA)",
			"value": "NMA",
			"domestic": 0
		}, {
			"label": "Nightmute, United States - Nightmute Airport (NME)",
			"value": "NME",
			"domestic": 1
		}, {
			"label": "Namsang, Myanmar (Burma) - Namsang Airport (NMS)",
			"value": "NMS",
			"domestic": 0
		}, {
			"label": "Santa Ana, Solomon Islands - Santa Ana Airport (NNB)",
			"value": "NNB",
			"domestic": 0
		}, {
			"label": "Nanning, China - Nanning (NNG)",
			"value": "NNG",
			"domestic": 0
		}, {
			"label": "Naryan-Mar, Russia - Naryan-Mar Airport (NNM)",
			"value": "NNM",
			"domestic": 0
		}, {
			"label": "Nan, Thailand - Nan Airport (NNT)",
			"value": "NNT",
			"domestic": 0
		}, {
			"label": "Nunukan, Indonesia - Nunukan Airport (NNX)",
			"value": "NNX",
			"domestic": 0
		}, {
			"label": "Nanyang, China - Nanyang Airport (NNY)",
			"value": "NNY",
			"domestic": 0
		}, {
			"label": "Nowra, Australia - Nowra (NOA)",
			"value": "NOA",
			"domestic": 0
		}, {
			"label": "Nosara Beach, Costa Rica - Nosara Beach Airport (NOB)",
			"value": "NOB",
			"domestic": 0
		}, {
			"label": "Knock, Ireland - Ireland West Knock Airport (NOC)",
			"value": "NOC",
			"domestic": 0
		}, {
			"label": "Nogales, Mexico - Nogales Airport (NOG)",
			"value": "NOG",
			"domestic": 0
		}, {
			"label": "Nojabrxsk, Russia - Nojabrxsk Airport (NOJ)",
			"value": "NOJ",
			"domestic": 0
		}, {
			"label": "Nossi-be, Madagascar - Fascene Airport (NOS)",
			"value": "NOS",
			"domestic": 0
		}, {
			"label": "Novato, United States - Gnoss Field (NOT)",
			"value": "NOT",
			"domestic": 1
		}, {
			"label": "Noumea, New Caledonia - Tontouta (NOU)",
			"value": "NOU",
			"domestic": 0
		}, {
			"label": "Huambo, Angola - Huambo Airport (NOV)",
			"value": "NOV",
			"domestic": 0
		}, {
			"label": "Novokuznetsk, Russia - Novokuznetsk Airport (NOZ)",
			"value": "NOZ",
			"domestic": 0
		}, {
			"label": "Napier, New Zealand - Hawkes Bay (NPE)",
			"value": "NPE",
			"domestic": 0
		}, {
			"label": "New Plymouth, New Zealand - New Plymouth (NPL)",
			"value": "NPL",
			"domestic": 0
		}, {
			"label": "Newport, OR, United States - State (NPT)",
			"value": "NPT",
			"domestic": 1
		}, {
			"label": "Neuquen, Argentina - Neuquen Airport (NQN)",
			"value": "NQN",
			"domestic": 0
		}, {
			"label": "Nottingham, United Kingdom - Nottingham (NQT)",
			"value": "NQT",
			"domestic": 0
		}, {
			"label": "Nuqui, Colombia - Nuqui Airport (NQU)",
			"value": "NQU",
			"domestic": 0
		}, {
			"label": "Newquay, United Kingdom - Newquay Civil (NQY)",
			"value": "NQY",
			"domestic": 0
		}, {
			"label": "Narrandera, Australia - Narrandera (NRA)",
			"value": "NRA",
			"domestic": 0
		}, {
			"label": "Norderney, Germany - Norderney (NRD)",
			"value": "NRD",
			"domestic": 0
		}, {
			"label": "Norrkoping, Sweden - Kungsangen (NRK)",
			"value": "NRK",
			"domestic": 0
		}, {
			"label": "North Ronaldsay, United Kingdom - North Ronaldsay Airport (NRL)",
			"value": "NRL",
			"domestic": 0
		}, {
			"label": "Tokyo, Japan - Narita (NRT)",
			"value": "NRT",
			"domestic": 0
		}, {
			"label": "Now Shahr, Iran - Now Shahr Airport (NSH)",
			"value": "NSH",
			"domestic": 0
		}, {
			"label": "Yaounde, Cameroon - Nsimalen (NSI)",
			"value": "NSI",
			"domestic": 0
		}, {
			"label": "Noril\'sk, Russia - Noril\'sk Airport (NSK)",
			"value": "NSK",
			"domestic": 0
		}, {
			"label": "Nelson, New Zealand - Nelson (NSN)",
			"value": "NSN",
			"domestic": 0
		}, {
			"label": "Scone, Australia - Scone (NSO)",
			"value": "NSO",
			"domestic": 0
		}, {
			"label": "Nakhon Si Thammarat, Thailand - Nakhon Si Thammarat Airport (NST)",
			"value": "NST",
			"domestic": 0
		}, {
			"label": "Nantes, France - Nantes Atlantique (NTE)",
			"value": "NTE",
			"domestic": 0
		}, {
			"label": "Nantong, China - Nantong (NTG)",
			"value": "NTG",
			"domestic": 0
		}, {
			"label": "Newcastle, Australia - Williamtown (NTL)",
			"value": "NTL",
			"domestic": 0
		}, {
			"label": "Normanton, Australia - Normanton Airport (NTN)",
			"value": "NTN",
			"domestic": 0
		}, {
			"label": "Wajima, Japan - Noto Airport (NTQ)",
			"value": "NTQ",
			"domestic": 0
		}, {
			"label": "Niuatoputapu, Tonga - Kuini Lavenia Airport (NTT)",
			"value": "NTT",
			"domestic": 0
		}, {
			"label": "Sun City, South Africa - Pilansberg (NTY)",
			"value": "NTY",
			"domestic": 0
		}, {
			"label": "Numbulwar, Australia - Numbulwar Airport (NUB)",
			"value": "NUB",
			"domestic": 0
		}, {
			"label": "Nuremberg, Germany - Nuremberg (NUE)",
			"value": "NUE",
			"domestic": 0
		}, {
			"label": "Nuiqsut, United States - Nuiqsut Airport (NUI)",
			"value": "NUI",
			"domestic": 1
		}, {
			"label": "Nulato, United States - Nulato Airport (NUL)",
			"value": "NUL",
			"domestic": 1
		}, {
			"label": "Nunapitchuk, United States - Nunapitchuk Airport (NUP)",
			"value": "NUP",
			"domestic": 1
		}, {
			"label": "Norsup, Vanuatu - Norsup Airport (NUS)",
			"value": "NUS",
			"domestic": 0
		}, {
			"label": "Nakuru, Kenya - Nakuru (NUU)",
			"value": "NUU",
			"domestic": 0
		}, {
			"label": "Novyj Urengoj, Russia - Novyj Urengoj Airport (NUX)",
			"value": "NUX",
			"domestic": 0
		}, {
			"label": "Neiva, Colombia - La Marguita Airport (NVA)",
			"value": "NVA",
			"domestic": 0
		}, {
			"label": "Navoi, Uzbekistan - Navoi Airport (NVI)",
			"value": "NVI",
			"domestic": 0
		}, {
			"label": "Narvik, Norway - Framnes (NVK)",
			"value": "NVK",
			"domestic": 0
		}, {
			"label": "Nevers, France - Fourchambault (NVS)",
			"value": "NVS",
			"domestic": 0
		}, {
			"label": "Navegantes, Brazil - Navegantes Airport (NVT)",
			"value": "NVT",
			"domestic": 0
		}, {
			"label": "Moheli, Comoros - Moheli Airport (NWA)",
			"value": "NWA",
			"domestic": 0
		}, {
			"label": "Norwich, United Kingdom - Norwich (NWI)",
			"value": "NWI",
			"domestic": 0
		}, {
			"label": "New York City, United States - All Airports (NYC)",
			"value": "NYC",
			"domestic": 1
		}, {
			"label": "Nyeri, Kenya - Nyeri (NYE)",
			"value": "NYE",
			"domestic": 0
		}, {
			"label": "Nanyuki, Kenya - Nanyuki (NYK)",
			"value": "NYK",
			"domestic": 0
		}, {
			"label": "Nadym, Russia - Nadym Airport (NYM)",
			"value": "NYM",
			"domestic": 0
		}, {
			"label": "Nyngan, Australia - Nyngan (NYN)",
			"value": "NYN",
			"domestic": 0
		}, {
			"label": "Stockholm, Sweden - Skavsta (NYO)",
			"value": "NYO",
			"domestic": 0
		}, {
			"label": "Nyaung-u, Myanmar (Burma) - Nyaung-u Airport (NYU)",
			"value": "NYU",
			"domestic": 0
		}, {
			"label": "Manzhouli, China - Manzhouli Airport (NZH)",
			"value": "NZH",
			"domestic": 0
		}, {
			"label": "Orange, Australia - Springhill (OAG)",
			"value": "OAG",
			"domestic": 0
		}, {
			"label": "Oakland, United States - Oakland (OAK)",
			"value": "OAK",
			"domestic": 1
		}, {
			"label": "Oamaru, New Zealand - Oamaru Airport (OAM)",
			"value": "OAM",
			"domestic": 0
		}, {
			"label": "Oaxaca, Mexico - Xoxocotlan Airport (OAX)",
			"value": "OAX",
			"domestic": 0
		}, {
			"label": "Obihiro, Japan - Obihiro (OBO)",
			"value": "OBO",
			"domestic": 0
		}, {
			"label": "Kobuk, United States - Kobuk/Wien Airport (OBU)",
			"value": "OBU",
			"domestic": 1
		}, {
			"label": "Ittoqqortoormiit, Greenland - Ittoqqortoormiit Airport (OBY)",
			"value": "OBY",
			"domestic": 0
		}, {
			"label": "Coca, Ecuador - Coca (OCC)",
			"value": "OCC",
			"domestic": 0
		}, {
			"label": "Ocean City, United States - Ocean City (OCE)",
			"value": "OCE",
			"domestic": 1
		}, {
			"label": "Ocala, United States - Jim Taylor Fld (OCF)",
			"value": "OCF",
			"domestic": 1
		}, {
			"label": "Nacogdoches, United States - Nacogdoches Mncpl (OCH)",
			"value": "OCH",
			"domestic": 1
		}, {
			"label": "Ocho Rios, Jamaica - Boscobel (OCJ)",
			"value": "OCJ",
			"domestic": 0
		}, {
			"label": "Oceanside, United States - Oceanside (OCN)",
			"value": "OCN",
			"domestic": 1
		}, {
			"label": "Cordoba, Spain - Cordoba (ODB)",
			"value": "ODB",
			"domestic": 0
		}, {
			"label": "Odense, Denmark - Odense (ODE)",
			"value": "ODE",
			"domestic": 0
		}, {
			"label": "Long Seridan, Malaysia - Long Seridan Airport (ODN)",
			"value": "ODN",
			"domestic": 0
		}, {
			"label": "Odessa, Ukraine - Central (ODS)",
			"value": "ODS",
			"domestic": 0
		}, {
			"label": "Oak Harbor, United States - Oak Harbor (ODW)",
			"value": "ODW",
			"domestic": 1
		}, {
			"label": "Oudomxay, Laos - Oudomxay Airport (ODY)",
			"value": "ODY",
			"domestic": 0
		}, {
			"label": "Orel, Russia - Orel (OEL)",
			"value": "OEL",
			"domestic": 0
		}, {
			"label": "Ornskoldsvik, Sweden - Ornskoldsvik (OER)",
			"value": "OER",
			"domestic": 0
		}, {
			"label": "San Antonio Oeste, Argentina - San Antonio Oeste Airport (OES)",
			"value": "OES",
			"domestic": 0
		}, {
			"label": "Ogallala, United States - Searle Fld (OGA)",
			"value": "OGA",
			"domestic": 1
		}, {
			"label": "Ogden, United States - Ogden Mncpl (OGD)",
			"value": "OGD",
			"domestic": 1
		}, {
			"label": "Kahului, United States - Kahului Airport (OGG)",
			"value": "OGG",
			"domestic": 1
		}, {
			"label": "Yonaguni, Japan - Yonaguni Airport (OGN)",
			"value": "OGN",
			"domestic": 0
		}, {
			"label": "Ogdensburg, United States - Ogdensburg Intl (OGS)",
			"value": "OGS",
			"domestic": 1
		}, {
			"label": "Ouargla, Algeria - Ain Beida Airport (OGX)",
			"value": "OGX",
			"domestic": 0
		}, {
			"label": "Vladikavkaz, Russia - Vladikavkaz Airport (OGZ)",
			"value": "OGZ",
			"domestic": 0
		}, {
			"label": "Ohrid, Macedonia - Ohrid (OHD)",
			"value": "OHD",
			"domestic": 0
		}, {
			"label": "Okhotsk, Russia - Okhotsk Airport (OHO)",
			"value": "OHO",
			"domestic": 0
		}, {
			"label": "Oshima Island, Japan - Oshima (OIM)",
			"value": "OIM",
			"domestic": 0
		}, {
			"label": "Oita, Japan - Oita (OIT)",
			"value": "OIT",
			"domestic": 0
		}, {
			"label": "Naha, Japan - Naha Fld (OKA)",
			"value": "OKA",
			"domestic": 0
		}, {
			"label": "Oklahoma City, United States - Oklahoma City (OKC)",
			"value": "OKC",
			"domestic": 1
		}, {
			"label": "Sapporo, Japan - Okadama (OKD)",
			"value": "OKD",
			"domestic": 0
		}, {
			"label": "Okino Erabu, Japan - Okino Erabu Airport (OKE)",
			"value": "OKE",
			"domestic": 0
		}, {
			"label": "Oki Island, Japan - Oki Island Airport (OKI)",
			"value": "OKI",
			"domestic": 0
		}, {
			"label": "Okayama, Japan - Okayama (OKJ)",
			"value": "OKJ",
			"domestic": 0
		}, {
			"label": "Kokomo, United States - Kokomo Mncpl (OKK)",
			"value": "OKK",
			"domestic": 1
		}, {
			"label": "Yorke Island, Australia - Yorke Island Airport (OKR)",
			"value": "OKR",
			"domestic": 0
		}, {
			"label": "Oakey, Australia - Oakey Airport (OKY)",
			"value": "OKY",
			"domestic": 0
		}, {
			"label": "Orland, Norway - Orland Airport (OLA)",
			"value": "OLA",
			"domestic": 0
		}, {
			"label": "Olbia, Italy - Costa Smeralda (OLB)",
			"value": "OLB",
			"domestic": 0
		}, {
			"label": "Wolf Point, United States - Wolf Point Intl (OLF)",
			"value": "OLF",
			"domestic": 1
		}, {
			"label": "Old Harbor, United States - Old Harbor Sea Plane Base (OLH)",
			"value": "OLH",
			"domestic": 1
		}, {
			"label": "Olpoi, Vanuatu - West Cost Santo Airport (OLJ)",
			"value": "OLJ",
			"domestic": 0
		}, {
			"label": "Olympia, United States - Olympia (OLM)",
			"value": "OLM",
			"domestic": 1
		}, {
			"label": "Olympic Dam, Australia - Olympic Dam Airport (OLP)",
			"value": "OLP",
			"domestic": 0
		}, {
			"label": "Nogales, United States - Nogales Intl (OLS)",
			"value": "OLS",
			"domestic": 1
		}, {
			"label": "Olney, United States - Olney-Noble (OLY)",
			"value": "OLY",
			"domestic": 1
		}, {
			"label": "Omaha, United States - Omaha (OMA)",
			"value": "OMA",
			"domestic": 1
		}, {
			"label": "Oranjemund, Namibia - Oranjemund Airport (OMD)",
			"value": "OMD",
			"domestic": 0
		}, {
			"label": "Nome, United States - Nome Airport (OME)",
			"value": "OME",
			"domestic": 1
		}, {
			"label": "Urmieh, Iran - Umieh Airport (OMH)",
			"value": "OMH",
			"domestic": 0
		}, {
			"label": "Omak, United States - Omak Mncpl (OMK)",
			"value": "OMK",
			"domestic": 1
		}, {
			"label": "Mostar, Bosnia and Herzegovina - Mostar Airport (OMO)",
			"value": "OMO",
			"domestic": 0
		}, {
			"label": "Oradea, Romania - Oradea (OMR)",
			"value": "OMR",
			"domestic": 0
		}, {
			"label": "Omsk, Russia - Omsk Airport (OMS)",
			"value": "OMS",
			"domestic": 0
		}, {
			"label": "Ondangwa, Namibia - Ondangwa Airport (OND)",
			"value": "OND",
			"domestic": 0
		}, {
			"label": "Mornington, Australia - Mornington Airport (ONG)",
			"value": "ONG",
			"domestic": 0
		}, {
			"label": "Oneonta, United States - Oneonta Mncpl (ONH)",
			"value": "ONH",
			"domestic": 1
		}, {
			"label": "Odate Noshiro, Japan - Odate Noshiro Airport (ONJ)",
			"value": "ONJ",
			"domestic": 0
		}, {
			"label": "O\'Neill, United States - O\'Neill Mncpl (ONL)",
			"value": "ONL",
			"domestic": 1
		}, {
			"label": "Newport, United States - Newport Mncpl (ONP)",
			"value": "ONP",
			"domestic": 1
		}, {
			"label": "Ontario, United States - Ontario (ONT)",
			"value": "ONT",
			"domestic": 1
		}, {
			"label": "Colon, Panama - Colon (ONX)",
			"value": "ONX",
			"domestic": 0
		}, {
			"label": "Toksook Bay, United States - Toksook Bay Airport (OOK)",
			"value": "OOK",
			"domestic": 1
		}, {
			"label": "Coolangatta, Australia - Gold Coast (OOL)",
			"value": "OOL",
			"domestic": 0
		}, {
			"label": "Cooma, Australia - Cooma (OOM)",
			"value": "OOM",
			"domestic": 0
		}, {
			"label": "Porto, Portugal - Dr Fran Carneiro (OPO)",
			"value": "OPO",
			"domestic": 0
		}, {
			"label": "Sinop, Brazil - Sinop Airport (OPS)",
			"value": "OPS",
			"domestic": 0
		}, {
			"label": "Balimo, Papua New Guinea - Balimo Airport (OPU)",
			"value": "OPU",
			"domestic": 0
		}, {
			"label": "Orebro, Sweden - Orebro-Bofors (ORB)",
			"value": "ORB",
			"domestic": 0
		}, {
			"label": "Chicago, United States - O\'Hare (ORD)",
			"value": "ORD",
			"domestic": 1
		}, {
			"label": "Orleans, France - Orleans (ORE)",
			"value": "ORE",
			"domestic": 0
		}, {
			"label": "Norfolk, VA, United States - Norfolk (ORF)",
			"value": "ORF",
			"domestic": 1
		}, {
			"label": "Worcester, United States - Worcester Airport (ORH)",
			"value": "ORH",
			"domestic": 1
		}, {
			"label": "Port Lions, United States - Port Lions Sea Plane Base (ORI)",
			"value": "ORI",
			"domestic": 1
		}, {
			"label": "Cork, Ireland - Cork International (ORK)",
			"value": "ORK",
			"domestic": 0
		}, {
			"label": "Orlando, United States - All Airports (ORL)",
			"value": "ORL",
			"domestic": 1
		}, {
			"label": "Oran, Algeria - Es Senia (ORN)",
			"value": "ORN",
			"domestic": 0
		}, {
			"label": "Orpheus Island, Australia - Waterport (ORS)",
			"value": "ORS",
			"domestic": 0
		}, {
			"label": "Northway, United States - Northway Airport (ORT)",
			"value": "ORT",
			"domestic": 1
		}, {
			"label": "Noorvik, United States - Curtis Memorial Airport (ORV)",
			"value": "ORV",
			"domestic": 1
		}, {
			"label": "Paris, France - Orly Field (ORY)",
			"value": "ORY",
			"domestic": 0
		}, {
			"label": "Osaka, Japan - All Airports (OSA)",
			"value": "OSA",
			"domestic": 0
		}, {
			"label": "Osage Beach, United States - Osage Beach (OSB)",
			"value": "OSB",
			"domestic": 1
		}, {
			"label": "Ostersund, Sweden - Froesoe (OSD)",
			"value": "OSD",
			"domestic": 0
		}, {
			"label": "Oshkosh, United States - Wittman Fld (OSH)",
			"value": "OSH",
			"domestic": 1
		}, {
			"label": "Osijek, Croatia - Osijek (OSI)",
			"value": "OSI",
			"domestic": 0
		}, {
			"label": "Oskarshamn, Sweden - Oskarshamn (OSK)",
			"value": "OSK",
			"domestic": 0
		}, {
			"label": "Oslo, Norway - Oslo Airport (OSL)",
			"value": "OSL",
			"domestic": 0
		}, {
			"label": "Ostrava, Czech Republic - Mosnov (OSR)",
			"value": "OSR",
			"domestic": 0
		}, {
			"label": "Osh, Kyrgyzstan - Osh Airport (OSS)",
			"value": "OSS",
			"domestic": 0
		}, {
			"label": "Ostend, Belgium - Ostend (OST)",
			"value": "OST",
			"domestic": 0
		}, {
			"label": "Orsk, Russia - Orsk Airport (OSW)",
			"value": "OSW",
			"domestic": 0
		}, {
			"label": "Namsos, Norway - Namsos (OSY)",
			"value": "OSY",
			"domestic": 0
		}, {
			"label": "Koszalin, Poland - Koszalin Airport (OSZ)",
			"value": "OSZ",
			"domestic": 0
		}, {
			"label": "Contadora Island, Panama - Contadora (OTD)",
			"value": "OTD",
			"domestic": 0
		}, {
			"label": "Worthington, United States - Worthington Mncpl (OTG)",
			"value": "OTG",
			"domestic": 1
		}, {
			"label": "North Bend, United States - North Bend Airport (OTH)",
			"value": "OTH",
			"domestic": 1
		}, {
			"label": "Ottumwa, United States - Industrial (OTM)",
			"value": "OTM",
			"domestic": 1
		}, {
			"label": "Bucharest, Romania - Otopeni Intl (OTP)",
			"value": "OTP",
			"domestic": 0
		}, {
			"label": "Coto 47, Costa Rica - Coto 47 Airport (OTR)",
			"value": "OTR",
			"domestic": 0
		}, {
			"label": "Anacortes, United States - Anacortes (OTS)",
			"value": "OTS",
			"domestic": 1
		}, {
			"label": "Ontong Java, Solomon Islands - Ontong Java Airport (OTV)",
			"value": "OTV",
			"domestic": 0
		}, {
			"label": "Kotzebue, United States - Kotzebue Airport (OTZ)",
			"value": "OTZ",
			"domestic": 1
		}, {
			"label": "Ouagadougou, Burkina Faso - Ouagadougou (OUA)",
			"value": "OUA",
			"domestic": 0
		}, {
			"label": "Oujda, Morocco - Les Angades (OUD)",
			"value": "OUD",
			"domestic": 0
		}, {
			"label": "Oudtshoorn, South Africa - Oudtshoorn (OUH)",
			"value": "OUH",
			"domestic": 0
		}, {
			"label": "Oulu, Finland - Oulu (OUL)",
			"value": "OUL",
			"domestic": 0
		}, {
			"label": "Novosibirsk, Russia - Tolmachevo (OVB)",
			"value": "OVB",
			"domestic": 0
		}, {
			"label": "Asturias, Spain - Oviedo (OVD)",
			"value": "OVD",
			"domestic": 0
		}, {
			"label": "Owensboro, United States - Owensboro-Daviess (OWB)",
			"value": "OWB",
			"domestic": 1
		}, {
			"label": "Bissau, Guinea-Bissau - Osvaldo Vieira (OXB)",
			"value": "OXB",
			"domestic": 0
		}, {
			"label": "Oxford, United Kingdom - Oxford (OXF)",
			"value": "OXF",
			"domestic": 0
		}, {
			"label": "Oxnard, United States - Oxnard Airport (OXR)",
			"value": "OXR",
			"domestic": 1
		}, {
			"label": "Oyem, Gabon - Oyem Airport (OYE)",
			"value": "OYE",
			"domestic": 0
		}, {
			"label": "Moyo, Uganda - Moyo Airport (OYG)",
			"value": "OYG",
			"domestic": 0
		}, {
			"label": "Yosemite Natl Pk, United States - Yosemite Natl Park (OYS)",
			"value": "OYS",
			"domestic": 1
		}, {
			"label": "Zaporozhye, Ukraine - Zaporozhye (OZH)",
			"value": "OZH",
			"domestic": 0
		}, {
			"label": "Ouarzazate, Morocco - Ouarzazate (OZZ)",
			"value": "OZZ",
			"domestic": 0
		}, {
			"label": "Panama City, Panama - Paitilla (PAC)",
			"value": "PAC",
			"domestic": 0
		}, {
			"label": "Paderborn, Germany - Paderborn (PAD)",
			"value": "PAD",
			"domestic": 0
		}, {
			"label": "Everett, United States - Snohomish Cty (PAE)",
			"value": "PAE",
			"domestic": 1
		}, {
			"label": "Pagadian, Philippines - Pagadian Airport (PAG)",
			"value": "PAG",
			"domestic": 0
		}, {
			"label": "Paducah, United States - Barkley Regional Airport (PAH)",
			"value": "PAH",
			"domestic": 1
		}, {
			"label": "Port au Prince, Haiti - Mais Gate Airport (PAP)",
			"value": "PAP",
			"domestic": 0
		}, {
			"label": "Paris, France  - All Airports (PAR)",
			"value": "PAR",
			"domestic": 0
		}, {
			"label": "Paros, Greece - Paros (PAS)",
			"value": "PAS",
			"domestic": 0
		}, {
			"label": "Patna, India - Patna (PAT)",
			"value": "PAT",
			"domestic": 0
		}, {
			"label": "Poza Rica, Mexico - Tajin (PAZ)",
			"value": "PAZ",
			"domestic": 0
		}, {
			"label": "Puebla, Mexico - Hermanos Serdan (PBC)",
			"value": "PBC",
			"domestic": 0
		}, {
			"label": "Porbandar, India - Porbandar Airport (PBD)",
			"value": "PBD",
			"domestic": 0
		}, {
			"label": "Pine Bluff, United States - Grider Field (PBF)",
			"value": "PBF",
			"domestic": 1
		}, {
			"label": "Plattsburgh, United States - Plattsburgh International Airport (PBG)",
			"value": "PBG",
			"domestic": 1
		}, {
			"label": "Paro, Bhutan - Paro (PBH)",
			"value": "PBH",
			"domestic": 0
		}, {
			"label": "West Palm Beach, United States - West Palm Beach (PBI)",
			"value": "PBI",
			"domestic": 1
		}, {
			"label": "Paama, Vanuatu - Paama Airport (PBJ)",
			"value": "PBJ",
			"domestic": 0
		}, {
			"label": "Paramaribo, Suriname - Zanderij Intl (PBM)",
			"value": "PBM",
			"domestic": 0
		}, {
			"label": "Paraburdoo, Australia - Paraburdoo Airport (PBO)",
			"value": "PBO",
			"domestic": 0
		}, {
			"label": "Punta Islita, Costa Rica - Punta Islita (PBP)",
			"value": "PBP",
			"domestic": 0
		}, {
			"label": "Putao, Myanmar (Burma) - Putao Airport (PBU)",
			"value": "PBU",
			"domestic": 0
		}, {
			"label": "Plettenberg Bay, South Africa - Plettenberg (PBZ)",
			"value": "PBZ",
			"domestic": 0
		}, {
			"label": "Pucallpa, Peru - Capitan Rolden Airport (PCL)",
			"value": "PCL",
			"domestic": 0
		}, {
			"label": "Playa del Carmen, Mexico - Playa del Carmen (PCM)",
			"value": "PCM",
			"domestic": 0
		}, {
			"label": "Phongsaly, Laos - Bounneua Airport (PCQ)",
			"value": "PCQ",
			"domestic": 0
		}, {
			"label": "Puerto Carreno, Colombia - Puerto Carreno Airport (PCR)",
			"value": "PCR",
			"domestic": 0
		}, {
			"label": "Princeton, United States - Princeton (PCT)",
			"value": "PCT",
			"domestic": 1
		}, {
			"label": "Puerto Inirida, Colombia - Puerto Inirida Airport (PDA)",
			"value": "PDA",
			"domestic": 0
		}, {
			"label": "Padang, Indonesia - Tabing (PDG)",
			"value": "PDG",
			"domestic": 0
		}, {
			"label": "Ponta Delgada, Portugal - Nordela (PDL)",
			"value": "PDL",
			"domestic": 0
		}, {
			"label": "Punta del Este, Uruguay - Punta del Este (PDP)",
			"value": "PDP",
			"domestic": 0
		}, {
			"label": "Piedras Negras, Mexico - Piedras Negras Mncpl (PDS)",
			"value": "PDS",
			"domestic": 0
		}, {
			"label": "Pendleton, United States - Pendleton Airport (PDT)",
			"value": "PDT",
			"domestic": 1
		}, {
			"label": "Plovdiv, Bulgaria - Plovdiv (PDV)",
			"value": "PDV",
			"domestic": 0
		}, {
			"label": "Portland, OR, United States - Portland (PDX)",
			"value": "PDX",
			"domestic": 1
		}, {
			"label": "Pelican, United States - Pelican Sea Plane Base (PEC)",
			"value": "PEC",
			"domestic": 1
		}, {
			"label": "Perm, Russia - Perm (PEE)",
			"value": "PEE",
			"domestic": 0
		}, {
			"label": "Perugia, Italy - Sant Egidio (PEG)",
			"value": "PEG",
			"domestic": 0
		}, {
			"label": "Pereira, Colombia - Matecana (PEI)",
			"value": "PEI",
			"domestic": 0
		}, {
			"label": "Beijing, China - Capital Airport (PEK)",
			"value": "PEK",
			"domestic": 0
		}, {
			"label": "Puerto Maldonado, Peru - Puerto Maldonado (PEM)",
			"value": "PEM",
			"domestic": 0
		}, {
			"label": "Penang, Malaysia - Penang Intl (PEN)",
			"value": "PEN",
			"domestic": 0
		}, {
			"label": "Perth, Australia - Perth International (PER)",
			"value": "PER",
			"domestic": 0
		}, {
			"label": "Petrozavodsk, Russia - Petrozavodsk (PES)",
			"value": "PES",
			"domestic": 0
		}, {
			"label": "Puerto Lempira, Honduras - Puerto Lempira Airport (PEU)",
			"value": "PEU",
			"domestic": 0
		}, {
			"label": "Peshawar, Pakistan - Peshawar (PEW)",
			"value": "PEW",
			"domestic": 0
		}, {
			"label": "Pechora, Russia - Pechora Airport (PEX)",
			"value": "PEX",
			"domestic": 0
		}, {
			"label": "Penza, Russia - Penza Airport (PEZ)",
			"value": "PEZ",
			"domestic": 0
		}, {
			"label": "Passo Fundo, Brazil - Passo Fundo Airport (PFB)",
			"value": "PFB",
			"domestic": 0
		}, {
			"label": "Patreksfjordur, Iceland - Patreksfjordur (PFJ)",
			"value": "PFJ",
			"domestic": 0
		}, {
			"label": "Panama City, United States - Bay County Airport (PFN)",
			"value": "PFN",
			"domestic": 1
		}, {
			"label": "Paphos, Cyprus - Paphos Intl (PFO)",
			"value": "PFO",
			"domestic": 0
		}, {
			"label": "Parsabad, Iran - Parsabad Airport (PFQ)",
			"value": "PFQ",
			"domestic": 0
		}, {
			"label": "Page, United States - Page Municipal Airport (PGA)",
			"value": "PGA",
			"domestic": 1
		}, {
			"label": "Punta Gorda, United States - Charlotte Cty (PGD)",
			"value": "PGD",
			"domestic": 1
		}, {
			"label": "Perpignan, France - Rivesaltes (PGF)",
			"value": "PGF",
			"domestic": 0
		}, {
			"label": "Pangkalpinang, Indonesia - Pangkalpinang Airport (PGK)",
			"value": "PGK",
			"domestic": 0
		}, {
			"label": "Port Graham, United States - Port Graham Airport (PGM)",
			"value": "PGM",
			"domestic": 1
		}, {
			"label": "Peach Springs, United States - Peach Springs Mncpl (PGS)",
			"value": "PGS",
			"domestic": 1
		}, {
			"label": "Greenville, NC, United States - Greenville (PGV)",
			"value": "PGV",
			"domestic": 1
		}, {
			"label": "Perigueux, France - Perigueux (PGX)",
			"value": "PGX",
			"domestic": 0
		}, {
			"label": "Port Harcourt, Nigeria - Port Harcourt (PHC)",
			"value": "PHC",
			"domestic": 0
		}, {
			"label": "Port Hedland, Australia - Port Hedland (PHE)",
			"value": "PHE",
			"domestic": 0
		}, {
			"label": "Newport News, United States - Newport News/Williamsburg Airport (PHF)",
			"value": "PHF",
			"domestic": 1
		}, {
			"label": "Port Harcourt, Nigeria - Port Harcourt City (PHG)",
			"value": "PHG",
			"domestic": 0
		}, {
			"label": "Philadelphia, United States - Philadelphia (PHL)",
			"value": "PHL",
			"domestic": 1
		}, {
			"label": "Point Hope, United States - Point Hope Airport (PHO)",
			"value": "PHO",
			"domestic": 1
		}, {
			"label": "Phitsanulok, Thailand - Phitsanulok Airport (PHS)",
			"value": "PHS",
			"domestic": 0
		}, {
			"label": "Phalaborwa, South Africa - Phalaborwa (PHW)",
			"value": "PHW",
			"domestic": 0
		}, {
			"label": "Phoenix, United States - Phoenix (PHX)",
			"value": "PHX",
			"domestic": 1
		}, {
			"label": "Peoria, United States - Greater Peoria Airport (PIA)",
			"value": "PIA",
			"domestic": 1
		}, {
			"label": "Laurel, MS, United States - Hattiesburg-Laurel Regional Airport (PIB)",
			"value": "PIB",
			"domestic": 1
		}, {
			"label": "St. Petersburg, United States - St. Petersburg (PIE)",
			"value": "PIE",
			"domestic": 1
		}, {
			"label": "Pingtung, Taiwan - Pingtung (PIF)",
			"value": "PIF",
			"domestic": 0
		}, {
			"label": "Pocatello, United States - Pocatello (PIH)",
			"value": "PIH",
			"domestic": 1
		}, {
			"label": "Glasgow, United Kingdom - Prestwick (PIK)",
			"value": "PIK",
			"domestic": 0
		}, {
			"label": "Parintins, Brazil - Parintins Airport (PIN)",
			"value": "PIN",
			"domestic": 0
		}, {
			"label": "Pilot Point, United States - Pilot Point Airport (PIP)",
			"value": "PIP",
			"domestic": 1
		}, {
			"label": "Pierre, United States - Pierre Airport (PIR)",
			"value": "PIR",
			"domestic": 1
		}, {
			"label": "Poitiers, France - Biard (PIS)",
			"value": "PIS",
			"domestic": 0
		}, {
			"label": "Pittsburgh, United States - Pittsburgh (PIT)",
			"value": "PIT",
			"domestic": 1
		}, {
			"label": "Piura, Peru - Piura Airport (PIU)",
			"value": "PIU",
			"domestic": 0
		}, {
			"label": "Pico Island (Azores), Portugal - Pico Island Airport (PIX)",
			"value": "PIX",
			"domestic": 0
		}, {
			"label": "Point Lay, United States - Dew Station Airport (PIZ)",
			"value": "PIZ",
			"domestic": 1
		}, {
			"label": "Pajala, Sweden - Pajala Airport (PJA)",
			"value": "PJA",
			"domestic": 0
		}, {
			"label": "Payson, United States - Payson Mncpl (PJB)",
			"value": "PJB",
			"domestic": 1
		}, {
			"label": "Panjgur, Pakistan - Panjgur Airport (PJG)",
			"value": "PJG",
			"domestic": 0
		}, {
			"label": "Puerto Jimenez, Costa Rica - Puerto Jimenez (PJM)",
			"value": "PJM",
			"domestic": 0
		}, {
			"label": "Napaskiak, United States - Napaskiak Sea Plane Base (PKA)",
			"value": "PKA",
			"domestic": 1
		}, {
			"label": "Parkersburg, United States - Wood Cty (PKB)",
			"value": "PKB",
			"domestic": 1
		}, {
			"label": "Petropavlovsk-Kamchatsky, Russia - Petropavlovsk-Kamcha (PKC)",
			"value": "PKC",
			"domestic": 0
		}, {
			"label": "Park Rapids, United States - Park Rapids Mncpl (PKD)",
			"value": "PKD",
			"domestic": 1
		}, {
			"label": "Parkes, Australia - Parkes (PKE)",
			"value": "PKE",
			"domestic": 0
		}, {
			"label": "Pangkor, Malaysia - Pangkor Airport (PKG)",
			"value": "PKG",
			"domestic": 0
		}, {
			"label": "Pangkalanbuun, Indonesia - Pangkalanbuun Airport (PKN)",
			"value": "PKN",
			"domestic": 0
		}, {
			"label": "Pokhara, Nepal - Pokhara (PKR)",
			"value": "PKR",
			"domestic": 0
		}, {
			"label": "Pekanbaru, Indonesia - Simpang Tiga (PKU)",
			"value": "PKU",
			"domestic": 0
		}, {
			"label": "Selebi-Phikwe, Botswana - Selebi-Phikwe (PKW)",
			"value": "PKW",
			"domestic": 0
		}, {
			"label": "Palangkaraya, Indonesia - Palangkaraya Airport (PKY)",
			"value": "PKY",
			"domestic": 0
		}, {
			"label": "Pakse, Laos - Pakse Airport (PKZ)",
			"value": "PKZ",
			"domestic": 0
		}, {
			"label": "Playa Samara, Costa Rica - Playa Samara (PLD)",
			"value": "PLD",
			"domestic": 0
		}, {
			"label": "Plymouth, United Kingdom - Plymouth (PLH)",
			"value": "PLH",
			"domestic": 0
		}, {
			"label": "Placencia, Belize - Placencia (PLJ)",
			"value": "PLJ",
			"domestic": 0
		}, {
			"label": "Palembang, Indonesia - Mahmd Badaruddin II (PLM)",
			"value": "PLM",
			"domestic": 0
		}, {
			"label": "Pellston, United States - Emmet County Airport (PLN)",
			"value": "PLN",
			"domestic": 1
		}, {
			"label": "Port Lincoln, Australia - Port Lincoln Airport (PLO)",
			"value": "PLO",
			"domestic": 0
		}, {
			"label": "La Palma, Panama - La Palma Airport (PLP)",
			"value": "PLP",
			"domestic": 0
		}, {
			"label": "Palanga, Lithuania - Klaipeda/Palanga Int (PLQ)",
			"value": "PLQ",
			"domestic": 0
		}, {
			"label": "Providenciales, Turks and Caicos - Providenciales International Airport (PLS)",
			"value": "PLS",
			"domestic": 0
		}, {
			"label": "Belo Horizonte, Brazil - Pampulha (PLU)",
			"value": "PLU",
			"domestic": 0
		}, {
			"label": "Poltava, Ukraine - Poltava Airport (PLV)",
			"value": "PLV",
			"domestic": 0
		}, {
			"label": "Palu, Indonesia - Mutiara Airport (PLW)",
			"value": "PLW",
			"domestic": 0
		}, {
			"label": "Semipalatinsk, Kazakhstan - Semipalatinsk Airport (PLX)",
			"value": "PLX",
			"domestic": 0
		}, {
			"label": "Port Elizabeth, South Africa - Port Elizabeth (PLZ)",
			"value": "PLZ",
			"domestic": 0
		}, {
			"label": "Pemba, Tanzania - Wawi Airport (PMA)",
			"value": "PMA",
			"domestic": 0
		}, {
			"label": "Puerto Montt, Chile and Easter Island - Tepual (PMC)",
			"value": "PMC",
			"domestic": 0
		}, {
			"label": "Palmdale, United States - Palmdale Regl (PMD)",
			"value": "PMD",
			"domestic": 1
		}, {
			"label": "Milan, Italy - Parma (PMF)",
			"value": "PMF",
			"domestic": 0
		}, {
			"label": "Palma, Spain - Palma Mallorca (PMI)",
			"value": "PMI",
			"domestic": 0
		}, {
			"label": "Palm Island, Australia - Palm Island Airport (PMK)",
			"value": "PMK",
			"domestic": 0
		}, {
			"label": "Palermo, Italy - Punta Raisi (PMO)",
			"value": "PMO",
			"domestic": 0
		}, {
			"label": "Perito Moreno, Argentina - Perito Moreno Airport (PMQ)",
			"value": "PMQ",
			"domestic": 0
		}, {
			"label": "Palmerston North, New Zealand - Palmerston North (PMR)",
			"value": "PMR",
			"domestic": 0
		}, {
			"label": "Porlamar, Venezuela - Gen Santiago Marino (PMV)",
			"value": "PMV",
			"domestic": 0
		}, {
			"label": "Palmas, Brazil - Palmas Airport (PMW)",
			"value": "PMW",
			"domestic": 0
		}, {
			"label": "Puerto Madryn, Argentina - El Tehuelche Airport (PMY)",
			"value": "PMY",
			"domestic": 0
		}, {
			"label": "Palmar, Costa Rica - Palmar Sur (PMZ)",
			"value": "PMZ",
			"domestic": 0
		}, {
			"label": "Pamplona, Spain - Pamplona-Noain (PNA)",
			"value": "PNA",
			"domestic": 0
		}, {
			"label": "Ponca City, United States - Ponca City Mncpl (PNC)",
			"value": "PNC",
			"domestic": 1
		}, {
			"label": "Punta Gorda, Belize - Punta Gorda (PND)",
			"value": "PND",
			"domestic": 0
		}, {
			"label": "Phnom Penh, Cambodia - Pochentong (PNH)",
			"value": "PNH",
			"domestic": 0
		}, {
			"label": "Kolonia, Micronesia - Pohnpei (PNI)",
			"value": "PNI",
			"domestic": 0
		}, {
			"label": "Pontianak, Indonesia - Supadio Airport (PNK)",
			"value": "PNK",
			"domestic": 0
		}, {
			"label": "Pantelleria, Italy - Pantelleria (PNL)",
			"value": "PNL",
			"domestic": 0
		}, {
			"label": "Popondetta, Papua New Guinea - Girua Airport (PNP)",
			"value": "PNP",
			"domestic": 0
		}, {
			"label": "Pune, India - Lohegaon (PNQ)",
			"value": "PNQ",
			"domestic": 0
		}, {
			"label": "Pointe Noire, Republic of Congo - Pointe Noire (PNR)",
			"value": "PNR",
			"domestic": 0
		}, {
			"label": "Pensacola, United States - Pensacola Regional Airport (PNS)",
			"value": "PNS",
			"domestic": 1
		}, {
			"label": "Panguitch, United States - Panguitch Mncpl (PNU)",
			"value": "PNU",
			"domestic": 1
		}, {
			"label": "Sherman, United States - Grayson County (PNX)",
			"value": "PNX",
			"domestic": 1
		}, {
			"label": "Petrolina, Brazil - Petrolina Internacional Airport (PNZ)",
			"value": "PNZ",
			"domestic": 0
		}, {
			"label": "Porto Alegre, Brazil - Salgado Filho (POA)",
			"value": "POA",
			"domestic": 0
		}, {
			"label": "Poplar Bluff, United States - Earl Flds Memorial (POF)",
			"value": "POF",
			"domestic": 1
		}, {
			"label": "Port Gentil, Gabon - Port Gentil (POG)",
			"value": "POG",
			"domestic": 0
		}, {
			"label": "Patos De Minas, Brazil - Patos De Minas Airport (POJ)",
			"value": "POJ",
			"domestic": 0
		}, {
			"label": "Pemba, Mozambique - Pemba Airport (POL)",
			"value": "POL",
			"domestic": 0
		}, {
			"label": "Port Moresby, Papua New Guinea - Jackson Fld (POM)",
			"value": "POM",
			"domestic": 0
		}, {
			"label": "Puerto Plata, Dominican Republic - La Union Airport (POP)",
			"value": "POP",
			"domestic": 0
		}, {
			"label": "Pori, Finland - Pori (POR)",
			"value": "POR",
			"domestic": 0
		}, {
			"label": "Port of Spain, Trinidad and Tobago - Piarco Airport (POS)",
			"value": "POS",
			"domestic": 0
		}, {
			"label": "Port Antonio, Jamaica - Ken Jones (POT)",
			"value": "POT",
			"domestic": 0
		}, {
			"label": "Poughkeepsie, United States - Dutchess Cty (POU)",
			"value": "POU",
			"domestic": 1
		}, {
			"label": "Portoroz, Slovenia - Portoroz (POW)",
			"value": "POW",
			"domestic": 0
		}, {
			"label": "Pontoise, France - Paris Cergy Pontoise (POX)",
			"value": "POX",
			"domestic": 0
		}, {
			"label": "Powell, United States - Powell Mncpl (POY)",
			"value": "POY",
			"domestic": 1
		}, {
			"label": "Poznan, Poland - Lawica (POZ)",
			"value": "POZ",
			"domestic": 0
		}, {
			"label": "Presidente Prudente, Brazil - A. De Barros Airport (PPB)",
			"value": "PPB",
			"domestic": 0
		}, {
			"label": "Parsons, United States - Tri-City (PPF)",
			"value": "PPF",
			"domestic": 1
		}, {
			"label": "Pago Pago, American Samoa - Pago Pago (PPG)",
			"value": "PPG",
			"domestic": 0
		}, {
			"label": "Petropavlovsk, Kazakhstan - Petropavlovsk Airport (PPK)",
			"value": "PPK",
			"domestic": 0
		}, {
			"label": "Phaplu, Nepal - Phaplu Airport (PPL)",
			"value": "PPL",
			"domestic": 0
		}, {
			"label": "Popayan, Colombia - Machangara Airport (PPN)",
			"value": "PPN",
			"domestic": 0
		}, {
			"label": "Proserpine, Australia - Whitsunday Coast Airport (PPP)",
			"value": "PPP",
			"domestic": 0
		}, {
			"label": "Puerto Princesa, Philippines - Puerto Princesa (PPS)",
			"value": "PPS",
			"domestic": 0
		}, {
			"label": "Papeete, French Polynesia and Tahiti - Tahiti - Faa\'a Airport (PPT)",
			"value": "PPT",
			"domestic": 0
		}, {
			"label": "Port Protection, United States - Port Protection Airport (PPV)",
			"value": "PPV",
			"domestic": 1
		}, {
			"label": "Papa Westray, United Kingdom - Papa Westray Airport (PPW)",
			"value": "PPW",
			"domestic": 0
		}, {
			"label": "Phu Quoc, Vietnam - Duong Dang Airport (PQC)",
			"value": "PQC",
			"domestic": 0
		}, {
			"label": "Presque Isle, United States - Presque Isle (PQI)",
			"value": "PQI",
			"domestic": 1
		}, {
			"label": "Palenque, Mexico - Palenque Mncpl (PQM)",
			"value": "PQM",
			"domestic": 0
		}, {
			"label": "Port Macquarie, Australia - Port Macquarie (PQQ)",
			"value": "PQQ",
			"domestic": 0
		}, {
			"label": "Pilot Station, United States - Pilot Station Airport (PQS)",
			"value": "PQS",
			"domestic": 1
		}, {
			"label": "Parana, Argentina - Parana (PRA)",
			"value": "PRA",
			"domestic": 0
		}, {
			"label": "Paso Robles, United States - Paso Robles Mncpl (PRB)",
			"value": "PRB",
			"domestic": 1
		}, {
			"label": "Prescott, United States - Prescott Airport (PRC)",
			"value": "PRC",
			"domestic": 1
		}, {
			"label": "Prague, Czech Republic - Ruzyne (PRG)",
			"value": "PRG",
			"domestic": 0
		}, {
			"label": "Phrae, Thailand - Phrae Airport (PRH)",
			"value": "PRH",
			"domestic": 0
		}, {
			"label": "Praslin Island, Seychelles - Praslin Island (PRI)",
			"value": "PRI",
			"domestic": 0
		}, {
			"label": "Pristina, Kosovo - Pristina (PRN)",
			"value": "PRN",
			"domestic": 0
		}, {
			"label": "Pretoria, South Africa - Pretoria (PRY)",
			"value": "PRY",
			"domestic": 0
		}, {
			"label": "Pisa, Italy - Galileo Galilei (PSA)",
			"value": "PSA",
			"domestic": 0
		}, {
			"label": "Pasco, United States - Pasco (PSC)",
			"value": "PSC",
			"domestic": 1
		}, {
			"label": "Port Said, Egypt - Port Said (PSD)",
			"value": "PSD",
			"domestic": 0
		}, {
			"label": "Ponce, Puerto Rico - Mercedita (PSE)",
			"value": "PSE",
			"domestic": 0
		}, {
			"label": "Pittsfield, United States - Pittsfield Mncpl (PSF)",
			"value": "PSF",
			"domestic": 1
		}, {
			"label": "Petersburg, AK, United States - Petersburg Municipal Airport (PSG)",
			"value": "PSG",
			"domestic": 1
		}, {
			"label": "Pasni, Pakistan - Pasni Airport (PSI)",
			"value": "PSI",
			"domestic": 0
		}, {
			"label": "Portsmouth, United States - Pease International (PSM)",
			"value": "PSM",
			"domestic": 1
		}, {
			"label": "Pasto, Colombia - Cano (PSO)",
			"value": "PSO",
			"domestic": 0
		}, {
			"label": "Palm Springs, United States - Palm Springs (PSP)",
			"value": "PSP",
			"domestic": 1
		}, {
			"label": "Pescara, Italy - Liberi (PSR)",
			"value": "PSR",
			"domestic": 0
		}, {
			"label": "Posadas, Argentina - Posadas Airport (PSS)",
			"value": "PSS",
			"domestic": 0
		}, {
			"label": "Putussibau, Indonesia - Putussibau Airport (PSU)",
			"value": "PSU",
			"domestic": 0
		}, {
			"label": "Puerto Suarez, Bolivia - Puerto Suarez Airport (PSZ)",
			"value": "PSZ",
			"domestic": 0
		}, {
			"label": "Malololailai Island, Fiji - Malololailai Airport (PTF)",
			"value": "PTF",
			"domestic": 0
		}, {
			"label": "Polokwane, South Africa - Polokwane (PTG)",
			"value": "PTG",
			"domestic": 0
		}, {
			"label": "Port Heiden, United States - Port Heiden Airport (PTH)",
			"value": "PTH",
			"domestic": 1
		}, {
			"label": "Port Douglas, Australia - Port Douglas (PTI)",
			"value": "PTI",
			"domestic": 0
		}, {
			"label": "Portland, Australia - Portland (PTJ)",
			"value": "PTJ",
			"domestic": 0
		}, {
			"label": "Pontiac, United States - Oakland County (PTK)",
			"value": "PTK",
			"domestic": 1
		}, {
			"label": "Pointe a Pitre, Guadeloupe - Le Raizet Airport (PTP)",
			"value": "PTP",
			"domestic": 0
		}, {
			"label": "Platinum, United States - Platinum Airport (PTU)",
			"value": "PTU",
			"domestic": 1
		}, {
			"label": "Panama City, Panama - Tocumen International Airport (PTY)",
			"value": "PTY",
			"domestic": 0
		}, {
			"label": "Pueblo, United States - Pueblo Memorial Airport (PUB)",
			"value": "PUB",
			"domestic": 1
		}, {
			"label": "Price, United States - Carbon Cty (PUC)",
			"value": "PUC",
			"domestic": 1
		}, {
			"label": "Puerto Deseado, Argentina - Puerto Deseado Airport (PUD)",
			"value": "PUD",
			"domestic": 0
		}, {
			"label": "Puerto Obaldia, Panama - Puerto Obaldia Airport (PUE)",
			"value": "PUE",
			"domestic": 0
		}, {
			"label": "Pau, France - Uzein (PUF)",
			"value": "PUF",
			"domestic": 0
		}, {
			"label": "Port Augusta, Australia - Port Augusta/Pagas (PUG)",
			"value": "PUG",
			"domestic": 0
		}, {
			"label": "Punta Cana, Dominican Republic - Punta Cana Airport (PUJ)",
			"value": "PUJ",
			"domestic": 0
		}, {
			"label": "Punta Arenas, Chile and Easter Island - Presidente Ibanez (PUQ)",
			"value": "PUQ",
			"domestic": 0
		}, {
			"label": "Busan, South Korea - Gimhae (PUS)",
			"value": "PUS",
			"domestic": 0
		}, {
			"label": "Puttaparthi, India - Puttaprathe Airport (PUT)",
			"value": "PUT",
			"domestic": 0
		}, {
			"label": "Puerto Asis, Colombia - Puerto Asis Airport (PUU)",
			"value": "PUU",
			"domestic": 0
		}, {
			"label": "Poum, New Caledonia - Poum (PUV)",
			"value": "PUV",
			"domestic": 0
		}, {
			"label": "Pullman, United States - Moscow Regional Airport (PUW)",
			"value": "PUW",
			"domestic": 1
		}, {
			"label": "Pula, Croatia - Pula (PUY)",
			"value": "PUY",
			"domestic": 0
		}, {
			"label": "Puerto Cabezas, Nicaragua - Puerto Cabezas Airport (PUZ)",
			"value": "PUZ",
			"domestic": 0
		}, {
			"label": "Providencia, Colombia - Providencia (PVA)",
			"value": "PVA",
			"domestic": 0
		}, {
			"label": "Provincetown, United States - Provincetown Mncpl (PVC)",
			"value": "PVC",
			"domestic": 1
		}, {
			"label": "Providence, United States - Providence (PVD)",
			"value": "PVD",
			"domestic": 1
		}, {
			"label": "El Porvenir, Panama - El Porvenir Airport (PVE)",
			"value": "PVE",
			"domestic": 0
		}, {
			"label": "Shanghai, China - Pu Dong Airport (PVG)",
			"value": "PVG",
			"domestic": 0
		}, {
			"label": "Porto Velho, Brazil - Belmonte (PVH)",
			"value": "PVH",
			"domestic": 0
		}, {
			"label": "Preveza, Greece - Aktion (PVK)",
			"value": "PVK",
			"domestic": 0
		}, {
			"label": "Portoviejo, Ecuador - Portoviejo Airport (PVO)",
			"value": "PVO",
			"domestic": 0
		}, {
			"label": "Puerto Vallarta, Mexico - Ordaz (PVR)",
			"value": "PVR",
			"domestic": 0
		}, {
			"label": "Provo, United States - Provo Mncpl (PVU)",
			"value": "PVU",
			"domestic": 1
		}, {
			"label": "Portland, ME, United States - Portland (PWM)",
			"value": "PWM",
			"domestic": 1
		}, {
			"label": "Pavlodar, Kazakhstan - Pavlodar Airport (PWQ)",
			"value": "PWQ",
			"domestic": 0
		}, {
			"label": "Bremerton, United States - Bremerton Natl (PWT)",
			"value": "PWT",
			"domestic": 1
		}, {
			"label": "Puerto Escondido, Mexico - Puerto Escondido (PXM)",
			"value": "PXM",
			"domestic": 0
		}, {
			"label": "Porto Santo, Portugal - Porto Santo (PXO)",
			"value": "PXO",
			"domestic": 0
		}, {
			"label": "Pleiku, Vietnam - Pleiku Airport (PXU)",
			"value": "PXU",
			"domestic": 0
		}, {
			"label": "Playon Chico, Panama - Playon Chico Airport (PYC)",
			"value": "PYC",
			"domestic": 0
		}, {
			"label": "Penrhyn Island, Cook Islands - Penrhyn Island Airport (PYE)",
			"value": "PYE",
			"domestic": 0
		}, {
			"label": "Puerto Ayacucho, Venezuela - Puerto Ayacucho Airport (PYH)",
			"value": "PYH",
			"domestic": 0
		}, {
			"label": "Pietermaritzburg, South Africa - Pietermaritzburg (PZB)",
			"value": "PZB",
			"domestic": 0
		}, {
			"label": "Penzance, United Kingdom - Penzance (PZE)",
			"value": "PZE",
			"domestic": 0
		}, {
			"label": "Zhob, Pakistan - Zhob Airport (PZH)",
			"value": "PZH",
			"domestic": 0
		}, {
			"label": "Pan Zhi Hua, China - Pan Zhi Hua Bao An Ying (PZI)",
			"value": "PZI",
			"domestic": 0
		}, {
			"label": "Puerto Ordaz, Venezuela - Puerto Ordaz (PZO)",
			"value": "PZO",
			"domestic": 0
		}, {
			"label": "Port Sudan, Sudan - Port Sudan Airport (PZU)",
			"value": "PZU",
			"domestic": 0
		}, {
			"label": "Piestany, Slovakia - Piestany (PZY)",
			"value": "PZY",
			"domestic": 0
		}, {
			"label": "Bella Coola, Canada - Bella Coola Airport (QBC)",
			"value": "QBC",
			"domestic": 0
		}, {
			"label": "Akunnaaq, Greenland - Heliport (QCU)",
			"value": "QCU",
			"domestic": 0
		}, {
			"label": "Freiburg, Germany - Freiburg (QFB)",
			"value": "QFB",
			"domestic": 0
		}, {
			"label": "Iginniarfik, Greenland - Heliport (QFI)",
			"value": "QFI",
			"domestic": 0
		}, {
			"label": "Attu, Greenland - Attu Heliport (QGQ)",
			"value": "QGQ",
			"domestic": 0
		}, {
			"label": "Kangerluk, Greenland - Off-Line Point (QGR)",
			"value": "QGR",
			"domestic": 0
		}, {
			"label": "Lindau, Germany - Off-Line Point (QII)",
			"value": "QII",
			"domestic": 0
		}, {
			"label": "Kitsissuarsuit, Greenland - Heliport (QJE)",
			"value": "QJE",
			"domestic": 0
		}, {
			"label": "Ikamiut, Greenland - Heliport (QJI)",
			"value": "QJI",
			"domestic": 0
		}, {
			"label": "Kolobrzeg, Poland - Kolobrzeg Bus Service (QJY)",
			"value": "QJY",
			"domestic": 0
		}, {
			"label": "Lucerne, Switzerland - Off-Line Point (QLJ)",
			"value": "QLJ",
			"domestic": 0
		}, {
			"label": "Lausanne, Switzerland - Off-Line Point (QLS)",
			"value": "QLS",
			"domestic": 0
		}, {
			"label": "Niaqornaarsuk, Greenland - Heliport (QMK)",
			"value": "QMK",
			"domestic": 0
		}, {
			"label": "Owerri, Nigeria - Off-Line Point (QOW)",
			"value": "QOW",
			"domestic": 0
		}, {
			"label": "Kangaatsiaq, Greenland - Heliport (QPW)",
			"value": "QPW",
			"domestic": 0
		}, {
			"label": "Queretaro, Mexico - Queretaro Mncpl (QRO)",
			"value": "QRO",
			"domestic": 0
		}, {
			"label": "Warri, Nigeria - Warri Airport (QRW)",
			"value": "QRW",
			"domestic": 0
		}, {
			"label": "Ikerasaarsuk, Greenland - Ikerasaarsuk Heliport (QRY)",
			"value": "QRY",
			"domestic": 0
		}, {
			"label": "Setif, Algeria - Setif Airport (QSF)",
			"value": "QSF",
			"domestic": 0
		}, {
			"label": "Saqqaq, Greenland - Saqqaq Heliport (QUP)",
			"value": "QUP",
			"domestic": 0
		}, {
			"label": "Wuerzburg, Germany - Railway Station (QWU)",
			"value": "QWU",
			"domestic": 0
		}, {
			"label": "Aix-en-Provence, France - RAILWAY STATION (QXB)",
			"value": "QXB",
			"domestic": 0
		}, {
			"label": "Rabaul, Papua New Guinea - Tokua (RAB)",
			"value": "RAB",
			"domestic": 0
		}, {
			"label": "Arar, Saudi Arabia - Arar Airport (RAE)",
			"value": "RAE",
			"domestic": 0
		}, {
			"label": "Rafaela, Argentina - Rafaela Airport (RAF)",
			"value": "RAF",
			"domestic": 0
		}, {
			"label": "Rafha, Saudi Arabia - Rafha Airport (RAH)",
			"value": "RAH",
			"domestic": 0
		}, {
			"label": "Praia, Cape Verde - Francisco Mendes (RAI)",
			"value": "RAI",
			"domestic": 0
		}, {
			"label": "Rajkot, India - Rajkot (RAJ)",
			"value": "RAJ",
			"domestic": 0
		}, {
			"label": "Marrakech, Morocco - Menara (RAK)",
			"value": "RAK",
			"domestic": 0
		}, {
			"label": "Riverside, United States - Riverside Mncpl (RAL)",
			"value": "RAL",
			"domestic": 1
		}, {
			"label": "Ramingining, Australia - Ramingining Airport (RAM)",
			"value": "RAM",
			"domestic": 0
		}, {
			"label": "Ribeirao Preto, Brazil - Leite Lopes (RAO)",
			"value": "RAO",
			"domestic": 0
		}, {
			"label": "Rapid City, United States - Rapid City Regional Airport (RAP)",
			"value": "RAP",
			"domestic": 1
		}, {
			"label": "Rarotonga, Cook Islands - Rarotonga (RAR)",
			"value": "RAR",
			"domestic": 0
		}, {
			"label": "Rasht, Iran - Rasht Airport (RAS)",
			"value": "RAS",
			"domestic": 0
		}, {
			"label": "Rabat, Morocco - Sale (RBA)",
			"value": "RBA",
			"domestic": 0
		}, {
			"label": "Borba, Brazil - Borba Airport (RBB)",
			"value": "RBB",
			"domestic": 0
		}, {
			"label": "Ratanakiri, Cambodia - Ratanakiri Airport (RBE)",
			"value": "RBE",
			"domestic": 0
		}, {
			"label": "Big Bear City, United States - Big Bear Mncpl (RBF)",
			"value": "RBF",
			"domestic": 1
		}, {
			"label": "Roseburg, United States - Roseburg Mncpl (RBG)",
			"value": "RBG",
			"domestic": 1
		}, {
			"label": "Red Bluff, United States - Red Bluff Mncpl (RBL)",
			"value": "RBL",
			"domestic": 1
		}, {
			"label": "Rurrenabaque, Bolivia - Rurrenabaque Airport (RBQ)",
			"value": "RBQ",
			"domestic": 0
		}, {
			"label": "Rio Branco, Brazil - Pres. Medici Airport (RBR)",
			"value": "RBR",
			"domestic": 0
		}, {
			"label": "Ramata, Solomon Islands - Ramata Airport (RBV)",
			"value": "RBV",
			"domestic": 0
		}, {
			"label": "Walterboro, United States - Waterboro Mncpl (RBW)",
			"value": "RBW",
			"domestic": 1
		}, {
			"label": "Ruby, United States - Ruby Airport (RBY)",
			"value": "RBY",
			"domestic": 1
		}, {
			"label": "Richards Bay, South Africa - Richards Bay (RCB)",
			"value": "RCB",
			"domestic": 0
		}, {
			"label": "Roche Harbor, United States - Roche Harbor Mncpl (RCE)",
			"value": "RCE",
			"domestic": 1
		}, {
			"label": "Riohacha, Colombia - Riohacha Airport (RCH)",
			"value": "RCH",
			"domestic": 0
		}, {
			"label": "Redcliffe, Vanuatu - Redcliffe Airport (RCL)",
			"value": "RCL",
			"domestic": 0
		}, {
			"label": "Richmond, Australia - Richmond Airport (RCM)",
			"value": "RCM",
			"domestic": 0
		}, {
			"label": "Rochefort, France - Rochefort (RCO)",
			"value": "RCO",
			"domestic": 0
		}, {
			"label": "Red Dog, United States - Red Dog Airport (RDB)",
			"value": "RDB",
			"domestic": 1
		}, {
			"label": "Redding, United States - Redding Airport (RDD)",
			"value": "RDD",
			"domestic": 1
		}, {
			"label": "Reading, United States - Reading Municipal Airport/Spaatz Field (RDG)",
			"value": "RDG",
			"domestic": 1
		}, {
			"label": "Redmond, United States - Roberts Field (RDM)",
			"value": "RDM",
			"domestic": 1
		}, {
			"label": "Redang, Malaysia - LTS Pulau Redang Airport (RDN)",
			"value": "RDN",
			"domestic": 0
		}, {
			"label": "Raleigh, United States - Raleigh (RDU)",
			"value": "RDU",
			"domestic": 1
		}, {
			"label": "Red Devil, United States - Red Devil Airport (RDV)",
			"value": "RDV",
			"domestic": 1
		}, {
			"label": "Rodez, France - Marcillac (RDZ)",
			"value": "RDZ",
			"domestic": 0
		}, {
			"label": "Recife, Brazil - Guararapes Intl (REC)",
			"value": "REC",
			"domestic": 0
		}, {
			"label": "Reggio di Calabria, Italy - Tito Menniti (REG)",
			"value": "REG",
			"domestic": 0
		}, {
			"label": "Reykjavik, Iceland - All Airports (REK)",
			"value": "REK",
			"domestic": 0
		}, {
			"label": "Trelew, Argentina - Trelew Airport (REL)",
			"value": "REL",
			"domestic": 0
		}, {
			"label": "Orenburg, Russia - Orenburg Airport (REN)",
			"value": "REN",
			"domestic": 0
		}, {
			"label": "Siem Reap, Cambodia - Siem Reap (REP)",
			"value": "REP",
			"domestic": 0
		}, {
			"label": "Resistencia, Argentina - Resistencia Airport (RES)",
			"value": "RES",
			"domestic": 0
		}, {
			"label": "Rost, Norway - Stolport Airport (RET)",
			"value": "RET",
			"domestic": 0
		}, {
			"label": "Reus, Spain - Reus (REU)",
			"value": "REU",
			"domestic": 0
		}, {
			"label": "Reynosa, Mexico - Gen Lucio Blanco (REX)",
			"value": "REX",
			"domestic": 0
		}, {
			"label": "Rockford, United States - Greater Rockford Airport (RFD)",
			"value": "RFD",
			"domestic": 1
		}, {
			"label": "Raiatea, French Polynesia and Tahiti - Uturoa (RFP)",
			"value": "RFP",
			"domestic": 0
		}, {
			"label": "Rio Grande, Argentina - Rio Grande Airport (RGA)",
			"value": "RGA",
			"domestic": 0
		}, {
			"label": "Rangiroa, French Polynesia and Tahiti - Rangiroa (RGI)",
			"value": "RGI",
			"domestic": 0
		}, {
			"label": "Rio Gallegos, Argentina - Rio Gallegos Internacional Airport (RGL)",
			"value": "RGL",
			"domestic": 0
		}, {
			"label": "Yangon, Myanmar (Burma) - Mingaladon (RGN)",
			"value": "RGN",
			"domestic": 0
		}, {
			"label": "Burgos, Spain - Villafria (RGS)",
			"value": "RGS",
			"domestic": 0
		}, {
			"label": "Reims, France - Reims-Champagne (RHE)",
			"value": "RHE",
			"domestic": 0
		}, {
			"label": "Ruhengeri, Rwanda - Ruhengeri (RHG)",
			"value": "RHG",
			"domestic": 0
		}, {
			"label": "Rhinelander, United States - Oneida County Airport (RHI)",
			"value": "RHI",
			"domestic": 1
		}, {
			"label": "Rhodes Is., Greece - Diagoras (RHO)",
			"value": "RHO",
			"domestic": 0
		}, {
			"label": "Riberalta, Bolivia - Gen Buech Airport (RIB)",
			"value": "RIB",
			"domestic": 0
		}, {
			"label": "Richmond, United States - Richmond (RIC)",
			"value": "RIC",
			"domestic": 1
		}, {
			"label": "Rice Lake, United States - Rice Lake Mncpl (RIE)",
			"value": "RIE",
			"domestic": 1
		}, {
			"label": "Richfield, United States - Richfield Mncpl (RIF)",
			"value": "RIF",
			"domestic": 1
		}, {
			"label": "Rio Grande, Brazil - Rio Grande (RIG)",
			"value": "RIG",
			"domestic": 0
		}, {
			"label": "Carrillo, Costa Rica - Carrillo Mncpl (RIK)",
			"value": "RIK",
			"domestic": 0
		}, {
			"label": "Rifle, United States - GarField Cty (RIL)",
			"value": "RIL",
			"domestic": 1
		}, {
			"label": "Rio de Janeiro, Brazil - All Airports (RIO)",
			"value": "RIO",
			"domestic": 0
		}, {
			"label": "Rishiri, Japan - Rishiri Airport (RIS)",
			"value": "RIS",
			"domestic": 0
		}, {
			"label": "Riverton, United States - Riverton Regional Airport (RIW)",
			"value": "RIW",
			"domestic": 1
		}, {
			"label": "Riga, Latvia - Riga Airport (RIX)",
			"value": "RIX",
			"domestic": 0
		}, {
			"label": "Riyan, Yemen - Riyan Airport (RIY)",
			"value": "RIY",
			"domestic": 0
		}, {
			"label": "Rajahmundry, India - Rajahmundry Airport (RJA)",
			"value": "RJA",
			"domestic": 0
		}, {
			"label": "Rajshahi, Bangladesh - Rajshahi Airport (RJH)",
			"value": "RJH",
			"domestic": 0
		}, {
			"label": "Rijeka, Croatia - Rijeka (RJK)",
			"value": "RJK",
			"domestic": 0
		}, {
			"label": "Logrono, Spain - Logrono (RJL)",
			"value": "RJL",
			"domestic": 0
		}, {
			"label": "Rafsanjan, Iran - Rafsanjan Airport (RJN)",
			"value": "RJN",
			"domestic": 0
		}, {
			"label": "Rockland, United States - Knox Cty Regl (RKD)",
			"value": "RKD",
			"domestic": 1
		}, {
			"label": "Rock Hill, United States - Bryant Field (RKH)",
			"value": "RKH",
			"domestic": 1
		}, {
			"label": "Rock Springs, United States - Rock Springs-Sweetwater County Airport (RKS)",
			"value": "RKS",
			"domestic": 1
		}, {
			"label": "Ras Al Khaimah, United Arab Emirates - Ras Al Khaimah (RKT)",
			"value": "RKT",
			"domestic": 0
		}, {
			"label": "Reykjavik, Iceland - Reykjavik Domestic (RKV)",
			"value": "RKV",
			"domestic": 0
		}, {
			"label": "Rockwood, United States - Rockwood Mncpl (RKW)",
			"value": "RKW",
			"domestic": 1
		}, {
			"label": "Rolla, United States - National (RLA)",
			"value": "RLA",
			"domestic": 1
		}, {
			"label": "Richland, United States - Richland Mncpl (RLD)",
			"value": "RLD",
			"domestic": 1
		}, {
			"label": "Rostock, Germany - Rostock (RLG)",
			"value": "RLG",
			"domestic": 0
		}, {
			"label": "Roma, Australia - Roma (RMA)",
			"value": "RMA",
			"domestic": 0
		}, {
			"label": "Marsa Alam, Egypt - Marsa Alam (RMF)",
			"value": "RMF",
			"domestic": 0
		}, {
			"label": "Rome, United States - Richard B. Russell (RMG)",
			"value": "RMG",
			"domestic": 1
		}, {
			"label": "Rimini, Italy - Miramare (RMI)",
			"value": "RMI",
			"domestic": 0
		}, {
			"label": "Renmark, Australia - Renmark (RMK)",
			"value": "RMK",
			"domestic": 0
		}, {
			"label": "Colombo, Sri Lanka - Ratmalana (RML)",
			"value": "RML",
			"domestic": 0
		}, {
			"label": "Rampart, United States - Rampart Airport (RMP)",
			"value": "RMP",
			"domestic": 1
		}, {
			"label": "Taichung, China - ChingChuanKang Airport (RMQ)",
			"value": "RMQ",
			"domestic": 0
		}, {
			"label": "Rimatara, French Polynesia and Tahiti - Rimatara Airport (RMT)",
			"value": "RMT",
			"domestic": 0
		}, {
			"label": "Arona, Solomon Islands - Ulawa Airport (RNA)",
			"value": "RNA",
			"domestic": 0
		}, {
			"label": "Ronneby, Sweden - Kallinge (RNB)",
			"value": "RNB",
			"domestic": 0
		}, {
			"label": "Roanne, France - Renaison (RNE)",
			"value": "RNE",
			"domestic": 0
		}, {
			"label": "Corn Island, Nicaragua - Corn Island Airport (RNI)",
			"value": "RNI",
			"domestic": 0
		}, {
			"label": "Yoronjima, Japan - Yoronjima Airport (RNJ)",
			"value": "RNJ",
			"domestic": 0
		}, {
			"label": "Rennell, Solomon Islands - Rennell Airport (RNL)",
			"value": "RNL",
			"domestic": 0
		}, {
			"label": "Ronne, Denmark - Bornholm (RNN)",
			"value": "RNN",
			"domestic": 0
		}, {
			"label": "Reno, United States - Reno (RNO)",
			"value": "RNO",
			"domestic": 1
		}, {
			"label": "Rongelap, Marshall Islands - Rongelap Airport (RNP)",
			"value": "RNP",
			"domestic": 0
		}, {
			"label": "Rennes, France - St Jacques (RNS)",
			"value": "RNS",
			"domestic": 0
		}, {
			"label": "Roanoke, United States - Roanoke Municipal Airport (ROA)",
			"value": "ROA",
			"domestic": 1
		}, {
			"label": "Monrovia, Liberia - Roberts Intl (ROB)",
			"value": "ROB",
			"domestic": 0
		}, {
			"label": "Rochester, United States - Rochester (ROC)",
			"value": "ROC",
			"domestic": 1
		}, {
			"label": "Rogers, United States - Rogers Mncpl (ROG)",
			"value": "ROG",
			"domestic": 1
		}, {
			"label": "Roi Et, Thailand - Roi Et Airport (ROI)",
			"value": "ROI",
			"domestic": 0
		}, {
			"label": "Rockhampton, Australia - Rockhampton (ROK)",
			"value": "ROK",
			"domestic": 0
		}, {
			"label": "Roosevelt, United States - Roosevelt Mncpl (ROL)",
			"value": "ROL",
			"domestic": 1
		}, {
			"label": "Rome, Italy - All Airports (ROM)",
			"value": "ROM",
			"domestic": 0
		}, {
			"label": "Rondonopolis, Brazil - Rondonopolis (ROO)",
			"value": "ROO",
			"domestic": 0
		}, {
			"label": "Rota, Northern Mariana Islands - Rota (ROP)",
			"value": "ROP",
			"domestic": 0
		}, {
			"label": "Koror, Palau - Airai (ROR)",
			"value": "ROR",
			"domestic": 0
		}, {
			"label": "Rosario, Argentina - Fisherton (ROS)",
			"value": "ROS",
			"domestic": 0
		}, {
			"label": "Rotorua, New Zealand - Rotorua (ROT)",
			"value": "ROT",
			"domestic": 0
		}, {
			"label": "Rousse, Bulgaria - Rousse (ROU)",
			"value": "ROU",
			"domestic": 0
		}, {
			"label": "Rostov, Russia - Rostov (ROV)",
			"value": "ROV",
			"domestic": 0
		}, {
			"label": "Roswell, United States - Roswell Industrial (ROW)",
			"value": "ROW",
			"domestic": 1
		}, {
			"label": "Rosh-Pina, Israel - Rosh-Pina (RPN)",
			"value": "RPN",
			"domestic": 0
		}, {
			"label": "Raipur, India - Raipur Airport (RPR)",
			"value": "RPR",
			"domestic": 0
		}, {
			"label": "Rodrigues Island, Mauritius - Rodrigues Island Airport (RRG)",
			"value": "RRG",
			"domestic": 0
		}, {
			"label": "Sorrento, Italy - Sorrento (RRO)",
			"value": "RRO",
			"domestic": 0
		}, {
			"label": "Roros, Norway - Roros (RRS)",
			"value": "RRS",
			"domestic": 0
		}, {
			"label": "Warroad, United States - Warroad Intl (RRT)",
			"value": "RRT",
			"domestic": 1
		}, {
			"label": "Santa Rosa, Argentina - Santa Rosa Airport (RSA)",
			"value": "RSA",
			"domestic": 0
		}, {
			"label": "Rock Sound, Bahamas - South Eleuthera Airport (RSD)",
			"value": "RSD",
			"domestic": 0
		}, {
			"label": "Russian Mission, United States - Russian Sea Plane Base (RSH)",
			"value": "RSH",
			"domestic": 1
		}, {
			"label": "Rio Sidra, Panama - Rio Sidra Airport (RSI)",
			"value": "RSI",
			"domestic": 0
		}, {
			"label": "Rosario, United States - Rosario Sea Plane Base (RSJ)",
			"value": "RSJ",
			"domestic": 1
		}, {
			"label": "Rochester, United States - Rochester Municipal Airport (RST)",
			"value": "RST",
			"domestic": 1
		}, {
			"label": "Yeosu, South Korea - Yeosu Airport (RSU)",
			"value": "RSU",
			"domestic": 0
		}, {
			"label": "Ft Myers, FL, United States - Ft. Myers (RSW)",
			"value": "RSW",
			"domestic": 1
		}, {
			"label": "Rotuma Island, Fiji - Rotuma Island Airport (RTA)",
			"value": "RTA",
			"domestic": 0
		}, {
			"label": "Roatan, Honduras - Roatan (RTB)",
			"value": "RTB",
			"domestic": 0
		}, {
			"label": "Rotterdam, Netherlands - Rotterdam (RTM)",
			"value": "RTM",
			"domestic": 0
		}, {
			"label": "Saratov, Russia - Saratov (RTW)",
			"value": "RTW",
			"domestic": 0
		}, {
			"label": "Arua, Uganda - Arua Airport (RUA)",
			"value": "RUA",
			"domestic": 0
		}, {
			"label": "Riyadh, Saudi Arabia - King Khaled Intl (RUH)",
			"value": "RUH",
			"domestic": 0
		}, {
			"label": "Ruidoso, United States - Ruidoso Mncpl (RUI)",
			"value": "RUI",
			"domestic": 1
		}, {
			"label": "Rukumkot, Nepal - Rukumkot Airport (RUK)",
			"value": "RUK",
			"domestic": 0
		}, {
			"label": "Rumjatar, Nepal - Rumjatar Airport (RUM)",
			"value": "RUM",
			"domestic": 0
		}, {
			"label": "St Denis, Reunion - Gillot (RUN)",
			"value": "RUN",
			"domestic": 0
		}, {
			"label": "Rurutu, French Polynesia and Tahiti - Rurutu Airport (RUR)",
			"value": "RUR",
			"domestic": 0
		}, {
			"label": "Marau Sound, Solomon Islands - Marau Sound Airport (RUS)",
			"value": "RUS",
			"domestic": 0
		}, {
			"label": "Rutland, United States - Rutland State (RUT)",
			"value": "RUT",
			"domestic": 1
		}, {
			"label": "Farafangana, Madagascar - Farafangana Airport (RVA)",
			"value": "RVA",
			"domestic": 0
		}, {
			"label": "Saravena, Colombia - Saravena Airport (RVE)",
			"value": "RVE",
			"domestic": 0
		}, {
			"label": "Roervik, Norway - Roervik (RVK)",
			"value": "RVK",
			"domestic": 0
		}, {
			"label": "Rovaniemi, Finland - Rovaniemi (RVN)",
			"value": "RVN",
			"domestic": 0
		}, {
			"label": "Green River, United States - Green River Mncpl (RVR)",
			"value": "RVR",
			"domestic": 1
		}, {
			"label": "Ravensthorpe, Australia - Ravensthorpe Airport (RVT)",
			"value": "RVT",
			"domestic": 0
		}, {
			"label": "Rairua, French Polynesia and Tahiti - Raivavae Airport (RVV)",
			"value": "RVV",
			"domestic": 0
		}, {
			"label": "Redwood Falls, United States - Redwood Falls Mncpl (RWF)",
			"value": "RWF",
			"domestic": 1
		}, {
			"label": "Rocky Mount, United States - Rocky Mount-Wilson (RWI)",
			"value": "RWI",
			"domestic": 1
		}, {
			"label": "Rawlins, United States - Rawlins (RWL)",
			"value": "RWL",
			"domestic": 1
		}, {
			"label": "Rawalpindi, Pakistan - Off-Line Point (RWP)",
			"value": "RWP",
			"domestic": 0
		}, {
			"label": "Roxas City, Philippines - Roxas City Airport (RXS)",
			"value": "RXS",
			"domestic": 0
		}, {
			"label": "Rygge, Norway - Moss Airport (RYG)",
			"value": "RYG",
			"domestic": 0
		}, {
			"label": "Rahim Yar Khan, Pakistan - Rahim Yar Khan Airport (RYK)",
			"value": "RYK",
			"domestic": 0
		}, {
			"label": "Royan, France - Medis (RYN)",
			"value": "RYN",
			"domestic": 0
		}, {
			"label": "Santa Cruz, Argentina - Santa Cruz Airport (RZA)",
			"value": "RZA",
			"domestic": 0
		}, {
			"label": "Rzeszow, Poland - Jasionka (RZE)",
			"value": "RZE",
			"domestic": 0
		}, {
			"label": "Ramsar, Iran - Ramsar Airport (RZR)",
			"value": "RZR",
			"domestic": 0
		}, {
			"label": "Roanoke Rapids, United States - Halifax County (RZZ)",
			"value": "RZZ",
			"domestic": 1
		}, {
			"label": "Saba, Curacao - J Yrausquin (SAB)",
			"value": "SAB",
			"domestic": 0
		}, {
			"label": "Sacramento, United States - All Airports (SAC)",
			"value": "SAC",
			"domestic": 1
		}, {
			"label": "Safford, United States - Safford Mncpl (SAD)",
			"value": "SAD",
			"domestic": 1
		}, {
			"label": "Santa Fe, United States - Santa Fe Airport (SAF)",
			"value": "SAF",
			"domestic": 1
		}, {
			"label": "Sana\'a, Yemen - Sana\'a Intl (SAH)",
			"value": "SAH",
			"domestic": 0
		}, {
			"label": "Saudarkrokur, Iceland - Saudarkrokur (SAK)",
			"value": "SAK",
			"domestic": 0
		}, {
			"label": "San Salvador, El Salvador - Comalapa International Airport (SAL)",
			"value": "SAL",
			"domestic": 0
		}, {
			"label": "San Diego, United States - San Diego (SAN)",
			"value": "SAN",
			"domestic": 1
		}, {
			"label": "Sao Paulo, Brazil - All Airports (SAO)",
			"value": "SAO",
			"domestic": 0
		}, {
			"label": "San Pedro Sula, Honduras - Ramon Villeda Morales Airport (SAP)",
			"value": "SAP",
			"domestic": 0
		}, {
			"label": "San Andros, Bahamas - San Andros Airport (SAQ)",
			"value": "SAQ",
			"domestic": 0
		}, {
			"label": "San Antonio, United States - San Antonio (SAT)",
			"value": "SAT",
			"domestic": 1
		}, {
			"label": "Savannah, United States - Savannah International Airport (SAV)",
			"value": "SAV",
			"domestic": 1
		}, {
			"label": "Istanbul, Turkey - Sabiha Gokcen (SAW)",
			"value": "SAW",
			"domestic": 0
		}, {
			"label": "Sambu, Panama - Sambu Airport (SAX)",
			"value": "SAX",
			"domestic": 0
		}, {
			"label": "Siena, Italy - Siena (SAY)",
			"value": "SAY",
			"domestic": 0
		}, {
			"label": "Santa Barbara, United States - Santa Barbara (SBA)",
			"value": "SBA",
			"domestic": 1
		}, {
			"label": "San Bernardino, United States - San Bernardino Intl (SBD)",
			"value": "SBD",
			"domestic": 1
		}, {
			"label": "St Barthelemy, Bahamas - St Barthelemy (SBH)",
			"value": "SBH",
			"domestic": 0
		}, {
			"label": "St Brieuc, France - Tremuson St Brieuc (SBK)",
			"value": "SBK",
			"domestic": 0
		}, {
			"label": "Sheboygan, United States - Sheboygan County Mem (SBM)",
			"value": "SBM",
			"domestic": 1
		}, {
			"label": "South Bend, United States - St Joseph County Airport (SBN)",
			"value": "SBN",
			"domestic": 1
		}, {
			"label": "San Luis Obispo, United States - County Airport (SBP)",
			"value": "SBP",
			"domestic": 1
		}, {
			"label": "Saibai Island, Australia - Saibai Island Airport (SBR)",
			"value": "SBR",
			"domestic": 0
		}, {
			"label": "Steamboat Springs, United States - Steamboat Springs Airport (SBS)",
			"value": "SBS",
			"domestic": 1
		}, {
			"label": "Sibu, Malaysia - Sibu (SBW)",
			"value": "SBW",
			"domestic": 0
		}, {
			"label": "Salisbury, United States - Salisbury (SBY)",
			"value": "SBY",
			"domestic": 1
		}, {
			"label": "Sibiu, Romania - Sibiu (SBZ)",
			"value": "SBZ",
			"domestic": 0
		}, {
			"label": "Prudhoe Bay, United States - Deadhorse Airport (SCC)",
			"value": "SCC",
			"domestic": 1
		}, {
			"label": "State College, United States - University Park Airport (SCE)",
			"value": "SCE",
			"domestic": 1
		}, {
			"label": "Schenectady, United States - Schenectady Mncpl (SCH)",
			"value": "SCH",
			"domestic": 1
		}, {
			"label": "Stockton, United States - Stockton Airport (SCK)",
			"value": "SCK",
			"domestic": 1
		}, {
			"label": "Santiago, Chile and Easter Island - Arturo Merino Benitez Airport (SCL)",
			"value": "SCL",
			"domestic": 0
		}, {
			"label": "Scammon Bay, United States - Scammon Bay Sea Plane Base (SCM)",
			"value": "SCM",
			"domestic": 1
		}, {
			"label": "Saarbruecken, Germany - Ensheim (SCN)",
			"value": "SCN",
			"domestic": 0
		}, {
			"label": "Aktau, Kazakhstan - Aktau (SCO)",
			"value": "SCO",
			"domestic": 0
		}, {
			"label": "Santiago de Compostela, Spain - Labacolla (SCQ)",
			"value": "SCQ",
			"domestic": 0
		}, {
			"label": "Socotra, Yemen - Socotra Airport (SCT)",
			"value": "SCT",
			"domestic": 0
		}, {
			"label": "Santiago de Cuba, Cuba - Antonio Maceo (SCU)",
			"value": "SCU",
			"domestic": 0
		}, {
			"label": "Suceava, Romania - Salcea (SCV)",
			"value": "SCV",
			"domestic": 0
		}, {
			"label": "Syktyvkar, Russia - Syktyvkar Airport (SCW)",
			"value": "SCW",
			"domestic": 0
		}, {
			"label": "Salina Cruz, Mexico - Salina Cruz Airport (SCX)",
			"value": "SCX",
			"domestic": 0
		}, {
			"label": "San Cristobal, Ecuador - San Cristobal Airport (SCY)",
			"value": "SCY",
			"domestic": 0
		}, {
			"label": "Santa Cruz Island, Solomon Islands - Santa Cruz Island Airport (SCZ)",
			"value": "SCZ",
			"domestic": 0
		}, {
			"label": "Lubango, Angola - Lubango Airport (SDD)",
			"value": "SDD",
			"domestic": 0
		}, {
			"label": "Santiago del Estero, Argentina - Santiago del Estero (SDE)",
			"value": "SDE",
			"domestic": 0
		}, {
			"label": "Louisville, United States - Louisville (SDF)",
			"value": "SDF",
			"domestic": 1
		}, {
			"label": "Sanandaj, Iran - Sanandaj Airport (SDG)",
			"value": "SDG",
			"domestic": 0
		}, {
			"label": "Saidor, Papua New Guinea - Saidor Airport (SDI)",
			"value": "SDI",
			"domestic": 0
		}, {
			"label": "Sendai, Japan - Sendai (SDJ)",
			"value": "SDJ",
			"domestic": 0
		}, {
			"label": "Sandakan, Malaysia - Sandakan (SDK)",
			"value": "SDK",
			"domestic": 0
		}, {
			"label": "Sundsvall, Sweden - Harnosand (SDL)",
			"value": "SDL",
			"domestic": 0
		}, {
			"label": "Sandane, Norway - Sandane Anda (SDN)",
			"value": "SDN",
			"domestic": 0
		}, {
			"label": "Sand Point, United States - Sand Point Municipal Airport (SDP)",
			"value": "SDP",
			"domestic": 1
		}, {
			"label": "Santo Domingo, Dominican Republic - Las Americas Airport (SDQ)",
			"value": "SDQ",
			"domestic": 0
		}, {
			"label": "Santander, Spain - Santander (SDR)",
			"value": "SDR",
			"domestic": 0
		}, {
			"label": "Saidu Sharif, Pakistan - Saidu Sharif (SDT)",
			"value": "SDT",
			"domestic": 0
		}, {
			"label": "Rio de Janeiro, Brazil - Santos Dumont (SDU)",
			"value": "SDU",
			"domestic": 0
		}, {
			"label": "Tel Aviv, Israel - Sde Dov (SDV)",
			"value": "SDV",
			"domestic": 0
		}, {
			"label": "Sedona, United States - Sedona (SDX)",
			"value": "SDX",
			"domestic": 1
		}, {
			"label": "Sidney, United States - Sidney/Richland (SDY)",
			"value": "SDY",
			"domestic": 1
		}, {
			"label": "Lerwick, United Kingdom - All Airports (SDZ)",
			"value": "SDZ",
			"domestic": 0
		}, {
			"label": "Seattle, United States - Seattle (SEA)",
			"value": "SEA",
			"domestic": 1
		}, {
			"label": "Sebha, Libya - Sebha Airport (SEB)",
			"value": "SEB",
			"domestic": 0
		}, {
			"label": "Sebring, United States - Industrial Park (SEF)",
			"value": "SEF",
			"domestic": 1
		}, {
			"label": "Seoul, South Korea - All Airports (SEL)",
			"value": "SEL",
			"domestic": 0
		}, {
			"label": "Southend, United Kingdom - Southend Mncpl (SEN)",
			"value": "SEN",
			"domestic": 0
		}, {
			"label": "Stephenville, United States - Clark Field Mncpl (SEP)",
			"value": "SEP",
			"domestic": 1
		}, {
			"label": "Mahe Island, Seychelles - Seychelles Intl (SEZ)",
			"value": "SEZ",
			"domestic": 0
		}, {
			"label": "Sfax, Tunisia - Sfax El Maou (SFA)",
			"value": "SFA",
			"domestic": 0
		}, {
			"label": "St Francois, St. Barthelemy - St Francois (SFC)",
			"value": "SFC",
			"domestic": 0
		}, {
			"label": "San Fernando De Apure, Venezuela - Las Flecheras Airport (SFD)",
			"value": "SFD",
			"domestic": 0
		}, {
			"label": "San Fernando, Philippines - San Fernando Airport (SFE)",
			"value": "SFE",
			"domestic": 0
		}, {
			"label": "Saint Martin, Guadeloupe - Esperance Airport (SFG)",
			"value": "SFG",
			"domestic": 0
		}, {
			"label": "Kangerlussuaq, Greenland - Kangerlussuaq (SFJ)",
			"value": "SFJ",
			"domestic": 0
		}, {
			"label": "Sanford, United States - Sanford (SFM)",
			"value": "SFM",
			"domestic": 1
		}, {
			"label": "Santa Fe, Argentina - Santa Fe Mncpl (SFN)",
			"value": "SFN",
			"domestic": 0
		}, {
			"label": "San Francisco, United States - SFO (SFO)",
			"value": "SFO",
			"domestic": 1
		}, {
			"label": "Sanliurfa, Turkey - Sanliurfa (SFQ)",
			"value": "SFQ",
			"domestic": 0
		}, {
			"label": "Subic Bay, Philippines - Subic Bay International Airport (SFS)",
			"value": "SFS",
			"domestic": 0
		}, {
			"label": "Skelleftea, Sweden - Skelleftea (SFT)",
			"value": "SFT",
			"domestic": 0
		}, {
			"label": "Surgut, Russia - Surgut (SGC)",
			"value": "SGC",
			"domestic": 0
		}, {
			"label": "Sonderborg, Denmark - Sonderborg (SGD)",
			"value": "SGD",
			"domestic": 0
		}, {
			"label": "Springfield, MO, United States - Springfield-Branson Regional Airport (SGF)",
			"value": "SGF",
			"domestic": 1
		}, {
			"label": "Sangapi, Papua New Guinea - Sangapi Airport (SGK)",
			"value": "SGK",
			"domestic": 0
		}, {
			"label": "Ho Chi Minh City, Vietnam - ho chi minh city (SGN)",
			"value": "SGN",
			"domestic": 0
		}, {
			"label": "St George, Australia - St. George Airport (SGO)",
			"value": "SGO",
			"domestic": 0
		}, {
			"label": "St. George, United States - Saint George Municipal Airport (SGU)",
			"value": "SGU",
			"domestic": 1
		}, {
			"label": "Skagway, United States - Skagway (SGY)",
			"value": "SGY",
			"domestic": 1
		}, {
			"label": "Shanghai, China - Hongqiao (SHA)",
			"value": "SHA",
			"domestic": 0
		}, {
			"label": "Nakashibetsu, Japan - Nakashibetsu Airport (SHB)",
			"value": "SHB",
			"domestic": 0
		}, {
			"label": "Indaselassie, Ethiopia - Indaselassie Airport (SHC)",
			"value": "SHC",
			"domestic": 0
		}, {
			"label": "Staunton, United States - Shenandoah Valley Airport (SHD)",
			"value": "SHD",
			"domestic": 1
		}, {
			"label": "Shenyang, China - Shenyang (SHE)",
			"value": "SHE",
			"domestic": 0
		}, {
			"label": "Shungnak, United States - Shungnak Airport (SHG)",
			"value": "SHG",
			"domestic": 1
		}, {
			"label": "Shishmaref, United States - Shishmaref Airport (SHH)",
			"value": "SHH",
			"domestic": 1
		}, {
			"label": "Sharjah, United Arab Emirates - Sharjah (SHJ)",
			"value": "SHJ",
			"domestic": 0
		}, {
			"label": "Shillong, India - Shillong Airport (SHL)",
			"value": "SHL",
			"domestic": 0
		}, {
			"label": "Shirahama, Japan - Shirahama Airport (SHM)",
			"value": "SHM",
			"domestic": 0
		}, {
			"label": "Sokcho, South Korea - Seolak (SHO)",
			"value": "SHO",
			"domestic": 0
		}, {
			"label": "Qinhuangdao, China - Qinhuangdao (SHP)",
			"value": "SHP",
			"domestic": 0
		}, {
			"label": "Sheridan, United States - Sheridan County Airport (SHR)",
			"value": "SHR",
			"domestic": 1
		}, {
			"label": "Shepparton, Australia - Shepparton (SHT)",
			"value": "SHT",
			"domestic": 0
		}, {
			"label": "Shreveport, United States - Shreveport (SHV)",
			"value": "SHV",
			"domestic": 1
		}, {
			"label": "Shreveport, United States - Shreveport Downtown (SHV)",
			"value": "SHV",
			"domestic": 1
		}, {
			"label": "Sharurah, Saudi Arabia - Sharurah Airport (SHW)",
			"value": "SHW",
			"domestic": 0
		}, {
			"label": "Shageluk, United States - Shageluk Airport (SHX)",
			"value": "SHX",
			"domestic": 1
		}, {
			"label": "Shinyanga, Tanzania - Shinyanga Airport (SHY)",
			"value": "SHY",
			"domestic": 0
		}, {
			"label": "Xi\'an, China - Xiguan (SIA)",
			"value": "SIA",
			"domestic": 0
		}, {
			"label": "Sal, Cape Verde - Amilcar Cabral International Airport (SID)",
			"value": "SID",
			"domestic": 0
		}, {
			"label": "Simara, Nepal - Simara Airport (SIF)",
			"value": "SIF",
			"domestic": 0
		}, {
			"label": "Sikeston, United States - Sikeston Memorial (SIK)",
			"value": "SIK",
			"domestic": 1
		}, {
			"label": "Singapore, Singapore - Changi Airport (SIN)",
			"value": "SIN",
			"domestic": 0
		}, {
			"label": "Simferopol, Ukraine - Simferopol Airport (SIP)",
			"value": "SIP",
			"domestic": 0
		}, {
			"label": "Sion, Switzerland - Sion (SIR)",
			"value": "SIR",
			"domestic": 0
		}, {
			"label": "Sitka, United States - Sitka Airport (SIT)",
			"value": "SIT",
			"domestic": 1
		}, {
			"label": "San Jose, United States - San Jose (SJC)",
			"value": "SJC",
			"domestic": 1
		}, {
			"label": "Los Cabos, Mexico - Los Cabos Airport (SJD)",
			"value": "SJD",
			"domestic": 0
		}, {
			"label": "San Jose Del Gua, Colombia - San Jose Del Gua Airport (SJE)",
			"value": "SJE",
			"domestic": 0
		}, {
			"label": "Cruz Bay, U.S. Virgin Islands - Seaplane Ramp (SJF)",
			"value": "SJF",
			"domestic": 0
		}, {
			"label": "San Jose, Philippines - McGuire Field (SJI)",
			"value": "SJI",
			"domestic": 0
		}, {
			"label": "Sarajevo, Bosnia and Herzegovina - Butmir (SJJ)",
			"value": "SJJ",
			"domestic": 0
		}, {
			"label": "Sao Jose dos Campos, Brazil - Sao Jose Dos Campos (SJK)",
			"value": "SJK",
			"domestic": 0
		}, {
			"label": "Sao Gabriel, Brazil - Da Cachoeira Airport (SJL)",
			"value": "SJL",
			"domestic": 0
		}, {
			"label": "San Jose, Costa Rica - Juan Santamaria International Airport (SJO)",
			"value": "SJO",
			"domestic": 0
		}, {
			"label": "Sao Jose do Rio Preto, Brazil - Sao Pedro Rio Preto (SJP)",
			"value": "SJP",
			"domestic": 0
		}, {
			"label": "San Angelo, United States - Mathis Field (SJT)",
			"value": "SJT",
			"domestic": 1
		}, {
			"label": "San Juan, Puerto Rico - San Juan (SJU)",
			"value": "SJU",
			"domestic": 0
		}, {
			"label": "Shijiazhuang, China - Shijiazhuang (SJW)",
			"value": "SJW",
			"domestic": 0
		}, {
			"label": "Seinajoki, Finland - Ilmajoki (SJY)",
			"value": "SJY",
			"domestic": 0
		}, {
			"label": "Sao Jorge Island, Portugal - Sao Jorge Island Airport (SJZ)",
			"value": "SJZ",
			"domestic": 0
		}, {
			"label": "St. Kitts, St. Kitts and Nevis - Golden Rock Airport (SKB)",
			"value": "SKB",
			"domestic": 0
		}, {
			"label": "Samarkand, Uzbekistan - Samarkand (SKD)",
			"value": "SKD",
			"domestic": 0
		}, {
			"label": "Skien, Norway - Skien (SKE)",
			"value": "SKE",
			"domestic": 0
		}, {
			"label": "Thessaloniki, Greece - Makedonia (SKG)",
			"value": "SKG",
			"domestic": 0
		}, {
			"label": "Surkhet, Nepal - Surkhet Airport (SKH)",
			"value": "SKH",
			"domestic": 0
		}, {
			"label": "Shaktoolik, United States - Shaktoolik Airport (SKK)",
			"value": "SKK",
			"domestic": 1
		}, {
			"label": "Broadford, United Kingdom - Broadford (SKL)",
			"value": "SKL",
			"domestic": 0
		}, {
			"label": "Stokmarknes, Norway - Skagen (SKN)",
			"value": "SKN",
			"domestic": 0
		}, {
			"label": "Sokoto, Nigeria - Sokoto Airport (SKO)",
			"value": "SKO",
			"domestic": 0
		}, {
			"label": "Skopje, Macedonia - Skopje (SKP)",
			"value": "SKP",
			"domestic": 0
		}, {
			"label": "Skiros, Greece - Skiros (SKU)",
			"value": "SKU",
			"domestic": 0
		}, {
			"label": "Sukkur, Pakistan - Sukkur Airport (SKZ)",
			"value": "SKZ",
			"domestic": 0
		}, {
			"label": "Salta, Argentina - Gen Belgrano (SLA)",
			"value": "SLA",
			"domestic": 0
		}, {
			"label": "Salt Lake City, United States - Salt Lake City (SLC)",
			"value": "SLC",
			"domestic": 1
		}, {
			"label": "Sliac, Slovakia - Sliac Airport (SLD)",
			"value": "SLD",
			"domestic": 0
		}, {
			"label": "Salem, United States - McNary Fld (SLE)",
			"value": "SLE",
			"domestic": 1
		}, {
			"label": "Sola, Vanuatu - Sola Airport (SLH)",
			"value": "SLH",
			"domestic": 0
		}, {
			"label": "Solwezi, Zambia - Solwezi Airport (SLI)",
			"value": "SLI",
			"domestic": 0
		}, {
			"label": "Saranac Lake, United States - Adirondack (SLK)",
			"value": "SLK",
			"domestic": 1
		}, {
			"label": "Salalah, Oman - Salalah (SLL)",
			"value": "SLL",
			"domestic": 0
		}, {
			"label": "Salamanca, Spain - Matacan (SLM)",
			"value": "SLM",
			"domestic": 0
		}, {
			"label": "Salina, United States - Salina Municipal Airport (SLN)",
			"value": "SLN",
			"domestic": 1
		}, {
			"label": "San Luis Potosi, Mexico - San Luis Potosi Airport (SLP)",
			"value": "SLP",
			"domestic": 0
		}, {
			"label": "Sleetmute, United States - Sleetmute Airport (SLQ)",
			"value": "SLQ",
			"domestic": 1
		}, {
			"label": "Sulphur Springs, United States - Sulphur Springs (SLR)",
			"value": "SLR",
			"domestic": 1
		}, {
			"label": "Castries, St. Lucia - Vigie Field (SLU)",
			"value": "SLU",
			"domestic": 0
		}, {
			"label": "Simla, India - Simla (SLV)",
			"value": "SLV",
			"domestic": 0
		}, {
			"label": "Saltillo, Mexico - Saltillo (SLW)",
			"value": "SLW",
			"domestic": 0
		}, {
			"label": "Salt Cay, Turks and Caicos - Salt Cay Mncpl (SLX)",
			"value": "SLX",
			"domestic": 0
		}, {
			"label": "Salekhard, Russia - Salehard Airport (SLY)",
			"value": "SLY",
			"domestic": 0
		}, {
			"label": "Sao Luiz, Brazil - Mal Cunha Machado (SLZ)",
			"value": "SLZ",
			"domestic": 0
		}, {
			"label": "Santa Maria, Portugal - Vila Do Porto (SMA)",
			"value": "SMA",
			"domestic": 0
		}, {
			"label": "Somerset, United States - Somerset (SME)",
			"value": "SME",
			"domestic": 1
		}, {
			"label": "Sacramento, United States - Sacramento (SMF)",
			"value": "SMF",
			"domestic": 1
		}, {
			"label": "Samos Island, Greece - Samos (SMI)",
			"value": "SMI",
			"domestic": 0
		}, {
			"label": "Saint Michael, United States - St. Michael Airport (SMK)",
			"value": "SMK",
			"domestic": 1
		}, {
			"label": "Stella Maris, Bahamas - Estate Airstrip (SML)",
			"value": "SML",
			"domestic": 0
		}, {
			"label": "Salmon, United States - Salmon (SMN)",
			"value": "SMN",
			"domestic": 1
		}, {
			"label": "Santa Monica, United States - Santa Monica Mncpl (SMO)",
			"value": "SMO",
			"domestic": 1
		}, {
			"label": "Sampit, Indonesia - Sampit Airport (SMQ)",
			"value": "SMQ",
			"domestic": 0
		}, {
			"label": "Santa Marta, Colombia - Simon Bolivar (SMR)",
			"value": "SMR",
			"domestic": 0
		}, {
			"label": "Sainte Marie, Madagascar - Sainte Marie Airport (SMS)",
			"value": "SMS",
			"domestic": 0
		}, {
			"label": "St Moritz, Switzerland - St Moritz/Samedan (SMV)",
			"value": "SMV",
			"domestic": 0
		}, {
			"label": "Santa Maria, United States - Santa Maria Public  Airport (SMX)",
			"value": "SMX",
			"domestic": 1
		}, {
			"label": "Santa Ana, United States - Orange/Santa Ana (SNA)",
			"value": "SNA",
			"domestic": 1
		}, {
			"label": "Salinas, Ecuador - Salinas Airport (SNC)",
			"value": "SNC",
			"domestic": 0
		}, {
			"label": "Shannon, Ireland - Shannon Airport (SNN)",
			"value": "SNN",
			"domestic": 0
		}, {
			"label": "Sakon Nakhon, Thailand - Sakon Nakhon Airport (SNO)",
			"value": "SNO",
			"domestic": 0
		}, {
			"label": "Saint Paul Island, United States - Saint Paul Island Airport (SNP)",
			"value": "SNP",
			"domestic": 1
		}, {
			"label": "St Nazaire, France - Montoir (SNR)",
			"value": "SNR",
			"domestic": 0
		}, {
			"label": "Salinas, United States - Salinas (SNS)",
			"value": "SNS",
			"domestic": 1
		}, {
			"label": "Santa Clara, Cuba - Santa Clara (SNU)",
			"value": "SNU",
			"domestic": 0
		}, {
			"label": "Thandwe, Myanmar (Burma) - Thandwe (SNW)",
			"value": "SNW",
			"domestic": 0
		}, {
			"label": "Balaton, Hungary - FlyBalaton Airport (SOB)",
			"value": "SOB",
			"domestic": 0
		}, {
			"label": "Solo, Indonesia - Adi Sumarmo (SOC)",
			"value": "SOC",
			"domestic": 0
		}, {
			"label": "Sofia, Bulgaria - Vrazhdebna (SOF)",
			"value": "SOF",
			"domestic": 0
		}, {
			"label": "Sogndal, Norway - Haukasen (SOG)",
			"value": "SOG",
			"domestic": 0
		}, {
			"label": "Sorkjosen, Norway - Sorkjosen (SOJ)",
			"value": "SOJ",
			"domestic": 0
		}, {
			"label": "San Tome, Venezuela - El Tigre Airport (SOM)",
			"value": "SOM",
			"domestic": 0
		}, {
			"label": "Espiritu Santo, Vanuatu - Pekoa (SON)",
			"value": "SON",
			"domestic": 0
		}, {
			"label": "Soderhamn, Sweden - Soderhamn (SOO)",
			"value": "SOO",
			"domestic": 0
		}, {
			"label": "Pinehurst, United States - Southern Pines (SOP)",
			"value": "SOP",
			"domestic": 1
		}, {
			"label": "Sorong, Indonesia - Jefman (SOQ)",
			"value": "SOQ",
			"domestic": 0
		}, {
			"label": "Southampton, United Kingdom - Southampton (SOU)",
			"value": "SOU",
			"domestic": 0
		}, {
			"label": "Seldovia, United States - Seldovia (SOV)",
			"value": "SOV",
			"domestic": 1
		}, {
			"label": "Show Low, United States - Show Low (SOW)",
			"value": "SOW",
			"domestic": 1
		}, {
			"label": "Stronsay, United Kingdom - Stronsay Airport (SOY)",
			"value": "SOY",
			"domestic": 0
		}, {
			"label": "Santa Cruz de la Palma, Spain - La Palma (SPC)",
			"value": "SPC",
			"domestic": 0
		}, {
			"label": "Saidpur, Bangladesh - Saidpur Airport (SPD)",
			"value": "SPD",
			"domestic": 0
		}, {
			"label": "Spearfish, United States - Black Hills (SPF)",
			"value": "SPF",
			"domestic": 1
		}, {
			"label": "Springfield, IL, United States - Capital Airport (SPI)",
			"value": "SPI",
			"domestic": 1
		}, {
			"label": "Sapporo, Japan - All Airports (SPK)",
			"value": "SPK",
			"domestic": 0
		}, {
			"label": "Saipan, Northern Mariana Islands - Saipan Intl (SPN)",
			"value": "SPN",
			"domestic": 0
		}, {
			"label": "Menongue, Angola - Menongue Airport (SPP)",
			"value": "SPP",
			"domestic": 0
		}, {
			"label": "San Pedro, Belize - San Pedro (SPR)",
			"value": "SPR",
			"domestic": 0
		}, {
			"label": "Wichita Falls, United States - Sheppard Air Force Base (SPS)",
			"value": "SPS",
			"domestic": 1
		}, {
			"label": "Split, Croatia - Split (SPU)",
			"value": "SPU",
			"domestic": 0
		}, {
			"label": "Spencer, United States - Spencer Mncpl (SPW)",
			"value": "SPW",
			"domestic": 1
		}, {
			"label": "Springdale, United States - Springdale (SPZ)",
			"value": "SPZ",
			"domestic": 1
		}, {
			"label": "Santa Ynez, United States - Santa Ynez Mncpl (SQA)",
			"value": "SQA",
			"domestic": 1
		}, {
			"label": "Sintang, Indonesia - Sintang Airport (SQG)",
			"value": "SQG",
			"domestic": 0
		}, {
			"label": "San Carlos, United States - San Carlos Mncpl (SQL)",
			"value": "SQL",
			"domestic": 1
		}, {
			"label": "Storuman, Sweden - Gunnarn Airport (SQO)",
			"value": "SQO",
			"domestic": 0
		}, {
			"label": "Sequim, United States - Sequim Valley (SQV)",
			"value": "SQV",
			"domestic": 1
		}, {
			"label": "Sucre, Bolivia - Sucre/Padilla (SRE)",
			"value": "SRE",
			"domestic": 0
		}, {
			"label": "Semarang, Indonesia - Achmad Uani (SRG)",
			"value": "SRG",
			"domestic": 0
		}, {
			"label": "Samarinda, Indonesia - Samarinda Airport (SRI)",
			"value": "SRI",
			"domestic": 0
		}, {
			"label": "San Borja, Bolivia - Capitan G Q Guardia Airport (SRJ)",
			"value": "SRJ",
			"domestic": 0
		}, {
			"label": "Strahan, Australia - Strahan (SRN)",
			"value": "SRN",
			"domestic": 0
		}, {
			"label": "Stord, Norway - Stord (SRP)",
			"value": "SRP",
			"domestic": 0
		}, {
			"label": "Sarasota, United States - Bradenton Airport (SRQ)",
			"value": "SRQ",
			"domestic": 1
		}, {
			"label": "Stony River, United States - Stony River Airport (SRV)",
			"value": "SRV",
			"domestic": 1
		}, {
			"label": "Sary, Iran - Dashte Naz Airport (SRY)",
			"value": "SRY",
			"domestic": 0
		}, {
			"label": "Santa Cruz, Bolivia - El Trompillo (SRZ)",
			"value": "SRZ",
			"domestic": 0
		}, {
			"label": "Salvador, Brazil - Luis E Magalhaes (SSA)",
			"value": "SSA",
			"domestic": 0
		}, {
			"label": "Malabo, Equatorial Guinea - Santa Isabel Airport (SSG)",
			"value": "SSG",
			"domestic": 0
		}, {
			"label": "Sharm el Sheikh, Egypt - Sharm el Sheikh Intl (SSH)",
			"value": "SSH",
			"domestic": 0
		}, {
			"label": "Brunswick, United States - McKinnon (SSI)",
			"value": "SSI",
			"domestic": 1
		}, {
			"label": "Sandnessjoen, Norway - Stokka (SSJ)",
			"value": "SSJ",
			"domestic": 0
		}, {
			"label": "Sault Ste Marie, United States - County (SSM)",
			"value": "SSM",
			"domestic": 1
		}, {
			"label": "Sara, Vanuatu - Sara Airport (SSR)",
			"value": "SSR",
			"domestic": 0
		}, {
			"label": "Samsun, Turkey - Samsun (SSX)",
			"value": "SSX",
			"domestic": 0
		}, {
			"label": "M\'Banza Congo, Angola - M\'Banza Congo Airport (SSY)",
			"value": "SSY",
			"domestic": 0
		}, {
			"label": "St. Cloud, United States - St. Cloud Municipal Airport (STC)",
			"value": "STC",
			"domestic": 1
		}, {
			"label": "Santo Domingo, Venezuela - Mayo Guerrero Airport (STD)",
			"value": "STD",
			"domestic": 0
		}, {
			"label": "Stevens Point, United States - Stevens Point Mncpl (STE)",
			"value": "STE",
			"domestic": 1
		}, {
			"label": "Saint George Island, United States - St. George Island Airport (STG)",
			"value": "STG",
			"domestic": 1
		}, {
			"label": "Santiago, Dominican Republic - Santiago Municipal Airport (STI)",
			"value": "STI",
			"domestic": 0
		}, {
			"label": "St Joseph, United States - Rosecrans Memorial (STJ)",
			"value": "STJ",
			"domestic": 1
		}, {
			"label": "Sterling, United States - Crosson Fld (STK)",
			"value": "STK",
			"domestic": 1
		}, {
			"label": "St. Louis, United States - St. Louis (STL)",
			"value": "STL",
			"domestic": 1
		}, {
			"label": "Santarem, Brazil - Eduardo Gomes Airport (STM)",
			"value": "STM",
			"domestic": 0
		}, {
			"label": "London, United Kingdom - Stansted (STN)",
			"value": "STN",
			"domestic": 0
		}, {
			"label": "Stockholm, Sweden - All Airports (STO)",
			"value": "STO",
			"domestic": 0
		}, {
			"label": "Stuttgart, Germany - Echterdingen Airport (STR)",
			"value": "STR",
			"domestic": 0
		}, {
			"label": "Santa Rosa, United States - Sonoma County Airport (STS)",
			"value": "STS",
			"domestic": 1
		}, {
			"label": "St. Thomas, U.S. Virgin Islands - Cyril E. King Airport (STT)",
			"value": "STT",
			"domestic": 0
		}, {
			"label": "Stavropol, Russia - Stavropol (STW)",
			"value": "STW",
			"domestic": 0
		}, {
			"label": "St. Croix, U.S. Virgin Islands - Alexander Hamilton Airport (STX)",
			"value": "STX",
			"domestic": 0
		}, {
			"label": "Surabaya, Indonesia - Juanda (SUB)",
			"value": "SUB",
			"domestic": 0
		}, {
			"label": "Sturgeon Bay, United States - Door County (SUE)",
			"value": "SUE",
			"domestic": 1
		}, {
			"label": "Lamezia Terme, Italy - S Eufemia (SUF)",
			"value": "SUF",
			"domestic": 0
		}, {
			"label": "Surigao, Philippines - Surigao Airport (SUG)",
			"value": "SUG",
			"domestic": 0
		}, {
			"label": "Satu Mare, Romania - Satu Mare (SUJ)",
			"value": "SUJ",
			"domestic": 0
		}, {
			"label": "Sun Valley, United States - Sun Valley Airport (SUN)",
			"value": "SUN",
			"domestic": 1
		}, {
			"label": "Summer Beaver, Canada - Summer Beaver Airport (SUR)",
			"value": "SUR",
			"domestic": 0
		}, {
			"label": "Suva, Fiji - Nausori (SUV)",
			"value": "SUV",
			"domestic": 0
		}, {
			"label": "Sioux City, United States - Sioux Gateway Airport (SUX)",
			"value": "SUX",
			"domestic": 1
		}, {
			"label": "Savoonga, United States - Savoonga Airport (SVA)",
			"value": "SVA",
			"domestic": 1
		}, {
			"label": "Sambava, Madagascar - Sambava Airport (SVB)",
			"value": "SVB",
			"domestic": 0
		}, {
			"label": "Silver City, United States - Grant Cty (SVC)",
			"value": "SVC",
			"domestic": 1
		}, {
			"label": "Kingstown, St. Vincent and the Grenadines - E T Joshua (SVD)",
			"value": "SVD",
			"domestic": 0
		}, {
			"label": "Stavanger, Norway - Sola (SVG)",
			"value": "SVG",
			"domestic": 0
		}, {
			"label": "Statesville, United States - Statesville Mncpl (SVH)",
			"value": "SVH",
			"domestic": 1
		}, {
			"label": "San Vicente, Colombia - San Vicente Airport (SVI)",
			"value": "SVI",
			"domestic": 0
		}, {
			"label": "Svolvaer, Norway - Helle (SVJ)",
			"value": "SVJ",
			"domestic": 0
		}, {
			"label": "Savonlinna, Finland - Savonlinna (SVL)",
			"value": "SVL",
			"domestic": 0
		}, {
			"label": "Moscow, Russia - Sheremetyevo Airport (SVO)",
			"value": "SVO",
			"domestic": 0
		}, {
			"label": "Seville, Spain - Seville San Pablo (SVQ)",
			"value": "SVQ",
			"domestic": 0
		}, {
			"label": "Stevens Village, United States - Stevens Village Airport (SVS)",
			"value": "SVS",
			"domestic": 1
		}, {
			"label": "Savusavu, Fiji - Savusavu (SVU)",
			"value": "SVU",
			"domestic": 0
		}, {
			"label": "Ekaterinburg, Russia - Koltsovo (SVX)",
			"value": "SVX",
			"domestic": 0
		}, {
			"label": "Shantou, China - Shantou (SWA)",
			"value": "SWA",
			"domestic": 0
		}, {
			"label": "Stawell, Australia - Stawell (SWC)",
			"value": "SWC",
			"domestic": 0
		}, {
			"label": "Seward, United States - Seward (SWD)",
			"value": "SWD",
			"domestic": 1
		}, {
			"label": "Newburgh, United States - Stewart Airport (SWF)",
			"value": "SWF",
			"domestic": 1
		}, {
			"label": "Swan Hill, Australia - Swan Hill (SWH)",
			"value": "SWH",
			"domestic": 0
		}, {
			"label": "Swindon, United Kingdom - Swindon Mncpl (SWI)",
			"value": "SWI",
			"domestic": 0
		}, {
			"label": "South West Bay, Vanuatu - South West Bay Airport (SWJ)",
			"value": "SWJ",
			"domestic": 0
		}, {
			"label": "Swakopmund, Namibia - Swakopmund (SWP)",
			"value": "SWP",
			"domestic": 0
		}, {
			"label": "Swansea, United Kingdom - Fairwood Common (SWS)",
			"value": "SWS",
			"domestic": 0
		}, {
			"label": "Shakawe, Botswana - Shakawe (SWX)",
			"value": "SWX",
			"domestic": 0
		}, {
			"label": "Strasbourg, France - Entzheim Airport (SXB)",
			"value": "SXB",
			"domestic": 0
		}, {
			"label": "Sale, Australia - Sale (SXE)",
			"value": "SXE",
			"domestic": 0
		}, {
			"label": "Berlin, Germany - Schoenefeld Airport (SXF)",
			"value": "SXF",
			"domestic": 0
		}, {
			"label": "Sligo, Ireland - Sligo/Collooney (SXL)",
			"value": "SXL",
			"domestic": 0
		}, {
			"label": "St. Maarten/St. Martin, St. Maarten/St. Martin - Skeldon Airport (SXM)",
			"value": "SXM",
			"domestic": 0
		}, {
			"label": "Sheldon Point, United States - Sheldon Sea Plane Base (SXP)",
			"value": "SXP",
			"domestic": 1
		}, {
			"label": "Soldotna, United States - Soldotna (SXQ)",
			"value": "SXQ",
			"domestic": 1
		}, {
			"label": "Srinagar, India - Srinagar (SXR)",
			"value": "SXR",
			"domestic": 0
		}, {
			"label": "Seal Bay, United States - Seal Bay Airport (SYB)",
			"value": "SYB",
			"domestic": 1
		}, {
			"label": "Sydney, Australia - Kingsford Smith Airport (SYD)",
			"value": "SYD",
			"domestic": 0
		}, {
			"label": "Shelbyville, United States - Bomar Field (SYI)",
			"value": "SYI",
			"domestic": 1
		}, {
			"label": "Stykkisholmur, Iceland - Stykkisholmur (SYK)",
			"value": "SYK",
			"domestic": 0
		}, {
			"label": "Simao, China - Simao Airport (SYM)",
			"value": "SYM",
			"domestic": 0
		}, {
			"label": "Shonai, Japan - Shonai Airport (SYO)",
			"value": "SYO",
			"domestic": 0
		}, {
			"label": "San Jose, Costa Rica - Tobias Bolanos Intl (SYQ)",
			"value": "SYQ",
			"domestic": 0
		}, {
			"label": "Syracuse, United States - Syracuse (SYR)",
			"value": "SYR",
			"domestic": 1
		}, {
			"label": "Sue Island, Australia - Warraber Island Airport (SYU)",
			"value": "SYU",
			"domestic": 0
		}, {
			"label": "Sanya, China - Sanya (SYX)",
			"value": "SYX",
			"domestic": 0
		}, {
			"label": "Stornoway, United Kingdom - Stornoway (SYY)",
			"value": "SYY",
			"domestic": 0
		}, {
			"label": "Shiraz, Iran - Shiraz Airport (SYZ)",
			"value": "SYZ",
			"domestic": 0
		}, {
			"label": "Soyo, Angola - Soyo Airport (SZA)",
			"value": "SZA",
			"domestic": 0
		}, {
			"label": "Kuala Lumpur, Malaysia - Abdul Aziz Shah (SZB)",
			"value": "SZB",
			"domestic": 0
		}, {
			"label": "Santa Cruz, Costa Rica - Guanacaste (SZC)",
			"value": "SZC",
			"domestic": 0
		}, {
			"label": "Sheffield, United Kingdom - Sheffield (SZD)",
			"value": "SZD",
			"domestic": 0
		}, {
			"label": "Samsun, Turkey - Carsamba Airport (SZF)",
			"value": "SZF",
			"domestic": 0
		}, {
			"label": "Salzburg, Austria - W.A. Mozart (SZG)",
			"value": "SZG",
			"domestic": 0
		}, {
			"label": "Skukuza, South Africa - Skukuza (SZK)",
			"value": "SZK",
			"domestic": 0
		}, {
			"label": "San Cristobal Las Casas, Mexico - San Cristobal Arpt (SZT)",
			"value": "SZT",
			"domestic": 0
		}, {
			"label": "Suzhou, China - Suzhou (SZV)",
			"value": "SZV",
			"domestic": 0
		}, {
			"label": "Shenzhen, China - Shenzhen (SZX)",
			"value": "SZX",
			"domestic": 0
		}, {
			"label": "Szczecin, Poland - Goleniow (SZZ)",
			"value": "SZZ",
			"domestic": 0
		}, {
			"label": "Scarborough, Trinidad and Tobago - Crown Point (TAB)",
			"value": "TAB",
			"domestic": 0
		}, {
			"label": "Tacloban, Philippines - D. Z. Romualdez Airport (TAC)",
			"value": "TAC",
			"domestic": 0
		}, {
			"label": "Trinidad, United States - Las Animas County (TAD)",
			"value": "TAD",
			"domestic": 1
		}, {
			"label": "Daegu, South Korea - Daegu (TAE)",
			"value": "TAE",
			"domestic": 0
		}, {
			"label": "Tagbilaran, Philippines - Tagbilaran Airport (TAG)",
			"value": "TAG",
			"domestic": 0
		}, {
			"label": "Tanna, Vanuatu - Tanna (TAH)",
			"value": "TAH",
			"domestic": 0
		}, {
			"label": "Taiz, Yemen - Al Janad Airport (TAI)",
			"value": "TAI",
			"domestic": 0
		}, {
			"label": "Takamatsu, Japan - Takamatsu (TAK)",
			"value": "TAK",
			"domestic": 0
		}, {
			"label": "Tanana, United States - Ralph Calhoun Airport (TAL)",
			"value": "TAL",
			"domestic": 1
		}, {
			"label": "Tampico, Mexico - Tampico (TAM)",
			"value": "TAM",
			"domestic": 0
		}, {
			"label": "Qingdao, China - Qingdao (TAO)",
			"value": "TAO",
			"domestic": 0
		}, {
			"label": "Tapachula, Mexico - Tapachula (TAP)",
			"value": "TAP",
			"domestic": 0
		}, {
			"label": "Taranto, Italy - M.A. Grottag (TAR)",
			"value": "TAR",
			"domestic": 0
		}, {
			"label": "Tashkent, Uzbekistan - Vostochny (TAS)",
			"value": "TAS",
			"domestic": 0
		}, {
			"label": "Poprad/Tatry, Slovakia - Poprad/Tatry Airport (TAT)",
			"value": "TAT",
			"domestic": 0
		}, {
			"label": "Ta\'u Is., American Samoa - Ta\'u (TAV)",
			"value": "TAV",
			"domestic": 0
		}, {
			"label": "Tartu, Estonia - Tartu (TAY)",
			"value": "TAY",
			"domestic": 0
		}, {
			"label": "Tuy Hoa, Vietnam - Tuy Hoa Airport (TBB)",
			"value": "TBB",
			"domestic": 0
		}, {
			"label": "Tabubil, Papua New Guinea - Tabubil Airport (TBG)",
			"value": "TBG",
			"domestic": 0
		}, {
			"label": "The Bight, Bahamas - The Bight Airport (TBI)",
			"value": "TBI",
			"domestic": 0
		}, {
			"label": "Tabarka, Tunisia - Tabarka (TBJ)",
			"value": "TBJ",
			"domestic": 0
		}, {
			"label": "Fort Leonard Wood, United States - Forney Army Air Field (TBN)",
			"value": "TBN",
			"domestic": 1
		}, {
			"label": "Tabora, Tanzania - Tabora Airport (TBO)",
			"value": "TBO",
			"domestic": 0
		}, {
			"label": "Tumbes, Peru - Tumbes Airport (TBP)",
			"value": "TBP",
			"domestic": 0
		}, {
			"label": "Statesboro, United States - Statesboro Regl (TBR)",
			"value": "TBR",
			"domestic": 1
		}, {
			"label": "Tbilisi, Georgia - Novo Alexeyevka (TBS)",
			"value": "TBS",
			"domestic": 0
		}, {
			"label": "Tabatinga, Brazil - Tabatinga Internacional Airport (TBT)",
			"value": "TBT",
			"domestic": 0
		}, {
			"label": "Nuku\'Alofa, Tonga - Fua\'Amotu Intl (TBU)",
			"value": "TBU",
			"domestic": 0
		}, {
			"label": "Tambov, Russia - Tambov Airport (TBW)",
			"value": "TBW",
			"domestic": 0
		}, {
			"label": "Tabriz, Iran - Tabriz Airport (TBZ)",
			"value": "TBZ",
			"domestic": 0
		}, {
			"label": "Tennant Creek, Australia - Tennant Creek (TCA)",
			"value": "TCA",
			"domestic": 0
		}, {
			"label": "Treasure Cay, Bahamas - Treasure Cay (TCB)",
			"value": "TCB",
			"domestic": 0
		}, {
			"label": "Tarapaca, Colombia - Tarapaca Airport (TCD)",
			"value": "TCD",
			"domestic": 0
		}, {
			"label": "Tulcea, Romania - Tulcea (TCE)",
			"value": "TCE",
			"domestic": 0
		}, {
			"label": "Tacheng, China - Tacheng Airport (TCG)",
			"value": "TCG",
			"domestic": 0
		}, {
			"label": "Tenerife, Spain - All Airports (TCI)",
			"value": "TCI",
			"domestic": 0
		}, {
			"label": "Tuscaloosa, United States - Van De Graaf (TCL)",
			"value": "TCL",
			"domestic": 1
		}, {
			"label": "Tumaco, Colombia - La Florida Airport (TCO)",
			"value": "TCO",
			"domestic": 0
		}, {
			"label": "Taba, Egypt - Taba Intl (TCP)",
			"value": "TCP",
			"domestic": 0
		}, {
			"label": "Tacna, Peru - Tacna Airport (TCQ)",
			"value": "TCQ",
			"domestic": 0
		}, {
			"label": "Tuticorin, India - Tuticorin Airport (TCR)",
			"value": "TCR",
			"domestic": 0
		}, {
			"label": "Takotna, United States - Takotna Airport (TCT)",
			"value": "TCT",
			"domestic": 1
		}, {
			"label": "Thaba Nchu, South Africa - Thaba Nchu (TCU)",
			"value": "TCU",
			"domestic": 0
		}, {
			"label": "Trinidad, Bolivia - Trinidad Airport (TDD)",
			"value": "TDD",
			"domestic": 0
		}, {
			"label": "Amarillo, United States - Tradewind (TDW)",
			"value": "TDW",
			"domestic": 1
		}, {
			"label": "Trat, Thailand - Trat (TDX)",
			"value": "TDX",
			"domestic": 0
		}, {
			"label": "Thisted, Denmark - Thisted (TED)",
			"value": "TED",
			"domestic": 0
		}, {
			"label": "Tebessa, Algeria - Tebessa Airport (TEE)",
			"value": "TEE",
			"domestic": 0
		}, {
			"label": "Tatitlek, United States - Tatitlek Airport (TEK)",
			"value": "TEK",
			"domestic": 1
		}, {
			"label": "Tongren, China - Tongren Airport (TEN)",
			"value": "TEN",
			"domestic": 0
		}, {
			"label": "Teptep, Papua New Guinea - Teptep Airport (TEP)",
			"value": "TEP",
			"domestic": 0
		}, {
			"label": "Terceira, Portugal - Lajes Airport (TER)",
			"value": "TER",
			"domestic": 0
		}, {
			"label": "Tete, Mozambique - Matundo Airport (TET)",
			"value": "TET",
			"domestic": 0
		}, {
			"label": "Te Anau, New Zealand - Manapouri (TEU)",
			"value": "TEU",
			"domestic": 0
		}, {
			"label": "Telluride, United States - Telluride (TEX)",
			"value": "TEX",
			"domestic": 1
		}, {
			"label": "Tezpur, India - Salonibari Airport (TEZ)",
			"value": "TEZ",
			"domestic": 0
		}, {
			"label": "Tefe, Brazil - Tefe Airport (TFF)",
			"value": "TFF",
			"domestic": 0
		}, {
			"label": "Tufi, Papua New Guinea - Tufi Airport (TFI)",
			"value": "TFI",
			"domestic": 0
		}, {
			"label": "Telefomin, Papua New Guinea - Telefomin Airport (TFM)",
			"value": "TFM",
			"domestic": 0
		}, {
			"label": "Tenerife, Spain - Norte Los Rodeos (TFN)",
			"value": "TFN",
			"domestic": 0
		}, {
			"label": "Tenerife, Spain - Sur Reina Sofia (TFS)",
			"value": "TFS",
			"domestic": 0
		}, {
			"label": "Podgorica, Montenegro - Golubovci (TGD)",
			"value": "TGD",
			"domestic": 0
		}, {
			"label": "Kuala Terengganu, Malaysia - Sultan Mahmood (TGG)",
			"value": "TGG",
			"domestic": 0
		}, {
			"label": "Tongoa, Vanuatu - Tongoa Airport (TGH)",
			"value": "TGH",
			"domestic": 0
		}, {
			"label": "Tiga, New Caledonia - Tiga Airport (TGJ)",
			"value": "TGJ",
			"domestic": 0
		}, {
			"label": "Tirgu Mures, Romania - Tirgu Mures (TGM)",
			"value": "TGM",
			"domestic": 0
		}, {
			"label": "Traralgon, Australia - La Trobe (TGN)",
			"value": "TGN",
			"domestic": 0
		}, {
			"label": "Tongliao, China - Tongliao Airport (TGO)",
			"value": "TGO",
			"domestic": 0
		}, {
			"label": "Touggourt, Algeria - Touggourt Airport (TGR)",
			"value": "TGR",
			"domestic": 0
		}, {
			"label": "Tanga, Tanzania - Tanga (TGT)",
			"value": "TGT",
			"domestic": 0
		}, {
			"label": "Tegucigalpa, Honduras - Toncontin Airport (TGU)",
			"value": "TGU",
			"domestic": 0
		}, {
			"label": "Tuxtla Gutierrez, Mexico - Llano San Juan (TGZ)",
			"value": "TGZ",
			"domestic": 0
		}, {
			"label": "Tullahoma, United States - William Northern (THA)",
			"value": "THA",
			"domestic": 1
		}, {
			"label": "Teresina, Brazil - Teresina (THE)",
			"value": "THE",
			"domestic": 0
		}, {
			"label": "Tachilek, Myanmar (Burma) - Tachilek Airport (THL)",
			"value": "THL",
			"domestic": 0
		}, {
			"label": "Trollhattan, Sweden - Trollhattan (THN)",
			"value": "THN",
			"domestic": 0
		}, {
			"label": "Thorshofn, Iceland - Thorshofn Airport (THO)",
			"value": "THO",
			"domestic": 0
		}, {
			"label": "Tehran, Iran - Mehrabad (THR)",
			"value": "THR",
			"domestic": 0
		}, {
			"label": "Sukhothai, Thailand - Sukhothai Airport (THS)",
			"value": "THS",
			"domestic": 0
		}, {
			"label": "Tirana, Albania - Rinas (TIA)",
			"value": "TIA",
			"domestic": 0
		}, {
			"label": "Tippi, Ethiopia - Tippi Airport (TIE)",
			"value": "TIE",
			"domestic": 0
		}, {
			"label": "Taif, Saudi Arabia - Taif (TIF)",
			"value": "TIF",
			"domestic": 0
		}, {
			"label": "Tikehau, French Polynesia and Tahiti - Tikehau (TIH)",
			"value": "TIH",
			"domestic": 0
		}, {
			"label": "Tijuana, Mexico - Rodriguez (TIJ)",
			"value": "TIJ",
			"domestic": 0
		}, {
			"label": "Tembagapura, Indonesia - Timika (TIM)",
			"value": "TIM",
			"domestic": 0
		}, {
			"label": "Tindouf, Algeria - Tindouf Airport (TIN)",
			"value": "TIN",
			"domestic": 0
		}, {
			"label": "Tripoli, Libya - Tripoli Intl (TIP)",
			"value": "TIP",
			"domestic": 0
		}, {
			"label": "Tinian, Northern Mariana Islands - Tinian (TIQ)",
			"value": "TIQ",
			"domestic": 0
		}, {
			"label": "Tirupati, India - Tirupati (TIR)",
			"value": "TIR",
			"domestic": 0
		}, {
			"label": "Thursday Island, Australia - Thursday Island Airport (TIS)",
			"value": "TIS",
			"domestic": 0
		}, {
			"label": "Timaru, New Zealand - Timaru (TIU)",
			"value": "TIU",
			"domestic": 0
		}, {
			"label": "Tivat, Montenegro - Tivat (TIV)",
			"value": "TIV",
			"domestic": 0
		}, {
			"label": "Tari, Papua New Guinea - Tari Airport (TIZ)",
			"value": "TIZ",
			"domestic": 0
		}, {
			"label": "Tarija, Bolivia - Tarija/Oriel Lea (TJA)",
			"value": "TJA",
			"domestic": 0
		}, {
			"label": "Toyooka, Japan - Tajima Airport (TJH)",
			"value": "TJH",
			"domestic": 0
		}, {
			"label": "Tokat, Turkey - Tokat (TJK)",
			"value": "TJK",
			"domestic": 0
		}, {
			"label": "Tyumen, Russia - Tyumen (TJM)",
			"value": "TJM",
			"domestic": 0
		}, {
			"label": "Tanjung Pandan, Indonesia - Bulutumbang Airport (TJQ)",
			"value": "TJQ",
			"domestic": 0
		}, {
			"label": "Tanjung Selor, Indonesia - Tanjung Selor Airport (TJS)",
			"value": "TJS",
			"domestic": 0
		}, {
			"label": "Talkeetna, United States - Talkeetna Mncpl (TKA)",
			"value": "TKA",
			"domestic": 1
		}, {
			"label": "Tenakee Springs, United States - Tenakee Springs Sea Plane Base (TKE)",
			"value": "TKE",
			"domestic": 1
		}, {
			"label": "Truckee, United States - Truckee Mncpl (TKF)",
			"value": "TKF",
			"domestic": 1
		}, {
			"label": "Bandar Lampung, Indonesia - Branti (TKG)",
			"value": "TKG",
			"domestic": 0
		}, {
			"label": "Tok, United States - Tok (TKJ)",
			"value": "TKJ",
			"domestic": 1
		}, {
			"label": "Weno, Micronesia - Truk (TKK)",
			"value": "TKK",
			"domestic": 0
		}, {
			"label": "Tokunoshima, Japan - Tokunoshima Airport (TKN)",
			"value": "TKN",
			"domestic": 0
		}, {
			"label": "Takapoto, French Polynesia and Tahiti - Takapoto Airport (TKP)",
			"value": "TKP",
			"domestic": 0
		}, {
			"label": "Kigoma, Tanzania - Kigoma Airport (TKQ)",
			"value": "TKQ",
			"domestic": 0
		}, {
			"label": "Tokushima, Japan - Tokushima (TKS)",
			"value": "TKS",
			"domestic": 0
		}, {
			"label": "Turku, Finland - Turku (TKU)",
			"value": "TKU",
			"domestic": 0
		}, {
			"label": "Takaroa, French Polynesia and Tahiti - Takaroa Airport (TKX)",
			"value": "TKX",
			"domestic": 0
		}, {
			"label": "Teller, United States - Teller Airport (TLA)",
			"value": "TLA",
			"domestic": 1
		}, {
			"label": "Toluca, Mexico - Toluca (TLC)",
			"value": "TLC",
			"domestic": 0
		}, {
			"label": "Tuli Block, Botswana - Limpopo Valley (TLD)",
			"value": "TLD",
			"domestic": 0
		}, {
			"label": "Tulear, Madagascar - Tulear Airport (TLE)",
			"value": "TLE",
			"domestic": 0
		}, {
			"label": "Tallahassee, United States - Tallahassee Municipal Airport (TLH)",
			"value": "TLH",
			"domestic": 1
		}, {
			"label": "Tallinn, Estonia - Ulemiste Airport (TLL)",
			"value": "TLL",
			"domestic": 0
		}, {
			"label": "Tlemcen, Algeria - Zenata Airport (TLM)",
			"value": "TLM",
			"domestic": 0
		}, {
			"label": "Toulon, France - Hyeres (TLN)",
			"value": "TLN",
			"domestic": 0
		}, {
			"label": "Toulouse, France - Blagnac Airport (TLS)",
			"value": "TLS",
			"domestic": 0
		}, {
			"label": "Tuluksak, United States - Tuluksak Airport (TLT)",
			"value": "TLT",
			"domestic": 1
		}, {
			"label": "Tel Aviv, Israel - Ben Gurion International Airport (TLV)",
			"value": "TLV",
			"domestic": 0
		}, {
			"label": "Plastun, Russia - Plastun Airport (TLY)",
			"value": "TLY",
			"domestic": 0
		}, {
			"label": "Tifton, United States - Henry Tift Myers (TMA)",
			"value": "TMA",
			"domestic": 1
		}, {
			"label": "Tambolaka, Indonesia - Tambolaka Airport (TMC)",
			"value": "TMC",
			"domestic": 0
		}, {
			"label": "Tame, Colombia - Tame Airport (TME)",
			"value": "TME",
			"domestic": 0
		}, {
			"label": "Tomanggong, Malaysia - Tomanggong Airport (TMG)",
			"value": "TMG",
			"domestic": 0
		}, {
			"label": "Termez, Uzbekistan - Termez Airport (TMJ)",
			"value": "TMJ",
			"domestic": 0
		}, {
			"label": "Tamale, Ghana - Tamale Airport (TML)",
			"value": "TML",
			"domestic": 0
		}, {
			"label": "Tamatave, Madagascar - Tamatave Airport (TMM)",
			"value": "TMM",
			"domestic": 0
		}, {
			"label": "Tampere, Finland - Tampere-Pirkkala (TMP)",
			"value": "TMP",
			"domestic": 0
		}, {
			"label": "Tamenghest, Algeria - Aguemar Airport (TMR)",
			"value": "TMR",
			"domestic": 0
		}, {
			"label": "Sao Tome Is., Sao Tome and Principe - Sao Tome Is (TMS)",
			"value": "TMS",
			"domestic": 0
		}, {
			"label": "Trombetas, Brazil - Trombetas Airport (TMT)",
			"value": "TMT",
			"domestic": 0
		}, {
			"label": "Tambor, Costa Rica - Tambor (TMU)",
			"value": "TMU",
			"domestic": 0
		}, {
			"label": "Tamworth, Australia - Tamworth (TMW)",
			"value": "TMW",
			"domestic": 0
		}, {
			"label": "Jinan, China - Jinan (TNA)",
			"value": "TNA",
			"domestic": 0
		}, {
			"label": "Tin City, United States - Tin City Air Force Station (TNC)",
			"value": "TNC",
			"domestic": 1
		}, {
			"label": "Tanegashima, Japan - Tanegashima Airport (TNE)",
			"value": "TNE",
			"domestic": 0
		}, {
			"label": "Tangier, Morocco - Boukhalef (TNG)",
			"value": "TNG",
			"domestic": 0
		}, {
			"label": "Tununak, United States - Tununak Airport (TNK)",
			"value": "TNK",
			"domestic": 1
		}, {
			"label": "Ternopol, Ukraine - Ternopol (TNL)",
			"value": "TNL",
			"domestic": 0
		}, {
			"label": "T\'ainan, Taiwan - Tainan (TNN)",
			"value": "TNN",
			"domestic": 0
		}, {
			"label": "Tamarindo, Costa Rica - Tamarindo (TNO)",
			"value": "TNO",
			"domestic": 0
		}, {
			"label": "Twentynine Palms, United States - Twentynine Palms (TNP)",
			"value": "TNP",
			"domestic": 1
		}, {
			"label": "Antananarivo, Madagascar - Antananarivo (TNR)",
			"value": "TNR",
			"domestic": 0
		}, {
			"label": "Newton, United States - Newton Mncpl (TNU)",
			"value": "TNU",
			"domestic": 1
		}, {
			"label": "Tosontsengel, Mongolia - Tosontsengel Airport (TNZ)",
			"value": "TNZ",
			"domestic": 0
		}, {
			"label": "Torrance, United States - Torrance Mncpl (TOA)",
			"value": "TOA",
			"domestic": 1
		}, {
			"label": "Tioman, Malaysia - Tioman (TOD)",
			"value": "TOD",
			"domestic": 0
		}, {
			"label": "Tozeur, Tunisia - Tozeur (TOE)",
			"value": "TOE",
			"domestic": 0
		}, {
			"label": "Tomsk, Russia - Tomsk Airport (TOF)",
			"value": "TOF",
			"domestic": 0
		}, {
			"label": "Togiak Village, United States - Togiak Village Airport (TOG)",
			"value": "TOG",
			"domestic": 1
		}, {
			"label": "Torres, Vanuatu - Torres Airstrip (TOH)",
			"value": "TOH",
			"domestic": 0
		}, {
			"label": "Troy, United States - Troy Mncpl (TOI)",
			"value": "TOI",
			"domestic": 1
		}, {
			"label": "Toledo, United States - Express Airport (TOL)",
			"value": "TOL",
			"domestic": 1
		}, {
			"label": "Timbuktu, Mali - Timbuktu (TOM)",
			"value": "TOM",
			"domestic": 0
		}, {
			"label": "Topeka, United States - Philip Billard Mncpl (TOP)",
			"value": "TOP",
			"domestic": 1
		}, {
			"label": "Tromso, Norway - Tromso/Langnes (TOS)",
			"value": "TOS",
			"domestic": 0
		}, {
			"label": "Touho, New Caledonia - Touho Airport (TOU)",
			"value": "TOU",
			"domestic": 0
		}, {
			"label": "Tortola, British Virgin Islands - West End SPB (TOV)",
			"value": "TOV",
			"domestic": 0
		}, {
			"label": "Toyama, Japan - Toyama (TOY)",
			"value": "TOY",
			"domestic": 0
		}, {
			"label": "Tampa, United States - Tampa (TPA)",
			"value": "TPA",
			"domestic": 1
		}, {
			"label": "Taipei, Taiwan - Chiang Kai Shek Airport (TPE)",
			"value": "TPE",
			"domestic": 0
		}, {
			"label": "Tonopah, United States - Tonopah (TPH)",
			"value": "TPH",
			"domestic": 1
		}, {
			"label": "Taplejung, Nepal - Taplejung Airport (TPJ)",
			"value": "TPJ",
			"domestic": 0
		}, {
			"label": "Temple, United States - Draughon-Miller (TPL)",
			"value": "TPL",
			"domestic": 1
		}, {
			"label": "Tarapoto, Peru - Tarapoto (TPP)",
			"value": "TPP",
			"domestic": 0
		}, {
			"label": "Tepic, Mexico - Tepic Mncpl (TPQ)",
			"value": "TPQ",
			"domestic": 0
		}, {
			"label": "Trapani, Italy - Birgi (TPS)",
			"value": "TPS",
			"domestic": 0
		}, {
			"label": "Torreon, Mexico - Torreon (TRC)",
			"value": "TRC",
			"domestic": 0
		}, {
			"label": "Trondheim, Norway - Vaernes (TRD)",
			"value": "TRD",
			"domestic": 0
		}, {
			"label": "Tiree, Inner Hebrides, United Kingdom - Tiree Airport (TRE)",
			"value": "TRE",
			"domestic": 0
		}, {
			"label": "Oslo, Norway - Sandefjord (TRF)",
			"value": "TRF",
			"domestic": 0
		}, {
			"label": "Tauranga, New Zealand - Tauranga (TRG)",
			"value": "TRG",
			"domestic": 0
		}, {
			"label": "Tarakan, Indonesia - Tarakan Airport (TRK)",
			"value": "TRK",
			"domestic": 0
		}, {
			"label": "Turin, Italy - Citta Di Torino (TRN)",
			"value": "TRN",
			"domestic": 0
		}, {
			"label": "Taree, Australia - Taree (TRO)",
			"value": "TRO",
			"domestic": 0
		}, {
			"label": "Trieste, Italy - Dei Legionari (TRS)",
			"value": "TRS",
			"domestic": 0
		}, {
			"label": "Trujillo, Peru - Trujillo (TRU)",
			"value": "TRU",
			"domestic": 0
		}, {
			"label": "Thiruvananthapuram, India - Trivandrum (TRV)",
			"value": "TRV",
			"domestic": 0
		}, {
			"label": "Tarawa, Kiribati - Bonriki Airport (TRW)",
			"value": "TRW",
			"domestic": 0
		}, {
			"label": "Tiruchirappalli, India - Civil (TRZ)",
			"value": "TRZ",
			"domestic": 0
		}, {
			"label": "Taipei, Taiwan - Sung Shan (TSA)",
			"value": "TSA",
			"domestic": 0
		}, {
			"label": "Tsumeb, Namibia - Tsumeb (TSB)",
			"value": "TSB",
			"domestic": 0
		}, {
			"label": "Tshipise, South Africa - Tshipise (TSD)",
			"value": "TSD",
			"domestic": 0
		}, {
			"label": "Astana, Kazakhstan - Astana (TSE)",
			"value": "TSE",
			"domestic": 0
		}, {
			"label": "Venice, Italy - Treviso (TSF)",
			"value": "TSF",
			"domestic": 0
		}, {
			"label": "Tsushima, Japan - Tsushima Airport (TSJ)",
			"value": "TSJ",
			"domestic": 0
		}, {
			"label": "Taos, United States - Taos Regional (TSM)",
			"value": "TSM",
			"domestic": 1
		}, {
			"label": "Tianjin, China - Tianjin (TSN)",
			"value": "TSN",
			"domestic": 0
		}, {
			"label": "Tehachapi, United States - Kern County (TSP)",
			"value": "TSP",
			"domestic": 1
		}, {
			"label": "Timisoara, Romania - Timisoara (TSR)",
			"value": "TSR",
			"domestic": 0
		}, {
			"label": "Trang, Thailand - Trang (TST)",
			"value": "TST",
			"domestic": 0
		}, {
			"label": "Townsville, Australia - Townsville (TSV)",
			"value": "TSV",
			"domestic": 0
		}, {
			"label": "Tan Tan, Morocco - Tan Tan Airport (TTA)",
			"value": "TTA",
			"domestic": 0
		}, {
			"label": "Ternate, Indonesia - Babullah Airport (TTE)",
			"value": "TTE",
			"domestic": 0
		}, {
			"label": "Tottori, Japan - Tottori (TTJ)",
			"value": "TTJ",
			"domestic": 0
		}, {
			"label": "Trenton, United States - Mercer County Airport (TTN)",
			"value": "TTN",
			"domestic": 1
		}, {
			"label": "Tortuguero, Costa Rica - Tortuquero (TTQ)",
			"value": "TTQ",
			"domestic": 0
		}, {
			"label": "Tana Toraja, Indonesia - Tanatoraja (TTR)",
			"value": "TTR",
			"domestic": 0
		}, {
			"label": "Tsaratanana, Madagascar - Tsaratanana Airport (TTS)",
			"value": "TTS",
			"domestic": 0
		}, {
			"label": "Taitung, Taiwan - Taitung (TTT)",
			"value": "TTT",
			"domestic": 0
		}, {
			"label": "Tulcan, Ecuador - Tulcan Airport (TUA)",
			"value": "TUA",
			"domestic": 0
		}, {
			"label": "Tubuai Island, French Polynesia and Tahiti - Tubuai Island Airport (TUB)",
			"value": "TUB",
			"domestic": 0
		}, {
			"label": "San Miguel de Tucuman, Argentina - Benjamin Matienzo (TUC)",
			"value": "TUC",
			"domestic": 0
		}, {
			"label": "Tambacounda, Senegal - Tambacounda Airport (TUD)",
			"value": "TUD",
			"domestic": 0
		}, {
			"label": "Tours, France - St Symphorien (TUF)",
			"value": "TUF",
			"domestic": 0
		}, {
			"label": "Tuguegarao, Philippines - Tuguegarao Airport (TUG)",
			"value": "TUG",
			"domestic": 0
		}, {
			"label": "Turaif, Saudi Arabia - Turaif Airport (TUI)",
			"value": "TUI",
			"domestic": 0
		}, {
			"label": "Turbat, Pakistan - Turbat Airport (TUK)",
			"value": "TUK",
			"domestic": 0
		}, {
			"label": "Tulsa, United States - Tulsa (TUL)",
			"value": "TUL",
			"domestic": 1
		}, {
			"label": "Tumut, Australia - Tumut (TUM)",
			"value": "TUM",
			"domestic": 0
		}, {
			"label": "Tunis, Tunisia - Carthage (TUN)",
			"value": "TUN",
			"domestic": 0
		}, {
			"label": "Taupo, New Zealand - Taupo (TUO)",
			"value": "TUO",
			"domestic": 0
		}, {
			"label": "Tupelo, United States - Lemons Municipal Airport (TUP)",
			"value": "TUP",
			"domestic": 1
		}, {
			"label": "Tucurui, Brazil - Tucurui Airport (TUR)",
			"value": "TUR",
			"domestic": 0
		}, {
			"label": "Tucson, United States - Tucson (TUS)",
			"value": "TUS",
			"domestic": 1
		}, {
			"label": "Tabuk, Saudi Arabia - Tabuk Airport (TUU)",
			"value": "TUU",
			"domestic": 0
		}, {
			"label": "Morafenobe, Madagascar - Morafenobe Airport (TVA)",
			"value": "TVA",
			"domestic": 0
		}, {
			"label": "Traverse City, United States - Traverse City Airport (TVC)",
			"value": "TVC",
			"domestic": 1
		}, {
			"label": "Thief River Falls, United States - Thief River Falls (TVF)",
			"value": "TVF",
			"domestic": 1
		}, {
			"label": "Taveuni Island, Fiji - Matei (TVU)",
			"value": "TVU",
			"domestic": 0
		}, {
			"label": "Dawe, Myanmar (Burma) - Dawe Airport (TVY)",
			"value": "TVY",
			"domestic": 0
		}, {
			"label": "Twin Hills, United States - Twin Hills Airport (TWA)",
			"value": "TWA",
			"domestic": 1
		}, {
			"label": "Toowoomba, Australia - Toowoomba (TWB)",
			"value": "TWB",
			"domestic": 0
		}, {
			"label": "Port Townsend, United States - Jefferson County (TWD)",
			"value": "TWD",
			"domestic": 1
		}, {
			"label": "Twin Falls, United States - Twin Falls Regl (TWF)",
			"value": "TWF",
			"domestic": 1
		}, {
			"label": "Tawitawi, Philippines - Tawitawi Airport (TWT)",
			"value": "TWT",
			"domestic": 0
		}, {
			"label": "Tawau, Malaysia - Tawau Airport (TWU)",
			"value": "TWU",
			"domestic": 0
		}, {
			"label": "Taichung, Taiwan - Taichung (TXG)",
			"value": "TXG",
			"domestic": 0
		}, {
			"label": "Texarkana, United States - Texarkana Municipal Airport (TXK)",
			"value": "TXK",
			"domestic": 1
		}, {
			"label": "Berlin, Germany - Tegel Airport (TXL)",
			"value": "TXL",
			"domestic": 0
		}, {
			"label": "Tunxi, China - Tunxi (TXN)",
			"value": "TXN",
			"domestic": 0
		}, {
			"label": "Torsby, Sweden - Torsby Airport (TYF)",
			"value": "TYF",
			"domestic": 0
		}, {
			"label": "Taiyuan, China - Taiyuan (TYN)",
			"value": "TYN",
			"domestic": 0
		}, {
			"label": "Tokyo, Japan - All Airports (TYO)",
			"value": "TYO",
			"domestic": 0
		}, {
			"label": "Tyler, United States - Pounds Field (TYR)",
			"value": "TYR",
			"domestic": 1
		}, {
			"label": "Knoxville, United States - Knoxville (TYS)",
			"value": "TYS",
			"domestic": 1
		}, {
			"label": "Taylor, United States - Taylor Mncpl (TYZ)",
			"value": "TYZ",
			"domestic": 1
		}, {
			"label": "South Andros, Bahamas - Congo Town (TZN)",
			"value": "TZN",
			"domestic": 0
		}, {
			"label": "Trabzon, Turkey - Trabzon (TZX)",
			"value": "TZX",
			"domestic": 0
		}, {
			"label": "Ua Huka, French Polynesia and Tahiti - Ua Huka Airport (UAH)",
			"value": "UAH",
			"domestic": 0
		}, {
			"label": "Narsarsuaq, Greenland - Narsarsuaq (UAK)",
			"value": "UAK",
			"domestic": 0
		}, {
			"label": "Ua Pou, Marquesas Island, French Polynesia and Tahiti - Ua Pou Airport (UAP)",
			"value": "UAP",
			"domestic": 0
		}, {
			"label": "San Juan, Argentina - San Juan Airport (UAQ)",
			"value": "UAQ",
			"domestic": 0
		}, {
			"label": "Samburu, Kenya - Samburu Airport (UAS)",
			"value": "UAS",
			"domestic": 0
		}, {
			"label": "Uberaba, Brazil - Uberaba (UBA)",
			"value": "UBA",
			"domestic": 0
		}, {
			"label": "Mabuiag Island, Australia - Mabuiag Island Airport (UBB)",
			"value": "UBB",
			"domestic": 0
		}, {
			"label": "Ube, Japan - Ube (UBJ)",
			"value": "UBJ",
			"domestic": 0
		}, {
			"label": "Ubon Ratchathani, Thailand - Muang Ubon Airport (UBP)",
			"value": "UBP",
			"domestic": 0
		}, {
			"label": "Utica, United States - Oneida Cty (UCA)",
			"value": "UCA",
			"domestic": 1
		}, {
			"label": "Ukhta, Russia - Ukhta Airport (UCT)",
			"value": "UCT",
			"domestic": 0
		}, {
			"label": "Union City, United States - Everette-Stewart (UCY)",
			"value": "UCY",
			"domestic": 1
		}, {
			"label": "Uberlandia, Brazil - Eduardo Gomes (UDI)",
			"value": "UDI",
			"domestic": 0
		}, {
			"label": "Uzhgorod, Ukraine - Uzhgorod (UDJ)",
			"value": "UDJ",
			"domestic": 0
		}, {
			"label": "Udaipur, India - Dabok (UDR)",
			"value": "UDR",
			"domestic": 0
		}, {
			"label": "Queenstown, Australia - Queenstown (UEE)",
			"value": "UEE",
			"domestic": 0
		}, {
			"label": "Quelimane, Mozambique - Quelimane Airport (UEL)",
			"value": "UEL",
			"domestic": 0
		}, {
			"label": "Kume Island, Japan - Kume Jima (UEO)",
			"value": "UEO",
			"domestic": 0
		}, {
			"label": "Waukesha, United States - Waukesha County (UES)",
			"value": "UES",
			"domestic": 1
		}, {
			"label": "Quetta, Pakistan - Quetta (UET)",
			"value": "UET",
			"domestic": 0
		}, {
			"label": "Ufa, Russia - Ufa (UFA)",
			"value": "UFA",
			"domestic": 0
		}, {
			"label": "Bulgan, Mongolia - Bulgan Airport (UGA)",
			"value": "UGA",
			"domestic": 0
		}, {
			"label": "Pilot Point, United States - Ugashik Bay Airport (UGB)",
			"value": "UGB",
			"domestic": 1
		}, {
			"label": "Urgench, Uzbekistan - Urgench (UGC)",
			"value": "UGC",
			"domestic": 0
		}, {
			"label": "Waukegan, United States - Waukegan Regl (UGN)",
			"value": "UGN",
			"domestic": 1
		}, {
			"label": "Quibdo, Colombia - Quibdo Airport (UIB)",
			"value": "UIB",
			"domestic": 0
		}, {
			"label": "Qui Nhon, Vietnam - Qui Nhon Airport (UIH)",
			"value": "UIH",
			"domestic": 0
		}, {
			"label": "Utila, Honduras - Utila Airport (UII)",
			"value": "UII",
			"domestic": 0
		}, {
			"label": "Quincy, United States - Quincy Municipal Airport (UIN)",
			"value": "UIN",
			"domestic": 1
		}, {
			"label": "Quito, Ecuador - Mariscal Sucre Airport (UIO)",
			"value": "UIO",
			"domestic": 0
		}, {
			"label": "Quimper, France - Pluguffan (UIP)",
			"value": "UIP",
			"domestic": 0
		}, {
			"label": "Jaluit, Marshall Islands - Jaluit Airport (UIT)",
			"value": "UIT",
			"domestic": 0
		}, {
			"label": "Ujae Island, Marshall Islands - Ujae Island Airport (UJE)",
			"value": "UJE",
			"domestic": 0
		}, {
			"label": "Osaka, Japan - Kobe Airport (UKB)",
			"value": "UKB",
			"domestic": 0
		}, {
			"label": "Ukiah, United States - Ukiah Mncpl (UKI)",
			"value": "UKI",
			"domestic": 1
		}, {
			"label": "Ust-Kamenogorsk, Kazakhstan - Ust-Kamenogorsk (UKK)",
			"value": "UKK",
			"domestic": 0
		}, {
			"label": "Nuku, Papua New Guinea - Nuku Airport (UKU)",
			"value": "UKU",
			"domestic": 0
		}, {
			"label": "San Julian, Argentina - San Julian Airport (ULA)",
			"value": "ULA",
			"domestic": 0
		}, {
			"label": "Ulei, Vanuatu - Ulei Airport (ULB)",
			"value": "ULB",
			"domestic": 0
		}, {
			"label": "Ulundi, South Africa - Ulundi (ULD)",
			"value": "ULD",
			"domestic": 0
		}, {
			"label": "Ulgit, Mongolia - Ulgit Airport (ULG)",
			"value": "ULG",
			"domestic": 0
		}, {
			"label": "New Ulm, United States - New Ulm Mncpl (ULM)",
			"value": "ULM",
			"domestic": 1
		}, {
			"label": "Ulaanbaatar, Mongolia - Buyant Uhaa (ULN)",
			"value": "ULN",
			"domestic": 0
		}, {
			"label": "Ulaangom, Mongolia - Ulaangom Airport (ULO)",
			"value": "ULO",
			"domestic": 0
		}, {
			"label": "Quilpie, Australia - Quilpie Airport (ULP)",
			"value": "ULP",
			"domestic": 0
		}, {
			"label": "Gulu, Uganda - Gulu Airport (ULU)",
			"value": "ULU",
			"domestic": 0
		}, {
			"label": "Ulyanovsk, Russia - Ulyanovsk Airport (ULY)",
			"value": "ULY",
			"domestic": 0
		}, {
			"label": "Uliastai, Mongolia - Uliastai Airport (ULZ)",
			"value": "ULZ",
			"domestic": 0
		}, {
			"label": "Uummannaq, Greenland - Uummannaq (UMD)",
			"value": "UMD",
			"domestic": 0
		}, {
			"label": "Umea, Sweden - Umea (UME)",
			"value": "UME",
			"domestic": 0
		}, {
			"label": "Una, Brazil - Una Airport (UNA)",
			"value": "UNA",
			"domestic": 0
		}, {
			"label": "Kiunga, Papua New Guinea - Kiunga Airport (UNG)",
			"value": "UNG",
			"domestic": 0
		}, {
			"label": "Union Island, St. Vincent and the Grenadines - Union Is (UNI)",
			"value": "UNI",
			"domestic": 0
		}, {
			"label": "Unalakleet, United States - Unalakleet Airport (UNK)",
			"value": "UNK",
			"domestic": 1
		}, {
			"label": "Ranong, Thailand - Ranong (UNN)",
			"value": "UNN",
			"domestic": 0
		}, {
			"label": "University, United States - University/Oxford (UOX)",
			"value": "UOX",
			"domestic": 1
		}, {
			"label": "Ujung Pandang, Indonesia - Hasanuddin (UPG)",
			"value": "UPG",
			"domestic": 0
		}, {
			"label": "Uruapan, Mexico - Uruapan (UPN)",
			"value": "UPN",
			"domestic": 0
		}, {
			"label": "Uralsk, Kazakhstan - Uralsk Airport (URA)",
			"value": "URA",
			"domestic": 0
		}, {
			"label": "Urumqi, China - Urumqi (URC)",
			"value": "URC",
			"domestic": 0
		}, {
			"label": "Kuressaare, Estonia - Kuressaare Airport (URE)",
			"value": "URE",
			"domestic": 0
		}, {
			"label": "Uraj, Russia - Uraj Airport (URJ)",
			"value": "URJ",
			"domestic": 0
		}, {
			"label": "Rouen, France - Boos (URO)",
			"value": "URO",
			"domestic": 0
		}, {
			"label": "Kursk, Russia - Kursk Airport (URS)",
			"value": "URS",
			"domestic": 0
		}, {
			"label": "Surat Thani, Thailand - Surat Thani (URT)",
			"value": "URT",
			"domestic": 0
		}, {
			"label": "Gurayat, Saudi Arabia - Gurayat Airport (URY)",
			"value": "URY",
			"domestic": 0
		}, {
			"label": "Ushuaia, Argentina - Islas Malvinas (USH)",
			"value": "USH",
			"domestic": 0
		}, {
			"label": "Usinsk, Russia - Usinsk Airport (USK)",
			"value": "USK",
			"domestic": 0
		}, {
			"label": "Koh Samui, Thailand - Koh Samui (USM)",
			"value": "USM",
			"domestic": 0
		}, {
			"label": "Ulsan, South Korea - Ulsan (USN)",
			"value": "USN",
			"domestic": 0
		}, {
			"label": "Usak, Turkey - Usak Airport (USQ)",
			"value": "USQ",
			"domestic": 0
		}, {
			"label": "St Augustine, United States - St Augustine (UST)",
			"value": "UST",
			"domestic": 1
		}, {
			"label": "Busuanga, Philippines - Busuanga Airport (USU)",
			"value": "USU",
			"domestic": 0
		}, {
			"label": "Mutare, Zimbabwe - Mutare (UTA)",
			"value": "UTA",
			"domestic": 0
		}, {
			"label": "Udon Thani, Thailand - Udon Thani (UTH)",
			"value": "UTH",
			"domestic": 0
		}, {
			"label": "Utirik Island, Marshall Islands - Utirik Island Airport (UTK)",
			"value": "UTK",
			"domestic": 0
		}, {
			"label": "Tunica, United States - Tunica (UTM)",
			"value": "UTM",
			"domestic": 1
		}, {
			"label": "Upington, South Africa - Upington (UTN)",
			"value": "UTN",
			"domestic": 0
		}, {
			"label": "Utapao, Thailand - Utapao Airport (UTP)",
			"value": "UTP",
			"domestic": 0
		}, {
			"label": "Umtata, South Africa - Umtata (UTT)",
			"value": "UTT",
			"domestic": 0
		}, {
			"label": "Ustupo, Panama - Ustupo Airport (UTU)",
			"value": "UTU",
			"domestic": 0
		}, {
			"label": "Ulan-Ude, Russia - Ulan-Ude Airport (UUD)",
			"value": "UUD",
			"domestic": 0
		}, {
			"label": "Yuzhno-Sakhalinsk, Russia - Yuzhno-Sakhalinsk (UUS)",
			"value": "UUS",
			"domestic": 0
		}, {
			"label": "Ouvea, New Caledonia - Ouvea Airport (UVE)",
			"value": "UVE",
			"domestic": 0
		}, {
			"label": "Castries, St. Lucia - St. Lucia (UVF)",
			"value": "UVF",
			"domestic": 0
		}, {
			"label": "Nyala, Sudan - Nyala Airport (UYL)",
			"value": "UYL",
			"domestic": 0
		}, {
			"label": "Yulin, China - Yulin Airport (UYN)",
			"value": "UYN",
			"domestic": 0
		}, {
			"label": "Vaasa, Finland - Vaasa (VAA)",
			"value": "VAA",
			"domestic": 0
		}, {
			"label": "Valence, France - Chabeuil (VAF)",
			"value": "VAF",
			"domestic": 0
		}, {
			"label": "Vanimo, Papua New Guinea - Vanimo Airport (VAI)",
			"value": "VAI",
			"domestic": 0
		}, {
			"label": "Chevak, United States - Chevak Airport (VAK)",
			"value": "VAK",
			"domestic": 1
		}, {
			"label": "Van, Turkey - Van (VAN)",
			"value": "VAN",
			"domestic": 0
		}, {
			"label": "Suavanao, Solomon Islands - Suavanao Airstrip (VAO)",
			"value": "VAO",
			"domestic": 0
		}, {
			"label": "Valparaiso, Chile and Easter Island - Valparaiso (VAP)",
			"value": "VAP",
			"domestic": 0
		}, {
			"label": "Varna, Bulgaria - Varna (VAR)",
			"value": "VAR",
			"domestic": 0
		}, {
			"label": "Sivas, Turkey - Sivas Airport (VAS)",
			"value": "VAS",
			"domestic": 0
		}, {
			"label": "Vava\'u, Tonga - Lupepau\'u Airport (VAV)",
			"value": "VAV",
			"domestic": 0
		}, {
			"label": "Vardoe, Norway - Vardoe Airport (VAW)",
			"value": "VAW",
			"domestic": 0
		}, {
			"label": "Vanuabalavu, Fiji - Vanuabalavu Airport (VBV)",
			"value": "VBV",
			"domestic": 0
		}, {
			"label": "Visby, Sweden - Visby (VBY)",
			"value": "VBY",
			"domestic": 0
		}, {
			"label": "Venice, Italy - Marco Polo Airport (VCE)",
			"value": "VCE",
			"domestic": 0
		}, {
			"label": "Tamky-Chulai Airport, Vietnam - Chulai Airport (VCL)",
			"value": "VCL",
			"domestic": 0
		}, {
			"label": "Sao Paulo, Brazil - Viracopos (VCP)",
			"value": "VCP",
			"domestic": 0
		}, {
			"label": "Con Dao, Vietnam - Coong Airport (VCS)",
			"value": "VCS",
			"domestic": 0
		}, {
			"label": "Victoria, United States - County-Foster Airport (VCT)",
			"value": "VCT",
			"domestic": 1
		}, {
			"label": "Ovda, Israel - Ovda Airport (VDA)",
			"value": "VDA",
			"domestic": 0
		}, {
			"label": "Fagernes, Norway - Valdres (VDB)",
			"value": "VDB",
			"domestic": 0
		}, {
			"label": "Vitoria da Conquista, Brazil - Vitoria Da Conquista Airport (VDC)",
			"value": "VDC",
			"domestic": 0
		}, {
			"label": "Valverde, Spain - Hierro (VDE)",
			"value": "VDE",
			"domestic": 0
		}, {
			"label": "Vidalia, United States - Vidalia Mncpl (VDI)",
			"value": "VDI",
			"domestic": 1
		}, {
			"label": "Viedma, Argentina - Viedma Airport (VDM)",
			"value": "VDM",
			"domestic": 0
		}, {
			"label": "Vadso, Norway - Vadso (VDS)",
			"value": "VDS",
			"domestic": 0
		}, {
			"label": "Valdez, United States - Valdez (VDZ)",
			"value": "VDZ",
			"domestic": 1
		}, {
			"label": "Venetie, United States - Venetie Airport (VEE)",
			"value": "VEE",
			"domestic": 1
		}, {
			"label": "Vernal, United States - Vernal Airport (VEL)",
			"value": "VEL",
			"domestic": 1
		}, {
			"label": "Veracruz, Mexico - Las Bajadas Airport (VER)",
			"value": "VER",
			"domestic": 0
		}, {
			"label": "Vestmannaeyjar, Iceland - Vestmannaeyjar (VEY)",
			"value": "VEY",
			"domestic": 0
		}, {
			"label": "Victoria Falls, Zimbabwe - Victoria Falls (VFA)",
			"value": "VFA",
			"domestic": 0
		}, {
			"label": "Vijayawada, India - Vijayawa (VGA)",
			"value": "VGA",
			"domestic": 0
		}, {
			"label": "Vigo, Spain - Vigo (VGO)",
			"value": "VGO",
			"domestic": 0
		}, {
			"label": "Villagarzon, Colombia - Villagarzon Airport (VGZ)",
			"value": "VGZ",
			"domestic": 0
		}, {
			"label": "Vilhelmina, Sweden - Vilhelmina Airport (VHM)",
			"value": "VHM",
			"domestic": 0
		}, {
			"label": "Vichy, France - Charmeil (VHY)",
			"value": "VHY",
			"domestic": 0
		}, {
			"label": "Vicenza, Italy - Vicenza (VIC)",
			"value": "VIC",
			"domestic": 0
		}, {
			"label": "Vienna, Austria - Vienna International Airport (VIE)",
			"value": "VIE",
			"domestic": 0
		}, {
			"label": "El Vigia, Venezuela - El Vigia Airport (VIG)",
			"value": "VIG",
			"domestic": 0
		}, {
			"label": "Vinh City, Vietnam - Vinh City Airport (VII)",
			"value": "VII",
			"domestic": 0
		}, {
			"label": "Spanish Town, British Virgin Islands - Virgin Gorda (VIJ)",
			"value": "VIJ",
			"domestic": 0
		}, {
			"label": "Dakhla, Morocco - Dakhla Airport (VIL)",
			"value": "VIL",
			"domestic": 0
		}, {
			"label": "Visalia, United States - Visalia Airport (VIS)",
			"value": "VIS",
			"domestic": 1
		}, {
			"label": "Vitoria, Spain - Vitoria (VIT)",
			"value": "VIT",
			"domestic": 0
		}, {
			"label": "Vitoria, Brazil - Eurico Sales (VIX)",
			"value": "VIX",
			"domestic": 0
		}, {
			"label": "Velizy-Villacoublay, France - Villa Coublay (VIY)",
			"value": "VIY",
			"domestic": 0
		}, {
			"label": "Abingdon, United States - Virginia Highlands (VJI)",
			"value": "VJI",
			"domestic": 1
		}, {
			"label": "Rach Gia, Vietnam - Rach Gia Airport (VKG)",
			"value": "VKG",
			"domestic": 0
		}, {
			"label": "Moscow, Russia - Vnukovo Airport (VKO)",
			"value": "VKO",
			"domestic": 0
		}, {
			"label": "Vorkuta, Russia - Vorkuta Airport (VKT)",
			"value": "VKT",
			"domestic": 0
		}, {
			"label": "Valencia, Spain - Valencia (VLC)",
			"value": "VLC",
			"domestic": 0
		}, {
			"label": "Valdosta, United States - Valdosta Regional Airport (VLD)",
			"value": "VLD",
			"domestic": 1
		}, {
			"label": "Villa Gesell, Argentina - Villa Gesell Airport (VLG)",
			"value": "VLG",
			"domestic": 0
		}, {
			"label": "Port Vila, Vanuatu - Bauerfield (VLI)",
			"value": "VLI",
			"domestic": 0
		}, {
			"label": "Valladolid, Spain - Valladolid (VLL)",
			"value": "VLL",
			"domestic": 0
		}, {
			"label": "Valencia, Venezuela - Valencia Airport (VLN)",
			"value": "VLN",
			"domestic": 0
		}, {
			"label": "Valesdir, Vanuatu - Valesdir Airport (VLS)",
			"value": "VLS",
			"domestic": 0
		}, {
			"label": "Valera, Venezuela - Carvajal Airport (VLV)",
			"value": "VLV",
			"domestic": 0
		}, {
			"label": "Baimuru, Papua New Guinea - Baimuru Airport (VMU)",
			"value": "VMU",
			"domestic": 0
		}, {
			"label": "Venice, United States - Venice Mncpl (VNC)",
			"value": "VNC",
			"domestic": 1
		}, {
			"label": "Vannes, France - Vannes (VNE)",
			"value": "VNE",
			"domestic": 0
		}, {
			"label": "Vilnius, Lithuania - Vilnius Airport (VNO)",
			"value": "VNO",
			"domestic": 0
		}, {
			"label": "Varanasi, India - Varanasi Babatpur (VNS)",
			"value": "VNS",
			"domestic": 0
		}, {
			"label": "Vilanculos, Mozambique - Vilanculos Airport (VNX)",
			"value": "VNX",
			"domestic": 0
		}, {
			"label": "Volgograd, Russia - Volgograd (VOG)",
			"value": "VOG",
			"domestic": 0
		}, {
			"label": "Volos, Greece - Volos (VOL)",
			"value": "VOL",
			"domestic": 0
		}, {
			"label": "Voronezh, Russia - Voronezh Airport (VOZ)",
			"value": "VOZ",
			"domestic": 0
		}, {
			"label": "Ongiva, Angola - Ongiva Airport (VPE)",
			"value": "VPE",
			"domestic": 0
		}, {
			"label": "Vopnafjordur, Iceland - Vopnafjordur (VPN)",
			"value": "VPN",
			"domestic": 0
		}, {
			"label": "Chimoio, Mozambique - Chimoio Airport (VPY)",
			"value": "VPY",
			"domestic": 0
		}, {
			"label": "Valparaiso, United States - Porter Cty (VPZ)",
			"value": "VPZ",
			"domestic": 1
		}, {
			"label": "Vieques Island, Puerto Rico - Isla de Vieques (VQS)",
			"value": "VQS",
			"domestic": 0
		}, {
			"label": "Varadero, Cuba - Juan Gualberto Gomez (VRA)",
			"value": "VRA",
			"domestic": 0
		}, {
			"label": "Vero Beach, United States - Vero Beach Mncpl (VRB)",
			"value": "VRB",
			"domestic": 1
		}, {
			"label": "Virac, Philippines - Virac Airport (VRC)",
			"value": "VRC",
			"domestic": 0
		}, {
			"label": "Varkaus, Finland - Varkaus (VRK)",
			"value": "VRK",
			"domestic": 0
		}, {
			"label": "Vila Real, Portugal - Vila Real (VRL)",
			"value": "VRL",
			"domestic": 0
		}, {
			"label": "Verona, Italy - Verona (VRN)",
			"value": "VRN",
			"domestic": 0
		}, {
			"label": "Vaeroy, Norway - Stolport Airport (VRY)",
			"value": "VRY",
			"domestic": 0
		}, {
			"label": "Villahermosa, Mexico - Villahermosa (VSA)",
			"value": "VSA",
			"domestic": 0
		}, {
			"label": "Viseu, Portugal - Viseu (VSE)",
			"value": "VSE",
			"domestic": 0
		}, {
			"label": "Springfield, United States - State (VSF)",
			"value": "VSF",
			"domestic": 1
		}, {
			"label": "Lugansk, Ukraine - Lugansk Airport (VSG)",
			"value": "VSG",
			"domestic": 0
		}, {
			"label": "Vientiane, Laos - Wattay (VTE)",
			"value": "VTE",
			"domestic": 0
		}, {
			"label": "Las Tunas, Cuba - Las Tunas Airport (VTU)",
			"value": "VTU",
			"domestic": 0
		}, {
			"label": "Vishakhapatnam, India - Vishakhapatnam (VTZ)",
			"value": "VTZ",
			"domestic": 0
		}, {
			"label": "Valledupar, Colombia - Valledupar Airport (VUP)",
			"value": "VUP",
			"domestic": 0
		}, {
			"label": "Villavicencio, Colombia - La Vanguardia Airport (VVC)",
			"value": "VVC",
			"domestic": 0
		}, {
			"label": "Santa Cruz, Bolivia - Viru Viru Intl (VVI)",
			"value": "VVI",
			"domestic": 0
		}, {
			"label": "Vladivostok, Russia - Vladivostok (VVO)",
			"value": "VVO",
			"domestic": 0
		}, {
			"label": "Illizi, Algeria - Illizi Airport (VVZ)",
			"value": "VVZ",
			"domestic": 0
		}, {
			"label": "Lichinga, Mozambique - Lichinga Airport (VXC)",
			"value": "VXC",
			"domestic": 0
		}, {
			"label": "Vaxjo, Sweden - Vaxjo (VXO)",
			"value": "VXO",
			"domestic": 0
		}, {
			"label": "Peru, United States - IL Valley Regl (VYS)",
			"value": "VYS",
			"domestic": 1
		}, {
			"label": "Wales, United States - Wales Airport (WAA)",
			"value": "WAA",
			"domestic": 1
		}, {
			"label": "Wadi Ad Dawasir, Saudi Arabia - Wadi Ad Dawasir Airport (WAE)",
			"value": "WAE",
			"domestic": 0
		}, {
			"label": "Wanganui, New Zealand - Wanganui (WAG)",
			"value": "WAG",
			"domestic": 0
		}, {
			"label": "Wahpeton, United States - Harry Stern (WAH)",
			"value": "WAH",
			"domestic": 1
		}, {
			"label": "Antsohihy, Madagascar - Antsohihy Airport (WAI)",
			"value": "WAI",
			"domestic": 0
		}, {
			"label": "Antsalova, Madagascar - Antsalova Airport (WAQ)",
			"value": "WAQ",
			"domestic": 0
		}, {
			"label": "Washington, D.C., United States - All Airports (WAS)",
			"value": "WAS",
			"domestic": 1
		}, {
			"label": "Waterford, Ireland - Waterford (WAT)",
			"value": "WAT",
			"domestic": 0
		}, {
			"label": "Warsaw, Poland - Frederic Chopin (WAW)",
			"value": "WAW",
			"domestic": 0
		}, {
			"label": "Stebbins, United States - Stebbins Airport (WBB)",
			"value": "WBB",
			"domestic": 1
		}, {
			"label": "Wapenamanda, Papua New Guinea - Wapenamanda Airport (WBM)",
			"value": "WBM",
			"domestic": 0
		}, {
			"label": "Beaver, United States - Beaver Airport (WBQ)",
			"value": "WBQ",
			"domestic": 1
		}, {
			"label": "Enid, United States - Woodring Mnpl (WDG)",
			"value": "WDG",
			"domestic": 1
		}, {
			"label": "Windhoek, Namibia - Hosea Kutako Intl (WDH)",
			"value": "WDH",
			"domestic": 0
		}, {
			"label": "Weifang, China - Weifang (WEF)",
			"value": "WEF",
			"domestic": 0
		}, {
			"label": "Weihai, China - Weihai (WEH)",
			"value": "WEH",
			"domestic": 0
		}, {
			"label": "Weipa, Australia - Weipa Airport (WEI)",
			"value": "WEI",
			"domestic": 0
		}, {
			"label": "Fianarantsoa, Madagascar - Fianarantsoa Airport (WFI)",
			"value": "WFI",
			"domestic": 0
		}, {
			"label": "Wagga Wagga, Australia - Forest Hill (WGA)",
			"value": "WGA",
			"domestic": 0
		}, {
			"label": "Winchester, United States - Winchester (WGO)",
			"value": "WGO",
			"domestic": 1
		}, {
			"label": "Waingapu, Indonesia - Waingapu Airport (WGP)",
			"value": "WGP",
			"domestic": 0
		}, {
			"label": "Wangaratta, Australia - Wangaratta (WGT)",
			"value": "WGT",
			"domestic": 0
		}, {
			"label": "Hyder, United States - Hyder Sea Plane Base (WHD)",
			"value": "WHD",
			"domestic": 1
		}, {
			"label": "Wadi Halfa, Sudan - Wadi Halfa Airport (WHF)",
			"value": "WHF",
			"domestic": 0
		}, {
			"label": "Whakatane, New Zealand - Whakatane Airport (WHK)",
			"value": "WHK",
			"domestic": 0
		}, {
			"label": "Wharton, United States - Wharton Mncpl (WHT)",
			"value": "WHT",
			"domestic": 1
		}, {
			"label": "Wick, United Kingdom - Wick (WIC)",
			"value": "WIC",
			"domestic": 0
		}, {
			"label": "Nairobi, Kenya - Wilson (WIL)",
			"value": "WIL",
			"domestic": 0
		}, {
			"label": "Winton, Australia - Winton Airport (WIN)",
			"value": "WIN",
			"domestic": 0
		}, {
			"label": "Woja, Marshall Islands - Woja Airport (WJA)",
			"value": "WJA",
			"domestic": 0
		}, {
			"label": "Wonju, South Korea - Wonju Airport (WJU)",
			"value": "WJU",
			"domestic": 0
		}, {
			"label": "Wanaka, New Zealand - Wanaka (WKA)",
			"value": "WKA",
			"domestic": 0
		}, {
			"label": "Wakkanai, Japan - Wakkanai (WKJ)",
			"value": "WKJ",
			"domestic": 0
		}, {
			"label": "Walker\'s Cay, Bahamas - Walker\'s Cay Mncpl (WKR)",
			"value": "WKR",
			"domestic": 0
		}, {
			"label": "Winfield, United States - Strother Field (WLD)",
			"value": "WLD",
			"domestic": 1
		}, {
			"label": "Wellington, New Zealand - Wellington Intl (WLG)",
			"value": "WLG",
			"domestic": 0
		}, {
			"label": "Walaha, Vanuatu - Walaha Airport (WLH)",
			"value": "WLH",
			"domestic": 0
		}, {
			"label": "Selawik, United States - Selawik Airport (WLK)",
			"value": "WLK",
			"domestic": 1
		}, {
			"label": "Wallis Island, Wallis and Futuna - Wallis Island Airport (WLS)",
			"value": "WLS",
			"domestic": 0
		}, {
			"label": "Warrnambool, Australia - Warrnambool (WMB)",
			"value": "WMB",
			"domestic": 0
		}, {
			"label": "Winnemucca, United States - Winnemucca Mncpl (WMC)",
			"value": "WMC",
			"domestic": 1
		}, {
			"label": "Meyers Chuck, United States - Meyers Chuck Sea Plane Base (WMK)",
			"value": "WMK",
			"domestic": 1
		}, {
			"label": "Maroantsetra, Madagascar - Maroantsetra Airport (WMN)",
			"value": "WMN",
			"domestic": 0
		}, {
			"label": "White Mountain, United States - White Mountain Airport (WMO)",
			"value": "WMO",
			"domestic": 1
		}, {
			"label": "Napakiak, United States - Napakiak Sea Plane Base (WNA)",
			"value": "WNA",
			"domestic": 1
		}, {
			"label": "Wenshan, China - Wenshan Puzhehei (WNH)",
			"value": "WNH",
			"domestic": 0
		}, {
			"label": "Wunnummin Lake, Canada - Wunnummin Lake Airport (WNN)",
			"value": "WNN",
			"domestic": 0
		}, {
			"label": "Naga, Philippines - Naga Airport (WNP)",
			"value": "WNP",
			"domestic": 0
		}, {
			"label": "Windorah, Australia - Windorah Airport (WNR)",
			"value": "WNR",
			"domestic": 0
		}, {
			"label": "Wenzhou, China - Wenzhou (WNZ)",
			"value": "WNZ",
			"domestic": 0
		}, {
			"label": "Wollongong, Australia - Wollongong (WOL)",
			"value": "WOL",
			"domestic": 0
		}, {
			"label": "Wonan, Taiwan - Wonan Airport (WOT)",
			"value": "WOT",
			"domestic": 0
		}, {
			"label": "Whangarei, New Zealand - Whangarei (WRE)",
			"value": "WRE",
			"domestic": 0
		}, {
			"label": "Wrangell, United States - Wrangell Sea Plane Base (WRG)",
			"value": "WRG",
			"domestic": 1
		}, {
			"label": "Worland, United States - Worland Municipal Airport (WRL)",
			"value": "WRL",
			"domestic": 1
		}, {
			"label": "Wroclaw, Poland - Strachowice (WRO)",
			"value": "WRO",
			"domestic": 0
		}, {
			"label": "Westray, United Kingdom - Westray Airport (WRY)",
			"value": "WRY",
			"domestic": 0
		}, {
			"label": "Washington, United States - Washington County (WSG)",
			"value": "WSG",
			"domestic": 1
		}, {
			"label": "South Naknek, United States - South Naknek Airport (WSN)",
			"value": "WSN",
			"domestic": 1
		}, {
			"label": "Westerly, United States - Westerly State (WST)",
			"value": "WST",
			"domestic": 1
		}, {
			"label": "Westsound, United States - Westsound Airport (WSX)",
			"value": "WSX",
			"domestic": 1
		}, {
			"label": "Airlie Beach, Australia - Whitsunday (WSY)",
			"value": "WSY",
			"domestic": 0
		}, {
			"label": "Westport, New Zealand - Westport (WSZ)",
			"value": "WSZ",
			"domestic": 0
		}, {
			"label": "Tambohorano, Madagascar - Tambohorano Airport (WTA)",
			"value": "WTA",
			"domestic": 0
		}, {
			"label": "Wotje, Marshall Islands - Wotje Airport (WTE)",
			"value": "WTE",
			"domestic": 0
		}, {
			"label": "Noatak, United States - Noatak Airport (WTK)",
			"value": "WTK",
			"domestic": 1
		}, {
			"label": "Tuntutuliak, United States - Tuntutuliak Airport (WTL)",
			"value": "WTL",
			"domestic": 1
		}, {
			"label": "Wotho, Marshall Islands - Wotho Airport (WTO)",
			"value": "WTO",
			"domestic": 0
		}, {
			"label": "Tsiroanomandidy, Madagascar - Tsiroanomandidy Airport (WTS)",
			"value": "WTS",
			"domestic": 0
		}, {
			"label": "Wu Hai, China - Wu Hai Airport (WUA)",
			"value": "WUA",
			"domestic": 0
		}, {
			"label": "Wuhan, China - Wuhan (WUH)",
			"value": "WUH",
			"domestic": 0
		}, {
			"label": "Wiluna, Australia - Wiluna Airport (WUN)",
			"value": "WUN",
			"domestic": 0
		}, {
			"label": "Wuyishan, China - Wuyishan (WUS)",
			"value": "WUS",
			"domestic": 0
		}, {
			"label": "Wuxi, China - Wuxi (WUX)",
			"value": "WUX",
			"domestic": 0
		}, {
			"label": "Walvis Bay, Namibia - Rooikop (WVB)",
			"value": "WVB",
			"domestic": 0
		}, {
			"label": "Watsonville, United States - Watsonville Mcpl (WVI)",
			"value": "WVI",
			"domestic": 1
		}, {
			"label": "Manakara, Madagascar - Manakara Airport (WVK)",
			"value": "WVK",
			"domestic": 0
		}, {
			"label": "Waterville, United States - Robert Lafleur (WVL)",
			"value": "WVL",
			"domestic": 1
		}, {
			"label": "Cape May, United States - Cape May County (WWD)",
			"value": "WWD",
			"domestic": 1
		}, {
			"label": "Wewak, Papua New Guinea - Boram Airport (WWK)",
			"value": "WWK",
			"domestic": 0
		}, {
			"label": "Whale Pass, United States - Whale Pass Airport (WWP)",
			"value": "WWP",
			"domestic": 1
		}, {
			"label": "Newtok, United States - Newtok Airport (WWT)",
			"value": "WWT",
			"domestic": 1
		}, {
			"label": "West Wyalong, Australia - West Wyalong (WWY)",
			"value": "WWY",
			"domestic": 0
		}, {
			"label": "Wanxian, China - Wanxian Airport (WXN)",
			"value": "WXN",
			"domestic": 0
		}, {
			"label": "Whyalla, Australia - Whyalla (WYA)",
			"value": "WYA",
			"domestic": 0
		}, {
			"label": "Yes Bay, United States - Yes Bay Sea Plane Base (WYB)",
			"value": "WYB",
			"domestic": 1
		}, {
			"label": "West Yellowstone, United States - Yellowstone (WYS)",
			"value": "WYS",
			"domestic": 1
		}, {
			"label": "Chapeco, Brazil - Chapeco Airport (XAP)",
			"value": "XAP",
			"domestic": 0
		}, {
			"label": "Bearskin Lake, Canada - Bearskin Lake Airport (XBE)",
			"value": "XBE",
			"domestic": 0
		}, {
			"label": "Birjand, Iran - Birjand Airport (XBJ)",
			"value": "XBJ",
			"domestic": 0
		}, {
			"label": "Brockville, Canada - Brockville (XBR)",
			"value": "XBR",
			"domestic": 0
		}, {
			"label": "Christmas Island, Christmas Island - Christmas Island Airport (XCH)",
			"value": "XCH",
			"domestic": 0
		}, {
			"label": "Coron, Philippines - Coron (XCN)",
			"value": "XCN",
			"domestic": 0
		}, {
			"label": "Xiangfan, China - Xiangfan (XFN)",
			"value": "XFN",
			"domestic": 0
		}, {
			"label": "Kangiqsualujjuaq, Canada - Kangiqsualujjuaq Airport (XGR)",
			"value": "XGR",
			"domestic": 0
		}, {
			"label": "Xichang, China - Xichang (XIC)",
			"value": "XIC",
			"domestic": 0
		}, {
			"label": "Xilinhot, China - Xilinhot Airport (XIL)",
			"value": "XIL",
			"domestic": 0
		}, {
			"label": "Ilimanaq, Greenland - Ilimanaq Harbour Airport (XIQ)",
			"value": "XIQ",
			"domestic": 0
		}, {
			"label": "Xi\'an, China - Xianyang (XIY)",
			"value": "XIY",
			"domestic": 0
		}, {
			"label": "Xieng Khouang, Laos - Xieng Khouang Airport (XKH)",
			"value": "XKH",
			"domestic": 0
		}, {
			"label": "Kasabonika, Canada - Kasabonika Airport (XKS)",
			"value": "XKS",
			"domestic": 0
		}, {
			"label": "Lac Brochet, Canada - Lac Brochet Airport (XLB)",
			"value": "XLB",
			"domestic": 0
		}, {
			"label": "Saint Louis, Senegal - St. Louis Airport (XLS)",
			"value": "XLS",
			"domestic": 0
		}, {
			"label": "Manihi, French Polynesia and Tahiti - Manihi (XMH)",
			"value": "XMH",
			"domestic": 0
		}, {
			"label": "Xiamen, China - Xiamen Intl (XMN)",
			"value": "XMN",
			"domestic": 0
		}, {
			"label": "Macas, Ecuador - Macas Airport (XMS)",
			"value": "XMS",
			"domestic": 0
		}, {
			"label": "Yam Island, Australia - Yam Island Airport (XMY)",
			"value": "XMY",
			"domestic": 0
		}, {
			"label": "Xining, China - Xining (XNN)",
			"value": "XNN",
			"domestic": 0
		}, {
			"label": "Puerto Quepos, Costa Rica - Puerto Quepos Mncpl (XQP)",
			"value": "XQP",
			"domestic": 0
		}, {
			"label": "Qualicum, Canada - Qualicum Airport (XQU)",
			"value": "XQU",
			"domestic": 0
		}, {
			"label": "Ross River, Canada - Ross River (XRR)",
			"value": "XRR",
			"domestic": 0
		}, {
			"label": "Jerez de la Frontera, Spain - La Parra (XRY)",
			"value": "XRY",
			"domestic": 0
		}, {
			"label": "South Caicos, Turks and Caicos - South Caicos Mncpl (XSC)",
			"value": "XSC",
			"domestic": 0
		}, {
			"label": "South Indian Lake, Canada - South Indian Lake Airport (XSI)",
			"value": "XSI",
			"domestic": 0
		}, {
			"label": "Thargomindah, Australia - Thargomindah Airport (XTG)",
			"value": "XTG",
			"domestic": 0
		}, {
			"label": "Tadoule Lake, Canada - Tadoule Lake Airport (XTL)",
			"value": "XTL",
			"domestic": 0
		}, {
			"label": "Xuzhou, China - Xuzhou (XUZ)",
			"value": "XUZ",
			"domestic": 0
		}, {
			"label": "Anahim Lake, Canada - Anahim Lake Airport (YAA)",
			"value": "YAA",
			"domestic": 0
		}, {
			"label": "Cat Lake, Canada - Cat Lake Airport (YAC)",
			"value": "YAC",
			"domestic": 0
		}, {
			"label": "Fort Frances, Canada - Fort Frances Mncpl (YAG)",
			"value": "YAG",
			"domestic": 0
		}, {
			"label": "Yakutat, United States - Yakutat Airport (YAK)",
			"value": "YAK",
			"domestic": 1
		}, {
			"label": "Sault Ste. Marie, Canada - Sault Ste. Marie Airport (YAM)",
			"value": "YAM",
			"domestic": 0
		}, {
			"label": "Yaounde, Cameroon - Yaounde (YAO)",
			"value": "YAO",
			"domestic": 0
		}, {
			"label": "Colonia, Micronesia - Yap (YAP)",
			"value": "YAP",
			"domestic": 0
		}, {
			"label": "Attawapiskat, Canada - Attawapiskat Airport (YAT)",
			"value": "YAT",
			"domestic": 0
		}, {
			"label": "Angling Lake, Canada - Angling Lake Airport (YAX)",
			"value": "YAX",
			"domestic": 0
		}, {
			"label": "Saint Anthony, Canada - St. Anthony Airport (YAY)",
			"value": "YAY",
			"domestic": 0
		}, {
			"label": "Tofino, Canada - Tofino Mncpl (YAZ)",
			"value": "YAZ",
			"domestic": 0
		}, {
			"label": "Baie-Comeau, Canada - Baie Comeau Airport (YBC)",
			"value": "YBC",
			"domestic": 0
		}, {
			"label": "New Westminster, Canada - New Westminster (YBD)",
			"value": "YBD",
			"domestic": 0
		}, {
			"label": "Uranium City, Canada - Uranium City (YBE)",
			"value": "YBE",
			"domestic": 0
		}, {
			"label": "Bagotville, Canada - Bagotville Airport (YBG)",
			"value": "YBG",
			"domestic": 0
		}, {
			"label": "Black Tickle, Canada - Black Tickle Airport (YBI)",
			"value": "YBI",
			"domestic": 0
		}, {
			"label": "Baker Lake, Canada - Baker Lake (YBK)",
			"value": "YBK",
			"domestic": 0
		}, {
			"label": "Campbell River, Canada - Campbell River Airport (YBL)",
			"value": "YBL",
			"domestic": 0
		}, {
			"label": "Yibin, China - Yibin (YBP)",
			"value": "YBP",
			"domestic": 0
		}, {
			"label": "Brandon, Canada - Brandon Mncpl (YBR)",
			"value": "YBR",
			"domestic": 0
		}, {
			"label": "Brochet, Canada - Brochet Airport (YBT)",
			"value": "YBT",
			"domestic": 0
		}, {
			"label": "Berens River, Canada - Berens River Airport (YBV)",
			"value": "YBV",
			"domestic": 0
		}, {
			"label": "Blanc Sablon, Canada - Blanc Sablon Airport (YBX)",
			"value": "YBX",
			"domestic": 0
		}, {
			"label": "Cambridge Bay, Canada - Cambridge Bay (YCB)",
			"value": "YCB",
			"domestic": 0
		}, {
			"label": "Cornwall, Canada - Cornwall Mncpl (YCC)",
			"value": "YCC",
			"domestic": 0
		}, {
			"label": "Nanaimo, Canada - Nanaimo Airport (YCD)",
			"value": "YCD",
			"domestic": 0
		}, {
			"label": "Castlegar, Canada - Castlegar Airport (YCG)",
			"value": "YCG",
			"domestic": 0
		}, {
			"label": "Miramichi, Canada - Miramichi (YCH)",
			"value": "YCH",
			"domestic": 0
		}, {
			"label": "Colville Lake, Canada - Colville Lake Airport (YCK)",
			"value": "YCK",
			"domestic": 0
		}, {
			"label": "St Catharines, Canada - St Catharines Mncpl (YCM)",
			"value": "YCM",
			"domestic": 0
		}, {
			"label": "Cochrane, Canada - Cochrane Mncpl (YCN)",
			"value": "YCN",
			"domestic": 0
		}, {
			"label": "Kugluktuk Coppermine, Canada - Kugluktuk (YCO)",
			"value": "YCO",
			"domestic": 0
		}, {
			"label": "Chetwynd, Canada - Chetwynd Mncpl (YCQ)",
			"value": "YCQ",
			"domestic": 0
		}, {
			"label": "Cross Lake, Canada - Cross Lake Airport (YCR)",
			"value": "YCR",
			"domestic": 0
		}, {
			"label": "Chesterfield Inlet, Canada - Chesterfield Inlet Airport (YCS)",
			"value": "YCS",
			"domestic": 0
		}, {
			"label": "Yun Cheng, China - Yun Cheng Airport (YCU)",
			"value": "YCU",
			"domestic": 0
		}, {
			"label": "Chilliwack, Canada - Chilliwack (YCW)",
			"value": "YCW",
			"domestic": 0
		}, {
			"label": "Clyde River, Canada - Clyde River Airport (YCY)",
			"value": "YCY",
			"domestic": 0
		}, {
			"label": "Dawson City, Canada - Dawson City (YDA)",
			"value": "YDA",
			"domestic": 0
		}, {
			"label": "Drayton Valley, Canada - Industrial (YDC)",
			"value": "YDC",
			"domestic": 0
		}, {
			"label": "Deer Lake, NL, Canada - Deer Lake Airport (YDF)",
			"value": "YDF",
			"domestic": 0
		}, {
			"label": "Digby, Canada - Digby (YDG)",
			"value": "YDG",
			"domestic": 0
		}, {
			"label": "Dauphin, Canada - Dauphin Mncpl (YDN)",
			"value": "YDN",
			"domestic": 0
		}, {
			"label": "Dolbeau, Canada - Dolbeau Mncpl (YDO)",
			"value": "YDO",
			"domestic": 0
		}, {
			"label": "Nain, Canada - Nain Airport (YDP)",
			"value": "YDP",
			"domestic": 0
		}, {
			"label": "Dawson Creek, Canada - Dawson Creek Airport (YDQ)",
			"value": "YDQ",
			"domestic": 0
		}, {
			"label": "Edmonton, Canada - All Airports (YEA)",
			"value": "YEA",
			"domestic": 0
		}, {
			"label": "Edmonton, Canada - Edmonton (YEG)",
			"value": "YEG",
			"domestic": 0
		}, {
			"label": "Asaloyeh, Iran - Asaloyeh Airport (YEH)",
			"value": "YEH",
			"domestic": 0
		}, {
			"label": "Arviat, Canada - Arviat Mncpl (YEK)",
			"value": "YEK",
			"domestic": 0
		}, {
			"label": "Elliot Lake, Canada - Elliot Lake (YEL)",
			"value": "YEL",
			"domestic": 0
		}, {
			"label": "Fort Severn, Canada - Fort Severn Airport (YER)",
			"value": "YER",
			"domestic": 0
		}, {
			"label": "Yasouj, Iran - Yasouj Airport (YES)",
			"value": "YES",
			"domestic": 0
		}, {
			"label": "Inuvik, Canada - Inuvik (YEV)",
			"value": "YEV",
			"domestic": 0
		}, {
			"label": "Fort Albany, Canada - Fort Albany Airport (YFA)",
			"value": "YFA",
			"domestic": 0
		}, {
			"label": "Iqaluit, Canada - Frobisher/Iqaluit (YFB)",
			"value": "YFB",
			"domestic": 0
		}, {
			"label": "Fredericton, Canada - Fredericton Airport (YFC)",
			"value": "YFC",
			"domestic": 0
		}, {
			"label": "Forestville, Canada - Forestville Mncpl (YFE)",
			"value": "YFE",
			"domestic": 0
		}, {
			"label": "Fort Hope, Canada - Fort Hope Airport (YFH)",
			"value": "YFH",
			"domestic": 0
		}, {
			"label": "Snare Lake, Canada - Snare Lake Airport (YFJ)",
			"value": "YFJ",
			"domestic": 0
		}, {
			"label": "Flin Flon, Canada - Flin Flon (YFO)",
			"value": "YFO",
			"domestic": 0
		}, {
			"label": "Fort Simpson, Canada - Fort Simpson (YFS)",
			"value": "YFS",
			"domestic": 0
		}, {
			"label": "Fox Harbour, Canada - Fox Harbour Airport (YFX)",
			"value": "YFX",
			"domestic": 0
		}, {
			"label": "Gillies Bay, Canada - Gillies Bay Airport (YGB)",
			"value": "YGB",
			"domestic": 0
		}, {
			"label": "Fort Good Hope, Canada - Fort Good Hope Airport (YGH)",
			"value": "YGH",
			"domestic": 0
		}, {
			"label": "Yonago, Japan - Yonago (YGJ)",
			"value": "YGJ",
			"domestic": 0
		}, {
			"label": "Kingston, Canada - Kingston Airport (YGK)",
			"value": "YGK",
			"domestic": 0
		}, {
			"label": "La Grande, Canada - La Grande Airport (YGL)",
			"value": "YGL",
			"domestic": 0
		}, {
			"label": "Gods Narrows, Canada - Gods Narrows Airport (YGO)",
			"value": "YGO",
			"domestic": 0
		}, {
			"label": "Gaspe, Canada - Metropolitan Area Airport (YGP)",
			"value": "YGP",
			"domestic": 0
		}, {
			"label": "Iles de la Madeleine, Canada - Iles De La Madeleine Airport (YGR)",
			"value": "YGR",
			"domestic": 0
		}, {
			"label": "Igloolik, Canada - Igloolik Mncpl (YGT)",
			"value": "YGT",
			"domestic": 0
		}, {
			"label": "Havre St. Pierre, Canada - Havre St. Pierre Airport (YGV)",
			"value": "YGV",
			"domestic": 0
		}, {
			"label": "Kuujjuarapik, Canada - Kuujjuarapik Airport (YGW)",
			"value": "YGW",
			"domestic": 0
		}, {
			"label": "Gillam, Canada - Gillam Airport (YGX)",
			"value": "YGX",
			"domestic": 0
		}, {
			"label": "Grise Fiord, Canada - Grise Fiord Airport (YGZ)",
			"value": "YGZ",
			"domestic": 0
		}, {
			"label": "Port Hope Simpson, Canada - Port Hope Simpson Airport (YHA)",
			"value": "YHA",
			"domestic": 0
		}, {
			"label": "Dryden, Canada - Dryden Mncpl (YHD)",
			"value": "YHD",
			"domestic": 0
		}, {
			"label": "Charlottetown, Canada - Charlottetown Mncpl (YHG)",
			"value": "YHG",
			"domestic": 0
		}, {
			"label": "Holman, Canada - Holman Airport (YHI)",
			"value": "YHI",
			"domestic": 0
		}, {
			"label": "Gjoa Haven, Canada - Gjoa Haven Airport (YHK)",
			"value": "YHK",
			"domestic": 0
		}, {
			"label": "Hamilton, Canada - Hamilton Airport (YHM)",
			"value": "YHM",
			"domestic": 0
		}, {
			"label": "Hopedale, Canada - Hopedale Airport (YHO)",
			"value": "YHO",
			"domestic": 0
		}, {
			"label": "Poplar Hill, Canada - Poplar Hill Airport (YHP)",
			"value": "YHP",
			"domestic": 0
		}, {
			"label": "Chevery, Canada - Chevery Airport (YHR)",
			"value": "YHR",
			"domestic": 0
		}, {
			"label": "Montreal, Canada - St. Hubert (YHU)",
			"value": "YHU",
			"domestic": 0
		}, {
			"label": "Hay River, Canada - Hay River (YHY)",
			"value": "YHY",
			"domestic": 0
		}, {
			"label": "Halifax, Canada - Halifax (YHZ)",
			"value": "YHZ",
			"domestic": 0
		}, {
			"label": "Pakuashipi, Canada - Pakuashipi Airport (YIF)",
			"value": "YIF",
			"domestic": 0
		}, {
			"label": "Yichang, China - Yichang (YIH)",
			"value": "YIH",
			"domestic": 0
		}, {
			"label": "Ivujivik, Canada - Ivujivik Airport (YIK)",
			"value": "YIK",
			"domestic": 0
		}, {
			"label": "Yining, China - Yining Airport (YIN)",
			"value": "YIN",
			"domestic": 0
		}, {
			"label": "Pond Inlet, Canada - Pond Inlet (YIO)",
			"value": "YIO",
			"domestic": 0
		}, {
			"label": "Island Lake/Garden Hill, Canada - Island Lake/Garden Hill Airport (YIV)",
			"value": "YIV",
			"domestic": 0
		}, {
			"label": "Yiwu, China - Yiwu (YIW)",
			"value": "YIW",
			"domestic": 0
		}, {
			"label": "Jasper, Canada - Jasper Mncpl (YJA)",
			"value": "YJA",
			"domestic": 0
		}, {
			"label": "Stephenville, Canada - Stephenville Airport (YJT)",
			"value": "YJT",
			"domestic": 0
		}, {
			"label": "Kamloops, Canada - Kamloops Airport (YKA)",
			"value": "YKA",
			"domestic": 0
		}, {
			"label": "Waterloo, Canada - Kitchener-Waterloo Regional Airport (YKF)",
			"value": "YKF",
			"domestic": 0
		}, {
			"label": "Kangirsuk, Canada - Kangirsuk Airport (YKG)",
			"value": "YKG",
			"domestic": 0
		}, {
			"label": "Schefferville, Canada - Schefferville (YKL)",
			"value": "YKL",
			"domestic": 0
		}, {
			"label": "Yakima, United States - Yakima Air Terminal (YKM)",
			"value": "YKM",
			"domestic": 1
		}, {
			"label": "Yankton, United States - Chan Gurney (YKN)",
			"value": "YKN",
			"domestic": 1
		}, {
			"label": "Waskaganish, Canada - Waskaganish Airport (YKQ)",
			"value": "YKQ",
			"domestic": 0
		}, {
			"label": "Yakutsk, Russia - Yakutsk Airport (YKS)",
			"value": "YKS",
			"domestic": 0
		}, {
			"label": "Klemtu, Canada - Klemtu Airport (YKT)",
			"value": "YKT",
			"domestic": 0
		}, {
			"label": "Chisasibi, Canada - Chisasibi Airport (YKU)",
			"value": "YKU",
			"domestic": 0
		}, {
			"label": "Kirkland Lake, Canada - Kirkland Lake (YKX)",
			"value": "YKX",
			"domestic": 0
		}, {
			"label": "Kimmirut Lake Harbour, Canada - Kimmirut (YLC)",
			"value": "YLC",
			"domestic": 0
		}, {
			"label": "Chapleau, Canada - Chapleau Mncpl (YLD)",
			"value": "YLD",
			"domestic": 0
		}, {
			"label": "Wha Ti/Lac La Martre, Canada - Wha Ti Airport (YLE)",
			"value": "YLE",
			"domestic": 0
		}, {
			"label": "Lansdowne House, Canada - Lansdowne House Airport (YLH)",
			"value": "YLH",
			"domestic": 0
		}, {
			"label": "Meadow Lake, Canada - Meadow Lake Airport (YLJ)",
			"value": "YLJ",
			"domestic": 0
		}, {
			"label": "Lloydminster, Canada - Lloydminster Airport (YLL)",
			"value": "YLL",
			"domestic": 0
		}, {
			"label": "Mingan, Canada - Mingan (YLP)",
			"value": "YLP",
			"domestic": 0
		}, {
			"label": "Leaf Rapids, Canada - Leaf Rapids Mncpl (YLR)",
			"value": "YLR",
			"domestic": 0
		}, {
			"label": "Kelowna, Canada - Kelowna Airport (YLW)",
			"value": "YLW",
			"domestic": 0
		}, {
			"label": "Langley, Canada - Langley (YLY)",
			"value": "YLY",
			"domestic": 0
		}, {
			"label": "Merritt, Canada - Merritt Mncpl (YMB)",
			"value": "YMB",
			"domestic": 0
		}, {
			"label": "Matane, Canada - Matane Mncpl (YME)",
			"value": "YME",
			"domestic": 0
		}, {
			"label": "Mary\'s Harbour, Canada - Mary\'s Harbour Airport (YMH)",
			"value": "YMH",
			"domestic": 0
		}, {
			"label": "Ft McMurray, Canada - Fort McMurray Airport (YMM)",
			"value": "YMM",
			"domestic": 0
		}, {
			"label": "Makkovik, Canada - Makkovik Airport (YMN)",
			"value": "YMN",
			"domestic": 0
		}, {
			"label": "Moosonee, Canada - Moosonee Airport (YMO)",
			"value": "YMO",
			"domestic": 0
		}, {
			"label": "Montreal, Canada - All Airports (YMQ)",
			"value": "YMQ",
			"domestic": 0
		}, {
			"label": "Chibougamau, Canada - Chibougamau Mncpl (YMT)",
			"value": "YMT",
			"domestic": 0
		}, {
			"label": "Natashquan, Canada - Natashquan Airport (YNA)",
			"value": "YNA",
			"domestic": 0
		}, {
			"label": "Yanbu, Saudi Arabia - Yanbu (YNB)",
			"value": "YNB",
			"domestic": 0
		}, {
			"label": "Wemindji, Canada - Wemindji Airport (YNC)",
			"value": "YNC",
			"domestic": 0
		}, {
			"label": "Gatineau, Canada - Gatineau Mncpl (YND)",
			"value": "YND",
			"domestic": 0
		}, {
			"label": "Norway House, Canada - Norway House Airport (YNE)",
			"value": "YNE",
			"domestic": 0
		}, {
			"label": "Youngstown, United States - Youngstown Mncpl (YNG)",
			"value": "YNG",
			"domestic": 1
		}, {
			"label": "Yanji, China - Yanji (YNJ)",
			"value": "YNJ",
			"domestic": 0
		}, {
			"label": "Points North Landing, Canada - Points North Landing Airport (YNL)",
			"value": "YNL",
			"domestic": 0
		}, {
			"label": "North Spirit Lake, Canada - North Spirit Lake Airport (YNO)",
			"value": "YNO",
			"domestic": 0
		}, {
			"label": "Natuashish, Canada - Natuashish Airport (YNP)",
			"value": "YNP",
			"domestic": 0
		}, {
			"label": "Nemiscau, Canada - Nemiscau Airport (YNS)",
			"value": "YNS",
			"domestic": 0
		}, {
			"label": "Yantai, China - Laishan (YNT)",
			"value": "YNT",
			"domestic": 0
		}, {
			"label": "Yangyang, South Korea - Yangyang Airport (YNY)",
			"value": "YNY",
			"domestic": 0
		}, {
			"label": "Yancheng, China - Yancheng Airport (YNZ)",
			"value": "YNZ",
			"domestic": 0
		}, {
			"label": "Old Crow, Canada - Old Crow Airport (YOC)",
			"value": "YOC",
			"domestic": 0
		}, {
			"label": "Ogoki, Canada - Ogoki Airport (YOG)",
			"value": "YOG",
			"domestic": 0
		}, {
			"label": "Oxford House, Canada - Oxford House Airport (YOH)",
			"value": "YOH",
			"domestic": 0
		}, {
			"label": "High Level, Canada - Footner Lake (YOJ)",
			"value": "YOJ",
			"domestic": 0
		}, {
			"label": "Oshawa, Canada - Oshawa (YOO)",
			"value": "YOO",
			"domestic": 0
		}, {
			"label": "Rainbow Lake, Canada - Rainbow Lake Mncpl (YOP)",
			"value": "YOP",
			"domestic": 0
		}, {
			"label": "Owen Sound, Canada - Billy Bishop Regl (YOS)",
			"value": "YOS",
			"domestic": 0
		}, {
			"label": "Ottawa, Canada - Ottawa (YOW)",
			"value": "YOW",
			"domestic": 0
		}, {
			"label": "Prince Albert, Canada - Prince Albert (YPA)",
			"value": "YPA",
			"domestic": 0
		}, {
			"label": "Port Alberni, Canada - Port Alberni (YPB)",
			"value": "YPB",
			"domestic": 0
		}, {
			"label": "Paulatuk, Canada - Paulatuk Airport (YPC)",
			"value": "YPC",
			"domestic": 0
		}, {
			"label": "Parry Sound, Canada - Parry Sound Mncpl (YPD)",
			"value": "YPD",
			"domestic": 0
		}, {
			"label": "Peace River, Canada - Peace River Airport (YPE)",
			"value": "YPE",
			"domestic": 0
		}, {
			"label": "Inukjuak, Canada - Inukjuak Airport (YPH)",
			"value": "YPH",
			"domestic": 0
		}, {
			"label": "Aupaluk, Canada - Aupaluk Airport (YPJ)",
			"value": "YPJ",
			"domestic": 0
		}, {
			"label": "Pickle Lake, Canada - Provincial (YPL)",
			"value": "YPL",
			"domestic": 0
		}, {
			"label": "Pikangikum, Canada - Pikangikum Airport (YPM)",
			"value": "YPM",
			"domestic": 0
		}, {
			"label": "Port Menier, Canada - Port Menier Airport (YPN)",
			"value": "YPN",
			"domestic": 0
		}, {
			"label": "Peawanuk, Canada - Peawanuk Airport (YPO)",
			"value": "YPO",
			"domestic": 0
		}, {
			"label": "Peterborough, Canada - Peterborough Mncpl (YPQ)",
			"value": "YPQ",
			"domestic": 0
		}, {
			"label": "Prince Rupert, Canada - Digby Island Airport (YPR)",
			"value": "YPR",
			"domestic": 0
		}, {
			"label": "Port Hawkesbury, Canada - Port Hawkesbury (YPS)",
			"value": "YPS",
			"domestic": 0
		}, {
			"label": "Pender Harbor, Canada - Pender Harbor Mncpl (YPT)",
			"value": "YPT",
			"domestic": 0
		}, {
			"label": "Powell River, Canada - Powell River (YPW)",
			"value": "YPW",
			"domestic": 0
		}, {
			"label": "Povungnituk, Canada - Puvirnituq Airport (YPX)",
			"value": "YPX",
			"domestic": 0
		}, {
			"label": "Quebec City, Canada - Quebec City Airport (YQB)",
			"value": "YQB",
			"domestic": 0
		}, {
			"label": "Quaqtaq, Canada - Quaqtaq Airport (YQC)",
			"value": "YQC",
			"domestic": 0
		}, {
			"label": "The Pas, Canada - The Pas (YQD)",
			"value": "YQD",
			"domestic": 0
		}, {
			"label": "Red Deer, Canada - Red Deer Industrial (YQF)",
			"value": "YQF",
			"domestic": 0
		}, {
			"label": "Windsor, Canada - Windsor Airport (YQG)",
			"value": "YQG",
			"domestic": 0
		}, {
			"label": "Watson Lake, Canada - Watson Lake (YQH)",
			"value": "YQH",
			"domestic": 0
		}, {
			"label": "Yarmouth, Canada - Yarmouth Airport (YQI)",
			"value": "YQI",
			"domestic": 0
		}, {
			"label": "Kenora, Canada - Kenora (YQK)",
			"value": "YQK",
			"domestic": 0
		}, {
			"label": "Lethbridge, Canada - Lethbridge Airport (YQL)",
			"value": "YQL",
			"domestic": 0
		}, {
			"label": "Moncton, Canada - Metropolitan Area (YQM)",
			"value": "YQM",
			"domestic": 0
		}, {
			"label": "Nakina, Canada - Nakina Airport (YQN)",
			"value": "YQN",
			"domestic": 0
		}, {
			"label": "Comox, Canada - Comox Airport (YQQ)",
			"value": "YQQ",
			"domestic": 0
		}, {
			"label": "Regina, Canada - Regina Airport (YQR)",
			"value": "YQR",
			"domestic": 0
		}, {
			"label": "Thunder Bay, Canada - Thunder Bay Airport (YQT)",
			"value": "YQT",
			"domestic": 0
		}, {
			"label": "Grande Prairie, Canada - Grande Prairie Airport (YQU)",
			"value": "YQU",
			"domestic": 0
		}, {
			"label": "Yorkton, Canada - Yorkton (YQV)",
			"value": "YQV",
			"domestic": 0
		}, {
			"label": "North Battleford, Canada - North Battleford (YQW)",
			"value": "YQW",
			"domestic": 0
		}, {
			"label": "Gander, Canada - Gander Airport (YQX)",
			"value": "YQX",
			"domestic": 0
		}, {
			"label": "Sydney, Canada - Sydney Airport (YQY)",
			"value": "YQY",
			"domestic": 0
		}, {
			"label": "Quesnel, Canada - Quesnel (YQZ)",
			"value": "YQZ",
			"domestic": 0
		}, {
			"label": "Rae Lakes, Canada - Rae Lakes Airport (YRA)",
			"value": "YRA",
			"domestic": 0
		}, {
			"label": "Resolute, Canada - Resolute Bay (YRB)",
			"value": "YRB",
			"domestic": 0
		}, {
			"label": "Cartwright, Canada - Cartwright Airport (YRF)",
			"value": "YRF",
			"domestic": 0
		}, {
			"label": "Rigolet, Canada - Rigolet Airport (YRG)",
			"value": "YRG",
			"domestic": 0
		}, {
			"label": "Roberval, Canada - Roberval Mncpl (YRJ)",
			"value": "YRJ",
			"domestic": 0
		}, {
			"label": "Red Lake, Canada - Red Lake Airport (YRL)",
			"value": "YRL",
			"domestic": 0
		}, {
			"label": "Red Sucker Lake, Canada - Red Sucker Lake Airport (YRS)",
			"value": "YRS",
			"domestic": 0
		}, {
			"label": "Rankin Inlet, Canada - Rankin Inlet (YRT)",
			"value": "YRT",
			"domestic": 0
		}, {
			"label": "Revelstoke, Canada - Revelstoke (YRV)",
			"value": "YRV",
			"domestic": 0
		}, {
			"label": "Sudbury, Canada - Sudbury Airport (YSB)",
			"value": "YSB",
			"domestic": 0
		}, {
			"label": "Sherbrooke, Canada - Sherbrooke (YSC)",
			"value": "YSC",
			"domestic": 0
		}, {
			"label": "Stony Rapids, Canada - Stony Rapids (YSF)",
			"value": "YSF",
			"domestic": 0
		}, {
			"label": "Lutselk\'e, Canada - Lutselke Airport (YSG)",
			"value": "YSG",
			"domestic": 0
		}, {
			"label": "Saint John, Canada - Saint John Airport (YSJ)",
			"value": "YSJ",
			"domestic": 0
		}, {
			"label": "Sanikiluaq, Canada - Sanikiluaq (YSK)",
			"value": "YSK",
			"domestic": 0
		}, {
			"label": "St. Leonard, Canada - Edmunston Airport (YSL)",
			"value": "YSL",
			"domestic": 0
		}, {
			"label": "Ft Smith, Canada - Fort Smith Airport (YSM)",
			"value": "YSM",
			"domestic": 0
		}, {
			"label": "Salmon Arm, Canada - Salmon Arm Mncpl (YSN)",
			"value": "YSN",
			"domestic": 0
		}, {
			"label": "Postville, Canada - Postville Airport (YSO)",
			"value": "YSO",
			"domestic": 0
		}, {
			"label": "Marathon, Canada - Marathon Mncpl (YSP)",
			"value": "YSP",
			"domestic": 0
		}, {
			"label": "Nanisivik, Canada - Nanisivik Airport (YSR)",
			"value": "YSR",
			"domestic": 0
		}, {
			"label": "Sainte Therese Point, Canada - Ste. Therese Point Airport (YST)",
			"value": "YST",
			"domestic": 0
		}, {
			"label": "Summerside, Canada - Summerside Mncpl (YSU)",
			"value": "YSU",
			"domestic": 0
		}, {
			"label": "Sachs Harbour, Canada - Sachs Harbour Airport (YSY)",
			"value": "YSY",
			"domestic": 0
		}, {
			"label": "Pembroke, Canada - Pembroke (YTA)",
			"value": "YTA",
			"domestic": 0
		}, {
			"label": "Cape Dorset, Canada - Cape Dorset Mncpl (YTE)",
			"value": "YTE",
			"domestic": 0
		}, {
			"label": "Alma, Canada - Alma Mncpl (YTF)",
			"value": "YTF",
			"domestic": 0
		}, {
			"label": "Thompson, Canada - Thompson Airport (YTH)",
			"value": "YTH",
			"domestic": 0
		}, {
			"label": "Terrace Bay, Canada - Terrace Bay (YTJ)",
			"value": "YTJ",
			"domestic": 0
		}, {
			"label": "Big Trout, Canada - Big Trout Airport (YTL)",
			"value": "YTL",
			"domestic": 0
		}, {
			"label": "Mont-Tremblant, Canada - La Macaza (YTM)",
			"value": "YTM",
			"domestic": 0
		}, {
			"label": "Toronto, Canada - All Airports (YTO)",
			"value": "YTO",
			"domestic": 0
		}, {
			"label": "Tasiujuaq, Canada - Tasiujuaq Airport (YTQ)",
			"value": "YTQ",
			"domestic": 0
		}, {
			"label": "Trenton, Canada - Trenton Air Base (YTR)",
			"value": "YTR",
			"domestic": 0
		}, {
			"label": "Timmins, Canada - Timmins Airport (YTS)",
			"value": "YTS",
			"domestic": 0
		}, {
			"label": "Toronto, Canada - City Centre (YTZ)",
			"value": "YTZ",
			"domestic": 0
		}, {
			"label": "Tuktoyaktuk, Canada - Tuktoyaktuk (YUB)",
			"value": "YUB",
			"domestic": 0
		}, {
			"label": "Umiujag, Canada - Umiujag Airport (YUD)",
			"value": "YUD",
			"domestic": 0
		}, {
			"label": "Montreal, Canada - Montreal (YUL)",
			"value": "YUL",
			"domestic": 0
		}, {
			"label": "Yuma, United States - Yuma International Airport (YUM)",
			"value": "YUM",
			"domestic": 1
		}, {
			"label": "Repulse Bay, Canada - Repulse Bay Airport (YUT)",
			"value": "YUT",
			"domestic": 0
		}, {
			"label": "Hall Beach, Canada - Hall Beach Airport (YUX)",
			"value": "YUX",
			"domestic": 0
		}, {
			"label": "Rouyn, Canada - Rouyn (YUY)",
			"value": "YUY",
			"domestic": 0
		}, {
			"label": "Moroni, Comoros - Iconi Airport (YVA)",
			"value": "YVA",
			"domestic": 0
		}, {
			"label": "Bonaventure, Canada - Bonaventure Mncpl (YVB)",
			"value": "YVB",
			"domestic": 0
		}, {
			"label": "La Ronge, Canada - La Ronge (YVC)",
			"value": "YVC",
			"domestic": 0
		}, {
			"label": "Vernon, Canada - Vernon Mncpl (YVE)",
			"value": "YVE",
			"domestic": 0
		}, {
			"label": "Broughton Island, Canada - Broughton Island Airport (YVM)",
			"value": "YVM",
			"domestic": 0
		}, {
			"label": "Val-d\'Or, Canada - Val DOr Airport (YVO)",
			"value": "YVO",
			"domestic": 0
		}, {
			"label": "Kuujjuaq, Canada - Kuujjuaq Airport (YVP)",
			"value": "YVP",
			"domestic": 0
		}, {
			"label": "Norman Wells, Canada - Norman Wells (YVQ)",
			"value": "YVQ",
			"domestic": 0
		}, {
			"label": "Vancouver, Canada - Vancouver (YVR)",
			"value": "YVR",
			"domestic": 0
		}, {
			"label": "Buffalo Narrows, Canada - Buffalo Narrows Airport (YVT)",
			"value": "YVT",
			"domestic": 0
		}, {
			"label": "Deer Lake, ON, Canada - Deer Lake Airport (YVZ)",
			"value": "YVZ",
			"domestic": 0
		}, {
			"label": "Kangiqsujuaq, Canada - Kangiqsujuaq Airport (YWB)",
			"value": "YWB",
			"domestic": 0
		}, {
			"label": "Winnipeg, Canada - Winnipeg (YWG)",
			"value": "YWG",
			"domestic": 0
		}, {
			"label": "Victoria, Canada - Inner Harbour (YWH)",
			"value": "YWH",
			"domestic": 0
		}, {
			"label": "Deline, Canada - Deline Airport (YWJ)",
			"value": "YWJ",
			"domestic": 0
		}, {
			"label": "Wabush, Canada - Wabush Airport (YWK)",
			"value": "YWK",
			"domestic": 0
		}, {
			"label": "Williams Lake, Canada - Williams Lake (YWL)",
			"value": "YWL",
			"domestic": 0
		}, {
			"label": "Williams Harbour, Canada - Williams Harbour Airport (YWM)",
			"value": "YWM",
			"domestic": 0
		}, {
			"label": "Webequie, Canada - Webequie Airport (YWP)",
			"value": "YWP",
			"domestic": 0
		}, {
			"label": "Whistler, Canada - Whistler Mncpl (YWS)",
			"value": "YWS",
			"domestic": 0
		}, {
			"label": "Cranbrook, Canada - Cranbrook Airport (YXC)",
			"value": "YXC",
			"domestic": 0
		}, {
			"label": "Edmonton, Canada - Edmonton City Centre (YXD)",
			"value": "YXD",
			"domestic": 0
		}, {
			"label": "Saskatoon, Canada - Saskatoon Airport (YXE)",
			"value": "YXE",
			"domestic": 0
		}, {
			"label": "Medicine Hat, Canada - Medicine Hat Airport (YXH)",
			"value": "YXH",
			"domestic": 0
		}, {
			"label": "Ft St. John, Canada - Fort St. John Airport (YXJ)",
			"value": "YXJ",
			"domestic": 0
		}, {
			"label": "Rimouski, Canada - Rimouski Mncpl (YXK)",
			"value": "YXK",
			"domestic": 0
		}, {
			"label": "Sioux Lookout, Canada - Sioux Lookout Mncpl (YXL)",
			"value": "YXL",
			"domestic": 0
		}, {
			"label": "Whale Cove, Canada - Whale Cove Airport (YXN)",
			"value": "YXN",
			"domestic": 0
		}, {
			"label": "Pangnirtung, Canada - Pangnirtung Airport (YXP)",
			"value": "YXP",
			"domestic": 0
		}, {
			"label": "Prince George, Canada - Prince George Airport (YXS)",
			"value": "YXS",
			"domestic": 0
		}, {
			"label": "Terrace, Canada - Terrace Airport (YXT)",
			"value": "YXT",
			"domestic": 0
		}, {
			"label": "London, Canada - Metropolitan Area Airport (YXU)",
			"value": "YXU",
			"domestic": 0
		}, {
			"label": "Abbotsford, Canada - Abbotsford Airport (YXX)",
			"value": "YXX",
			"domestic": 0
		}, {
			"label": "Whitehorse, Canada - Whitehorse Airport (YXY)",
			"value": "YXY",
			"domestic": 0
		}, {
			"label": "Wawa, Canada - Wawa Mncpl (YXZ)",
			"value": "YXZ",
			"domestic": 0
		}, {
			"label": "North Bay, Canada - North Bay Airport (YYB)",
			"value": "YYB",
			"domestic": 0
		}, {
			"label": "Calgary, Canada - Calgary (YYC)",
			"value": "YYC",
			"domestic": 0
		}, {
			"label": "Smithers, Canada - Smithers Airport (YYD)",
			"value": "YYD",
			"domestic": 0
		}, {
			"label": "Ft Nelson, Canada - Fort Nelson Airport (YYE)",
			"value": "YYE",
			"domestic": 0
		}, {
			"label": "Penticton, Canada - Penticton Airport (YYF)",
			"value": "YYF",
			"domestic": 0
		}, {
			"label": "Charlottetown, Canada - Charlottetown Airport (YYG)",
			"value": "YYG",
			"domestic": 0
		}, {
			"label": "Taloyoak, Canada - Taloyoak Airport (YYH)",
			"value": "YYH",
			"domestic": 0
		}, {
			"label": "Victoria, Canada - Victoria (YYJ)",
			"value": "YYJ",
			"domestic": 0
		}, {
			"label": "Lynn Lake, Canada - Lynn Lake (YYL)",
			"value": "YYL",
			"domestic": 0
		}, {
			"label": "Swift Current, Canada - Swift Current Mncpl (YYN)",
			"value": "YYN",
			"domestic": 0
		}, {
			"label": "Churchill, Canada - Churchill (YYQ)",
			"value": "YYQ",
			"domestic": 0
		}, {
			"label": "Goose Bay, Canada - Goose Bay Airport (YYR)",
			"value": "YYR",
			"domestic": 0
		}, {
			"label": "St. John\'s, Canada - St. Johns Airport (YYT)",
			"value": "YYT",
			"domestic": 0
		}, {
			"label": "Kapuskasing, Canada - Kapuskasing (YYU)",
			"value": "YYU",
			"domestic": 0
		}, {
			"label": "Mont-Joli, Canada - Mont Joli Airport (YYY)",
			"value": "YYY",
			"domestic": 0
		}, {
			"label": "Toronto, Canada - Pearson (YYZ)",
			"value": "YYZ",
			"domestic": 0
		}, {
			"label": "Gore Bay, Canada - Gore Bay Mncpl (YZE)",
			"value": "YZE",
			"domestic": 0
		}, {
			"label": "Yellowknife, Canada - Yellowknife Airport (YZF)",
			"value": "YZF",
			"domestic": 0
		}, {
			"label": "Salluit, Canada - Salluit Airport (YZG)",
			"value": "YZG",
			"domestic": 0
		}, {
			"label": "Slave Lake, Canada - Slave Lake Mncpl (YZH)",
			"value": "YZH",
			"domestic": 0
		}, {
			"label": "Sandspit, Canada - Sandspit Airport (YZP)",
			"value": "YZP",
			"domestic": 0
		}, {
			"label": "Sarnia, Canada - Sarnia Airport (YZR)",
			"value": "YZR",
			"domestic": 0
		}, {
			"label": "Coral Harbour, Canada - Coral Harbour (YZS)",
			"value": "YZS",
			"domestic": 0
		}, {
			"label": "Port Hardy, Canada - Port Hardy (YZT)",
			"value": "YZT",
			"domestic": 0
		}, {
			"label": "Whitecourt, Canada - Whitecourt (YZU)",
			"value": "YZU",
			"domestic": 0
		}, {
			"label": "Sept-Iles, Canada - Sept-Iles Airport (YZV)",
			"value": "YZV",
			"domestic": 0
		}, {
			"label": "Trail, Canada - Trail (YZZ)",
			"value": "YZZ",
			"domestic": 0
		}, {
			"label": "York Landing, Canada - York Landing Airport (ZAC)",
			"value": "ZAC",
			"domestic": 0
		}, {
			"label": "Zadar, Croatia - Zadar (ZAD)",
			"value": "ZAD",
			"domestic": 0
		}, {
			"label": "Zagreb, Croatia - Pleso (ZAG)",
			"value": "ZAG",
			"domestic": 0
		}, {
			"label": "Zahedan, Iran - Zahedan Airport (ZAH)",
			"value": "ZAH",
			"domestic": 0
		}, {
			"label": "Valdivia, Chile and Easter Island - Pichoy (ZAL)",
			"value": "ZAL",
			"domestic": 0
		}, {
			"label": "Zamboanga, Philippines - Zamboanga (ZAM)",
			"value": "ZAM",
			"domestic": 0
		}, {
			"label": "Zhaotong, China - Zhaotong Airport (ZAT)",
			"value": "ZAT",
			"domestic": 0
		}, {
			"label": "Zaragoza, Spain - Zaragoza (ZAZ)",
			"value": "ZAZ",
			"domestic": 0
		}, {
			"label": "Bathurst, Canada - Bathurst Airport (ZBF)",
			"value": "ZBF",
			"domestic": 0
		}, {
			"label": "Biloela, Australia - Biloela Airport (ZBL)",
			"value": "ZBL",
			"domestic": 0
		}, {
			"label": "Bromont, Canada - Regional (ZBM)",
			"value": "ZBM",
			"domestic": 0
		}, {
			"label": "Chah-Bahar, Iran - Chah-Bahar Airport (ZBR)",
			"value": "ZBR",
			"domestic": 0
		}, {
			"label": "Sayaboury, Laos - Sayaboury Airport (ZBY)",
			"value": "ZBY",
			"domestic": 0
		}, {
			"label": "Zacatecas, Mexico - La Calera Airport (ZCL)",
			"value": "ZCL",
			"domestic": 0
		}, {
			"label": "Temuco, Chile and Easter Island - Temuco (ZCO)",
			"value": "ZCO",
			"domestic": 0
		}, {
			"label": "Garmisch-Partenkirchen, Germany - Railway Station (ZEI)",
			"value": "ZEI",
			"domestic": 0
		}, {
			"label": "Bella Bella, Canada - Bella Bella Airport (ZEL)",
			"value": "ZEL",
			"domestic": 0
		}, {
			"label": "East Main, Canada - East Main Airport (ZEM)",
			"value": "ZEM",
			"domestic": 0
		}, {
			"label": "Goettingen, Germany - Railway Station (ZEU)",
			"value": "ZEU",
			"domestic": 0
		}, {
			"label": "Fond Du Lac, Canada - Fond Du Lac Airport (ZFD)",
			"value": "ZFD",
			"domestic": 0
		}, {
			"label": "Fort McPherson, Canada - Fort Mcpherson Airport (ZFM)",
			"value": "ZFM",
			"domestic": 0
		}, {
			"label": "Tulita/Fort Norman, Canada - Tulita Airport (ZFN)",
			"value": "ZFN",
			"domestic": 0
		}, {
			"label": "Grand Forks, Canada - Grand Forks Airport (ZGF)",
			"value": "ZGF",
			"domestic": 0
		}, {
			"label": "Gods River, Canada - Gods River Airport (ZGI)",
			"value": "ZGI",
			"domestic": 0
		}, {
			"label": "Gethsemani, Canada - Gethsemani Airport (ZGS)",
			"value": "ZGS",
			"domestic": 0
		}, {
			"label": "Gaua, Vanuatu - Gaua Airport (ZGU)",
			"value": "ZGU",
			"domestic": 0
		}, {
			"label": "Kuching Port, Malaysia - Kuching Port (ZGY)",
			"value": "ZGY",
			"domestic": 0
		}, {
			"label": "Zhanjiang, China - Zhangjiang (ZHA)",
			"value": "ZHA",
			"domestic": 0
		}, {
			"label": "Ziguinchor, Senegal - Ziguinchor Airport (ZIG)",
			"value": "ZIG",
			"domestic": 0
		}, {
			"label": "Ixtapa, Mexico - Extapa/Zihuatanejo Internacional Airport (ZIH)",
			"value": "ZIH",
			"domestic": 0
		}, {
			"label": "Kaschechewan, Canada - Kaschechewan Airport (ZKE)",
			"value": "ZKE",
			"domestic": 0
		}, {
			"label": "Kegaska, Canada - Kegaska Airport (ZKG)",
			"value": "ZKG",
			"domestic": 0
		}, {
			"label": "Manzanillo, Mexico - Manzanillo Airport (ZLO)",
			"value": "ZLO",
			"domestic": 0
		}, {
			"label": "La Tabatiere, Canada - La Tabatiere Airport (ZLT)",
			"value": "ZLT",
			"domestic": 0
		}, {
			"label": "Masset, Canada - Masset Airport (ZMT)",
			"value": "ZMT",
			"domestic": 0
		}, {
			"label": "Nanaimo, Canada - Harbor (ZNA)",
			"value": "ZNA",
			"domestic": 0
		}, {
			"label": "Newman, Australia - Newman (ZNE)",
			"value": "ZNE",
			"domestic": 0
		}, {
			"label": "Ingolstadt, Germany - Railway Station (ZNQ)",
			"value": "ZNQ",
			"domestic": 0
		}, {
			"label": "Zanzibar, Tanzania - Zanzibar Kisauni (ZNZ)",
			"value": "ZNZ",
			"domestic": 0
		}, {
			"label": "Osorno, Chile and Easter Island - Osorno (ZOS)",
			"value": "ZOS",
			"domestic": 0
		}, {
			"label": "Sachigo Lake, Canada - Sachigo Lake Airport (ZPB)",
			"value": "ZPB",
			"domestic": 0
		}, {
			"label": "Pucon, Chile and Easter Island - Pucon (ZPC)",
			"value": "ZPC",
			"domestic": 0
		}, {
			"label": "Regensburg, Germany - Regensburg Railway Service (ZPM)",
			"value": "ZPM",
			"domestic": 0
		}, {
			"label": "Queenstown, New Zealand - Frankton (ZQN)",
			"value": "ZQN",
			"domestic": 0
		}, {
			"label": "Saarbruecken, Germany - Zweibruecken Off-Line Point (ZQW)",
			"value": "ZQW",
			"domestic": 0
		}, {
			"label": "Zurich, Switzerland - Zurich Airport (ZRH)",
			"value": "ZRH",
			"domestic": 0
		}, {
			"label": "Round Lake, Canada - Round Lake Airport (ZRJ)",
			"value": "ZRJ",
			"domestic": 0
		}, {
			"label": "San Salvador Island, Bahamas - San Salvador (ZSA)",
			"value": "ZSA",
			"domestic": 0
		}, {
			"label": "St Pierre, Reunion - St. Pierre de la Reunion Airport (ZSE)",
			"value": "ZSE",
			"domestic": 0
		}, {
			"label": "Sandy Lake, Canada - Sandy Lake Airport (ZSJ)",
			"value": "ZSJ",
			"domestic": 0
		}, {
			"label": "Tete-A-La Baleine, Canada - Tete-A-La Baleine Airport (ZTB)",
			"value": "ZTB",
			"domestic": 0
		}, {
			"label": "Zakinthos, Greece - Zakinthos (ZTH)",
			"value": "ZTH",
			"domestic": 0
		}, {
			"label": "Shamattawa, Canada - Shamattawa Airport (ZTM)",
			"value": "ZTM",
			"domestic": 0
		}, {
			"label": "Zhuhai, China - Zhuhai (ZUH)",
			"value": "ZUH",
			"domestic": 0
		}, {
			"label": "Churchill Falls, Canada - Churchill Falls Airport (ZUM)",
			"value": "ZUM",
			"domestic": 0
		}, {
			"label": "Wollaston Lake, Canada - Wollaston Lake Airport (ZWL)",
			"value": "ZWL",
			"domestic": 0
		}, {
			"label": "Sylhet, Bangladesh - Civil Airport (ZYL)",
			"value": "ZYL",
			"domestic": 0
		}, {
			"label": "Mzuzu, Malawi - Mzuzu (ZZU)",
			"value": "ZZU",
			"domestic": 0
		}, {
			"label": "Zanesville, United States - Zanesville Mncpl (ZZV)",
			"value": "ZZV",
			"domestic": 1
		}, {
			"label": "Dubai, United Arab Emirates - Al Maktoum International (DWC)",
			"value": "DWC",
			"domestic": 0
		}
	]


	return {
		run: run
	}

}());
