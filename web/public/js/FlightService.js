//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('FlightService', [])

	.service('FlightService', function($http, $q, CABIN_CLASSES_CODE, $cookies, $timeout, $rootScope, localStorageService, TEST, PHONE_COUNTRY_CODES){

		var getResultsPromise;
		this.getResults = function(search){
			if(getResultsPromise) return getResultsPromise;

			var defer = $q.defer();

			search = search.replace(/(&?\w+=((?=$)|(?=&)))/g,'');

			localStorageService.set('flightSearchQuery', search)

			$.ajax({
				type: 'GET',
				url: (TEST) ? App.uploadsDir + '/air.json' : App.baseAPIUrl + '/air' + search + '&currency=' + App.selectedCurrency,
				dataType: 'json',
				xhrFields: {
					withCredentials: true
				},
				xhr: function () {
					var xhr = $.ajaxSettings.xhr();
					xhr.onprogress = function(e){
						if (e.lengthComputable) {
							defer.notify(e.loaded / e.total);
						}
					}
					return xhr;
				},
			}).then(function(response, textStatus){
				if(textStatus != 'nocontent'){
					defer.resolve(response)
				} else {
					defer.resolve({
						fareGroup: []
					})
				}

			}, function(response){
				defer.reject(response.responseJSON)
				$rootScope.errorHandler(response.responseJSON)
			});


			return getResultsPromise = defer.promise;
		}

		this.getResultsMatrix = function(){
			var defer = $q.defer();

			getResultsPromise.then(function(data){
				var matrix = {}

				_.each(data.fareGroup, function(fare){
					var carrier = fare.flightGroup[0][0].segments[0].carrier;
					var stops = fare.flightGroup[0][0].segments.length - 1;

					matrix[carrier.code] = matrix[carrier.code] || {}

					if(matrix[carrier.code][stops]){
						matrix[carrier.code][stops].totalPrice = Math.min(matrix[carrier.code][stops].totalPrice, fare.totalPrice);
					} else {
						matrix[carrier.code][stops] = {
							totalPrice: fare.totalPrice,
							carrier: carrier
						}
					}

					matrix[carrier.code][stops].interesting = Math.sin((stops + 0.2) / matrix[carrier.code][stops].totalPrice);
				})


				var carriers = [];
				var stopsArr = [];

				_.each(matrix, function(byStops){
					_.each(byStops, function(matrixItem, stops){
						if(!_.find(carriers, function(carrier){ return carrier.code == matrixItem.carrier.code})){
							carriers.push(matrixItem.carrier)
						}

						if(_.indexOf(stopsArr, stops) == -1){
							stopsArr.push(stops);
						}

					})
				})

				defer.resolve({
					carriers: carriers,
					stops: stopsArr,
					matrix: matrix
				});
			})

			return defer.promise
		}


		this.filter = function(filters){
			var defer = $q.defer();

			getResultsPromise.then(function(data){

				defer.resolve(_.filter(data.fareGroup, function(fareGroup){
					var validCabinClass = false;
					var validStops = false;
					var validDeparture = false;
					var validDuration = false;
					var validPrice = true;
					var validCarrier = false;

					//Price
					if(fareGroup.totalPrice < filters.price.from || fareGroup.totalPrice > filters.price.to) {
						validPrice = false;
					}

					// Departure
					var dateFormatFix = (fareGroup.flightGroup[0][0].segments[0].departure).replace(' ', 'T')
					var departure = +new Date(dateFormatFix);
					if(departure >= filters.departure.from && departure <= filters.departure.to){
						validDeparture = true;
					}

					// Duration
					var duration = parseInt(fareGroup.flightGroup[0][0].segments[0].duration.replace(':', ''), 10);
					if(duration >= filters.duration.from && duration <= filters.duration.to){
						validDuration = true;
					}

					// console.group()
					// console.log(moment(filters.departure.from).format(), moment(filters.departure.to).format())
					// console.log(fareGroup.flightGroup[0][0].segments[0].departure, departure >- filters.departure.from && departure <= filters.departure.to)
					// console.groupEnd()


					_.each(fareGroup.flightGroup, function(flightDestination){
						_.each(flightDestination, function(flight){

							//Cabin class
							if(_.indexOf(filters.classes, flight.segments[0].cabinClass.code) != -1) {
								validCabinClass = true;
							}

							//Stops
							var stops = flight.segments.length;

							if(stops == 1) {
								if(_.indexOf(filters.stops, 'Direct') != -1) {
									validStops = true;
								}
							}

							if(stops == 2) {
								if(_.indexOf(filters.stops, '1') != -1) {
									validStops = true;
								}
							}

							if(stops > 2) {
								if(_.indexOf(filters.stops, '2+') != -1) {
									validStops = true;
								}
							}


							// Carrier
							_.each(filters.airlines, function(airline){
								if(airline.code == flight.segments[0].carrier.code) {
									validCarrier = true;
								}
							})


						})
					})

					return (validPrice && validCabinClass && validStops && validDeparture && validDuration && validCarrier)

				}));
			})

			return defer.promise;
		}

		var getAirportsCodePromise;
		this.getAirportsCode = function(){
			if(getAirportsCodePromise) return getAirportsCodePromise;

			var defer = $q.defer();

			$http.get(App.uploadsDir + '/airports.json').then(function(response){
				defer.resolve(response.data);
			})

			return getAirportsCodePromise = defer.promise;
		}


		//Modal
		var selectedFareGroup;
		var selectedFlights;
		this.setSelectedFareGroup = function(fareGroup, selected){
			selectedFareGroup = fareGroup;
			selectedFlights = selected;
		}

		this.getSelectedFareGroup = function(){
			return selectedFareGroup;
		}
		this.getSelectedFlights = function(){
			return selectedFlights;
		}
		//



		// this.getTemporaryFlight = function(){
		// 	return JSON.parse(localStorage.getItem('flight'));
		// }

		// var getSearchParams = function(){
		// 	return JSON.parse(localStorage.getItem('search'));
		// }

		var getPassengersPromise;
		this.getPassengers = function(){
			if(getPassengersPromise) return getPassengersPromise;

			var defer = $q.defer();
			var passengers = localStorageService.get('passengers');

			defer.resolve(passengers);

			return getPassengersPromise = defer.promise;
		}


		var getCheckoutPromise;
		this.getCheckout = function(){
			if(getCheckoutPromise) return getCheckoutPromise;

			var defer = $q.defer();

			var selectedFlights = localStorageService.get('selectedFlights');
			var selectedFareID = localStorageService.get('selectedFareID');
			var pricing = localStorageService.get('pricing');

			if(pricing) {
				defer.resolve(pricing)
			} else {
				$http.get((TEST) ? App.uploadsDir + '/air_checkout.json' : App.baseAPIUrl + '/air/checkout?fltGrpId=' + encodeURIComponent(selectedFareID) + '&fltIndex=' + _.pluck(selectedFlights, 'flightIndex').join(','), {withCredentials: true}).then(function(response){
					localStorageService.set('pricing', response.data)
					defer.resolve(response.data)
				}, function(response){
					defer.reject(response.data)
					$rootScope.errorHandler(response.data)
				})
			}



			// defer.resolve({flight: JSON.parse(localStorage.getItem('flight')), passengers: [{type: 'ADT'}]})

			return getCheckoutPromise = defer.promise;
		}

		this.bookSeats = function(flights, passengers){

			var passengerDetail = {}

			var request = {
				selectedSeats: []
			}

			_.each(flights, function(flight, flightIndex){
				var selectedSeats = {
					flightKey: [
						flight.flightGroup.departure.cityCode,
						moment(flight.flightGroup.departure.date).format('YYYY-MM-DD'),
						flight.flightGroup.arrival.cityCode, moment(flight.flightGroup.departure.date).format('YYYY-MM-DD'),
						flight.flightGroup.carrier.airlineCode + flight.flightGroup.carrier.flightNumber.value
					].join('|'),

					passengers: []
				};

				_.each(passengers, function(passenger){
					if(flightIndex in passenger.selectedSeat){
						selectedSeats.passengers.push({
							id: passenger.id,
							seat: passenger.selectedSeat[flightIndex]
						})
					}
				})

				if(selectedSeats.passengers.length) request.selectedSeats.push(selectedSeats);
			})

			$rootScope.$emit('updateSeats', request.selectedSeats)
			return $http.post(App.baseAPIUrl + '/air/book_seat', request, {withCredentials: true});
		}


		this.bookInsurance = function(passengers){
			var defer = $q.defer();

			$timeout(function(){
				defer.resolve();
			}, 1000)

			return defer.promise;
		}


		this.setPersonalDetails = function(passengers, user){

			passengers = angular.copy(passengers);

			_.each(passengers, function(passenger){
				var countryObj = _.find(PHONE_COUNTRY_CODES, function(obj){return obj.phoneCode == passenger.personalDetails.phoneCode})

				passenger.personalDetails.travellerType = passenger.type;
				passenger.personalDetails.gender = (passenger.personalDetails.title == 'MR') ? 'Male' : 'Female'
				// passenger.personalDetails.phoneNumber = "+" + passenger.personalDetails.phoneCode + passenger.personalDetails.phoneNumber

				if(countryObj){
					passenger.personalDetails.country = countryObj.countryCode;
				}

			})

			// } else {
			// 	var passenger = passengers[0];
			// 	passenger.personalDetails = passenger.personalDetails || {};

			// 	passenger.personalDetails.title = user.title;
			// 	passenger.personalDetails.travellerType = passenger.type;
			// 	passenger.personalDetails.firstName = user.first_name;
			// 	passenger.personalDetails.lastName = user.last_name;
			// 	passenger.personalDetails.gender = !!(user.gender) ? 'Male' : 'Female'
			// 	passenger.personalDetails.frequentFlyer = user.frequent_flyer;
			// 	passenger.personalDetails.phoneNumber = "+" + user.phone_code + user.phone_number;
			// 	passenger.personalDetails.passportCountry = user.passport_country;
			// 	passenger.personalDetails.passportNumber = user.passport_number;
			// 	passenger.personalDetails.passportExpiryDate = user.passport_expiration_y +'-' + user.passport_expiration_m + '-' + user.passport_expiration_d;

			// }

			var data = {
				bookingContactInformation: {
					isUser: !!(user.id),
					firstName: user.first_name || passengers[0].personalDetails.firstName,
					lastName: user.last_name || passengers[0].personalDetails.lastName,
					title: user.title || passengers[0].personalDetails.title,
					// address1: user.address_address1,
					// address2: user.address_address2,
					// city: user.address_city,
					// zip: user.address_zipcode,
					country: user.address_country || passengers[0].personalDetails.country,
					telephone: (user.phone_number) ? (user.phone_number) : passengers[0].personalDetails.phoneNumber,
					userID: user.id,
					email: user.email || passengers[0].personalDetails.email,
					mobile: (user.mobile_number) ? (user.mobile_number) : passengers[0].personalDetails.phoneNumber,
				},
				passengers: passengers
			}

			if(TEST){
				return $http.get(App.uploadsDir + '/personal_details.json');
			} else {
				return $http.post(App.baseAPIUrl + '/air/checkout/personal_details', data, {withCredentials: true});
			}
		}

		var PNR;
		this.setPNR = function(pnr){
			PNR = pnr;
		}

		this.getPNR = function(){
			return PNR;
		}

		// this.setCoupon = function(code){
		// 	var defer = $q.defer();

		// 	$http.post(App.baseAPIUrl + '/air/checkout/discount', {type: 'coupon', data: code}, {withCredentials: true}).then(function(response){

		// 		var coupon = _.find(response.data.pricing, function(pricing){
		// 			if(pricing.type == 'CPN'){
		// 				return true;
		// 			}

		// 		});

		// 		if(coupon){
		// 			response.data.couponValue = coupon.value * -1;
		// 			defer.resolve(response.data);
		// 		} else {
		// 			defer.reject();
		// 		}

		// 	}, function(){
		// 		defer.reject();
		// 	});

		// 	return defer.promise;
		// }

		// this.removeCoupon = function(){
		// 	return $http['delete'](App.baseAPIUrl + '/air/checkout/discount', {type: 'coupon'}, {withCredentials: true})
		// }


		this.setTwitterDiscount = function(){
			var defer = $q.defer();

			$http.post(App.baseAPIUrl + '/air/checkout/discount', {type: 'twitter'}, {withCredentials: true}).then(function(response){
				defer.resolve(response.data);
			}, function(){
				defer.reject();
			});

			return defer.promise;
		}

		this.removeTwitterDiscount = function(){
			return $http['delete'](App.baseAPIUrl + '/air/checkout/discount', {type: 'twitter'}, {withCredentials: true})
		}


		var getPaymentDetailsPromise;
		this.getPaymentDetails = function(){
			if(getPaymentDetailsPromise) return getPaymentDetailsPromise;

			var defer = $q.defer();

			defer.resolve({}); //just to share with othe controllers

			return getPaymentDetailsPromise = defer.promise;

		}



		var getSeatMapPromise;
		this.getSeatMap = function(){
			if(getSeatMapPromise) return getSeatMapPromise;

			var defer = $q.defer();

			var selectedFlights = localStorageService.get('selectedFlights');
			var selectedFareID = localStorageService.get('selectedFareID');


			$http.get((TEST) ? App.uploadsDir + '/seatmap.json' : App.baseAPIUrl + '/air/seatmap?fltGrpId=' + encodeURIComponent(selectedFareID) + '&fltIndex=' + _.pluck(selectedFlights, 'flightIndex').join(','), {withCredentials: true}).then(function(response){
				defer.resolve(response.data)
			}, function(response){
				defer.reject(response.data)
			})

			return getSeatMapPromise = defer.promise;
		}


		var bookPromise;
		this.book = function(){
			if(bookPromise) return bookPromise;

			var defer = $q.defer();

			$http.post(App.baseAPIUrl + '/air/book', {"sendAnyData": 34}, {withCredentials: true}).then(function(response){
				defer.resolve(response.data)
			}, function(response){
				defer.reject(response)
			})

			return bookPromise = defer.promise;
		}

		var ticketPromise;
		this.ticket = function(datatrans_response){
			if(ticketPromise) return ticketPromise;

			var defer = $q.defer();

			$http.post(App.baseAPIUrl + '/air/issue_ticket', datatrans_response, {withCredentials: true}).then(function(response){
				defer.resolve(response.data)
			}, function(response){
				defer.reject(response)
			})

			return ticketPromise = defer.promise;
		}


	})

