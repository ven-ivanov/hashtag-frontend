//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('UserMyAccountApp', ['ngRoute', 'UserMyAccount.Controllers', 'UserMyAccount.Directives', 'UserMyAccount.Services', 'UserMyAccount.Filters' ,'HashtagDirectives'])

	.config(function($interpolateProvider, $routeProvider) {
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');

		$routeProvider
			.when('/bookings', {
				templateUrl: App.baseUrl + '/user/bookings',
				controller: 'my-bookings'
			})

			.when('/personal_details', {
				templateUrl: App.baseUrl + '/user/personal_details',
				controller: 'personal-details'
			})

			.when('/settings', {
				templateUrl: App.baseUrl + '/user/settings',
				controller: 'settings'
			})

			.when('/rewards', {
				templateUrl: App.baseUrl + '/user/rewards',
				controller: 'rewards'
			})

			.when('/notifications', {
				templateUrl: App.baseUrl + '/user/notifications',
				controller: 'notifications'
			})

			.otherwise({
				redirectTo: '/bookings'
			})

	})

	.run(function($rootScope, $location){

		$rootScope.$on('$routeChangeSuccess', function(){
			$rootScope.currentRoute = $location.path();
		})

		$rootScope.modal = false;

		$rootScope.closeModal = function(e){
			e.preventDefault();
			$rootScope.modal = false;
		}

	})









//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('UserMyAccount.Controllers', [])

	.controller('my-bookings', function($scope, $q, Bookings) {

		$q.all([Bookings.getBookings(), Bookings.getAirportsCode()]).then(function(responses){
			$scope.bookings = responses[0].current;
			$scope.tab = 'current';
			$scope.airports = responses[1];
		});

		$scope.showDetails = function(e) {
			e.preventDefault();
			this.booking.visible = !(this.booking.visible);
		}

		$scope.setContent = function(e, tab) {
			e.preventDefault();

			Bookings.getBookings().then(function(response){
				$scope.bookings = response[tab]
				$scope.tab = tab;
			})

		}

	})

	.controller('personal-details', function($scope, $q, PersonalDetails, $timeout) {

		$scope.form_state = 'loading';

		$scope.$watch('user', function(user){
			if(!user) return;

			if(user.birthdate && user.birthdate !== '00-00-00'){
				var birthdate = moment(user.birthdate);
				user.birthdate_d = birthdate.format('DD')
				user.birthdate_m = birthdate.format('MM')
				user.birthdate_y = birthdate.format('YYYY')
			} else {
				user.birthdate_d = '';
				user.birthdate_m = '';
				user.birthdate_y = '';
			}

			if(user.passport_expiration){
				var passport_expiration = moment(user.passport_expiration);
				user.passport_expiration_d = passport_expiration.format('DD')
				user.passport_expiration_m = passport_expiration.format('MM')
				user.passport_expiration_y = passport_expiration.format('YYYY')
			} else {
				user.passport_expiration_d = null;
				user.passport_expiration_m = null;
				user.passport_expiration_y = null;
			}

			if(!user.title){
				user.title = 'MR'
			}
		})

		$scope.$watch('user', function(user){
			if($scope.form_state == 'saved' || $scope.form_state == 'error') {
				$scope.form_state = 'ready';
				$scope.error_message = null;
			}
		}, true)

		$q.all(PersonalDetails.getPersonalDetails()).then(function(response){
			$scope.form_state = 'ready';
			$scope.user = response;

		});

		$scope.save = function(e){

			$timeout(function(){
				var parsleyValidation = $(e.target).parsley('validate');
				if(parsleyValidation.validationResult && $scope.form_state == 'ready') {
					$scope.form_state = 'saving';

					PersonalDetails.setPersonalDetails($scope.user).then(function(response){
						$scope.form_state = 'saved';
					}, function(response){
						$scope.form_state = 'error';
						$scope.error_message = response.message;
					})
				}
			})

		}

	})

	.controller('settings', function($rootScope, $scope, Settings, $timeout) {
		$scope.user = App.user;

		$scope.$watch('form', function(form){
			if($scope.form_state == 'saved' || $scope.form_state == 'error' || !$scope.form_state) {
				$scope.form_state = 'ready';
				$scope.error_message = null;
			}
		}, true)

		$scope.save = function(e){

			$timeout(function(){
				var parsleyValidation = $(e.target).parsley('validate');
				if(parsleyValidation.validationResult && $scope.form_state == 'ready') {
					$scope.form_state = 'saving';

					console.log($scope.form);

					Settings.setNewPassword($scope.form).then(function(response){
						$scope.form_state = 'saved';
					}, function(response){
						$scope.form_state = 'error';
						$scope.error_message = response.message;
					})
				}
			})

		}

		$scope.openModalDelete = function(e){
			e.preventDefault();
			$rootScope.modal = 'modal-delete-account';
		}

	})

	.controller('rewards', function($rootScope, $scope, $q, Rewards, $filter) {

		$q.all(Rewards.getRewards()).then(function(response){

			response.historyReverse = false;
			response.couponsReverse = false;

			_.each(response['history'], function(reward){
				reward.earnedTimeStamp = +new Date(reward.earned)
			})

			_.each(response['coupons'], function(coupon){
				coupon.generatedTimeStamp = +new Date(coupon.generated)
			})

			$scope.rewards = response;

		});


		$scope.openModalCoupon = function(e){
			e.preventDefault();
			$rootScope.modal = 'modal-generate-coupon';
		}

		$scope.openModalTransfer = function(e){
			e.preventDefault();
			$rootScope.modal = 'modal-transfer-hashtags';
		}

	})

	.controller('notifications', function($scope, $q, Notifications) {

		$scope.$watch('notifications', function(notifications){
			if($scope.form_state == 'saved' || $scope.form_state == 'error' || !$scope.form_state) {
				$scope.form_state = 'ready';
				$scope.error_message = null;
			}
		}, true)


		$q.all(Notifications.getNotifications()).then(function(response){
			$scope.notifications = response;
		});

		$scope.save = function(e){

			if($scope.form_state == 'ready') {
				$scope.form_state = 'saving';

				Notifications.setNotifications($scope.notifications).then(function(response){
					$scope.form_state = 'saved';
				}, function(response){
					$scope.form_state = 'error';
					$scope.error_message = response.message;
				})
			}


		}
	})

	.controller('modal-generate-coupon', function($scope, $q) {
		console.log('modal generate coupon');
	})

	.controller('modal-transfer-hashtags', function($scope, $q) {
		console.log('modal transfer hashtags');
	})

	.controller('modal-delete-account', function($scope, $q, $http, $window) {
		console.log('modal delete account');
		// $window.location.assign(App.baseUrl);
		$scope.deleteAccount = function(e){
			e.preventDefault();

			$http.delete(App.baseUrl + '/delete_account').then(function() {
				$window.location.href = App.baseUrl;
			})

		};
	})



//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('UserMyAccount.Directives', [])
	.directive('internalTabs', function(){
		return {
			restrict: 'C',
			link: function(scope, elm, attrs){
				App.Modules['internal-tabs'].run();
			}

		}
	})

	.directive('parsleyValidate', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				$(elm).parsley();
			}

		}
	})

//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('UserMyAccount.Services', [])
	.service('Bookings', function($http, $q){

		var getBookingsPromise;
		this.getBookings = function() {
			if(getBookingsPromise) return getBookingsPromise;

			var defer = $q.defer();

			$http.get(App.uploadsDir + '/bookings.json').then(function(response){
				defer.resolve(response.data);
			})

			return getBookingsPromise = defer.promise;
		}


		var getAirportsCodePromise;
		this.getAirportsCode = function(){
			if(getAirportsCodePromise) return getAirportsCodePromise;

			var defer = $q.defer();

			$http.get(App.uploadsDir + '/airports.json').then(function(response){
				defer.resolve(response.data);
			})

			return getAirportsCodePromise = defer.promise;
		}

	})

	.service('PersonalDetails', function($http, $q){

		var getPersonalDetailsPromise;
		this.getPersonalDetails = function(){
			if(getPersonalDetailsPromise) return getPersonalDetailsPromise;

			var defer = $q.defer();

			$http.get('personal_details.json').then(function(response){
				defer.resolve(response.data);
			})

			return getPersonalDetailsPromise = defer.promise;
		}

		var setPersonalDetailsPromise;
		this.setPersonalDetails = function(user){
			if(setPersonalDetailsPromise) return setPersonalDetailsPromise;

			var defer = $q.defer();

			$http.put('personal_details.json', user).then(function(response){
				defer.resolve(response.data);
				setPersonalDetailsPromise = undefined;
			}, function(response){
				defer.reject(response.data);
				setPersonalDetailsPromise = undefined;
			})

			return setPersonalDetailsPromise = defer.promise;
		}

	})


	.service('Settings', function($http, $q){
		var setNewPasswordPromise;
		this.setNewPassword = function(data){
			if(setNewPasswordPromise) return setNewPasswordPromise;

			var defer = $q.defer();

			$http.put('settings.json', data).then(function(response){
				defer.resolve(response.data);
				setNewPasswordPromise = undefined;
			}, function(response){
				defer.reject(response.data);
				setNewPasswordPromise = undefined;
			})

			return setNewPasswordPromise = defer.promise;
		}
	})

	.service('Rewards', function($http, $q){

		var getRewardsPromise;
		this.getRewards = function(){
			if(getRewardsPromise) return getRewardsPromise;

			var defer = $q.defer();

			$http.get(App.uploadsDir + '/rewards.json').then(function(response){
				defer.resolve(response.data);
			})

			return getRewardsPromise = defer.promise;
		}

	})


	.service('Notifications', function($http, $q){

		var getNotificationsPromise;
		this.getNotifications = function(){
			if(getNotificationsPromise) return getNotificationsPromise;

			var defer = $q.defer();

			$http.get('notifications.json').then(function(response){
				defer.resolve(response.data);
			})

			return getNotificationsPromise = defer.promise;
		}


		var setNotificationsPromise;
		this.setNotifications = function(notifications){
			if(setNotificationsPromise) return setNotificationsPromise;

			var defer = $q.defer();

			$http.put('notifications.json', notifications).then(function(response){
				defer.resolve(response.data);
				setNotificationsPromise = undefined;
			}, function(response){
				defer.reject(response.data);
				setNotificationsPromise = undefined;
			})

			return setNotificationsPromise = defer.promise;
		}

	})







//  _____ _ _ _
// |  ___(_) | |_ ___ _ __ ___
// | |_  | | | __/ _ \ '__/ __|
// |  _| | | | ||  __/ |  \__ \
// |_|   |_|_|\__\___|_|  |___/

angular.module('UserMyAccount.Filters', [])
	.filter('formatDate', function(){
		return function(data, format){
			return moment(data).format(format);
		}
	})


	.filter('buttonStates', function(){
		return function(state, prefix){
			var states = {
				loading: 'Save ' + prefix,
				ready: 'Save ' + prefix,
				saving: 'Saving',
				saved: 'Saved',
				error: 'Save ' + prefix
			}

			return states[state];
		}
	})




