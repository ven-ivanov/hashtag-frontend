//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('HotelCheckout', ['HotelCheckout.Controllers', 'HotelCheckout.Services', 'HotelCheckout.Directives',
        'HotelService', 'HotelCheckout.Filters', 'HashtagControllers', 'HashtagFilters', 'ngCookies', 'LocalStorageModule', 'HashtagDirectives'])
	.config(function($interpolateProvider, $httpProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})
	.run(function($rootScope, State, HotelService, $locale, $location, localStorageService){
		$rootScope.state = State.getCurrentState();
		$rootScope.previousState = State.getPreviousState();

		$rootScope.backToPreviousState = function(e){
			e.preventDefault()

			if(State.getPreviousState()){
				State.backToPreviousState()
			} else {
				window.location = $(e.target).attr('href') + localStorageService.get('hotelSearch');
			}
		}

		$rootScope.modal = false;

		$rootScope.closeModal = function(e){
			e.preventDefault();
			$rootScope.modal = false;
		}

		State.onStateChange = function(){
			$rootScope.state = State.getCurrentState();
			$rootScope.previousState = State.getPreviousState();
		}

	})

	.constant('LOADING_MESSAGES', {
		FLIGHT_PRICING: {
			title: 'Loading your trip',
			message: 'This process can take a few seconds.'
		},

		PERSONAL_DETAILS: {
			title: 'Validating your personal details',
			message: 'Please wait...'
		},

		FLIGHT_OPTIONS: {
			title: 'Loading options for your flight',
			message: 'This process can take a few seconds.'
		},

		BOOKING: {
			title: 'Booking your flight',
			message: 'This process can take a few seconds.'
		},

		TICKETING: {
			title: 'Ticketing your flight',
			message: 'This process can take a few seconds.'
		},

		SEATS: {
			title: 'Booking your seats',
			message: 'This process can take a few seconds.'
		},

		INSURANCE: {
			title: 'Processing your request',
			message: 'This process can take a few seconds.'
		}

	})


//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('HotelCheckout.Controllers', [])

	.controller('summary', function($scope, $q, $rootScope, HotelService, localStorageService){
		$rootScope.loading = true;

		$scope.hotel = localStorageService.get('selectedHotel');
		$scope.room = localStorageService.get('selectedRoom');

		HotelService.getPricing().then(function(policies){
			$rootScope.loading = false;
			$rootScope.modal = 'hotel-policy';
			$rootScope.policies = policies;

			if(parseInt(policies.formattedTotalPrice) > 0)
				$scope.room.formattedTotalPrice = policies.formattedTotalPrice;
		})

		$scope.hotelPolicy = function(e){
			e.preventDefault();
		}
	})

	.controller('hotel-policy', function($scope, $q, $rootScope, LOADING_MESSAGES, $timeout, localStorageService, HotelService, State){
		$rootScope.$watch('policies', function(policies){
			$scope.policies = policies;
		})
	})

	.controller('personal-details', function($scope, $q, $rootScope, LOADING_MESSAGES, $timeout, localStorageService, HotelService, State){
		HotelService.getGuests().then(function(guests){
			$scope.guests = guests;
		})

		$scope.$watch('user', function(user){
			if(!user) return;

			HotelService.getGuests().then(function(guests){
				if(guests[0]){
					guests[0].firstName = user.first_name;
					guests[0].lastName = user.last_name;
					guests[0].email = user.email;
					guests[0].title = user.title;
					guests[0].age = ageCheck(user.birthdate);
				}
			})
		})

		$scope.user = App.user || {};

		$scope.check_for_duplicate_guests = function(e){
			results = []

			_.each($scope.guests, function(guest, index){
				guests_without_current = _.reject($scope.guests, { 
					'id' : guest.id
				});

				result = _.find(guests_without_current, { 
					'firstName' : guest.firstName,
					'lastName' : guest.lastName
				});

				if(result)
					results.push(result)
			})

			return results.length;
		}

		$scope.remove_special_char = function(scope, value, model){
			var desired = value.replace(/[^a-zA-Z ]/g, '')

			if(model == 'firstName'){
				scope.p.firstName = desired
				return scope.p.firstName
			} else if(model == 'lastName'){
				scope.p.lastName = desired
				return scope.p.lastName
			}
		}

		$scope.submitPersonalDetails = function(e){
			e.preventDefault();

			$scope.hotel = localStorageService.get('selectedHotel');
			$scope.room = localStorageService.get('selectedRoom');

			$timeout(function(){
				var parsleyValidation = $(e.target).parsley('validate');
				if(parsleyValidation.validationResult) {

					if(!$scope.check_for_duplicate_guests()){
						$rootScope.loading = LOADING_MESSAGES['PERSONAL_DETAILS'];

						data = {}
						data.selectedRoom = {}
						
						data.guests = $scope.guests
						data.selectedRoom = $scope.room

						localStorageService.set('personalData', data)

						HotelService.setPersonalDetails(data).then(function(response){
							$rootScope.loading = false;
							localStorageService.set('hotelPaymentDetails', response.data)
							State.nextState();
						}, function(response){
							//Error
							$rootScope.loading = false;
							$scope.error_message = response.data.error;
						})
					} else {
						$rootScope.loading = false;
						alert('Duplicate guests');
					}
				}
			})
		}
	})


	.controller('payment-details', function($scope, HotelService, $q, $rootScope, State, $timeout, LOADING_MESSAGES, $http, localStorageService){
		if(window.DATATRANS_RESPONSE){

			if(window.DATATRANS_RESPONSE.status == 'success'){
				State.nextState();
				$rootScope.loading = LOADING_MESSAGES['TICKETING'];

				// HotelService.ticket(DATATRANS_RESPONSE).then(function(){
				// 	$rootScope.loading = false;
				// }, function(){
				// 	$rootScope.loading = false;
				// })

				return;

			} else {
				$scope.error_message = window.DATATRANS_RESPONSE.errorDetail;
			}
		}

		HotelService.getPaymentDetails().then(function(paymentDetails){
			console.log(paymentDetails)

			$scope.payment = paymentDetails;

			$scope.payment.method = 'ECA';
			$scope.payment.creditCardNumber = '5200000000000007'
			$scope.payment.expirationMonth = 12
			$scope.payment.expirationYear = 15
			$scope.payment.securityCode = 123
			$scope.payment.amount = paymentDetails.totalBookingAmountFormatted
			$scope.payment.ref = paymentDetails.refno
			$scope.payment.sign = paymentDetails.datasign

			localStorageService.set('hotelPayment', $scope.payment)

		})

		// $scope.$watch('payment', function(data){
		// 	if(!data) return;

		// 	$http.post('/payment/sign', {amount: data.amount, currency: data.currency, ref: data.ref}).then(function(response){
		// 		$scope.payment.sign = response.data;
		// 		console.error('WARNING, GETTING SIGN FROM PHP. INSECURE!!')
		// 		// console.log(response.data)
		// 	});
		// }, true)

		$scope.submitPaymentDetails = function(e){
			$rootScope.loading = LOADING_MESSAGES['BOOKING'];
			$(e.target).submit();

			// if(!$scope.payment.ref) {
			// 	e.preventDefault();
			// 	console.log($scope.payment)

			// 	HotelService.book().then(function(data){

			// 		$scope.payment.amount = data.totalBookingAmount;
			// 		$scope.payment.currency = App.selectedCurrency;
			// 		$scope.payment.ref = data.pnr;
			// 		$scope.payment.sign = data.datasign;

			// 		console.log(data)

			// 		if(data.isPnrCreated) {
			// 			localStorageService.set('pnr', data);

			// 			setTimeout(function(){
			// 				// $rootScope.loading = false;
			// 				$(e.target).submit();
			// 			}, 500);

			// 		} else {
			// 			console.error('Pnr Not created')
			// 			$rootScope.loading = false;
			// 			State.nextState();
			// 		}

			// 	}, function(){
			// 		$rootScope.loading = false;
			// 	})
			// }


			// $timeout(function(){
				// var parsleyValidation = $(e.target).parsley('validate');
				// if(parsleyValidation.validationResult) {

					// $rootScope.loading = LOADING_MESSAGES['BOOKING'];

					// HotelService.book().then(function(){
						// $rootScope.loading = false;

						// State.nextState();
					// })


				// }
			// })
		}
	})

	.controller('coupon', function($scope, $q, $rootScope, State){


	})

	.controller('confirmation', function($scope, HotelService, $q, $rootScope, State, localStorageService){
		$scope.personalData = localStorageService.get('personalData')
		$scope.paymentTransaction = localStorageService.get('hotelPayment')
		
		$scope.bookData = {}
		$scope.bookData.guests = $scope.personalData.guests
		$scope.bookData.selectedRoom = $scope.personalData.selectedRoom
		$scope.bookData.paymentTransaction = $scope.paymentTransaction

		HotelService.booking($scope.bookData).then(function(response){
			$scope.bookResponse = response
			localStorageService.set('bookResponse', $scope.bookResponse)
		})
	})


//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('HotelCheckout.Services', ['LocalStorageModule'])
	.service('State', function(localStorageService){
		var currentStateIndex = parseInt(localStorageService.get('hotelCheckoutCurrentStateIndex'), 10) || 0;
		var states = [
			{name: 'personal-details', string: 'Personal Details'},
			{name: 'payment-details', string: 'Payment Details'},
			{name: 'confirmation', string: 'Confirmation'}
		]

		this.setState = function(index){
			localStorageService.set('hotelCheckoutCurrentStateIndex', index)
			currentStateIndex = index;
			this.onStateChange(index);
		}

		this.nextState = function(){
			this.setState(currentStateIndex+1);
		}

		this.backToPreviousState = function(){
			this.setState(currentStateIndex-1);
		}

		this.getPreviousState = function(){
			return states[currentStateIndex-1];
		}

		this.getCurrentState = function(){
			return states[currentStateIndex];
		}

		this.onStateChange = function(){};

	})

//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('HotelCheckout.Directives', [])
	.directive('internalTabs', function(){
		return {
			restrict: 'C',
			link: function(scope, elm, attrs){
				App.Modules['internal-tabs'].run();
			}

		}
	})

	.directive('checkoutCreditCardInput', function(){
		return {
			restrict: 'C',
			require: 'ngModel',
			link: function(scope, elm, attrs, ctrl){
				var justNumbersReg = /^\d+$/;

				$(elm).validateCreditCard(function(result) {

					var creditCardVal = elm.val(),
						lng = creditCardVal.length;

					// is it not a number?
					if( !justNumbersReg.test(creditCardVal[lng - 1]) ) {
						elm.val( creditCardVal.substr(0, lng - 1) );
						return;
					}

					creditCardVal = creditCardVal.split(' ').join('');

					// apply spaces mask
					if (creditCardVal.length > 0) {
						creditCardVal = creditCardVal.match( new RegExp('.{1,4}', 'g') ).join(' ');
					}

					elm.val( creditCardVal );

					// get the type
					if(result.card_type !== null) {

						elm.attr('data-type-card', result.card_type.name);
						elm.removeClass('parsley-error');
						$('#parsley-id-' + elm.attr('data-parsley-id')).empty();

					} else {
						elm.attr('data-type-card', '');
						elm.addClass('parsley-error');
						$('#parsley-id-' + elm.attr('data-parsley-id')).html('<li>' + elm.attr('data-parsley-error-message') + '</li>');
					}

					scope.$apply();

				});

			}

		}
	})

	.directive('addroom', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				console.log(scope)
				console.log(elm)
			}

		}
	})


//  _____ _ _ _
// |  ___(_) | |_ ___ _ __ ___
// | |_  | | | __/ _ \ '__/ __|
// |  _| | | | ||  __/ |  \__ \
// |_|   |_|_|\__\___|_|  |___/

angular.module('HotelCheckout.Filters', [])
	.filter('formatDate', function(){
		return function(data, format){
			return moment(data).format(format);
		}
	})
	.filter('passengerType', function(){
		return function(data, format){
			var types = {
				ADT: 'Adult'
			}
			return types[data];
		}
	})

