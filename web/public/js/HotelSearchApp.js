//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('HotelSearchApp', ['ngSanitize', 'HotelSearchApp.Controllers', 'HotelSearchApp.Directives', 'HotelService', 'HashtagControllers', 'HashtagServices', 'HashtagFilters', 'LocalStorageModule'])

	.config(function($interpolateProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})

	.run(function($rootScope) {
		$rootScope.showFacilities = false;
		$rootScope.modal = false;

		/////////////// WARNING
		if(typeof(App) != 'undefined'){
			App.Modules['autocomplete_hotel'].run();
		}
		/////////////// WARNING

		var errorHandlerTimeout;
		$rootScope.errorHandler = function(response){

			clearTimeout(errorHandlerTimeout);

			errorHandlerTimeout = setTimeout(function(){
				alert(response.responseText)

				// switch(response.error){
				// 	// case 'invalid.departure.inThePast': {
				// 	// 	alert(ERROR_MESSAGES[response.error]);
				// 	// 	break;
				// 	// }

				// 	default: {
				// 		alert(response.error);
				// 	}
				// }
			}, 300)
		}


	})



//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('HotelSearchApp.Controllers', [])

	.controller('filters', function($rootScope, $scope, $q, HotelService, localStorageService) {

		HotelService.getResults(location.search).then(function(response) {

			localStorageService.set('hotelSearch', location.search);

			var priceMin = 99999999999999;
			var priceMax = 0;

			$rootScope.loading = false;

			//Currency
			$scope.hotels = response.hotels;

			$scope.arrival = response.arrival;
			$scope.currency = App.selectedCurrency;
			$scope.departure = response.departure;
			$scope.locality = response.locality;
			$scope.city = $scope.locality;

			$rootScope.orderBy = {
				value: 'lowestPrice',
				key: 'startPrice',
				reverse: false
			}

			var facilitiesOptions = [];
			$rootScope.facilitiesActive = [];

			_.each(response.hotels, function(hotel){
				if(hotel.error) return;

				priceMin = Math.min(priceMin, hotel.startPrice)
				priceMax = Math.max(priceMax, hotel.startPrice)

				// facilities
				_.each(hotel.facilities, function(facility){
					var count = _.indexOf(facilitiesOptions, facility);
					if(count == -1) {
						facilitiesOptions.push(facility);
					}
				})

			});

			$rootScope.facilitiesOptions = _.map(facilitiesOptions, function(facility){
				return {text: facility, active: false}
			});

			$scope.toggleFacilities = function() {
				$rootScope.showFacilities = !$rootScope.showFacilities;
			}


			// Price
			$scope.priceBounds = {
				from: Math.floor(priceMin),
				to: Math.ceil(priceMax)
			}

			$scope.price = {
				from: Math.floor(priceMin),
				to: Math.ceil(priceMax)
			}


			// stars
			$scope.stars = 1;


			// trigger change filters
			var keysToTriggerChangeFilters = ['price', 'stars', 'facilitiesOptions'];

			_.each(keysToTriggerChangeFilters, function(item){
				$scope.$watch(item, _.throttle(function(newval, current){

					if(facilitiesOptions) {
						$rootScope.facilitiesActive = _.where($rootScope.facilitiesOptions, {active:true});
					}

					$rootScope.$emit('changeFilters', {
						price: $scope.price,
						stars: +$scope.stars,
						facilities: $rootScope.facilitiesActive
					})
				}, 500), true)
			});

			$rootScope.$watch('orderBy.value', function(newval){
				$rootScope.orderBy.reverse = true;
				if(newval == 'ratingTripAdvisor') {
					$rootScope.orderBy.key = 'tripAdvisorRating';
				} else if(newval == 'lowestPrice') {
					$rootScope.orderBy.key = 'startPrice';
					$rootScope.orderBy.reverse = false;
				} else if(newval == 'highestPrice') {
					$rootScope.orderBy.key = 'startPrice';
				}
			});

		});

	})


	.controller('results', function($rootScope, $scope, $sce, $q, $location, HotelService, Router, localStorageService) {

		$rootScope.loading = true;
		$scope.links_disabled = false;

		$scope.SkipValidation = function(value) {
			return $sce.trustAsHtml(value);
		};

		$scope.blockScreen = function(e){
			$scope.links_disabled = true;
		}

		HotelService.getResults(location.search).then(function(response){

			$rootScope.loading = false;
			$rootScope.limit = 10;

			localStorageService.set('guests', response.guests);

			_.each(response.hotels, function(hotel) {
				hotel.stars = parseInt(Math.ceil(hotel.stars));
				if(hotel.tripAdvisorRating === null) hotel.tripAdvisorRating = 1;
			});

			// Results
			$scope.hotels = response.hotels;

			$scope.arrival = response.arrival;
			$scope.currency = App.selectedCurrency;
			$scope.departure = response.departure;
			$scope.locality = response.locality;

			// map
			if(response.hotels.length && !$rootScope.map) {
				App.Modules['hotel-map'].run();
				App.Modules['hotel-map'].render(response.hotels);
			}


			$scope.btnLoadMore = function(e){
				$rootScope.limit += 10;
			}

			$scope.setHotel = function(e, id) {
				App.Modules['hotel-map'].setHotel(id);
			}

			function setHotelDetail(id) {
				var hotel = _.where(response.hotels, { hotelId: +id });
				if(hotel.length) {
					$rootScope.hotelDetail(false, hotel[0]);
				}
			}

			function closeDetail() {
				$rootScope.detail = false;
			}

			$rootScope.$on('changeFilters', function(e, filters){
				HotelService.filter(filters).then(function(filteredHotels){
					$scope.hotels = filteredHotels;
					mapHotels = []

					_.each($scope.hotels, function(hotel){
						// lat && long
						if(hotel.latitude != 0 && hotel.latitude != null && hotel.longitude != 0 && hotel.longitude != null){
							mapHotels.push(hotel)
						}
					})

					if(!$rootScope.map) App.Modules['hotel-map'].filter(mapHotels);
				});
			});

			$rootScope.$on('resetLoading', function(a,c) {
				var hotels = response.hotels;
				_.each(hotels, function(hotel) {
					hotel.loading = false;
				});
			});


			$rootScope.$on('$locationChangeSuccess', function() {
				Router.run($location.path(), {
					'/': closeDetail,
					'/hotel/(:id)': setHotelDetail
				})
			})

			$rootScope.$emit('$locationChangeSuccess');

		});


	})


	.controller('detail', function($rootScope, $scope, $q, HotelDetailService, $location, localStorageService) {


		$scope.openShareModal = function(e){
			e.preventDefault();
			$rootScope.modal = 'hotel';
			$rootScope.shareUrl = $location.absUrl();
		}

		$rootScope.hotelDetail = function(e, hotel) {
			if(e) e.preventDefault();

			var id = hotel.hotelId;
		
			hotel.loading = true;

			$q.all([HotelDetailService.getDetail(id, hotel.searchID, location.search, hotel.cacheId), HotelDetailService.getRoomPictures(id, hotel.searchID, location.search, hotel.cacheId)]).then(function(responses){

				var hotel = responses[0],
					pictures = responses[1];

			 	$rootScope.detail = hotel;
			 	$rootScope.$emit('resetLoading');

				_.each(hotel.rooms, function(room) {
					var picture = _.where(pictures, { roomId: room.id });
					if(picture.length) room.picture = picture[0].url;
				});

			 	$scope.currency = App.selectedCurrency;
				$scope.hotel = hotel;

			});

			$scope.save_infos = function(e, hotel, room){
				e.preventDefault();

				var searchObject = $.parseParams(location.search)

				var sum = 0;

				var totalAdults = (searchObject.numberOfAdults) ? searchObject.numberOfAdults.split(',') : null
				totalAdults = $.each(totalAdults,function(){
					sum += parseFloat(this[0]) || 0;
				});
				totalAdults = sum

				var totalChilds = (searchObject.childAges) ? searchObject.childAges.split(',').length : null

				room.guests = totalAdults + totalChilds

				localStorageService.set('selectedHotel', hotel);
				localStorageService.set('selectedRoom', room);
				localStorageService.set('hotelCheckoutCurrentStateIndex', null)

				setTimeout(function(){
					window.location = 'checkout';
				})
			}

		}

	})

	.controller('print-details', function($scope, HotelService, $q, $rootScope, $location, localStorageService){
		$scope.bookResponse = localStorageService.get('bookResponse').data
		$scope.leader = $scope.guests = localStorageService.get('personalData').guests[0]
		$scope.guests = localStorageService.get('personalData').guests
		$scope.selectedRoom = localStorageService.get('personalData').selectedRoom
		$scope.selectedHotel = localStorageService.get('selectedHotel')
		$scope.hotelPaymentDetails = localStorageService.get('hotelPaymentDetails')

		$scope.getNumber = function(num) {
			var array = new Array(num)
			return array;   
		}
	})



//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('HotelSearchApp.Directives', [])

	.directive('selectMultiple', function() {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function($scope, elm, attrs, ctrl) {

				elm = $(elm);

				elm.multipleSelect({
					placeholder: elm.attr('data-placeholder'),
					onClick: function($el, view){

						elm.find('option[value="'+ view.value +'"]').attr('selected', view.checked)
						elm.trigger('change');

						if(view.checked) {
							$el.addClass('active');
						} else {
							$el.removeClass('active');
						}
					},
					onCheckAll: function($el) {
						$el.addClass('active');
						elm.trigger('change');
					},
					onUncheckAll: function($el) {
						$el.removeClass('active');
						elm.trigger('change');
					}
				});

				$scope.$watch('facilitiesOptions', function(){
					elm.multipleSelect("refresh");
				}, true)

				elm.on('change', function(){
					var values = $(this).val();

					$scope.$apply(function(){
						ctrl.$setViewValue(values);
					});
				});
			}
		}
	})


	// .directive('hotelMap', function() {
	// 	return {
	// 		restrict: 'A',
	// 		link: function($scope, elm, attrs, ctrl) {
	// 			elm = $(elm);

	// 		}
	// 	}
	// })


	//https://github.com/vasyabigi/angular-nouislider
	.directive('slider', function () {
		return {
			restrict: 'A',
			scope: {
				start: '@',
				step: '@',
				end: '@',
				callback: '@',
				margin: '@',
				ngModel: '=',
				ngFrom: '=',
				ngTo: '='
			},
			link: function (scope, element, attrs) {
				var callback, fromParsed, parsedValue, slider, toParsed;
				slider = $(element);
				callback = scope.callback ? scope.callback : 'slide';
				if (scope.ngFrom != null && scope.ngTo != null) {
					fromParsed = null;
					toParsed = null;
					slider.noUiSlider({
						start: [
							scope.ngFrom || scope.start,
							scope.ngTo || scope.end
						],
						step: parseFloat(scope.step || 1),
						connect: true,
						margin: parseFloat(scope.margin || 0),
						range: {
							min: [parseFloat(scope.start)],
							max: [parseFloat(scope.end)]
						}
					});
					slider.on(callback, function () {
						var from, to, _ref;
						_ref = slider.val(), from = _ref[0], to = _ref[1];
						fromParsed = parseFloat(from);
						toParsed = parseFloat(to);
						return scope.$apply(function () {
							scope.ngFrom = fromParsed;
							return scope.ngTo = toParsed;
						});
					});

					scope.$watch('ngFrom', function (newVal, oldVal) {
						if (newVal !== fromParsed) {
							return slider.val([
								newVal,
								null
							]);
						}
					});

					return scope.$watch('ngTo', function (newVal, oldVal) {
						if (newVal !== toParsed) {
							return slider.val([
								null,
								newVal
							]);
						}
					});
				} else {
					parsedValue = null;
					slider.noUiSlider({
						start: [scope.ngModel || scope.start],
						step: parseFloat(scope.step || 1),
						range: {
							min: [parseFloat(scope.start)],
							max: [parseFloat(scope.end)]
						}
					});
					slider.on(callback, function () {
						parsedValue = parseFloat(slider.val());
						return scope.$apply(function () {
							return scope.ngModel = parsedValue;
						});
					});
					return scope.$watch('ngModel', function (newVal, oldVal) {
						if (newVal !== parsedValue) {
							return slider.val(newVal);
						}
					});
				}
			}
		};
	})

	.directive('assistOverflow', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs) {

				$('#map').on('setMarker', function(e, id){
					var box = $('#hotel-results__box-' + id);

					// $('.hotel-results__box--over-from-map').removeClass('hotel-results__box--over-from-map')

					if(box.length){
						$(elm).animate({scrollTop: box.offset().top + $(elm).scrollTop() - $(elm).offset().top - 10 + 'px'})
					} else {

						var maxTries = 20;
						var tries = 0;

						function tryToFindBox() {
							console.log('trying')
							scope.$apply(function(){
								scope.limit += 20;
							})

							box = $('#hotel-results__box-' + id);

							if(!box.length) {
								tries++;
								if(tries < maxTries) setTimeout(tryToFindBox, 200)
							} else {
								box.addClass('hotel-results__box--over-from-map')
								$(elm).animate({scrollTop: box.offset().top + $(elm).scrollTop() - $(elm).offset().top - 10 + 'px'})
							}
						}

						tryToFindBox()

					}

				})

				$('#map').on('overMarker', function(e, id){
					var box = $('#hotel-results__box-' + id);

					if(box.length) box.addClass('hotel-results__box--over-from-map');
				})

				$('#map').on('outMarker', function(e, id){
					var box = $('#hotel-results__box-' + id);

					if(box.length) box.removeClass('hotel-results__box--over-from-map');
				})



				$(elm).on('scroll', function(e){

					$(elm).find('.results-list__facilities').css('top', $(this).scrollTop());

					if($(this).scrollTop() + 100 > Math.abs($(this).height() - $(this).get(0).scrollHeight)){
						scope.$apply(function(){
							scope.limit += 5;
						})
					}

				})
			}
		}
	})

	.directive('tripAdvisor', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs) {

				var id = Math.ceil(Math.random() * 9000);

				setTimeout(function(){
					$(elm).html('<div id="TA_cdsratingsonlynarrow'+id+'" class="TA_cdsratingsonlynarrow"> \
						<ul id="CUUG2Ky" class="TA_links LdIcGZ list-unstyled"> \
							<li id="DzY1Oz1LNmI" class="JK5ROVnpzq"> \
								<a target="_blank" href="http://www.tripadvisor.com/"> \
									<img src="http://www.tripadvisor.com/img/cdsi/img2/branding/tripadvisor_logo_transp_340x80-18034-2.png" width="100px" alt="TripAdvisor"/> \
								</a> \
							</li> \
						</ul> \
					</div> \
					<script src="http://www.jscache.com/wejs?wtype=cdsratingsonlynarrow&amp;uniq='+id+'&amp;locationId=206289&amp;lang=en_US&amp;border=true"></script>');
				}, 1000)

			}
		}
	})

	.directive('onLastRepeat', function() {
		return function(scope, element, attrs) {
			if (scope.$last) setTimeout(function(){
				App.Modules['carrousel'].run();
			}, 1);
		};
	})

	.directive('copyClipboard', function() {
		return function(scope, element, attrs) {
			var client = new ZeroClipboard( element );

			client.on( "ready", function( readyEvent ) {
				client.on( "aftercopy", function( event ) {
					alert("Copied text to clipboard: " + event.data["text/plain"] );
				});
			});
		};
	})

	.directive('noMap', function($rootScope) {
		return function(scope, el, attrs) {
			$rootScope.map = true;
		}
	})


