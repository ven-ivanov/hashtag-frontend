/**
 * Calendar Plugin
 * @version 0.0.1
 *
 * The Calendar Plugin uses the MomentJS to format the dates
 * (http://momentjs.com)
 */
;(function ( $, window, document, undefined ) {

	var name = "calendar",
		defaults = {
			maxMonths: 12
		};

	App.multicityDates = [];

	function Plugin ( element, options, index ) {
		this.element = element;
		this.index = index;
		this.settings = $.extend( {}, defaults, options );
		this._defaults = defaults;
		this._name = name;
		this.init();
	}

	Plugin.prototype = {

		init: function () {

			var _t = this,
				$el = $(_t.element),
				$datepicker = $el.find('.datepicker[data-calendar-type]'),
				setDate = $el.attr('data-calendar-set-date'),

				startDate = $datepicker.eq(0).find('.datepicker-input').val(),
				endDate = $datepicker.length == 2 ? $datepicker.eq(1).find('.datepicker-input').val() : null,

				arrStartDate = App.today.split('-'),

				today = moment(App.today);

			_t.$el = $el;
			_t.$datepicker = $datepicker;

			_t.indexDay = 0;

			_t.today = today;

			_t.curMonth = today.month();
			_t.curYear = today.year();
			_t.curDay = today.date();


			// calendar dates

			_t.startDate = (startDate) ? startDate : null;
			_t.endDate = (endDate) ? endDate : null;


			if(setDate) {
				_t.startDate = startDate;
				_t.endDate = endDate;
				startDate = endDate = null;
			}


			// get position of the first month's date

			_t.firstMonthDay = moment(arrStartDate[0]+'-'+ arrStartDate[1] +'-01').day();

			_t.$parent = $el.closest($el.attr('data-calendar-parent'));


			// is there checkout datepicker?
			_t.checkout = $datepicker.length == 2 ? true : false;

			// is there time calendar?
			_t.calendarTime = $el.find('.datepicker--time').length;

			// Multicity
			_t.multicity = $el.attr('data-calendar-multicity');

			if(_t.multicity) {
				_t.multicityIndex = App.multicityDates.length;
				App.multicityDates.push(startDate);
			}


			_t.datepickerClicked = null;

			$el.attr({ 'data-id': _t.index });

			_t.$calendar = $('<div class="calendar" data-id="'+ _t.index +'" />');
			_t.$calendar.appendTo( _t.$parent );


			_t.render();

			_t.setDates(startDate, endDate);

			_t.attachEvents();


		},


		getCountMonthDays: function(month, year) {
			return new Date(year, month + 1, 0).getDate();
		},


		render : function() {

			var _t = this,

				countMonth = _t.curMonth,
				countYear = _t.curYear,
				maxMonths = _t.settings.maxMonths,

				$calendar = _t.$calendar,
				$datepicker = _t.$datepicker,

				html = '',
				listMonthsHTML = '',
				htmlTime = '<div class="calendar__time" />';

			_t.indexDay = 0;
			_t.countFirstMonthDay = _t.firstMonthDay;

			function getHeaderHTML(time) {
				var title = '<h4>Choose date</h4>',
					closeHTML = '<span class="calendar__close" />';

				if(time) {
					title = '<h4>Choose time</h4>';
					closeHTML = '';
				}

				return '<div class="calendar__header">'+ title + closeHTML+'</div>';
			}


			for(var i = 0; i < maxMonths; i++) {

				listMonthsHTML += _t.renderMonth( countMonth, countYear, _t.getCountMonthDays(countMonth, countYear) );
				countMonth++;

				if(countMonth > 11) {
					countMonth = 0;
					countYear++;
				}
			}


			html += '<div class="calendar__box"> \
					  	' + getHeaderHTML() + ' \
					  	<div class="calendar__list">'+ listMonthsHTML + '</div> \
					</div>';


			if(_t.calendarTime) {

				html += '<div class="calendar__box"> \
							'+ getHeaderHTML(true) +' \
							<div class="calendar__list"> \
								<div class="calendar__col"> \
									<h5>Pick-up</h5> \
									<div class="customSelect"> \
										<select class="customSelect-select" name="calendar-time-checkin">'+ _t.renderTimeList($datepicker.find('.datetime-input').eq(0).val()) +'</select> \
									</div> \
								</div> \
								<div class="calendar__col"> \
									<h5>Drop-off</h5> \
									<div class="customSelect"> \
										<select class="customSelect-select" name="calendar-time-checkout">'+ _t.renderTimeList($datepicker.find('.datetime-input').eq(1).val()) +'</select> \
									</div> \
								</div> \
							</div> \
						  </div>';

				html += '<div class="calendar__btn-container"> \
							<input type="submit" class="calendar__btn" value="Done" /> \
						</div>';

				$calendar.addClass('calendar--time');
			}

			$calendar.html(html);


		},

		renderTimeList: function(selectTime) {

			var html = '',
				separator = ':',
				formattedNumber = null,
				fullNumber = null;

			function formatNumber(num) {
				return num < 10 ? '0' + num : num;
			}

			function getSelected() {
				var selected = '';
				if(fullNumber == selectTime) {
					selected = 'selected="selected"';
				} else {
					selected = '';
				}
				return selected;
			}



			for(var i = 0; i < 24; i++) {
				formattedNumber = formatNumber(i);

				fullNumber = formattedNumber +':00';
                if(i != 9)
				    html += '<option value="'+ fullNumber +'" '+ getSelected(fullNumber) +'>'+ fullNumber +'</option>';
                else
                    html += '<option value="'+ fullNumber +'" '+ 'selected="selected"' +'>'+ fullNumber +'</option>';

				fullNumber = formattedNumber +':30';
				html += '<option value="'+ fullNumber +'" '+ getSelected(fullNumber) +'>'+ fullNumber +'</option>';
			}

			return html;

		},


		/**
		 * renderLink
		 *
		 * @param  {int} 	day
		 * @param  {int} 	month
		 * @param  {int} 	year
		 * @return {html}	Return the html day rendered
		 *
		 */
		renderLink: function(day, month, year) {

			var _t = this,

				date = moment({ month : month, year : year, day : day }),
				dateFormated = date.format('YYYY-MM-DD'),

				diff = date.diff(_t.today, 'days'),
				diffStartDate = null,
				diffEndDate = null,

				curDay = _t.curDay,
				curMonth = _t.curMonth,
				curYear = _t.curYear,

				startDate = _t.startDate,
				endDate = _t.endDate,

				indexDay = _t.indexDay,

				c = 'calendar__day',
				attrDataIndex = 'data-index="'+ indexDay+'"';


			if(_t.multicityIndex - 1 >= 0) {
				diff = date.diff(moment(App.multicityDates[_t.multicityIndex - 1]), 'days');
			}


			// set class
			if(diff < -0) {
				c = ' calendar__day-disabled';
				attrDataIndex = '';
			} else {

			 	if(diff == -0)
			 		c += ' calendar__day--today';

			 	if(startDate) {

			 		diffStartDate = date.diff(moment(startDate), 'days');

			 		if(diffStartDate < -0 && _t.datepickerClicked == 'checkout')
			 			c = 'calendar__day-disabled';

			 		if(diffStartDate == -0)
			 			c += ' calendar__day--start';
			 	}

			 	if(endDate) {

			 		diffEndDate = date.diff(moment(endDate), 'days');

			 		if(diffEndDate == -0)
			 			c += ' calendar__day--end';

			 	}

				indexDay++;
			}

			_t.indexDay = indexDay;

			return '<td class="'+ c +'" '+ attrDataIndex +' data-date="'+ dateFormated +'">'+ day +'</td>';

		},


		/**
		 * renderMonth
		 *
		 * @param  {int} 	month
		 * @param  {int} 	year
		 * @param  {int} 	totalDays - count total days on month
		 * @return {html}	Return the html month rendered
		 *
		 */
		renderMonth: function(month, year, totalDays) {

			var _t = this,
				countDays = 1,
				firstMonthDay = _t.countFirstMonthDay,
				nextFirstMonthDay = null,
				html = '<table class="calendar__table">',
				i = 0;


			html += '<thead> \
							<tr> \
								<th class="calendar__title" colspan="7">'+ moment({ month : month }).format('MMMM') +' <span>'+year+'</span></th> \
							</tr> \
							<tr> \
								<th class="calendar__title-day">'+ moment.weekdays('', 0).substr(0,3) +'</th> \
								<th class="calendar__title-day">'+ moment.weekdays('', 1).substr(0,3) +'</th> \
								<th class="calendar__title-day">'+ moment.weekdays('', 2).substr(0,3) +'</th> \
								<th class="calendar__title-day">'+ moment.weekdays('', 3).substr(0,3) +'</th> \
								<th class="calendar__title-day">'+ moment.weekdays('', 4).substr(0,3) +'</th> \
								<th class="calendar__title-day">'+ moment.weekdays('', 5).substr(0,3) +'</th> \
								<th class="calendar__title-day">'+ moment.weekdays('', 6).substr(0,3) +'</th> \
							</tr> \
						</thead>';


			html += '<tbody>';



			// first line
			if(firstMonthDay < 7) {
				html += '<tr>';

				for (i = 0; i < 7; i++){
					if(i < firstMonthDay) {
						html += '<td class="calendar__day-empty">&nbsp;</td>';
					} else {
						html += _t.renderLink(countDays, month, year);
						countDays++;
					}
				}

				html += "</tr>";
			}



			// lines
			while (countDays <= totalDays) {
				html += "<tr>";

				for (i = 0; i <= 6; i++) {
					if(countDays <= totalDays) {
						html += _t.renderLink(countDays, month, year);
						countDays++;
						nextFirstMonthDay = i;
					} else {
						html += '<td class="calendar__day-empty">&nbsp;</td>';
					}
				}

				html += "</tr>";
				_t.countFirstMonthDay = nextFirstMonthDay + 1;
			}


			html += "</tr></tbody></table>";

			return html;
		},


		attachEvents: function() {

			var _t = this,
				$el = $(_t.element),
				$calendar = _t.$calendar,
				$datepicker = _t.$datepicker,
				checkout = _t.checkout;


			// show|hide calendar
			$datepicker.on('click', function(e){
				e.preventDefault();

				var $btn = $(this),
					changeClass = true,
					$datepickerMulticityActive = $('.datepicker-container[data-calendar-multicity="true"] .datepicker--calendar-active').not( $btn ),
					$datepickerMulticityContainerActive = null,
					noHighlight = false;


				if(!$datepickerMulticityActive.length && $calendar.hasClass('calendar--active') && !$btn.hasClass('datepicker--calendar-active')) {

					changeClass = false;

				} else if($datepickerMulticityActive.length && !$btn.hasClass('datepicker--calendar-active')) {

					$datepickerMulticityContainerActive = $datepickerMulticityActive.closest('.datepicker-container');

					$('.calendar[data-id='+ $datepickerMulticityContainerActive.attr('data-id') +']').removeClass('calendar--active');
					$datepickerMulticityActive.removeClass('datepicker--calendar-active');
					_t.mainToggle($datepickerMulticityContainerActive.closest($datepickerMulticityContainerActive$datepickerMulticityContainerActive.attr('data-calendar-parent')));

					$btn.addClass('datepicker--calendar-active');
					changeClass = true;

				}

				_t.datepickerClicked = $btn.attr('data-calendar-type');

				if(_t.checkout || _t.multicity) _t.render();

				_t.calendarToggle(changeClass);
			});


			// click day calendar
			$calendar.on('click mouseenter mouseleave', '.calendar__day', function(e) {

				var $btn = $(this),
					date = $btn.data('date'),
					index = $btn.data('index');


				if(e.type == 'click') {

					// if already there is checkin and checkout
					// and try select the dates

					if(_t.startDate && _t.endDate) {

						if(_t.datepickerClicked == 'checkin') {

							if(index > $calendar.find('.calendar__day--end').data('index')) {
								$calendar.find('.calendar__day').removeClass('calendar__day--start calendar__day--end');
								_t.startDate = null;
								_t.endDate = null;
							} else {
								$calendar.find('.calendar__day').removeClass('calendar__day--start');
								_t.startDate = null;
							}

						} else if(_t.datepickerClicked == 'checkout') {
							$calendar.find('.calendar__day').removeClass('calendar__day--end');
							_t.endDate = null;
						}


					}


					// active day
					if(checkout) {

						if(_t.datepickerClicked == 'checkin') {

							if(index > $calendar.find('.calendar__day--end').data('index')) {
								return;
							}

							_t.startDate = date;
							$calendar.find('.calendar__day--start').removeClass('calendar__day--start');
							$btn.addClass('calendar__day--start');

							if(!_t.endDate) {
								_t.datepickerClicked = 'checkout';
								_t.datepickerHighlight();
							}

						} else if(_t.datepickerClicked == 'checkout'){

							if(index < $calendar.find('.calendar__day--start').data('index')) {
								return;
							}

							_t.endDate = date;
							$calendar.find('.calendar__day--end').removeClass('calendar__day--end');
							$btn.addClass('calendar__day--end');

							if(!_t.startDate) {
								_t.datepickerClicked = 'checkin';
								_t.datepickerHighlight();
							}

						}

					} else {
						_t.startDate = date;
						$calendar.find('.calendar__day').removeClass('calendar__day--start');
						$btn.addClass('calendar__day--start');
					}


					// hide Calendar if there isn't checkout calendar,
					// or if already checkin date and checkout date and there is Calendar
					if((checkout && _t.startDate && _t.endDate && !_t.calendarTime) || !checkout) {
						_t.calendarToggle(true);
					}

					_t.setDates();

					if(!(_t.startDate && _t.endDate && _t.calendarTime)) {
						_t.datepickerHighlight();
					} else {
						_t.datepickerHighlight(true);
					}


				} else if(e.type == 'mouseenter' && checkout){

					_t.dayHighlight( $btn.data('index') );

				} else if(e.type == 'mouseleave' && checkout){

					_t.dayHighlight(0, true);

				}
			});


			$calendar.on('click', '.calendar__close, .calendar__btn', function(e) {
				e.preventDefault();

				if($(this).hasClass('calendar__btn'))
					_t.setDates();

				_t.calendarToggle(true);
			});


			$calendar.on('change', '.customSelect-select', function(e){
				_t.setDates();
			});

		},

		mainToggle: function($box) {

			var _t = this,
				$box = $box ? $box : _t.$parent;

			if($box.hasClass('main-sidebar')) {
				$box.toggleClass('main-sidebar--calendar-active');
			} else if($box.hasClass('main-results')) {
				$box.toggleClass('main-results--calendar-active');
			}

		},

		calendarToggle: function(changeClass) {

			var _t = this,
				$el = $(_t.element),
				$calendar = _t.$calendar,
				checkout = _t.checkout;


			if(changeClass) {
				$calendar.toggleClass('calendar--active');
				_t.mainToggle();
			}


			if(checkout) {

				if(_t.startDate && _t.endDate) {
					$calendar.find('.calendar__day--end').trigger('mouseenter');
				}

				if(_t.startDate && $calendar.hasClass('calendar--active')) {
					$('.calendar__list').scrollTop( $calendar.find('.calendar__day--start').parents('.calendar__table').offset().top - 120 );
				}

			}

			_t.dayHighlight(0, true);

			_t.datepickerHighlight();

		},



		/**
		 * dayHighlight
		 * @param  {int} 	index
		 * @param  {normal} normal
		 */
		dayHighlight: function(index, normal) {

			var _t = this,
				$calendar = _t.$calendar,
				datepickerClicked = _t.datepickerClicked;


			if(!_t.checkout) return;

			$calendar.find('.calendar__day').removeClass('calendar__day--highlighted');


			if(!normal) {

				start = (datepickerClicked == 'checkin') ? index : $calendar.find('.calendar__day--start').data('index');
				end = (datepickerClicked == 'checkout') ? index : $calendar.find('.calendar__day--end').data('index');

				if(_t.startDate && _t.endDate) {
					if(datepickerClicked == 'checkin') {
						$calendar.find('.calendar__day--start').addClass('calendar__day--start-disabled');
					}

					if(datepickerClicked == 'checkout') {
						$calendar.find('.calendar__day--end').addClass('calendar__day--end-disabled');
					}
				}

			} else {

				$calendar.find('.calendar__day--start').removeClass('calendar__day--start-disabled');
				$calendar.find('.calendar__day--end').removeClass('calendar__day--end-disabled');

				start = $calendar.find('.calendar__day--start').data('index');
				end = $calendar.find('.calendar__day--end').data('index');

				if(!start || !end) return;

			}

			for(var i = start + 1; i < end; i++) {
				$calendar.find('.calendar__day[data-index="'+ i +'"]').addClass('calendar__day--highlighted');
			}

		},

		setDates: function(startDate, endDate) {
			var _t = this,
				$el = $(_t.element),
				$datepickerStart = $el.find('.datepicker[data-calendar-type="checkin"]'),
				$datepickerEnd = $el.find('.datepicker[data-calendar-type="checkout"]'),
				$calendar = _t.$calendar,

				startDate = startDate ? startDate : _t.startDate,
				endDate = endDate ? endDate : _t.endDate,

				timeStart = timeStart ? timeStart : $calendar.find('select[name=calendar-time-checkin]').val(),
				timeEnd = timeEnd ? timeEnd : $calendar.find('select[name=calendar-time-checkout]').val();


			if(startDate) {
				$datepickerStart.find('input').val(startDate).trigger('change');

				$datepickerStart.find('.datepicker-day').text( moment(startDate).format('DD') );
				$datepickerStart.find('.datepicker-month').text( moment(startDate).format('MMM') );
				$datepickerStart.find('.datepicker-year').text( moment(startDate).format('YYYY') );

				if(_t.multicity) {
					App.multicityDates[_t.multicityIndex] = startDate;
				}
			}

			if(!endDate){
				endDate = new Date(startDate)
				endDate = endDate.setDate(endDate.getDate() + 1);
			}

			if(endDate) {
				$datepickerEnd.find('input').val(endDate).trigger('change');

				$datepickerEnd.find('.datepicker-day').text( moment(endDate).format('DD') );
				$datepickerEnd.find('.datepicker-month').text( moment(endDate).format('MMM') );
				$datepickerEnd.find('.datepicker-year').text( moment(endDate).format('YYYY') );
			}

			if(_t.calendarTime) {

				$datepickerStart.find('.datepicker-hour').text(timeStart);
				$datepickerStart.find('.datetime-input').val(timeStart).trigger('change');

				$datepickerEnd.find('.datepicker-hour').text(timeEnd)
				$datepickerEnd.find('.datetime-input').val(timeEnd).trigger('change');

			}


		},


		datepickerHighlight: function(clear) {

			var _t = this,
				className = 'datepicker--calendar-active',

				startDate = _t.startDate,
				endDate = _t.endDate,
				datepickerClicked = _t.datepickerClicked,

				$calendar = _t.$calendar,
				$datepicker = _t.$datepicker,
				$datepickerStart = $datepicker.filter('[data-calendar-type="checkin"]'),
				$datepickerEnd = $datepicker.filter('[data-calendar-type="checkout"]');

			$calendar.attr('data-datepicker-clicked', datepickerClicked);

			function reset() {
				$datepicker.removeClass(className);
			}


			if( !$calendar.hasClass('calendar--active') || clear ){
				reset();
				return;
			}

			if(_t.datepickerClicked == 'checkin') {
				reset();
				$datepickerStart.addClass(className);
			} else if(_t.datepickerClicked == 'checkout') {
				reset();
				$datepickerEnd.addClass(className);
			}

		},


		close: function() {
			var _t = this;
			if(_t.$calendar.hasClass('calendar--active')) {
				_t.calendarToggle(true);
			}
		}

	};



	$.fn[ name ] = function ( options ) {
		this.each(function(i) {
			if ( !$.data( this, 'plugin_' + name ) ) {
				$.data( this, 'plugin_' + name, new Plugin(this, options, i) );
			} else {
				var calendar = $.data( this, 'plugin_' + name);
				calendar.close();
			}
		});
		return this;
	};

})( jQuery, window, document );